/**
 * Helper functions for matlab
 * Wed Aug 20 22:28:02 EDT 2014
 *
 * All functions return 0 on success, < 0 on failure
 */

#ifndef _MATLAB_UTIL_H
#define _MATLAB_UTIL_H

/* C includes */

/* Other packages includes */
#include <engine.h>

/* Local includes */
#include "env_setup.h"

/* Convert marray to mxArray mxREAL */
int ml_marray2mx(marray *in, mxArray **out);


#endif
