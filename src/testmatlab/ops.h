#ifndef _OPS_H
#define _OPS_H

/* Local includes */
#include "env_setup.h"

void readin_images(handler_owner *client_data, struct JQueues *jqs);
void workflow_complete_processing(handler_owner *client_data, struct JQueues *jqs);
void init_jqueues(struct JQueues **jqs);
#endif
