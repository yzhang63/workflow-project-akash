/* C includes */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

/* Other includes */

/* Local includes */
#include "env_setup.h"
#include "ops.h"
#include "workflow.h"

int main(){
  struct JQueues *jqs = NULL;
  handler_owner *client_data = NULL;

  init_matlabenv(&client_data);
  init_jqueues(&jqs);

  /* readin the images all at once */
  readin_images(client_data, jqs);

  /* process one image at a time on all three steps */
  workflow_complete_processing(client_data, jqs);


  return 0;
    
}
