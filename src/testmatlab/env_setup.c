/* c includes */
#include <stdio.h>
#include <stdlib.h>

/* local includes */
#include "env_setup.h"
#include "workflow.h"


int
init_matlabenv(handler_owner **gdata){
  if(!gdata)
    return -1;

  *gdata = calloc(1, sizeof(handler_owner));
  if(!(*gdata))
    return -1;
  (*gdata)->rank = 0;
  (*gdata)->size = 1;

  /* fork the matlab engine process */
  if( !((*gdata)->ep = engOpen(""))){
    printf("\nCannot start Matlab engine @Rank#%d\n", (*gdata)->rank);
    exit(0);
  }
  /* add matlab path */
  add_matlabfunc_path((*gdata)->ep);

  /* manipulate matlab pars */
  (*gdata)->epars = (exppars *)calloc(1, sizeof(exppars));
  if (!(*gdata)->epars)
    return -1;
  init_exppars(*gdata);

  /* init matlab pars space */
  init_matlab((*gdata)->ep, *gdata);

  return 0;
}
