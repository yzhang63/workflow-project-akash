/* C includes */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <pthread.h>

/* Other includes */

/* Local includes */
#include "workflow.h"
#include "ops.h"

static void add_job_container(struct Job *job, int streamID, int qid, struct JQueues *jqs);
static void init_stream_container(struct Stream **stream, int streamID);
static void add_stream_container(struct Stream *stream, int qid, struct JQueues *jqs);
static void _add_stream_container(struct Stream *stream, struct list_head *qhead);
static void init_job_container(struct Job **job, int gid);
static void _add_job_container(struct Job *job, struct list_head *qhead);
static struct JItem* assemble_an_jobitem(void *ds, void *cm, int gid);
static void add_an_item(struct JItem *item, int qid, struct JQueues *jqs);
static void _add_an_item(struct JItem *item, struct list_head *qhead, int qid);
static void free_ds_matdata(datastream *ds);
static void fake_free_processed_jobs(struct JobsToProcess *j2p, int qid);
static int is_newstream(int streamID, int qid, struct JQueues *jqs);
static int _is_newstream(int streamID, struct list_head *qhead);
static int apply_complete_analysis(int qid, struct JobsToProcess *j2p, handler_owner *gdata);
static int obtain_processing_decision(int old, int *qid, int *num_tasks, int option, int faulty);
static struct JobsToProcess* active_dequeue(int count, int qid, struct JQueues *jqs);
static int _delete_stream_jobs(struct JobsToProcess *j2p, struct list_head *qhead, int count);
static int is_newjob(int streamID, int gid, int qid, struct JQueues *jqs);
static int get_my_laststageID();
static int _is_newjob(int gid, struct list_head *qhead);
static void _mark_a_readyjob(struct JItem *first, int qid);
static struct list_head* locate_streamqhead(int streamID, struct list_head* qhead);

static int TWORKLOAD = 1600;
static int QID = 0;
void init_jqueues(struct JQueues **jqs)
{
#if 0
  if (!jqs || !*jqs)
    return -1;
#endif
  assert(jqs);
  *jqs = (struct JQueues*)calloc(1, sizeof(struct JQueues));
  assert((*jqs));

  int i, qcount = 3;
  (*jqs)->qlocks = (pthread_mutex_t *)calloc(qcount, sizeof(pthread_mutex_t));
  (*jqs)->qheads = (struct list_head *)calloc(qcount, sizeof(struct list_head));
  assert((*jqs)->qlocks && (*jqs)->qheads);

  struct Job *first;
  for(i=0; i<qcount; i++){
    printf("Initing Queue#%d Info\n", i);
    pthread_mutex_init(&((*jqs)->qlocks[i]), NULL);
    INIT_LIST_HEAD(&((*jqs)->qheads[i]));

#ifdef ONESTREMDESIGN
    // to init the very first and special element (counter)
    first = (struct Job *)calloc(1, sizeof(struct Job));
    assert(first);
    first->jobs = 0;
    first->jhead = NULL;
    list_add_tail(&(first->link), &((*jqs)->qheads[i])); // head->first->...
#endif
  }

}
void readin_images(handler_owner *client_data, struct JQueues *jqs){
  int qid = 0, groupID;
  datastream *ds = NULL;
  int i;

  // assemble_datastream
  datastream *streamobj = NULL;
  streamobj = (datastream *)calloc((IMAGE_COUNT/client_data->size+1), sizeof(datastream));
  assert(streamobj != NULL);
  for(i=0; i <((GNUM)/(client_data->size)); i++) {
    groupID = client_data->rank*((GNUM)/(client_data->size)) + (i+1);

    //printf("Rank#%d start to init_stream %d with GID#%d\n", client_data->rank, i, groupID);
    fake_init_stream(&streamobj[i], client_data);

    printf("Rank#%d assembling %dth stream object/image\n", client_data->rank, groupID);
    assemble_streamObjs(client_data->ep, &streamobj[i], groupID);
  }

  for(i=0; i < ((GNUM)/(client_data->size)); i++) {
    fprintf(stderr, "client_data->rank%d sent out %dth datastreams srcID#%d, GID#%d\n", 
        client_data->rank, i, streamobj[i].srcID, streamobj[i].groupID);
    ds = &streamobj[i];

    struct Stream *stream = NULL;
    if(is_newstream(ds->streamID, qid, jqs)){
      // to add a new list for the new stream to hold window-size buffer
      //add_faulty_sbuffer(ds->streamID);

      init_stream_container(&stream, ds->streamID);
      add_stream_container(stream, qid, jqs);
    }

    struct Job *job = NULL;
    if(is_newjob(ds->streamID, ds->groupID, qid, jqs)){
      init_job_container(&job, ds->groupID);
      add_job_container(job, ds->streamID, qid, jqs);
    }

    struct JItem *item = assemble_an_jobitem((void *)ds, NULL, ds->groupID);
    assert(item);
    add_an_item(item, qid, jqs);
  }

}

void workflow_complete_processing(handler_owner *client_data, struct JQueues *jqs){
  int qid = -1, num_tasks, ret;
  struct JobsToProcess *j2p = NULL;

  static int faulty_count = 0;

  static int finished_tasks = 0; 
  int option = INVILID_INT;
  while(1){
    ret = obtain_processing_decision(qid, &qid, &num_tasks, finished_tasks, faulty_count);
    //printf("Current Decision: QID with NUM_tasks (%d,%d)\n", qid,num_tasks);
    if (ret == CHANGED){
      finished_tasks = 0;
      option = INVILID_INT;
    }

    j2p = active_dequeue(num_tasks, qid, jqs); 
    if(!j2p){
      sleep(2);
      continue;
    }
    finished_tasks += j2p->count;
    //printf("Current Finished tasks = %d\n", finished_tasks);
    if (qid == 0){
      if(finished_tasks == TWORKLOAD) option = LAST;
    }
    else{
      if(finished_tasks == (TWORKLOAD - faulty_count)) option = LAST;
    }

    printf("\nFinished #%d tasks and to check out next task\n", finished_tasks);
    apply_complete_analysis(qid, j2p, client_data);

    if (qid == 0){
      // faulty image filter
      //ret = faulty_detection(&faulty_sdata, j2p, qid, option);  

      //if (ret == BAD) faulty_count += 1;
      //printf("(ret = %d) At-The-Moment faulty count = %d\n", ret, faulty_count);
      ret = GOOD;
    }
    if ( ((qid == 0)&&(ret == GOOD)) || (qid != 0) ) {
      //fake_disseminate_data(&(sdata[qid]), qid, j2p, client_data, option);
      printf("Disseminating a newly processed image...\n");
    }

  } 
}

// create faulty databuffer head
//void add_faulty_sbuffer(int streamID) 
//{
//  struct SBuffer *sbuffer = (struct SBuffer*)calloc(1, sizeof(struct SBuffer));
//  assert(sbuffer);
//
//  sbuffer->streamID = streamID;
//  sbuffer->dhead = (struct list_head*)calloc(1, sizeof(struct list_head));
//  assert(sbuffer->dhead);
//  INIT_LIST_HEAD(sbuffer->dhead);
//
//  list_add_tail(&(sbuffer->link), faultyhead);
//
//  return;
//}


static void fake_free_processed_jobs(struct JobsToProcess *j2p, int qid)
{
  assert(j2p);

  int i;
  struct list_head *jhead = NULL;
  struct JItem *item = NULL, *tmp = NULL;
  for(i=0; i<j2p->count; i++){
    jhead  = j2p->jobs[i];
    list_for_each_entry_safe(item, tmp, jhead, link){
      if(item->ds == NULL){
        //printf("(@Free_JOB) JOB#%d has %d items\n", item->gid, item->items);
        assert(item->is_ready);
        list_del(&(item->link));
        free(item);
        item = NULL;
        continue;
      }

      list_del(&(item->link)); // normal item - assemble_an_jobitem
      if(qid == (get_my_laststageID() - 1)){ // last stage
        //EVreturn_event_buffer((CManager)(item->cm), item->ds);
        free_ds_matdata((datastream *)(item->ds));
      } 
      free(item);
      item = NULL;
    }
    free(j2p->jobs[i]); //free: init_job_container-"(*job)->jhead
  }
  free(j2p);
  j2p = NULL;

  return;
} 

static void free_ds_matdata(datastream *ds){
  int i;
  for(i=0; i<MAX_MFIELD; i++){
    free(ds->mdatas[i].data);
    free(ds->srcname);
    free(ds->workflow_nextstep);
    //free(ds->workflows)
    //free(ds->phases)
  }
}


static void init_stream_container(struct Stream **stream, int streamID)
{
  assert(stream);
  *stream = (struct Stream*)calloc(1, sizeof(struct Stream));
  assert(*stream);

  (*stream)->streamID = streamID;
  (*stream)->qhead = (struct list_head *)calloc(1, sizeof(struct list_head));
  assert((*stream)->qhead);
  INIT_LIST_HEAD((*stream)->qhead);

  struct Job *first = (struct Job*)calloc(1, sizeof(struct Job));
  assert(first);
  first = (struct Job *)calloc(1, sizeof(struct Job));
  assert(first);
  first->jobs = 0;
  first->jhead = NULL;

  list_add_tail(&(first->link), (*stream)->qhead);

  return;
}

static void add_stream_container(struct Stream *stream, int qid, struct JQueues *jqs)
{
  assert(stream && jqs);

  struct list_head *qhead = &(jqs->qheads[qid]);
  pthread_mutex_lock(&(jqs->qlocks[qid]));

  _add_stream_container(stream, qhead);

  pthread_mutex_unlock(&(jqs->qlocks[qid]));

  return;
}

static void _add_stream_container(struct Stream *stream, struct list_head *qhead)
{
  list_add_tail(&(stream->link), qhead);

  return;
}

// to be called every time when a new job arrives at the system
// to init the job "head" & "metadata element (first)"
static void init_job_container(struct Job **job, int gid)
{
  assert(job);
  *job =  (struct Job*)calloc(1, sizeof(struct Job));
  assert((*job));

  (*job)->jobs = INVILID_INT;
  (*job)->jhead = (struct list_head*)calloc(1, sizeof(struct list_head));
  assert((*job)->jhead);
  INIT_LIST_HEAD((*job)->jhead);

  struct JItem *first =(struct JItem*)calloc(1, sizeof(struct JItem));
  assert(first);

  first->gid = gid;
  first->items = 0;
  first->is_ready = 0;
  first->cm = NULL;
  first->ds = NULL;
  list_add_tail(&(first->link), (*job)->jhead); // head->first->...

  return;
}

// to add the job (container) to JQueues->qheads[this]
// unique job's gid is assumed
static void add_job_container(struct Job *job, int streamID, int qid, struct JQueues *jqs)
{
  assert(job && jqs);

  struct list_head *sqhead = NULL;
  struct list_head *qhead = &(jqs->qheads[qid]);
  pthread_mutex_lock(&(jqs->qlocks[qid]));

  sqhead = locate_streamqhead(streamID, qhead);
  _add_job_container(job, sqhead);

  pthread_mutex_unlock(&(jqs->qlocks[qid]));

  return;
}

static void _add_job_container(struct Job *job, struct list_head *qhead)
{
  list_add_tail(&(job->link), qhead);

  struct Job *first = list_first_entry(qhead, struct Job, link);
  assert(first->jhead == NULL);
  first->jobs++;
  //printf("Queue holds #%d ready jobs now.\n", first->jobs);
  // may add a new field stating "all the existing jobs including not_ready"

  return;
}

// when a job is complete w.r.t all elements, it will update the "jobs" field at JQueues->jobs
static void add_an_item(struct JItem *item, int qid, struct JQueues *jqs)
{
  assert(item && jqs);

  int streamID = ((datastream *)(item->ds))->streamID;
  struct list_head *sqhead = NULL, *qhead = NULL;
  qhead = &(jqs->qheads[qid]);

  pthread_mutex_lock(&(jqs->qlocks[qid]));

  sqhead = locate_streamqhead(streamID, qhead);
  _add_an_item(item, sqhead, qid);

  //print_jobqueue(qhead, qid);

  pthread_mutex_unlock(&(jqs->qlocks[qid]));

  return;
}

static void _add_an_item(struct JItem *item, struct list_head *qhead, int qid)
{
  struct list_head *jhead = NULL;
  struct Job *job = NULL;
  struct JItem *first = NULL; 

  list_for_each_entry(job, qhead, link){
    if(job->jhead == NULL) continue; // skip the "first" element

    jhead = job->jhead;
    first = list_first_entry(jhead, struct JItem, link);
    assert(first->ds == NULL);
    if(first->gid == item->gid){
      list_add_tail(&(item->link), jhead);

      first->items++;
      _mark_a_readyjob(first, qid);
      break;
    }
  }

  return;
}

static struct JItem* assemble_an_jobitem(void *ds, void *cm, int gid)
{
  assert(ds );

  struct JItem *item = (struct JItem*)calloc(1, sizeof(struct JItem));
  assert(item);
  item->gid = gid;
  item->items = INVILID_INT;
  item->is_ready = INVILID_INT;
  item->cm = cm;
  item->ds = ds;

  return item;
}

static int is_newstream(int streamID, int qid, struct JQueues *jqs)
{
  assert(jqs);

  struct list_head *qhead = NULL;
  int ret;

  qhead = &(jqs->qheads[qid]);
  pthread_mutex_lock(&(jqs->qlocks[qid]));

  ret = _is_newstream(streamID, qhead);

  pthread_mutex_unlock(&(jqs->qlocks[qid]));

  return ret;
}

static int _is_newstream(int streamID, struct list_head *qhead)
{
  int ret = TRUE;
  struct Stream *stream = NULL;

  list_for_each_entry(stream, qhead, link){
    if(stream->streamID == streamID){
      //printf("@@@Stream#%d not a new stream\n", streamID);
      ret = FALSE;
      break;
    }
  }

  return ret;
}

static int apply_complete_analysis(int qid, struct JobsToProcess *j2p, handler_owner *gdata)
{
  if( (qid < 0) || (qid >= MSTONE_NUM) || (!j2p) || (!gdata) ){
    return -1;
  }
  if( j2p->count <= 0 ){
    return -1;
  }

  if(qid == 0){
    complete_workflow(j2p, gdata);
  }

  return 0;
}

static int obtain_processing_decision(int old, int *qid, int *num_tasks, int option, int faulty){
  assert(qid && num_tasks);

  /* to mimic a strategy to execute three queue One BY One. */
  // special case when reaches the last queue, since there may be more than TWORKLOAD tasks
  if (QID == 0){
    if(option == TWORKLOAD){
      *qid = ++QID;
      *num_tasks = 1;
    }
    else{
      *qid = QID;
      *num_tasks = 1;
    }
  }
  else if(QID == (MSTONE_NUM -1)){ 
    *qid = QID;
    *num_tasks = 1;
  }
  else{
    printf("Faulty No. = %d (option = %d, QID = %d)\n", faulty, option, QID);
    if(option == (TWORKLOAD - faulty)){
      *qid = ++QID;
      *num_tasks = 1;
    }
    else{
      *qid = QID;
      *num_tasks = 1;
    }
  }

  if(*qid != old) 
    return CHANGED;
  else
    return UNCHANGED; 
}

static struct JobsToProcess* active_dequeue(int count, int qid, struct JQueues *jqs)
{
  if(!jqs) return NULL;  // not ready yet

  struct JobsToProcess *j2p = NULL;
  j2p = (struct JobsToProcess *)calloc(1, sizeof(struct JobsToProcess));
  assert(j2p);
  j2p->jobs = (struct list_head**)calloc(count, sizeof(struct list_head*));
  assert(j2p->jobs);
  j2p->count = 0;

  // go through streamQ 1-by-1 and stop at "j2p->count <= count"
  struct list_head* qhead = &(jqs->qheads[qid]); 
  struct list_head* sqhead = NULL;
  struct Stream* stream = NULL;
  int jobs;

  //printf("@@@Dequeueing @Queue #%d\n", qid);
  pthread_mutex_lock(&(jqs->qlocks[qid]));

  list_for_each_entry(stream, qhead, link){
    sqhead = stream->qhead; // the qhead that points to "first" job

    jobs = _delete_stream_jobs(j2p, sqhead, count);
    //printf("Stream #%d dequeued %d jobs\n", stream->streamID, jobs);
    if(jobs == count) break;
  }

  pthread_mutex_unlock(&(jqs->qlocks[qid]));

  if(j2p->count == 0){ // no dequeued jobs yet
    //printf("j2p->count = %d\n", j2p->count);
    free(j2p->jobs);
    free(j2p);
    return NULL;
  }

  return j2p;
}

static int _delete_stream_jobs(struct JobsToProcess *j2p, struct list_head *qhead, int count)
{
  assert(j2p && qhead);

  struct Job *pos, *next, *qfirst;
  list_for_each_entry_safe(pos, next, qhead, link){
    if(pos->jhead == NULL) continue; // "first" metadata
    else{
      j2p->jobs[j2p->count] = pos->jhead; // record: init_job_container-"(*job)->jhead"
      j2p->count++;
      list_del(&(pos->link));
      free(pos); // free: init_job_container-"*job"

      qfirst = list_first_entry(qhead, struct Job, link);
      qfirst->jobs--;
      //printf("-->Remaining %d jobs\n", qfirst->jobs);

      if(j2p->count == count) break;
    }
  }


  return j2p->count;
}

static int is_newjob(int streamID, int gid, int qid, struct JQueues *jqs)
{
  int ret;

  struct list_head *qhead = NULL;
  qhead = &(jqs->qheads[qid]);
  struct list_head *sqhead = NULL; // the stream struct->qhead

  pthread_mutex_lock(&(jqs->qlocks[qid]));

  sqhead = locate_streamqhead(streamID, qhead);
  ret = _is_newjob(gid, sqhead);

  pthread_mutex_unlock(&(jqs->qlocks[qid]));

  return ret; 
}

static int get_my_laststageID()
{
  return 3;
}

static int _is_newjob(int gid, struct list_head *qhead)
{
  int ret = TRUE;
  struct list_head *jhead = NULL;
  struct Job *job = NULL;
  struct JItem *first = NULL; 

  list_for_each_entry(job, qhead, link){
    if(job->jhead == NULL) continue; // skip the "first" element

    jhead = job->jhead;
    first = list_first_entry(jhead, struct JItem, link);
    assert(first->ds == NULL);
    if(first->gid == gid){
      //printf("@@@Job#%d NOT a new job with exisiting #%d members\n\n", first->gid, first->items);
      ret = FALSE;
      break;
    }
  }

  return ret;
}

static void _mark_a_readyjob(struct JItem *first, int qid)
{
  assert(first);

  if(first->items == PERGSIZE){
    //printf("JOB#%d (w/ items #%d) turns ready @Q#%d\n", first->gid, first->items, qid);
    first->is_ready = 1;
  }

  return;
}

static struct list_head* locate_streamqhead(int streamID, struct list_head* qhead)
{
  assert(qhead);
  struct list_head* sqhead = NULL;

  struct Stream* stream = NULL;
  list_for_each_entry(stream, qhead, link){
    if(stream->streamID == streamID){
      //printf("JOB located its StreamID #%d\n", streamID);
      sqhead = stream->qhead;
      break;
    }
  }

  return sqhead;
}
