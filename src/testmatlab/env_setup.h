#ifndef _ENV_SETUP_H
#define _ENV_SETUP_H

/* C includes */

/* Other includes */
#include <engine.h>

/* Local include */
#include "comm_vars.h"
#include "list.h"

typedef struct _exppars{
  double S;
  double P;        
  double T;         
  double U;         
//double fuel;      
  double phi;       
  double SL0;       
  double expangle;  
  double ednum;     
  double exptime;   
//double vc7Folder; 
//double mvc7Folder;
//double   yl;        
  double bs;        
  double ol;        
  double N;         
  double corrected; 
//double filtSize;  
//double   method;
  double frame;
  double R;
//double   xl;
  double c;
//double   expdate;

}exppars, exppars_ptr;

typedef struct _handler_owner{
  // matlab pars 
  exppars *epars;
  Engine *ep;

  int rank;
  int size;

}handler_owner;

typedef struct _workflow_t{
  int workflow_length;
  int *workflow_content;
}workflow_t, *workflow_t_ptr;

typedef struct _phase_t{
  int phase_length; // == workflow_length
  int *phase_content;
}phase_t, *phase_t_ptr;

typedef struct _marray{
  int M;
  int N;
  int tsize; // M*N
  //size_t data_sz; // bytes
  double *data;

}marray, *marray_ptr;

typedef struct _datastream {
  int streamID; // data_generator rankID

  char *srcname; // imagename, imageID
  int srcID;

  // metadata: size of per datastream
  long datasize;

  // derived from workflow knowledge
  int groupsize;
  int groupID;

  //int num_outs; // number of outputs from this current phase
  //marray **mdatas;
  marray mdatas[MAX_MFIELD];

  int workflow_number;
  int *workflow_nextstep;
  workflow_t *workflows;
  phase_t *phases;

  long self_size;
  
  int src_stone;
  char *contact_string;

}datastream, *datastream_ptr;

struct JItem{
  int gid;
  int items; 
  int is_ready;
  void *cm;
  void *ds;
  struct list_head link;
};

struct Job{
  int jobs;
  struct list_head *jhead;
  struct list_head link;
};

struct Stream{
  int streamID;
  struct list_head *qhead;
  struct list_head link;
};

struct JQueues{
  pthread_mutex_t *qlocks;
  struct list_head *qheads;
};
struct JobsToProcess{
  int count;
  struct list_head **jobs; /* size = jobs */
};


int init_matlabenv(handler_owner **gdata);

#endif
