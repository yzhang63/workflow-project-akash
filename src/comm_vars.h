#ifndef COMM_VARS_H
#define COMM_VARS_H

#define STORE_TIMING

#define M_DEBUG

#define CORE_DUMP_DIR

#define DEBUG

#define DOWNSTREAM 100
#define UPSTREAM 59

#define TRUE 1
#define FALSE 0

#define INVILID_INT (-1337)

#define TOEXEC 1111
#define TOOUTPUT 2222

#define PLUS 1
#define MINUS -1

#define MSTONE_NUM 3 /* the max num of mstones in the system */
#define MSTONE_NUM_STR "3" /* XXX update this with MSTONE_NUM in the system */

#define IMAGE_COUNT 1600 
#define GNUM 1600 /* total num of groups in the system */
#define GNUM_STR "1600" /* XXX update this with GNUM, used by evpath */
#define PERGSIZE 1 /* the num of members in each group */
#define OBJ_PER_STREAM 1600 

#define NUM_RESOURCES 4 /* to define the maximal capacity */
#define NUM_PROCESSES 100 /* to define the maximal processing instances per site */
#define NUM_DG 500 /* to define the maximal DG processes  */

#define MAX_INSTANCES (NUM_RESOURCES*NUM_PROCESSES) /* to bound the total num of processing instances in the whole system */

#define QL_OFFLOADER_THRESHOLD 0.4

#define IDLE_BROADCAST 2345
#define REDIRECT_REQUEST 5678

#define AQLEVEL(q0, q1, q2) ( ((!q0) && (!q1) && (!q2)) == 1 ? 1:0 )


#define CONTACT_INFO_FILE "./contacts/contact_info.txt"
#define TEMP_CONTACT_INFO_FILE "./contacts/temp_contact_info.txt"
#define FORUPSTREAM_CONTACT_READY_FILE "./contacts/forupstream_contact_ready.txt"

#define TOTAL_RESOURCES 3 /* pure processing sites + end sites */
enum Resource_Station{
  ENDSITE,
  SITEONE,
  SITETWO,
  DG
};
extern enum Resource_Station clusterid;
extern int PLUGMPI;

enum DataName{
  XPOS    = 0,
  YPOS    = 1,
  BWMEANC = 2,
  IM      = 3,
  
  BWS1    = 4,
  I1      = 5,

  EDGE    = 6,

  XFIT    = 7,
  YFIT    = 8,
  CFIT    = 9,
  SFIT    = 10,
  KAPPA   = 11
};

#define HEADER_SIZE 21 // integer

#define SIZES 6,\
              1,\
              5
extern int VARSIZES[3];

#define CHECK_GSTAT_PERIOD sleep(0)
#define ADA_MONUP_PERIOD sleep(3)
#define ADA_MONDOWN_PERIOD sleep(6)
#define PULL_WAIT sleep(4)

#define BUFSIZE 1024000
#define MAX_MFIELD 15
#define MAX_MFIELD_STR "15"

#define QL_STEALER_THRESHOLD 2
#define OBJLISTFILE_LEN 1024

#define BATCHSIZE 1024*1024*8*1 // byte
#define BATCH 10 // 10 output at a time
#define UPLOAD 888
#define LAST 999

#define FAULTY_WINDOW 3
#define GOOD 333
#define BAD -444

#define UNCHANGED 0
#define CHANGED 1

//#define PLUGMPI

#endif

