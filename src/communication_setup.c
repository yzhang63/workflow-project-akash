#ifdef PLUGMPI
#include <mpi.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>

#include "comm_vars.h"
#include "communication_setup.h"
#include "ffs_datastream.h" /* for all the structures */
#include "service_chain_assembler.h"

static void log_localcontacts(handler_owner *gdata);

//JobQueues **jqueues = NULL;
dequeue_instr *deq_instr = NULL;
FILE **fp_deqslot = NULL, **fp_enqslot = NULL;
#if 0
int *fp_qlevel = NULL;
#endif

int fp_p1_time = -1, fp_p2_time = -1, fp_p3_time = -1;

int fp_wbuff_timer = -1, fp_wobuff_timer = -1;

// FD
int fp_final = -1;

void record_localcontacts(handler_owner *gdata, int clusterid)
{
  assert(gdata);

  log_localcontacts(gdata);

#ifdef PLUGMPI
  MPI_Barrier(gdata->comm); // needs to be here for correctness!!!
#endif
  
  if(clusterid != DG){
    obtain_localcontacts(gdata);
  }
 
  return;
}

void setup_finaloutput_actions(handler_owner *gdata, 
                              int (*dshandlerPtr)(CManager, void *, void *, attr_list), 
                              FMStructDescRec *msg_format_list)
{
  // set up for the local terminal stone
  //int (*dshandlerPtr)(CManager, void *, void *, attr_list);
  //dshandlerPtr = &output_handler;
  //static FMStructDescRec *msg_format_list = datastream_format_list;
  EVstone stone = EValloc_stone(gdata->cm);
  EVaction action;
  EVassoc_terminal_action(gdata->cm, stone, msg_format_list, 
      dshandlerPtr, (void *)gdata);

  gdata->to_finaloutput = EValloc_stone(gdata->cm);
  action = EVassoc_split_action(gdata->cm, gdata->to_finaloutput, NULL);
  EVaction_add_split_target(gdata->cm, gdata->to_finaloutput, action, 
      stone);
  EVcreate_submit_handle(gdata->cm, gdata->to_finaloutput, msg_format_list);

  return;
}


/* to inform only the 1st mstoneID & contact_string
 *  
 * In particular, there will be a inform block for each resource location
 * in the system, storing information in the format as follows:
 * BLOCK0: (always ENDSITE info)
 * @line1: #mpisize
 * @line2: mappedto_cluster:mappedto_rank:mstone0ID:contact_string (rank 0)
 * @line3: mappedto_cluster:mappedto_rank:mstone0ID:contact_string (rank 1)
 * ...
 *
 * */
static void
log_localcontacts(handler_owner *gdata)
{
  assert(gdata != NULL);
#ifdef DEBUG
  fprintf(stderr, "@log_localcontacts\n");
#endif

  int i;
  int rank = gdata->rank;
  int size = gdata->size;

  char contact_info_filename[250];
  char forupstream_contact_ready_filename[250];
  sprintf(contact_info_filename, "%s", CONTACT_INFO_FILE);
  sprintf(forupstream_contact_ready_filename, "%s", FORUPSTREAM_CONTACT_READY_FILE);

  // to store the newly registered resource contact info
  char temp_contact_info_filename[250];
  sprintf(temp_contact_info_filename, "%s", TEMP_CONTACT_INFO_FILE);

  char *string_list = NULL;
  attr_list contact_list;
  char permstone_contact_info[50]; //%d:%d:%d:%s or %d 
  char mstones_contact_info[1000]; // %d:%s * ~20 mstones
  memset(&mstones_contact_info[0], '\0', 1000);

  contact_list = CMget_contact_list(gdata->cm);
  string_list = attr_list_to_string( contact_list );
  assert(string_list != NULL);
  gdata->src_contactstr = strdup(string_list); 
  assert(gdata->src_contactstr != NULL);
  assert(gdata->num_multiactions > 0);
  
  // only to record the mstone #0 contact info
  memset(&permstone_contact_info[0], '\0', 50);
  sprintf(&permstone_contact_info[0], "%d:%d:%d:%s\n", 
                                      gdata->mappedto_cluster,
                                      gdata->mappedto_rank,
                                      gdata->multiq_stones[0], 
                                      string_list);
  strncat(&mstones_contact_info[0], permstone_contact_info, 
                                   strlen(permstone_contact_info));

#ifdef DEBUG
  printf("local rank %d mstones_contact info:\n", gdata->rank);
  printf("%s\n", mstones_contact_info);
#endif

  char *recvbuf = NULL;
  if(rank == 0){
    recvbuf = (char *) calloc(1000*size, sizeof(char));
    assert(recvbuf != NULL);
  }  
#ifdef PLUGMPI
  MPI_Gather(mstones_contact_info, 1000, MPI_CHAR, recvbuf,
      1000, MPI_CHAR, 0, gdata->comm);
#else
  memcpy(recvbuf, mstones_contact_info, 1000);
  printf("Recv Buff for stone contact: %s\n", recvbuf);
#endif

  if(rank == 0){
    FILE * fp_out = fopen(contact_info_filename, "a");
    if(!fp_out){
      fprintf(stderr, "File for contact_info_filename cannot be opened for write.\n");
      exit(1);
    }

    FILE * temp_fp_out = fopen(temp_contact_info_filename, "w");
    if(!temp_fp_out){
      fprintf(stderr, "File for temp_contact_info_filename cantbe opened for write.\n");
      exit(1);
    }

    // to write the localmpisize for remote party
    memset(&permstone_contact_info[0], '\0', 50);
    sprintf(&permstone_contact_info[0], "%d\n", gdata->size );
    fprintf(fp_out, "%s", permstone_contact_info);
    fprintf(temp_fp_out, "%s", permstone_contact_info);

    for(i=0; i<size; i++){
      fprintf(fp_out, "%s", &recvbuf[i*1000]);
      fprintf(temp_fp_out, "%s", &recvbuf[i*1000]);
    }
    fclose(fp_out);
    fclose(temp_fp_out);
    
    fp_out = fopen(forupstream_contact_ready_filename, "a+");
    if(!fp_out){
      fprintf(stderr, "File for forupstream_contact_ready_filename cannot be opened for write.\n");
      exit(1);
    }
    fprintf(fp_out, "cluster%d ready\n", clusterid);
    fclose(fp_out);

    printf("rank %d inform local conn info ready for ***upstream***\n\n", gdata->rank);
    free(recvbuf);
  } // @rank = 0

  return;

}

/* to populate the intra-resource location process-based contact information
 * As a peer to remote downstreamers/upstreamer for adaptive action
 *
 * */
void
obtain_localcontacts(handler_owner *gdata)
{
  char temp_contact_info_filename[250];
  sprintf(temp_contact_info_filename, "%s", TEMP_CONTACT_INFO_FILE);
  FILE *fp_in = NULL;
  char in_contact[50];
  memset(in_contact, 0, 250);
  int rstone, num_procs, rank;
  int clustersize, rankidx, clusteridx, ret; 

  fp_in = fopen(temp_contact_info_filename, "r");
  while(!fp_in){
    fp_in = fopen(temp_contact_info_filename, "r");
  }
  
  while( (ret = fscanf(fp_in, "%d:%d:%d:%s", &num_procs, &rank, 
                              &rstone, in_contact)) != EOF ){
    if(ret == 1){ // a block for a new resource location starts
      clustersize = num_procs; 
      gdata->resource_mpisizes[gdata->num_resources] = clustersize;
      gdata->num_resources += 1;
      rankidx = 0;
    }
    else{ // still in the same block as the last resource
      clusteridx = gdata->num_resources - 1;
      gdata->downstream_contacts[clusteridx][rankidx].remote_stone = 
                                                               rstone;
      gdata->downstream_contacts[clusteridx][rankidx].string_list = 
                                                          strdup(in_contact);
      assert(gdata->downstream_contacts[clusteridx][rankidx].string_list 
                                                                != NULL);
      gdata->reports += 1;
      rankidx += 1; 
    }

  }
  fclose(fp_in);

  return;
}

/* to be used @send_stream.cod
 * for ID identifier for msg receiver
 * */
int 
get_my_resourceID()
{
  return client_data->itself;
}

int
get_my_rankID()
{
  return client_data->rank;  
}

int 
get_my_laststageID()
{
  return client_data->num_multiactions;
}

/* -end- */


