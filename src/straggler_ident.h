#ifndef STRAGGLER_IDENT_H
#define STRAGGLER_IDENT_H

#include <time.h>
#include <pthread.h>

#include "comm_vars.h"
#include "ffs_datastream.h"
#define STRAGGLER_TIMEBOUND 1000 

typedef struct _stream_info_t{
  int groupID;
  int srcID;

}stream_info_t, *stream_info_ptr;

typedef struct _group_map_t{
  int groupID;
  int cur_gsize;
  time_t start;

  int if_straggler;
  int mapped_rank;

  pthread_mutex_t gstat_lock;
//  struct _group_map_t *next;

}group_map_t;


typedef struct _straggler_map_t{
  int size;
  int *groupIDs;
  int output_target;

}straggler_map_t, *straggler_map_t_ptr;

typedef struct _gsize_t{
  int groupID;
  int groupSize;

}gsize_t;

typedef struct _decision_factor_t{
  time_t threshold;
  gsize_t *groups_info;
  int mstoneID;

}decision_factor_t;

extern group_map_t **group_stat;
extern pthread_t *gstat_mon_thread;
extern decision_factor_t **dfactors;

extern straggler_map_t **stragglers; // only one straggler structure

extern void *check_group_status(void *decision_factors);
extern int if_a_complete_group(int groupID, int cur_gsize, gsize_t *e_gsize);
extern int if_a_straggler_group(int groupID, straggler_map_t *straggler_info);
extern void set_ctl_msg(toexec_ctl_msg *toexec_ctl_data, group_map_t *completeg_info);

void
update_and_check_group_status(toexec_ctl_msg *toexec_ctl_data, int which_mstone, int which_group);

/* to fill the mon_data or update the groupSizes
 * */
extern void update_mondata_by_reducegsize(int mstoneID, monitor_info_t *mon_data,
					 int gsize_toreduce, int gidx);
extern void update_mondata_by_increasegsize(int groupID, int mstoneID, monitor_info_t *mon_data); 
extern int ret_mon_data(int groupID, int mstoneID, monitor_info_t *mon_data, 
			int gsize_toreduce, int gidx);

extern void update_thisgroup_arrivalinfo(int groupID, 
                                  int mstoneID, 
                                  int gsize_toreduce, 
                                  int gidx);
extern int 
if_in_complset(int groupID, int *groupset);

static FMField stream_info_fields[] = {
  {"groupID", "integer", sizeof(int), FMOffset(stream_info_ptr, groupID)},
  {"srcID", "integer", sizeof(int), FMOffset(stream_info_ptr, srcID)},
  {NULL, NULL, 0, 0}
};

static FMStructDescRec stream_info_format_list[] = {
  {"stream_info_t", stream_info_fields, sizeof(stream_info_t), NULL},
  {NULL, NULL, 0, NULL}
};

static FMField straggler_map_fields[] = {
  {"size", "integer", sizeof(int), FMOffset(straggler_map_t_ptr, size)},
  {"groupIDs", "integer[size]", sizeof(int), FMOffset(straggler_map_t_ptr, groupIDs)},
  {"output_target", "integer", sizeof(int), FMOffset(straggler_map_t_ptr, output_target)},
  {NULL, NULL, 0, 0}
};

static FMStructDescRec straggler_map_format_list[] = {
  {"straggler_map_t", straggler_map_fields, sizeof(straggler_map_t), NULL},
  {NULL, NULL, 0, NULL}
};

#endif

