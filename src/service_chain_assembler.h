#ifndef SERVICE_CHAIN_ASSEMBLER_H
#define SERVICE_CHAIN_ASSEMBLER_H

#include "ffs_datastream.h"
#include "cod.h"
#include "straggler_ident.h"

//#include "cloudupload.h"
//#include "cloudupload_supplement.h"
#include "comm_vars.h"

struct MemoryStruct{
    char *memory; // struct StatusInfo or inter-mediate data
    size_t bytesize;
};

struct StatusInfo{ // meta-data
    int cid;
    int pid;
    int tasks[MSTONE_NUM]; // 3 is the total phase count
    size_t estime[MSTONE_NUM];
};

extern void
ret_moutputs_mapinfo(mstone_outputs_t *mouts, int mstoneID);

/*
 * to make schedule/map decisions such as "toexec" and "tooutput decisions"
 * as of the handler for monitoring data.
 * Currently, monitor_info_t includes: groupID, groupsize
 *
 * Note that 1) The decision will be then send to control stone which will then be delivered
 * to multi-stones. And 2) ctl_msg->mstone, and output_target are the critial control info.
 *
 * */
extern int 
monitor_handler(CManager cm, void *vevent, void *c_data, attr_list attrs);

extern void
set_memorybridge_toupstream(handler_owner *client_data);

extern int
taskready_handler(CManager cm, void *vevent, void *c_data, attr_list attrs);

#if 0
extern void 
enqueue_streams(int qid, void *msg, void *cm);

extern CompleteJobs * 
dequeue_streams(int qid, int num_jobs);
#endif

extern void *
consume_streams(void *deq_instr);

extern void *
upload_statdat(void *in);

extern void
response_with_workload(datastream *ds, int which_resource, int which_rank, 
    FMStructDescRec *msg_format_list);

extern int 
adamsg_responsor(CManager cm, void *vevent, void *c_data, attr_list attrs);

#if 0
extern void 
broadcast_idlestat(adamsg *info, FMStructDescRec *msg_format_list);
#endif

int obtain_processing_decision(int old, int *qid, int *num_tasks, int option, int faulty);

#if 0
int apply_analysis_routine(int qid, CompleteJobs *tasks, handler_owner *gdata);
#endif

void *analysis_engine(void *in);

// used otherwise
void init_system(int argc, char **argv,
    handler_owner **gdata,
    FMStructDescList *format_list, int clusterid);
void start_matlabengine(handler_owner *gdata, int clusterid);
void register_routines_toevpathspace(handler_owner *gdata, int clusterid);
void connect_system(handler_owner *gdata, int clusterid);
void broadcast_contactreadymsg();
#if 0
void flowout_data(CompleteJobs *tasks, int qid, handler_owner *gdata);
#endif
//void init_cloud(handler_owner* gdata, char **token, char **purl);
//void _get_cloudinfo(handler_owner *gdata, char **token, char **purl);
// will only be called in endsite
//int _create_datcontainers(char* token, char* url, handler_owner* gdata);
//int _create_statcontainers(char* token, char* url);
//void construct_dataobj(struct MemoryStruct* sdata, void* data, size_t size);
//void construct_dataobj(struct MemoryStruct* sdata, int start, int elements, datastream *ds);
//int extend_dataobjs(struct MemoryStruct* sdata, int start, int elements, datastream *ds, size_t bufsize, int option);
//void construct_statobj(struct MemoryStruct* sdata, struct StatusInfo* si);
//void upload_single_dataobj(const char *token, const char *url, const char *cname, datastream *ds, int stage);
void upload_statusdat(char* token, char* url, struct StatusInfo *si, struct MemoryStruct *sdata);

void *download_statdat(void *in);
void disseminate_data(struct MemoryStruct* sdata, int qid, struct JobsToProcess *j2p, handler_owner *gdata, int option);
int apply_analysis(int qid, struct JobsToProcess *j2p, handler_owner *gdata);
void free_processed_jobs(struct JobsToProcess *j2p, int qid);

struct JobsToProcess* active_dequeue(int count, int qid, struct JQueues *jqs);
void add_job_container(struct Job *job, int stramID, int qid, struct JQueues *jqs);
void add_an_item(struct JItem *item, int qid, struct JQueues *jqs);
struct JobsToProcess* passive_dequeue(struct JQueues *jqs);
//struct _marray* decode_interdata(struct MemoryStruct *sdata);
struct _marray** decode_interdataset(struct MemoryStruct *sdata);

//void upload_interdata_nobuffer(char* token, char* url, datastream *ds, int qid, handler_owner* gdata);
//void upload_interdata_withbuffer(char* token, char* url, struct MemoryStruct* sdata, datastream *ds, int qid, handler_owner* gdata, size_t bufsize, int option);
//void upload_nobuff(const char *token, const char *url, const char *cname, const char *oname, struct MemoryStruct *sdata);
//void upload_withbuff(const char *token, const char *url, const char *cname, const char *oname, struct MemoryStruct *sdata);
//void upload_imagedata(char* token, char* url, struct MemoryStruct* sdata, char* cname, char* oname, datastream *ds, int stage, handler_owner* gdata, size_t bufsize, int option);

/* auxiliary routines */
void print_jobqueue(struct list_head *qhead, int qid);


int faulty_detection(struct MemoryStruct* sdata, struct JobsToProcess *j2p, int qid, int option);
void add_faulty_dbuffer(void *ds, struct list_head* dhead);
void add_faulty_sbuffer(int streamID);
struct list_head* locate_faulty_stream(int streamID);

#ifdef PLUGMPI
void init_mpirun(int argc, char **argv, int *rank, int *size);
#endif

// for debugging
int apply_complete_analysis(int qid, struct JobsToProcess *j2p, handler_owner *gdata);
void *complete_analysis_engine(void *in);
void fake_taskready_handler();
void fake_disseminate_data(struct MemoryStruct* sdata, int qid, struct JobsToProcess *j2p, handler_owner *gdata, int option);
void fake_free_processed_jobs(struct JobsToProcess *j2p, int qid);
void free_ds_matdata(datastream *ds);


#endif
