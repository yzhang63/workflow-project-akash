#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <inttypes.h>

#include "evpath.h"
#include "ffs_datastream.h"
#include "comm_vars.h"
#include "service_chain_assembler.h"
#include "cod.h"
#include "communication_setup.h"
#include "cod_funcs.h"
#include "workflow.h"
#include "straggler_ident.h"

#define TIMING
#include "timer.h"

//#include "cloudupload.h"
//#include "cloudupload_supplement.h"

enum Resource_Station clusterid = ENDSITE;
handler_owner *client_data = NULL;
static FMStructDescList queue_list[] = {
  datastream_format_list, 
  monitor_info_format_list,
  groupsize_info_t_format_list, 
  toexec_ctl_msg_format_list, 
  straggler_map_format_list, 
  mstone_outputs_format_list,
  contactsready_msg_format_list,
  yesno_info_format_list,
  adamsg_format_list,
  NULL};

static TIMER_DECLARE1(timer);
static int output_handler_count = 0;
uint64_t my_clk = 0;

static int
output_handler(CManager cm, void *vevent, void *c_data, attr_list attrs)
{
  datastream *event = (datastream *)vevent;
  assert(event != NULL);
  
  output_handler_count++;
  if(output_handler_count > (GNUM * PERGSIZE)){
    printf("output_handler_count = %d\n", output_handler_count);
    return 1;
  }

  // to store the final result into a file
  char msg[30];
  printf("Writing %d event into file\n", output_handler_count);

  sprintf(msg, "%d\n", event->groupID);
  write(fp_final, msg, strlen(msg));

  if(output_handler_count == 1){
    TIMER_START(timer);
  }
  //if( output_handler_count == (GNUM * PERGSIZE - 32) ){
  if( output_handler_count == (GNUM * PERGSIZE) ){
    TIMER_END(timer, my_clk);
    printf("The total processing time in usec: %" PRIu64 "\n", my_clk);

    sprintf(msg, "%" PRIu64 "\n", my_clk);
    write(fp_final, msg, strlen(msg));
    close(fp_final);
  }

  return 1;
}

int main(int argc, char **argv)
{
  int i, j, ret;

  // independent system setup
  init_system(argc, argv, &client_data, queue_list, ENDSITE); 
  register_routines_toevpathspace(client_data, ENDSITE);

#ifdef PLUGMPI
  MPI_Barrier(client_data->comm);
#endif

  setup_finaloutput_actions(client_data, output_handler, datastream_format_list);
  //init_cloud(client_data, &token, &purl);
 
#ifdef PLUGMPI
  MPI_Barrier(client_data->comm);
#endif

  // to connect all the potential outputs of mstones 
  connect_system(client_data, ENDSITE);

  // to inform all the local potential contact stones' ID & strlist 
  record_localcontacts(client_data, ENDSITE);

  //CMsleep(client_data->cm, 18000);
  CMsleep(client_data->cm,2900);

#ifdef PLUGMPI
  MPI_Finalize();
#endif

  return EXIT_SUCCESS;
}
