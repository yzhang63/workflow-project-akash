#ifndef WORKFLOW_H
#define WORKFLOW_H

#include "comm_vars.h"
#include "engine.h"
#include "ffs_datastream.h"

extern void add_matlabfunc_path(Engine *ep);

extern void prepare_imagedata(Engine *ep, int timages);
extern void assemble_streamObjs(Engine *ep, datastream *ds, int whichimage);
extern void init_stream(datastream *ds, handler_owner *client_data);
//static void prepare_stream(datastream *ds, int whicharray, int M, int N, double *data);

#if 0
extern void binarize_imagestreams(OneJob  *ds_array, handler_owner *client_data); // stream pro
extern void extract_flameedgestreams(OneJob  *ds_array, Engine *ep);
extern void compute_curvaturestreams(OneJob  *ds_array, Engine *ep);
#endif

//static void binarize_image(Engine *ep, datastream *ds, handler_owner *client_data); // called internally
//static void extract_flameedge(Engine *ep, datastream *ds);
//static void compute_curvature(Engine *ep, datastream *ds);
extern void init_exppars(handler_owner *client_data);
//static void access_processed_data(Engine *ep, datastream *ds, char *varname, int whichmarray);

extern void cleanup_extradata(datastream *ds, int start);
extern void init_matlab(Engine *ep, handler_owner *client_data);

void binarize_images(struct JobsToProcess *j2p, handler_owner *gdata);
void extract_flameedges(struct JobsToProcess *j2p, handler_owner *gdata);
void compute_curvatures(struct JobsToProcess *j2p, handler_owner *gdata);

void complete_workflow(struct JobsToProcess *j2p, handler_owner *gdata);
extern void fake_init_stream(datastream *ds, handler_owner *client_data);
#endif
