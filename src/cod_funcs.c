#include <stdio.h>
#include <stdlib.h>
#include <linux/limits.h>
#include <assert.h>

#include "cod_funcs.h"
#include "comm_vars.h"

char *multiqueue_action = NULL;
char *multiqueue_action_2 = NULL;
char *multiqueue_action_3 = NULL;

int init_phasefunc(char **action, const char *path)
{
  unsigned long func_size, ret;
  FILE *fp = fopen(path, "r");
  assert(fp != NULL);

  fseek(fp, 0, SEEK_END);
  func_size = ftell(fp);
  fseek(fp, 0, SEEK_SET);

  *action = (char *)calloc(func_size+10, sizeof(char));
  assert( (*action) != NULL );

  ret = fread( (void *)(*action), sizeof(char), func_size, fp );
  assert(ret == func_size);

  fclose(fp);
  return 0;

}

int init_all_multiqueue_funcs(void)
{
  char *path_to_phase1 = "phase1.cod";
  char *path_to_phase2 = "phase2.cod";
  char *path_to_phase3 = "phase3.cod";

  init_phasefunc(&multiqueue_action, path_to_phase1);

  init_phasefunc(&multiqueue_action_2, path_to_phase2);

  init_phasefunc(&multiqueue_action_3, path_to_phase3);

  return 0;
}

