#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>

#include "evpath.h"
#include "ffs_datastream.h"
#include "comm_vars.h"
#include "service_chain_assembler.h"
#include "cod.h"
#include "communication_setup.h"
#include "cod_funcs.h"
#include "workflow.h"
#include "straggler_ident.h"

//#include "cloudupload.h"
//#include "cloudupload_supplement.h"

enum Resource_Station clusterid = SITEONE;
handler_owner *client_data = NULL;
static FMStructDescList queue_list[] = {
  datastream_format_list, 
  monitor_info_format_list,
  groupsize_info_t_format_list, 
  toexec_ctl_msg_format_list, 
  straggler_map_format_list, 
  mstone_outputs_format_list,
  contactsready_msg_format_list,
  yesno_info_format_list,
  adamsg_format_list,
  NULL};

int main(int argc, char **argv)
{
  // independent system setup
  init_system(argc, argv, &client_data, queue_list, SITEONE); 
  start_matlabengine(client_data, SITEONE);
  register_routines_toevpathspace(client_data, SITEONE);
  //init_cloud(client_data, &token, &purl);

#ifdef PLUGMPI
  MPI_Barrier(client_data->comm);
#endif

  // build up the bridge with the remote contact & set up the internal service line
  connect_system(client_data, SITEONE);

#ifdef PLUGMPI
  MPI_Barrier(client_data->comm);
#endif

  // to inform all the local potential contact stones' ID & strlist 
  record_localcontacts(client_data, SITEONE);

  if(client_data->rank == 0){
    broadcast_contactreadymsg();
  }

#ifdef PLUGMPI
  MPI_Barrier(client_data->comm);
#endif

#if 0
#ifdef CORE_DUMP_DIR
  //system("ulimit -c unlimited");
  //system("ulimit -a");
  int indicator = chdir("/net/hp100/ihpcae/yanwei/contactfiles");
  if(indicator){
    fprintf(stderr, "chdir failed\n");
#ifdef PLUGMPI
    MPI_Finalize();
#endif
  }
#endif
#endif

  CMsleep(client_data->cm, 2700);

  close(fp_p1_time);
  close(fp_p2_time);
  close(fp_p3_time);
#if 0
  for(i=0; i<client_data->num_multiactions; i++){
    close(fp_qlevel[i]);
    free(fp_qlevel);
  }
#endif
  //close(fp_datatimer);

  close(fp_wbuff_timer);
  close(fp_wobuff_timer);

  engClose(client_data->ep);
  client_data->ep = NULL;
  printf("Rank#%d closing engine#\n", client_data->rank);

  printf("workflow processing site one (w.r.t endsite direction) exits normally...\n"); 

#ifdef PLUGMPI
  MPI_Finalize();  
#endif

  return 0;
}
