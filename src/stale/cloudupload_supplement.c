#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "cloudupload_supplement.h"
#include "list.h"

char *token = NULL;
char *purl  = NULL;
struct PairStruct pars[4];

// to get the token & url for future cloud access
int prepare_cloudaccess()
{
    LIST_HEAD(PARSHEAD);
    init_pairstructs(&PARSHEAD);
    init_cloudaccess(&PARSHEAD);

    return 0;
}

int init_pairstructs(struct list_head *parshead)
{
    if(!parshead) return -1;

    pars[0].name = strdup("tname");
    //pars[0].value = strdup("yzhang");
    pars[0].value = strdup("test");
    if(!pars[0].name || !pars[0].value){
        return -1;
    }

    pars[1].name = strdup("username");
    //pars[1].value = strdup("shale");
    pars[1].value = strdup("tester");
    if(!pars[1].name || !pars[1].value){
        return -1;
    }

    pars[2].name = strdup("pswd");
    //pars[2].value = strdup("yanweipassword");
    pars[2].value = strdup("testing");
    if(!pars[2].name || !pars[2].value){
        return -1;
    }

    pars[3].name = strdup("addr");
    //pars[3].value = strdup("127.0.0.1");
    //pars[3].value = strdup("143.215.131.232");
    //pars[3].value = strdup("shale.cc.gt.atl.ga.us");
    pars[3].value = strdup("130.207.117.28");
    //pars[3].value = strdup("transwarp.cc.gatech.edu");
    //pars[3].value = strdup("http://127.0.0.1:8080/v1.0");
    //pars[3].value = strdup("http://127.0.0.1:8080/v1.0");
    if(!pars[3].name || !pars[3].value){
        return -1;
    }

    int i;
    for(i=0; i<4; i++){
        list_add(&(pars[i].link), parshead);
    }

    return 0;
    
}

int init_cloudaccess(struct list_head *parshead)
{
    if(!parshead) return -1;

    char *tname, *username, *pswd, *addr;

    struct PairStruct *item;
    list_for_each_entry(item, parshead, link){
        if (!strcasecmp (item->name, "tname")){
            tname = item->value;
        }
        else if(!strcasecmp(item->name, "username")){
            username = item->value;
        }
        else if(!strcasecmp(item->name, "pswd")){
            pswd = item->value;
        }
        else if(!strcasecmp(item->name, "addr")){
            addr = item->value;
        }
    }

    // cloudupload initialization
     
    if(get_token(tname, username, pswd, addr, &token, &purl) == 0){
        printf("[cloudupload] get_token success.\n");
        printf("[cloudupload] token: %s\n",token);
        printf("[cloudupload] purl: %s\n",purl);
        //memset(purl, 0, 80);
        //strcpy(purl, "https://transwarp.cc.gatech.edu/v1/AUTH_test");
        //printf("[cloudupload] purl: %s\n",purl);
    }
    else{
        printf("[cloudupload] Error: Cannot get token\n");
    }
     
    // cloudupload initialization end

    return 0;

}

int start_timer(struct timer *t)
{
  if(!t) return -1;

  //TIMER_DECLARE1(*t);
  TIMER_START(*t);

  return 0;
}
int end_timer(const char *cname, const size_t bytesize, struct timer *t)
{
  if(!cname || !t) return -1;

  uint64_t t_clk = 0;
  TIMER_END(*t, t_clk);
  char msg[100];

  if(strcmp(cname, "stat_con") == 0) // stat_con
    ;
  else{
    //sprintf(msg, "%s:%lu:%llu\n", cname, bytesize, t_clk);
    sprintf(msg, "%s:%lu:%" PRIu64 "\n", cname, bytesize, t_clk);
    write(fp_datatimer, msg);
  }
  
  return 0;
}

