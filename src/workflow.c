#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <inttypes.h>

#define TIMING
#include "timer.h"

#include "workflow.h"
#include "matlab_util.h"

#if defined(NDEBUG)
#error NDEBUG is defined
#endif

static void prepare_stream(datastream *ds, int whicharray, int M, int N, double *data);
static void access_processed_data(Engine *ep, datastream *ds, char *varname, int whichmarray);

static void _binarize_image(Engine *ep, datastream *ds, handler_owner *client_data, int groupID); // called internally
static void _extract_flameedge(Engine *ep, datastream *ds, int groupID);
static void _compute_curvature(Engine *ep, datastream *ds, int groupID);

static int _faulty_detection(struct list_head* dhead, void *ds);

static void access_processed_data_dummy(Engine *ep, datastream *ds, char *varname, int whichmarray);
static void prepare_stream_dummy(datastream *ds, int whicharray);

static void _binarize_image_dummy(Engine *ep, datastream *ds, handler_owner *client_data, int groupID); // called internally
static void _extract_flameedge_dummy(Engine *ep, datastream *ds, int groupID);
static void _compute_curvature_dummy(Engine *ep, datastream *ds, int groupID);

/* to add user-defined functions into
 * matlab space
 * */
void add_matlabfunc_path(Engine *ep){

  //char buffer[BUFSIZE+1];
  //buffer[BUFSIZE] = '\0';
  //memset(buffer, 0, BUFSIZE+1);
  //engOutputBuffer(ep, buffer, BUFSIZE);

  const int NUM_PATH = 12;
  const char *mypaths[NUM_PATH];
  mypaths[0]  = "addpath('/net/hu20/yzhang63/versace/workflow-project/lsb')";
  mypaths[1]  = "addpath('/net/hu20/yzhang63/versace/workflow-project/lsb/readimx')";
  mypaths[2]  = "addpath('/net/hu20/yzhang63/versace/workflow-project/lsb/subfunctions')";
  mypaths[3]  = "addpath('/net/hu20/yzhang63/versace/workflow-project/lsb/subfunctions/archive')";
  mypaths[4]  = "addpath('/net/hu20/yzhang63/versace/workflow-project/lsb/subfunctions/edge')";
  mypaths[5]  = "addpath('/net/hu20/yzhang63/versace/workflow-project/lsb/subfunctions/fit')";
  mypaths[6]  = "addpath('/net/hu20/yzhang63/versace/workflow-project/lsb/subfunctions/load')";
  mypaths[7]  = "addpath('/net/hu20/yzhang63/versace/workflow-project/lsb/subfunctions/plot')";
  mypaths[8]  = "addpath('/net/hu20/yzhang63/versace/workflow-project/lsb/subfunctions/save')";
  mypaths[9]  = "addpath('/net/hu20/yzhang63/versace/workflow-project/lsb/subfunctions/strain')";
  mypaths[10]  = "addpath('/net/hu20/yzhang63/versace/workflow-project/lsb/subfunctions/curvature')";
  mypaths[11]  = "addpath('/net/hu20/yzhang63/versace/workflow-project/lsb/subfunctions/curvefit/splines')";

  int i;
  for(i=0; i<NUM_PATH; i++){
    //printf("Evaluating %s\n", mypaths[i]);
    engEvalString(ep, mypaths[i]);
  }

  return; 
}


void binarize_images(struct JobsToProcess *j2p, handler_owner *gdata)
{
  assert(j2p && gdata);

  printf("%d jobs to process @stage1\n", j2p->count);

  int i;
  struct list_head *jhead = NULL;
  struct JItem *item = NULL;
  for(i=0; i<j2p->count; i++){
    printf("processing JOB #%d\n", i);

    jhead  = j2p->jobs[i];
    list_for_each_entry(item, jhead, link){
      if(item->ds == NULL){
        printf("JOB#%d has %d items\n", item->gid, item->items);
        assert(item->is_ready);
        continue;
      }

      _binarize_image(gdata->ep, (datastream *)item->ds, gdata, item->gid);
      //_binarize_image_dummy(gdata->ep, (datastream *)item->ds, gdata, item->gid);

      printf("##rank#%d-completed p#1 on gid %d\n", gdata->rank, item->gid);
      //((datastream *)item->ds)->workflow_nextstep[0] += 1;
      ((datastream *)item->ds)->workflow_nextstep[0] += 3;
      ((datastream *)item->ds)->groupsize = 1; // no dependency for nextP 
    }

  }

  return;
}

void extract_flameedges(struct JobsToProcess *j2p, handler_owner *gdata)
{
  assert(j2p && gdata);

  printf("%d jobs to process @stage2\n", j2p->count);

  int i;
  struct list_head *jhead = NULL;
  struct JItem *item = NULL;
  for(i=0; i<j2p->count; i++){
    printf("processing JOB #%d\n", i);

    jhead  = j2p->jobs[i];
    list_for_each_entry(item, jhead, link){
      if(item->ds == NULL){
        printf("JOB#%d has %d items\n", item->gid, item->items);
        assert(item->is_ready);
        continue;
      }

      _extract_flameedge(gdata->ep, (datastream *)item->ds, item->gid);
      //_extract_flameedge_dummy(gdata->ep, (datastream *)item->ds, item->gid);

      printf("##rank#%d-completed p#2 on gid %d\n", gdata->rank, item->gid);
      ((datastream *)item->ds)->workflow_nextstep[0] += 1;
      ((datastream *)item->ds)->groupsize = 1; // no dependency for nextP 
    }

  }

  return;
}

void compute_curvatures(struct JobsToProcess *j2p, handler_owner *gdata)
{
  assert(j2p && gdata);

  printf("%d jobs to process @stage3\n", j2p->count);

  int i;
  struct list_head *jhead = NULL;
  struct JItem *item = NULL;
  for(i=0; i<j2p->count; i++){
    printf("processing JOB #%d\n", i);

    jhead  = j2p->jobs[i];
    list_for_each_entry(item, jhead, link){
      if(item->ds == NULL){
        printf("JOB#%d has %d items\n", item->gid, item->items);
        assert(item->is_ready);
        continue;
      }
      printf("Getting into _compute_curvature func\n");
      _compute_curvature(gdata->ep, (datastream *)item->ds, item->gid);

      //_compute_curvature_dummy(gdata->ep, (datastream *)item->ds, item->gid);
      printf("Getting outof _compute_curvature func\n");

      printf("##rank#%d-completed p#3 on gid %d\n", gdata->rank, item->gid);
      ((datastream *)item->ds)->workflow_nextstep[0] += 1;
      ((datastream *)item->ds)->groupsize = 1; // no dependency for nextP 
    }

  }

  return;
}

/*
 * PART I
 *
 * For the first half of this demonstration,mdata,  send data
 * to MATLAB, analyze the data, and plot the mdata.
 */
static void _binarize_image(Engine *ep, datastream *ds, handler_owner *client_data, int groupID){

  printf("@_binarize_image on image#%d.\n", groupID);

  char buffer[BUFSIZE+1];
  buffer[BUFSIZE] = '\0';
  engOutputBuffer(ep, buffer, BUFSIZE);

  /* matlab behavior */
  mxArray *X = NULL, *Y = NULL, *Im = NULL;
  int err;

  err = ml_marray2mx(&ds->mdatas[XPOS], &X);
  assert(0 == err);

  err = ml_marray2mx(&ds->mdatas[YPOS], &Y);
  assert(0 == err);

  err = ml_marray2mx(&ds->mdatas[IM], &Im);
  assert(0 == err);


  err = engPutVariable(ep, "X_binarization", X);
  assert(0 == err);
  err = engPutVariable(ep, "Y_binarization", Y);
  assert(0 == err);
  err = engPutVariable(ep, "Im_binarization", Im);
  assert(0 == err);
  
  init_matlab(ep, client_data);

#if 0
  TIMER_DECLARE1(binarize_timer);
  uint64_t binarize_clk = 0;
  TIMER_START(binarize_timer);
#endif
  engEvalString(ep, "filtSize = [5 5];");
  engEvalString(ep, "[BWs_binarization, I1_binarization] = binarizeImages(X_binarization, Y_binarization, Im_binarization, 1, S, P, T, U, phi, expangle, R, c, frame, 'corrected', corrected, 'filtSize', filtSize);");
#if 0
  TIMER_END(binarize_timer, binarize_clk);

  printf("%s\n",buffer);
  char msg[100];
  //sprintf(msg, "%d:%llu\n", groupID, binarize_clk);
  sprintf(msg, "%d:%" PRIu64 "\n", groupID, binarize_clk);
  write(fp_p1_time, msg, strlen(msg));
#endif

  access_processed_data(ep, ds, "BWs_binarization", BWS1);
  access_processed_data(ep, ds, "I1_binarization", I1);

  //TODO: what to do with the overwritten "data" & datastream from evpath?
  //The engine application owns the original mxArray and is responsible for freeing its memory. Although the engPutVariable function sends a copy of the mxArray to the MATLAB workspace, the engine application does not need to account for or free memory for the copy. - MATLAB
  mxDestroyArray(X); 
  mxDestroyArray(Y);
  mxDestroyArray(Im);

  // clear removes all variables from the current workspace, releasing them from system memory.
  engEvalString(ep, "clear;"); 

  return;
}

/*
 * PART II
 *
 * For the second half of this demonstration, we will request
 * a MATLAB string, which should define a variable X.  MATLAB
 * will evaluate the string and create the variable.  We
 * will then recover the variable, and determine its type.
 */
static void _extract_flameedge(Engine *ep, datastream *ds, int groupID){

  printf("@_extract_flameedge on image %d.\n", groupID);
  /*
   * Use engOutputBuffer to capture MATLAB output, so we can
   * echo it back.  Ensure first that the buffer is always NULL
   * terminated.
   */
  char buffer[BUFSIZE+1];
  buffer[BUFSIZE] = '\0';
  engOutputBuffer(ep, buffer, BUFSIZE);

  /* matlab behavior */
  mxArray *X, *Y, *BWs;
  int err;

  err = ml_marray2mx(&ds->mdatas[XPOS], &X);
  assert(0 == err);

  err = ml_marray2mx(&ds->mdatas[YPOS], &Y);
  assert(0 == err);

  err = ml_marray2mx(&ds->mdatas[BWS1], &BWs);
  assert(0 == err);


  err = engPutVariable(ep, "X_flameedge", X);
  assert(0 == err);
  err = engPutVariable(ep, "Y_flameedge", Y);
  assert(0 == err);
  err = engPutVariable(ep, "BWs_flameedge", BWs);
  assert(0 == err);

#if 0
  TIMER_DECLARE1(flame_timer);
  uint64_t flame_clk = 0;
  TIMER_START(flame_timer);
#endif
  engEvalString(ep, "[Ecell_flameedge,Esize_flameedge,Emat_flameedge] = flameEdges(BWs_flameedge, X_flameedge, Y_flameedge);");
#if 0
  TIMER_END(flame_timer, flame_clk);

  char msg[100];
  //sprintf(msg, "%d:%llu\n", groupID, flame_clk);
  sprintf(msg, "%d:%" PRIu64 "\n", groupID, flame_clk);
  write(fp_p2_time, msg, strlen(msg));
#endif

  //printf("%s\n", buffer);
  access_processed_data(ep, ds, "Emat_flameedge", EDGE);

  mxDestroyArray(BWs);
  mxDestroyArray(X);
  mxDestroyArray(Y);

  // clear removes all variables from the current workspace, releasing them from system memory.
  engEvalString(ep, "clear;"); 

  return;
}

/*
 * PART III
 *
 * For the second half of this demonstration, we will request
 * a MATLAB string, which should define a variable X.  MATLAB
 * will evaluate the string and create the variable.  We
 * will then recover the variable, and determine its type.
 */
static void _compute_curvature(Engine *ep, datastream *ds, int groupID){

  printf("@_compute_curvature on image#%d.\n", groupID);

#if 0
  char buffer[BUFSIZE+1];
  buffer[BUFSIZE] = '\0';
  engOutputBuffer(ep, buffer, BUFSIZE);
#endif

  /* matlab behavior */
  mxArray *X = NULL, *Y = NULL, *BWmeanC = NULL, *Emat = NULL; //*E=NULL;
  int err;

  err = ml_marray2mx(&ds->mdatas[XPOS], &X);
  assert(0 == err);

  err = ml_marray2mx(&ds->mdatas[YPOS], &Y);
  assert(0 == err);

  err = ml_marray2mx(&ds->mdatas[BWMEANC], &BWmeanC);
  assert(0 == err);

  err = ml_marray2mx(&ds->mdatas[EDGE], &Emat);
  assert(0 == err);
 
  err = engPutVariable(ep, "X_curvature", X); // XXX
  assert(0 == err);

#if 0
  // to retrieve the error
  engEvalString(ep, "myErr=lasterror;");
  mxArray *err = engGetVariable(ep, "myErr");
  assert(err != NULL);
  if(mxIsStruct(err)){
    mxArray *errStr = mxGetField(err, 0, "message");
    if((errStr != NULL) && mxIsChar(errStr)){
      char str[1024];
      mxGetString(errStr, str, sizeof(str)/sizeof(str[0])+1);
      printf("errStr @Curvature after engputvar X: %s\n", str);
    }
  }
  mxDestroyArray(err);
  // end
#endif

  err = engPutVariable(ep, "Y_curvature", Y);
  assert(0 == err);
  err = engPutVariable(ep, "BWmeanC_curvature", BWmeanC);
  assert(0 == err);
  err = engPutVariable(ep, "Emat_curvature", Emat);
  assert(0 == err);

#if 0
  TIMER_DECLARE1(curvature_timer);
  uint64_t curvature_clk = 0;
  TIMER_START(curvature_timer);
#endif
  printf("Calling engEvalString(Curvature)\n");
  err = engEvalString(ep, "[xDS, yDS, cDS, sDS, mx, my, mc, xp, yp,"
      " xpp, ypp, ms, mkappa, R, nx, ny] = "
      "Curvature(X_curvature, Y_curvature,"
      " BWmeanC_curvature, Emat_curvature);"); // XXX
  assert(0 == err);

#if 0
  TIMER_END(curvature_timer, curvature_clk);
  printf("%s\n", buffer);
  printf("@after engEvalString(Curv): gid:processingtime>>>%d:%"
      PRIu64 "\n", groupID, curvature_clk);
#endif

#if 0
  char msg[100];
//  sprintf(msg, "%d:%llu\n", groupID, curvature_clk);
  write(fp_p3_time, msg, strlen(msg));

  #if 0
  access_processed_data_dummy(ep, ds, "mx", XFIT);
  access_processed_data_dummy(ep, ds, "my", YFIT);
  access_processed_data_dummy(ep, ds, "mc", CFIT);
  access_processed_data_dummy(ep, ds, "ms", SFIT);
  access_processed_data_dummy(ep, ds, "mkappa", KAPPA);
  #endif
#endif

  mxDestroyArray(X);
  mxDestroyArray(Y);
  mxDestroyArray(BWmeanC);
  mxDestroyArray(Emat);

  // clear removes all variables from the current workspace, releasing them from system memory.
#if 0
  err = engEvalString(ep, "mm_in_use=monitor_memory_whos();");
  assert(err == 0);
  mxArray *mminuse = engGetVariable(ep, "mm_in_use");
  assert(mminuse != NULL);
  double *cmminuse = mxGetPr(mminuse);
  char msg[100];
  sprintf(msg, "%.03f\n", *cmminuse);
  printf("matlab mm_in_use: %s\n", msg);
  write(fp_p3_time, msg, strlen(msg));
  mxDestroyArray(mminuse);
#endif

  err = engEvalString(ep, "clear;"); 
  assert(0 == err);

  return;
}

void assemble_streamObjs(Engine *ep, datastream *ds, int whichimage){
  char buffer[BUFSIZE+1];
  buffer[BUFSIZE] = '\0';
  engOutputBuffer(ep, buffer, BUFSIZE);
  double *cdata = NULL;
  int datasize = -1, ret;
  const int *datadims = NULL;

  mxArray *imageID = NULL;
  double imageid = whichimage;
  imageID = mxCreateDoubleMatrix(1,1,mxREAL); 
  memcpy((void *)mxGetPr(imageID), (void *)&imageid, sizeof(double));
  ret = engPutVariable(ep, "imageID", imageID);
  assert(ret == 0);

  ret = engEvalString(ep, "[X_readin,Y_readin,Im_readin,BWmeanC_readin] = readin_imagedata(imageID);");
  assert(ret == 0);
  //printf("%s\n", buffer);

  ds->groupID = whichimage;
  ds->srcID = whichimage;
  snprintf(ds->srcname, 12, "B%d", whichimage);
  access_processed_data(ep, ds, "X_readin", 0);  
  access_processed_data(ep, ds, "Y_readin", 1);  
  access_processed_data(ep, ds, "BWmeanC_readin", 2);  
  access_processed_data(ep, ds, "Im_readin", 3);  

  mxDestroyArray(imageID);

  return;

}

static void prepare_stream(datastream *ds, int whicharray, int M, int N, double *data){
  assert(data != NULL);
  if(!data){
    printf("!!!!!Aborted!!!!\n");
    abort();
  }
  assert( (whicharray >= 0) && (whicharray < MAX_MFIELD) );
  if(ds->mdatas[whicharray].M > 0){ // data from previous
    ds->mdatas[whicharray].M = INVILID_INT;
    ds->mdatas[whicharray].N = INVILID_INT;
    ds->mdatas[whicharray].tsize = INVILID_INT;
    //free(ds->mdatas[whicharray].data);
    ds->mdatas[whicharray].data = NULL;
  }

  int i;
  ds->mdatas[whicharray].M = M;
  ds->mdatas[whicharray].N = N;
  ds->mdatas[whicharray].tsize = M*N;
  ds->mdatas[whicharray].data = (double *)calloc(ds->mdatas[whicharray].tsize, sizeof(double));
  assert( ds->mdatas[whicharray].data != NULL );
  for(i=0; i<M*N; i++){
    ds->mdatas[whicharray].data[i] = data[i];
  }

  return;
}

void init_stream(datastream *ds, handler_owner *client_data){
  ds->contact_string = strdup(client_data->src_contactstr);
  assert(ds->contact_string);
  ds->src_stone = client_data->src_stone;

  ds->streamID = client_data->rank;

  ds->srcname = (char *)calloc(15, sizeof(char));
  assert(ds->srcname);
  ds->srcID = 0;

  ds->datasize = 2681; // in unit of byte
  ds->groupsize = 1; // each group has only one stream object
  ds->groupID = 0;

  int i, j;
  for(i=0; i<MAX_MFIELD; i++){
    ds->mdatas[i].M = 0;
    ds->mdatas[i].N = 0;
    ds->mdatas[i].tsize = 0;

    ds->mdatas[i].data = NULL;
  }

  /*
  for(i=0; i<MAX_MFIELD; i++){
    ds->mdatas[i].M = 300;
    ds->mdatas[i].N = 3;
    ds->mdatas[i].tsize = 900;

    ds->mdatas[i].data = (double *)calloc(ds->mdatas[i].tsize, sizeof(double));
    assert(ds->mdatas[i].data != NULL);
    for(j=0; j<ds->mdatas[i].tsize; j++){
      ds->mdatas[i].data[j] = 0.0001 * j;
    }
  }
  */
  
  ds->workflow_number = 1;
  ds->workflow_nextstep = (int *)calloc(ds->workflow_number, sizeof(int));
  assert(ds->workflow_nextstep != NULL);
  ds->workflow_nextstep[0] = 0;

  ds->workflows = (workflow_t *) calloc(ds->workflow_number, sizeof(workflow_t));
  ds->phases = (phase_t *) calloc(ds->workflow_number, sizeof(phase_t));
  assert(ds->workflows != NULL);
  assert(ds->phases != NULL);
  for(i=0; i<ds->workflow_number; i++){
    ds->workflows[i].workflow_length = 3; // workflows set up
    ds->workflows[i].workflow_content = (int *)calloc(ds->workflows[i].workflow_length, sizeof(int));
    assert(ds->workflows[i].workflow_content != NULL);

    for(j=0; j<ds->workflows[i].workflow_length; j++){
      ds->workflows[i].workflow_content[j] = 2*j+1;
    }

    ds->phases[i].phase_length = 3; // phases set up
    ds->phases[i].phase_content = (int *)calloc(ds->phases[i].phase_length, sizeof(int));
    assert(ds->phases[i].phase_content != NULL);

    for(j=0; j<ds->phases[i].phase_length; j++){
      ds->phases[i].phase_content[j] = 3*j + 1;
    }
  }

  return;
}

void init_exppars(handler_owner *client_data){
  assert(client_data != NULL);
  assert(client_data->epars != NULL);

  client_data->epars->S           = 0.58;          //swirl number
  client_data->epars->P           = 1;             //pressure (atm)
  client_data->epars->T           = 300;           //temperature (K)
  client_data->epars->U           = 30;            //velocity (m/s)
  //  client_data->epars->fuel        = '50-50 H2-CO'; //fuel composition ('%F1-%F2 F1-F2') 
  client_data->epars->phi         = 0.55;          //equivalence ratio (use 0 for nonreacting)
  client_data->epars->SL0         = 34;            //unstretched laminar flame speed (cm/s)
  client_data->epars->expangle       = 0;          // % expangle (deg)
  client_data->epars->ednum       = 20121030;      //experiment date as an integer value (yyyymmdd)
  client_data->epars->exptime     = 112150;        //experiment time
  //  client_data->epars->vc7Folder   = 'PIV_PreProc_MP(32x32_50%ov_ImgCorr)_PostProc';
  //  client_data->epars->mvc7Folder  = 'PIV_PreProc_MP(32x32_50%ov_ImgCorr)_PostProc'; 
  //  client_data->epars->yl          = [0.4 1.5];     //y limits (nondimensional: y/d)
  client_data->epars->bs          = 32;            //interrogation window binsize in pixels
  client_data->epars->ol          = 0.5;           //interrogation window overlap (0.5 = 50% overlap)
  client_data->epars->N           = 1;             //numberer of images
  client_data->epars->corrected   = 1;             //1 = world coordinates, 0 = raw coordinates
  //  client_data->epars->filtSize    = [5 5];         //median filter size in pixels
  //  client_data->epars->method      = "";            //setup @inputParameters.m
  client_data->epars->frame       = 1;             // laser+camera shot 1 or shot 2             
  client_data->epars->R           = 0.66;                          
  //  client_data->epars->xl          = [-1 1]*R*0.4;
  client_data->epars->c           = 0;              
  //  client_data->epars->expdate     = "";
}

static void access_processed_data(Engine *ep, datastream *ds, char *varname, int whichmarray){
  assert(ep != NULL);
  assert(ds != NULL);
  assert(varname != NULL);

  if (strcmp(varname, "I1_binarization") == 0) return; // logical

  mxArray *mdata = NULL;
  int i, datasize = -1;
  const int *datadims = NULL;
  double *cdata = NULL;

  char buffer[BUFSIZE+1];
  buffer[BUFSIZE] = '\0';
  engOutputBuffer(ep, buffer, BUFSIZE);
  engEvalString(ep, "who;");
  printf("%s\n",buffer);

  if ((mdata = engGetVariable(ep, varname)) != NULL){
    if(!mxIsDouble(mdata)) abort();
    fprintf(stderr, "\n***********ep: %p; varname: %p, %s\n", ep, varname, varname);
    datasize = mxGetNumberOfDimensions(mdata);
    datadims = mxGetDimensions(mdata);
    cdata = mxGetPr(mdata);
    assert(cdata != NULL);
  }
  else{
    fprintf(stderr, "\n***********ep: %p; varname: %p, %s\n", ep, varname, varname);
    printf("(should not reach here!) mdata = engGetVariable returns NULL at reading %s\n", varname);
    abort();
  }
  prepare_stream(ds, whichmarray, *(datadims), *(datadims+1), cdata);

  mxDestroyArray(mdata); 
  return;

}

void init_matlab(Engine *ep, handler_owner *client_data){

  mxArray *S, *P, *T, *U, *phi, *expangle, *c, *frame, *R, *corrected; //*filtSize;
  S = mxCreateDoubleMatrix(1,1,mxREAL);
  P = mxCreateDoubleMatrix(1,1,mxREAL);
  T = mxCreateDoubleMatrix(1,1,mxREAL);
  U = mxCreateDoubleMatrix(1,1,mxREAL);
  phi = mxCreateDoubleMatrix(1,1,mxREAL);
  expangle = mxCreateDoubleMatrix(1,1,mxREAL);
  c = mxCreateDoubleMatrix(1,1,mxREAL);
  frame = mxCreateDoubleMatrix(1,1,mxREAL);
  R = mxCreateDoubleMatrix(1,1,mxREAL);
  corrected = mxCreateDoubleMatrix(1,1,mxREAL);
  assert( (S != NULL) & (P != NULL) & (T != NULL) & (U != NULL) & (phi != NULL) &(expangle != NULL) & (c != NULL) & (frame != NULL) & (R != NULL) & (corrected != NULL) );
  //filtSize = mxCreateDoubleMatrix(1,1,mxREAL);
  memcpy((void *)mxGetPr(S), (void *)(&(client_data->epars->S)), sizeof(client_data->epars->S));
  memcpy((void *)mxGetPr(P), (void *)(&(client_data->epars->P)), sizeof(client_data->epars->P));
  memcpy((void *)mxGetPr(T), (void *)(&(client_data->epars->T)), sizeof(client_data->epars->T));
  memcpy((void *)mxGetPr(U), (void *)(&(client_data->epars->U)), sizeof(client_data->epars->U));
  memcpy((void *)mxGetPr(phi), (void *)(&(client_data->epars->phi)), sizeof(client_data->epars->phi));
  memcpy((void *)mxGetPr(expangle), (void *)(&(client_data->epars->expangle)), sizeof(client_data->epars->expangle));
  memcpy((void *)mxGetPr(c), (void *)(&(client_data->epars->c)), sizeof(client_data->epars->c));
  memcpy((void *)mxGetPr(frame), (void *)(&(client_data->epars->frame)), sizeof(client_data->epars->frame));
  memcpy((void *)mxGetPr(R), (void *)(&(client_data->epars->R)), sizeof(client_data->epars->R));
  memcpy((void *)mxGetPr(corrected), (void *)(&(client_data->epars->corrected)), sizeof(client_data->epars->corrected));
  //memcpy((void *)mxGetPr(filtSize), (void *)(&(client_data->epars->filtSize)), sizeof(client_data->epars->filtSize));

  int ret;
  ret = engPutVariable(ep, "S", S);
  assert(ret == 0);
  ret = engPutVariable(ep, "P", P);
  assert(ret == 0);
  ret = engPutVariable(ep, "T", T);
  assert(ret == 0);
  ret = engPutVariable(ep, "U", U);
  assert(ret == 0);
  ret = engPutVariable(ep, "phi", phi);
  assert(ret == 0);
  ret = engPutVariable(ep, "expangle", expangle);
  assert(ret == 0);
  ret = engPutVariable(ep, "c", c);
  assert(ret == 0);
  ret = engPutVariable(ep, "frame", frame);
  assert(ret == 0);
  ret = engPutVariable(ep, "R", R);
  assert(ret == 0);
  ret = engPutVariable(ep, "corrected", corrected);
  assert(ret == 0);
  //engPutVariable(ep, "filtSize", filtSize);
  
  mxDestroyArray(S);
  mxDestroyArray(P);
  mxDestroyArray(T);
  mxDestroyArray(U);
  mxDestroyArray(phi);
  mxDestroyArray(expangle);
  mxDestroyArray(c);
  mxDestroyArray(frame);
  mxDestroyArray(R);
  mxDestroyArray(corrected);

  return;

}

// dummy functions for debugging
static void access_processed_data_dummy(Engine *ep, datastream *ds, char *varname, int whichmarray){
  assert(ep != NULL);
  assert(ds != NULL);
  assert(varname != NULL);
  printf("Acessing var: %s\n", varname);

  mxArray *mdata = NULL;
  int i, datasize = -1;
  const int *datadims = NULL;
  double *cdata = NULL;

  char buffer[BUFSIZE+1];
  buffer[BUFSIZE] = '\0';
  engOutputBuffer(ep, buffer, BUFSIZE);
  engEvalString(ep, "who;");
  printf("%s\n",buffer);

  prepare_stream_dummy(ds, whichmarray);

  return;
}

static void prepare_stream_dummy(datastream *ds, int whicharray){
  assert((whicharray >= 0) && (whicharray < MAX_MFIELD));
  assert((ds->mdatas[whicharray].M == 0) && (ds->mdatas[whicharray].N == 0) && 
        (ds->mdatas[whicharray].tsize == 0) && (ds->mdatas[whicharray].data == NULL));

  int i;
  ds->mdatas[whicharray].M = 1;
  ds->mdatas[whicharray].N = 1;
  ds->mdatas[whicharray].tsize = 1;
  ds->mdatas[whicharray].data = (double *)calloc(ds->mdatas[whicharray].tsize, sizeof(double));
  assert( ds->mdatas[whicharray].data != NULL );
  for(i=0; i<1; i++){
    ds->mdatas[whicharray].data[i] = -33333.0;
  }

  return;
}

static void _binarize_image_dummy(Engine *ep, datastream *ds, handler_owner *client_data, int groupID){

  printf("@_binarize_image on image#%d.\n", groupID);

  char buffer[BUFSIZE+1];
  buffer[BUFSIZE] = '\0';
  engOutputBuffer(ep, buffer, BUFSIZE);
  double *cdata = NULL;
  int i, j, datasize = -1;
  const int *datadims = NULL;

  /*
  for(i=0; i<MAX_MFIELD; i++){
    for(j=0; j<ds->mdatas[i].tsize; j++){
      ds->mdatas[i].data[j] += 1;
    }
  }
  */

  access_processed_data_dummy(ep, ds, "BWs_binarization", BWS1);
  access_processed_data_dummy(ep, ds, "I1_binarization", I1);

#if 0
  /* matlab behavior */
  mxArray *X = NULL, *Y = NULL, *Im = NULL;
  int M, N, ret;
  M = ds->mdatas[XPOS].M;
  N = ds->mdatas[XPOS].N;
  X = mxCreateDoubleMatrix(M,N,mxREAL); 
  assert(X != NULL);
  M = ds->mdatas[YPOS].M;
  N = ds->mdatas[YPOS].N;
  Y = mxCreateDoubleMatrix(M,N,mxREAL); 
  assert(Y != NULL);
  M = ds->mdatas[IM].M;
  N = ds->mdatas[IM].N;
  Im = mxCreateDoubleMatrix(M,N,mxREAL); 
  assert(Im != NULL);
  memcpy((void *)mxGetPr(X), (void *)(ds->mdatas[XPOS].data), sizeof(ds->mdatas[XPOS].data[0])*ds->mdatas[XPOS].tsize);
  memcpy((void *)mxGetPr(Y), (void *)(ds->mdatas[YPOS].data), sizeof(ds->mdatas[YPOS].data[0])*ds->mdatas[YPOS].tsize);
  memcpy((void *)mxGetPr(Im), (void *)(ds->mdatas[IM].data), sizeof(ds->mdatas[IM].data[0])*ds->mdatas[IM].tsize);

  ret = engPutVariable(ep, "X_binarization", X);
  assert(ret == 0); // 0 if successful
  ret = engPutVariable(ep, "Y_binarization", Y);
  assert(ret == 0);
  ret = engPutVariable(ep, "Im_binarization", Im);
  assert(ret == 0);
  init_matlab(ep, client_data);

  TIMER_DECLARE1(binarize_timer);
  uint64_t binarize_clk = 0;
  engEvalString(ep, "filtSize = [5 5];");
  TIMER_START(binarize_timer);
  engEvalString(ep, "[BWs_binarization, I1_binarization] = binarizeImages(X_binarization, Y_binarization, Im_binarization, 1, S, P, T, U, phi, expangle, R, c, frame, 'corrected', corrected, 'filtSize', filtSize);");
  TIMER_END(binarize_timer, binarize_clk);

  printf("%s\n",buffer);
  char msg[100];
//  sprintf(msg, "%d:%llu\n", groupID, binarize_clk);
  write(fp_p1_time, msg, strlen(msg));

  access_processed_data(ep, ds, "BWs_binarization", BWS1);
  access_processed_data(ep, ds, "I1_binarization", I1);

  //TODO: what to do with the overwritten "data" & datastream from evpath?
  //The engine application owns the original mxArray and is responsible for freeing its memory. Although the engPutVariable function sends a copy of the mxArray to the MATLAB workspace, the engine application does not need to account for or free memory for the copy. - MATLAB
  mxDestroyArray(X); 
  mxDestroyArray(Y);
  mxDestroyArray(Im);

  // clear removes all variables from the current workspace, releasing them from system memory.
  engEvalString(ep, "clear"); 
  printf("%s\n", buffer);
#endif

  return;
}

/*
 * PART II
 *
 * For the second half of this demonstration, we will request
 * a MATLAB string, which should define a variable X.  MATLAB
 * will evaluate the string and create the variable.  We
 * will then recover the variable, and determine its type.
 */
static void _extract_flameedge_dummy(Engine *ep, datastream *ds, int groupID){

  printf("@_extract_flameedge on image %d.\n", groupID);
  /*
   * Use engOutputBuffer to capture MATLAB output, so we can
   * echo it back.  Ensure first that the buffer is always NULL
   * terminated.
   */
  char buffer[BUFSIZE+1];
  buffer[BUFSIZE] = '\0';
  engOutputBuffer(ep, buffer, BUFSIZE);
  double *cdata = NULL;
  int i,j, datasize = -1;
  const int *datadims = NULL;

  /*
  for(i=0; i<MAX_MFIELD; i++){
    for(j=0; j<ds->mdatas[i].tsize; j++){
      ds->mdatas[i].data[j] += 1;
    }
  }
  */

  access_processed_data_dummy(ep, ds, "Emat_flameedge", EDGE);

#if 0
  /* matlab behavior */
  mxArray *X, *Y, *BWs;
  int M, N, ret;
  M = ds->mdatas[XPOS].M;
  N = ds->mdatas[XPOS].N;
  X = mxCreateDoubleMatrix(M,N,mxREAL); 
  assert(X != NULL);
  M = ds->mdatas[YPOS].M;
  N = ds->mdatas[YPOS].N;
  Y = mxCreateDoubleMatrix(M,N,mxREAL); 
  assert(Y != NULL);
  M = ds->mdatas[BWS1].M;
  N = ds->mdatas[BWS1].N;
  BWs = mxCreateDoubleMatrix(M,N,mxREAL);
  assert(BWs != NULL);
  memcpy((void *)mxGetPr(X), (void *)(ds->mdatas[XPOS].data), sizeof(ds->mdatas[XPOS].data[0])*ds->mdatas[XPOS].tsize);
  memcpy((void *)mxGetPr(Y), (void *)(ds->mdatas[YPOS].data), sizeof(ds->mdatas[YPOS].data[0])*ds->mdatas[YPOS].tsize);
  memcpy((void *)mxGetPr(BWs), (void *)(ds->mdatas[BWS1].data), sizeof(ds->mdatas[BWS1].data[0])*ds->mdatas[BWS1].tsize);

  ret = engPutVariable(ep, "X_flameedge", X);
  assert(ret == 0);
  ret = engPutVariable(ep, "Y_flameedge", Y);
  assert(ret == 0);
  ret = engPutVariable(ep, "BWs_flameedge", BWs);
  assert(ret == 0);

  TIMER_DECLARE1(flame_timer);
  uint64_t flame_clk = 0;
  TIMER_START(flame_timer);
  engEvalString(ep, "[Ecell_flameedge,Esize_flameedge,Emat_flameedge] = flameEdges(BWs_flameedge, X_flameedge, Y_flameedge);");
  TIMER_END(flame_timer, flame_clk);

  printf("%s\n", buffer);
  char msg[100];
//  sprintf(msg, "%d:%llu\n", groupID, flame_clk);
  write(fp_p2_time, msg, strlen(msg));

  //printf("%s\n", buffer);
  access_processed_data(ep, ds, "Emat_flameedge", EDGE);

  mxDestroyArray(BWs);
  mxDestroyArray(X);
  mxDestroyArray(Y);

  // clear removes all variables from the current workspace, releasing them from system memory.
  engEvalString(ep, "clear"); 
  printf("%s\n", buffer);
#endif

  return;
}

/*
 * PART III
 *
 * For the second half of this demonstration, we will request
 * a MATLAB string, which should define a variable X.  MATLAB
 * will evaluate the string and create the variable.  We
 * will then recover the variable, and determine its type.
 */
static void _compute_curvature_dummy(Engine *ep, datastream *ds, int groupID){

  printf("\n@_compute_curvature on image#%d.\n", groupID);

  char buffer[BUFSIZE+1];
  buffer[BUFSIZE] = '\0';
  engOutputBuffer(ep, buffer, BUFSIZE);
  double *cdata = NULL;
  int i,j, datasize = -1;
  const int *datadims = NULL;

  /*
  for(i=0; i<MAX_MFIELD; i++){
    for(j=0; j<ds->mdatas[i].tsize; j++){
      ds->mdatas[i].data[j] += 1;
    }
  }
  */

  access_processed_data_dummy(ep, ds, "mx", XFIT);
  access_processed_data_dummy(ep, ds, "my", YFIT);
  access_processed_data_dummy(ep, ds, "mc", CFIT);
  access_processed_data_dummy(ep, ds, "ms", SFIT);
  access_processed_data_dummy(ep, ds, "mkappa", KAPPA);

#if 0
  /* matlab behavior */
  mxArray *X = NULL, *Y = NULL, *BWmeanC = NULL, *Emat = NULL; //*E=NULL;
  int M, N, ret;
  M = ds->mdatas[XPOS].M;
  N = ds->mdatas[XPOS].N;
  X = mxCreateDoubleMatrix(M,N,mxREAL); 
  assert(X != NULL);
  printf("X addr: %p\n", X);
  M = ds->mdatas[YPOS].M;
  N = ds->mdatas[YPOS].N;
  Y = mxCreateDoubleMatrix(M,N,mxREAL); 
  assert(Y != NULL);
  printf("Y addr: %p\n", Y);
  M = ds->mdatas[BWMEANC].M;
  N = ds->mdatas[BWMEANC].N;
  BWmeanC = mxCreateDoubleMatrix(M,N,mxREAL); 
  assert(BWmeanC != NULL);
  printf("BWmeanC addr: %p\n", BWmeanC);
  M = ds->mdatas[EDGE].M;
  N = ds->mdatas[EDGE].N;
  Emat = mxCreateDoubleMatrix(M,N,mxREAL); 
  assert(Emat != NULL);
  printf("Emat addr: %p\n", Emat);

  memcpy((void *)mxGetPr(X), (void *)(ds->mdatas[XPOS].data), sizeof(ds->mdatas[XPOS].data[0])*ds->mdatas[XPOS].tsize);
  memcpy((void *)mxGetPr(Y), (void *)(ds->mdatas[YPOS].data), sizeof(ds->mdatas[YPOS].data[0])*ds->mdatas[YPOS].tsize);
  memcpy((void *)mxGetPr(BWmeanC), (void *)(ds->mdatas[BWMEANC].data), sizeof(ds->mdatas[BWMEANC].data[0])*ds->mdatas[BWMEANC].tsize);
  memcpy((void *)mxGetPr(Emat), (void *)(ds->mdatas[EDGE].data), sizeof(ds->mdatas[EDGE].data[0])*ds->mdatas[EDGE].tsize);

  // for debugging
  int i, datasize = -1;
  const int *datadims = NULL;
  double *cdata = NULL;

  datasize = mxGetNumberOfDimensions(X);
  datadims = mxGetDimensions(X);
  cdata = mxGetPr(X);
  assert(cdata != NULL);
  for(i=0; i<((*datadims) * (*(datadims+1))); i++){
    assert(ds->mdatas[XPOS].data[i] == cdata[i]);
  }

  datasize = mxGetNumberOfDimensions(Y);
  datadims = mxGetDimensions(Y);
  cdata = mxGetPr(Y);
  assert(cdata != NULL);
  for(i=0; i<((*datadims) * (*(datadims+1))); i++){
    assert(ds->mdatas[YPOS].data[i] == cdata[i]);
  }
  
  datasize = mxGetNumberOfDimensions(BWmeanC);
  datadims = mxGetDimensions(BWmeanC);
  cdata = mxGetPr(BWmeanC);
  assert(cdata != NULL);
  for(i=0; i<((*datadims) * (*(datadims+1))); i++){
    assert(ds->mdatas[BWMEANC].data[i] == cdata[i]);
  }

  datasize = mxGetNumberOfDimensions(Emat);
  datadims = mxGetDimensions(Emat);
  cdata = mxGetPr(Emat);
  assert(cdata != NULL);
  for(i=0; i<((*datadims) * (*(datadims+1))); i++){
    assert(ds->mdatas[EDGE].data[i] == cdata[i]);
  }
  
  engEvalString(ep, "who;");
  printf("%s\n",buffer);

  ret = engPutVariable(ep, "X_curvature", X);
  assert(ret == 0);
  ret = engPutVariable(ep, "Y_curvature", Y);
  assert(ret == 0);
  ret = engPutVariable(ep, "BWmeanC_curvature", BWmeanC);
  assert(ret == 0);
  ret = engPutVariable(ep, "Emat_curvature", Emat);
  assert(ret == 0);

  engEvalString(ep, "who;");
  printf("%s\n",buffer);

  TIMER_DECLARE1(curvature_timer);
  uint64_t curvature_clk = 0;
  TIMER_START(curvature_timer);
  //engEvalString(ep, "[xDS, yDS, cDS, sDS, mx, my, mc, xp, yp, xpp, ypp, ms, mkappa, R, nx, ny] = Curvature(X_curvature, Y_curvature, BWmeanC_curvature, Emat_curvature);");
  TIMER_END(curvature_timer, curvature_clk);

  printf("%s\n", buffer);
  //printf("gid:processingtime>>>%d:%llu\n", groupID, curvature_clk);
  printf("gid:processingtime>>>%d:%" PRIu64 "\n", groupID, curvature_clk);
  char msg[100];
  //sprintf(msg, "%d:%llu\n", groupID, curvature_clk);
  sprintf(msg, "%d:%" PRIu64 "\n", groupID, curvature_clk);
  write(fp_p3_time, msg, strlen(msg));

  access_processed_data_dummy(ep, ds, "mx", XFIT);
  access_processed_data_dummy(ep, ds, "my", YFIT);
  access_processed_data_dummy(ep, ds, "mc", CFIT);
  access_processed_data_dummy(ep, ds, "ms", SFIT);
  access_processed_data_dummy(ep, ds, "mkappa", KAPPA);

  mxDestroyArray(X);
  mxDestroyArray(Y);
  mxDestroyArray(BWmeanC);
  mxDestroyArray(Emat);

  // clear removes all variables from the current workspace, releasing them from system memory.
  engEvalString(ep, "clear;"); 
  printf("%s\n", buffer);
#endif

  return;
}

// for debugging purpose
void complete_workflow(struct JobsToProcess *j2p, handler_owner *gdata)
{
  assert(j2p && gdata);

  printf("%d jobs to process @stage1\n", j2p->count);

  int i;
  struct list_head *jhead = NULL;
  struct JItem *item = NULL;
  for(i=0; i<j2p->count; i++){
    printf("processing JOB #%d\n", i);

    jhead  = j2p->jobs[i];
    list_for_each_entry(item, jhead, link){
      if(item->ds == NULL){
        printf("JOB#%d has %d items\n", item->gid, item->items);
        assert(item->is_ready);
        continue;
      }

      _binarize_image(gdata->ep, (datastream *)item->ds, gdata, item->gid);
      //_binarize_image_dummy(gdata->ep, (datastream *)item->ds, gdata, item->gid);

      _extract_flameedge(gdata->ep, (datastream *)item->ds, item->gid);
      _compute_curvature(gdata->ep, (datastream *)item->ds, item->gid);

      printf("##rank#%d-completed p#1,2,3 on gid %d\n", gdata->rank, item->gid);
      ((datastream *)item->ds)->workflow_nextstep[0] += 3;
      ((datastream *)item->ds)->groupsize = 1; // no dependency for nextP 
    }

  }

  return;
}

void fake_init_stream(datastream *ds, handler_owner *client_data){
  //ds->contact_string = strdup(client_data->src_contactstr);
  //assert(ds->contact_string);
  //ds->src_stone = client_data->src_stone;

  ds->contact_string = strdup("abcbdd");
  assert(ds->contact_string);
  ds->src_stone = 100;
  ds->streamID = client_data->rank;

  ds->srcname = (char *)calloc(15, sizeof(char));
  assert(ds->srcname);
  ds->srcID = 0;

  ds->datasize = 2681; // in unit of byte
  ds->groupsize = 1; // each group has only one stream object
  ds->groupID = 0;

  int i, j;
  for(i=0; i<MAX_MFIELD; i++){
    ds->mdatas[i].M = 0;
    ds->mdatas[i].N = 0;
    ds->mdatas[i].tsize = 0;

    ds->mdatas[i].data = NULL;
  }

  /*
  for(i=0; i<MAX_MFIELD; i++){
    ds->mdatas[i].M = 300;
    ds->mdatas[i].N = 3;
    ds->mdatas[i].tsize = 900;

    ds->mdatas[i].data = (double *)calloc(ds->mdatas[i].tsize, sizeof(double));
    assert(ds->mdatas[i].data != NULL);
    for(j=0; j<ds->mdatas[i].tsize; j++){
      ds->mdatas[i].data[j] = 0.0001 * j;
    }
  }
  */
  
  ds->workflow_number = 1;
  ds->workflow_nextstep = (int *)calloc(ds->workflow_number, sizeof(int));
  assert(ds->workflow_nextstep != NULL);
  ds->workflow_nextstep[0] = 0;

  ds->workflows = (workflow_t *) calloc(ds->workflow_number, sizeof(workflow_t));
  ds->phases = (phase_t *) calloc(ds->workflow_number, sizeof(phase_t));
  assert(ds->workflows != NULL);
  assert(ds->phases != NULL);
  for(i=0; i<ds->workflow_number; i++){
    ds->workflows[i].workflow_length = 3; // workflows set up
    ds->workflows[i].workflow_content = (int *)calloc(ds->workflows[i].workflow_length, sizeof(int));
    assert(ds->workflows[i].workflow_content != NULL);

    for(j=0; j<ds->workflows[i].workflow_length; j++){
      ds->workflows[i].workflow_content[j] = 2*j+1;
    }

    ds->phases[i].phase_length = 3; // phases set up
    ds->phases[i].phase_content = (int *)calloc(ds->phases[i].phase_length, sizeof(int));
    assert(ds->phases[i].phase_content != NULL);

    for(j=0; j<ds->phases[i].phase_length; j++){
      ds->phases[i].phase_content[j] = 3*j + 1;
    }
  }

  return;
}
