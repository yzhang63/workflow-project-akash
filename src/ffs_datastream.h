#ifndef FFS_DATASTREAM_H
#define FFS_DATASTREAM_H

#include <stdlib.h>
#include <stdio.h>
#ifdef PLUGMPI
#include <mpi.h>
#endif
#include <pthread.h>

#include "evpath.h"
#include "comm_vars.h"

#include "engine.h"
#include "list.h"

#include <sys/socket.h>
#include <netinet/in.h>

typedef struct _toexec_ctl_msg{
  int groupID[GNUM];
  int howmany_toexec[GNUM];
  int output_target;

  int srcID;
  int srcrank;

}toexec_ctl_msg, *toexec_ctl_msg_ptr;

typedef struct _workflow_t{
  int workflow_length;
  int *workflow_content;
}workflow_t, *workflow_t_ptr;

typedef struct _phase_t{
  int phase_length; // == workflow_length
  int *phase_content;
}phase_t, *phase_t_ptr;

typedef struct _contactsready_msg{
  int ready; 
  int src;
}contactsready_msg, *contactsready_msg_ptr;

/* data streams embedded with workflow knowledge
 * "workflow_number": workflows along with this datastream 
 * "workflow_nextstep": workflow execution status for each workflow 
 * "phases": contains the idxes for the cod codes for each workflow (cod code fields)
 * "workflows": contains the idxes for the phases for each workflow (workflow fields)
 */
typedef struct _marray{
  int M;
  int N;
  int tsize; // M*N
  //size_t data_sz; // bytes
  double *data;

}marray, *marray_ptr;

typedef struct _datastream {
  int streamID; // data_generator rankID

  char *srcname; // imagename, imageID
  int srcID;

  // metadata: size of per datastream
  long datasize;

  // derived from workflow knowledge
  int groupsize;
  int groupID;

  //int num_outs; // number of outputs from this current phase
  //marray **mdatas;
  marray mdatas[MAX_MFIELD];

  int workflow_number;
  int *workflow_nextstep;
  workflow_t *workflows;
  phase_t *phases;

  long self_size;
  
  int src_stone;
  char *contact_string;

}datastream, *datastream_ptr;

typedef struct _datastream_array_t{
  int groupID;
  int size;
  datastream **ds;
}datastream_array_t, *datastream_array_t_ptr;

typedef struct _groupsize_info_t{
  int size;
  int *groups;
}groupsize_info_t, *groupsize_info_t_ptr;

typedef struct _remote_contact_t{
  EVstone remote_stone; // contactee stone ID
  char *string_list;

}remote_contact_t;

typedef struct _mstone_outputs_t{
  int to_contactready;
  int to_adamsg;

  // for all mstones: the same 
  int to_taskready;
  int to_endsite;
  
  // different between the last mstone and others
  int to_nextphase;
  int to_nextsite;

  // to be only used at ENDSITE
  int to_finaloutput; 

  int num_mstones;

}mstone_outputs_t, *mstone_outputs_t_ptr;

typedef struct _adamsg{
  int resourceID;
  int rankID;
  int type;
  long mm_cap; 
  int cpu_cap;
}adamsg, *adamsg_ptr;

typedef struct _exppars{
  double S;
  double P;        
  double T;         
  double U;         
//double fuel;      
  double phi;       
  double SL0;       
  double expangle;  
  double ednum;     
  double exptime;   
//double vc7Folder; 
//double mvc7Folder;
//double   yl;        
  double bs;        
  double ol;        
  double N;         
  double corrected; 
//double filtSize;  
//double   method;
  double frame;
  double R;
//double   xl;
  double c;
//double   expdate;

}exppars, exppars_ptr;

/* to pass extra info with data streams, e.g., as a client data in evpath
 */
typedef struct _handler_owner{
  // matlab pars 
  exppars *epars;
  Engine *ep;

#ifdef PLUGMPI
  MPI_Comm comm;
#endif

  // self id
  CManager cm;
  int itself; //clusterid
  int rank;
  int size;
  int map_size;
  int stages_count;

  // self evpath contact info
  EVstone src_stone;
  char *src_contactstr; 
  // the unit this unit mapped to
  int mappedto_cluster;
  int mappedto_rank;

  // notification action in case a new system party joins
  EVstone contactorready_stone;
  
  // to move active objects in a completed group to echo-space queue, 
  // from evpath mqueue
  EVstone *taskready_stones;
   
  // including the end site
  int num_resources; 
  int resource_mpisizes[NUM_RESOURCES]; 
  remote_contact_t downstream_contacts[NUM_RESOURCES][NUM_PROCESSES];

  // direct path to endsite 
  EVstone toendsite_stone; 
  EVsource toendsite_src; 
  // the pre-defined (semi-static) mapping: one-direction conn
  EVstone to_nextsite; 

  int num_multiactions;
  EVstone *multiq_stones;
  EVaction *multiq_actions;
  // mstone output for processing incoming adaptive cap info, one per mstone
  // but only one is actually used due to only the first mstone's contact is exposed
  EVstone *adamsg_stones;
  // the adaptive mapping bridge
  EVsource adaptive_source;

  // to record all the potential output path
  mstone_outputs_t *mstone_outputs;

  // per one for each mstone
  // these split stones are shared 
  EVsource *to_localmstones;

  // output result to local resource station 
  EVstone to_finaloutput;

#if 0
  // used with stream_arrival_handler
  EVstone *groupstat_stones;
  EVaction *groupstat_actions;
#endif

  // to store paths for broadcasting idle_message&ackinfo 
  EVsource broadcast_srcs[NUM_RESOURCES][NUM_PROCESSES];
  
  // to build path to upstreams (for future use)
  EVsource acktopeers_srcs[NUM_RESOURCES][NUM_PROCESSES]; 
  int num_dgprocs;  
  remote_contact_t dgprocs_contacts[NUM_DG];
  EVsource acktodgprocs_srcs[NUM_DG];

  // the number of total expected report size
  int reports;

}handler_owner;

typedef struct _monitor_info_t{
  int groupID;
  int groupIdx;
  int groupSize;
  int mstoneID;

}monitor_info_t, *monitor_info_t_ptr;

typedef struct _yesno_info_t{
  int to_continue;
}yesno_info_t, *yesno_info_ptr;

static FMField yesno_info_fields[] = {
  {"to_continue", "integer", sizeof(int), FMOffset(yesno_info_ptr, to_continue)},
  {NULL, NULL, 0, 0}
};

static FMField monitor_info_fields[] = {
  {"groupID", "integer", sizeof(int), FMOffset(monitor_info_t_ptr, groupID)},
  {"groupIdx", "integer", sizeof(int), FMOffset(monitor_info_t_ptr, groupIdx)},
  {"groupSize", "integer", sizeof(int), FMOffset(monitor_info_t_ptr, groupSize)},
  {"mstoneID", "integer", sizeof(int), FMOffset(monitor_info_t_ptr, mstoneID)},
  {NULL, NULL, 0, 0}
};

static FMField toexec_ctl_msg_fields[] = {
  {"groupID", "integer[" GNUM_STR "]", sizeof(int), FMOffset(toexec_ctl_msg_ptr, groupID)},
  {"howmany_toexec", "integer[" GNUM_STR "]", sizeof(int), FMOffset(toexec_ctl_msg_ptr, howmany_toexec)},
  {"output_target", "integer", sizeof(int), FMOffset(toexec_ctl_msg_ptr, output_target)},
  {"srcID", "integer", sizeof(int), FMOffset(toexec_ctl_msg_ptr, srcID)},
  {"srcrank", "integer", sizeof(int), FMOffset(toexec_ctl_msg_ptr, srcrank)},
  {NULL, NULL, 0, 0}
};

static FMField workflow_t_fields[] = {
  {"workflow_length", "integer", sizeof(int), FMOffset(workflow_t_ptr, workflow_length)}, 
  {"workflow_content", "integer[workflow_length]", sizeof(int), FMOffset(workflow_t_ptr, workflow_content)}, 
  {NULL, NULL, 0, 0}
};

static FMField phase_t_fields[] = {
  {"phase_length", "integer", sizeof(int), FMOffset(phase_t_ptr, phase_length)}, 
  {"phase_content", "integer[phase_length]", sizeof(int), FMOffset(phase_t_ptr, phase_content)}, 
  {NULL, NULL, 0, 0}
};

static FMField contactsready_msg_fields[] = {
  {"ready", "integer", sizeof(int), FMOffset(contactsready_msg_ptr, ready)},   
  {"src", "integer", sizeof(int), FMOffset(contactsready_msg_ptr, src)},   
  {NULL, NULL, 0, 0}

};
static FMField datastream_fields[] = {
  {"streamID", "integer", sizeof(int), FMOffset(datastream_ptr, streamID) },
  {"srcname", "string", sizeof(char *), FMOffset(datastream_ptr, srcname)},
  {"srcID", "integer", sizeof(int), FMOffset(datastream_ptr, srcID) },
  
  {"datasize", "integer", sizeof(int), FMOffset(datastream_ptr, datasize) },

  {"groupsize", "integer", sizeof(int), FMOffset(datastream_ptr, groupsize) },
  {"groupID", "integer", sizeof(int), FMOffset(datastream_ptr, groupID) },

  {"mdatas", "marray[" MAX_MFIELD_STR "]", sizeof(marray), FMOffset(datastream_ptr, mdatas)},
  //{"num_outs", "integer", sizeof(int), FMOffset(datastream_ptr, num_outs) },
  //{"mdatas", "(*marray)[num_outs]", sizeof(marray), FMOffset(datastream_ptr, mdatas)},

  {"workflow_number", "integer", sizeof(int), FMOffset(datastream_ptr, workflow_number)},
  {"workflow_nextstep", "integer[workflow_number]", sizeof(int), FMOffset(datastream_ptr, workflow_nextstep)},
  {"workflows", "workflow_t[workflow_number]", sizeof(workflow_t), FMOffset(datastream_ptr, workflows)},
  {"phases", "phase_t[workflow_number]", sizeof(phase_t), FMOffset(datastream_ptr, phases)},

  {"self_size", "integer", sizeof(long), FMOffset(datastream_ptr, self_size)},
  
  {"src_stone", "integer", sizeof(int), FMOffset(datastream_ptr, src_stone)},
  {"contact_string", "string", sizeof(char *), FMOffset(datastream_ptr, contact_string)},

  {NULL, NULL, 0, 0}
};

static FMField marray_fields[] = {
  {"M", "integer", sizeof(int), FMOffset(marray_ptr, M)},
  {"N", "integer", sizeof(int), FMOffset(marray_ptr, N)},
  {"tsize", "integer", sizeof(int), FMOffset(marray_ptr, tsize)},
  {"data", "float[tsize]", sizeof(double), FMOffset(marray_ptr, data)},
  {NULL, NULL, 0, 0}
};

static FMStructDescRec datastream_format_list[] = {
  {"datastream", datastream_fields, sizeof(datastream), NULL},
  {"workflow_t", workflow_t_fields, sizeof(workflow_t), NULL},
  {"phase_t", phase_t_fields, sizeof(phase_t), NULL},
  {"marray", marray_fields, sizeof(marray), NULL},
  {NULL, NULL, 0, NULL}
};

static FMField mstone_outputs_t_fields[] = {
  {"to_contactready", "integer", sizeof(int), FMOffset(mstone_outputs_t_ptr, to_contactready)}, 
  {"to_nextphase", "integer", sizeof(int), FMOffset(mstone_outputs_t_ptr, to_nextphase)}, 
  {"to_endsite", "integer", sizeof(int), FMOffset(mstone_outputs_t_ptr, to_endsite)}, 
  {"to_nextsite", "integer", sizeof(int), FMOffset(mstone_outputs_t_ptr, to_nextsite)}, 
  //{"to_datasizestone", "integer", sizeof(int), FMOffset(mstone_outputs_t_ptr, to_datasizestone)}, 
  {"to_taskready", "integer", sizeof(int), FMOffset(mstone_outputs_t_ptr, to_taskready)}, 
  {"to_adamsg", "integer", sizeof(int), FMOffset(mstone_outputs_t_ptr, to_adamsg)}, 
  {"to_finaloutput", "integer", sizeof(int), FMOffset(mstone_outputs_t_ptr, to_finaloutput)}, 
  {"num_mstones", "integer", sizeof(int), FMOffset(mstone_outputs_t_ptr, num_mstones)}, 
  {NULL, NULL, 0, 0}
};

static FMField adamsg_fields[] = {
  {"resourceID", "integer", sizeof(int), FMOffset(adamsg_ptr, resourceID)},
  {"rankID", "integer", sizeof(int), FMOffset(adamsg_ptr, rankID)},
  {"type", "integer", sizeof(int), FMOffset(adamsg_ptr, type)},
  {"mm_cap", "integer", sizeof(long), FMOffset(adamsg_ptr, mm_cap)},
  {"cpu_cap", "integer", sizeof(long), FMOffset(adamsg_ptr, cpu_cap)},
  {NULL, NULL, 0, 0}
};

static FMField groupsize_info_t_fields[] = {
  {"size", "integer", sizeof(int), FMOffset(groupsize_info_t_ptr, size)},
  {"groups", "integer[size]", sizeof(int), FMOffset(groupsize_info_t_ptr, groups)},
  {NULL, NULL, 0, 0}
};

static FMStructDescRec adamsg_format_list[] = {
  {"adamsg", adamsg_fields, sizeof(adamsg), NULL},
  {NULL, NULL, 0, NULL}
};

static FMStructDescRec toexec_ctl_msg_format_list[] = {
  {"toexec_ctl_msg", toexec_ctl_msg_fields, sizeof(toexec_ctl_msg), NULL},
  {NULL, NULL, 0, NULL}
};

static FMStructDescRec contactsready_msg_format_list[] = {
  {"contactsready_msg", contactsready_msg_fields, sizeof(contactsready_msg), NULL}, 
  {NULL, NULL, 0, NULL}

};

static FMStructDescRec mstone_outputs_format_list[] = {
  {"mstone_outputs_t", mstone_outputs_t_fields, sizeof(mstone_outputs_t), NULL},
  {NULL, NULL, 0, NULL}
};

static FMStructDescRec groupsize_info_t_format_list[] = {	
  {"groupsize_info_t", groupsize_info_t_fields, sizeof(groupsize_info_t), NULL},	
  {NULL, NULL, 0, NULL}
};

static FMStructDescRec monitor_info_format_list[] = {
  {"monitor_info_t", monitor_info_fields, sizeof(monitor_info_t), NULL},
  {NULL, NULL, 0, NULL}
}; 

static FMField datastream_array_fields[] = {
  {"groupID", "integer", sizeof(int), FMOffset(datastream_array_t_ptr, groupID)},
  {"size", "integer", sizeof(int), FMOffset(datastream_array_t_ptr, size)},
  {"ds", "(*datastream)[size]", sizeof(struct _datastream), FMOffset(datastream_array_t_ptr, ds)},
  {NULL, NULL, 0, 0}
};

static FMStructDescRec datastream_array_format_list[] = {
  {"datastream_array_t", datastream_array_fields, sizeof(datastream_array_t), NULL},
  {"datastream", datastream_fields, sizeof(datastream), NULL},
  {"workflow_t", workflow_t_fields, sizeof(workflow_t), NULL},
  {"phase_t", phase_t_fields, sizeof(phase_t), NULL},
  {NULL, NULL, 0, NULL}
};

static FMStructDescRec yesno_info_format_list[] = {
  {"yesno_info_t", yesno_info_fields, sizeof(yesno_info_t), NULL },
  {NULL, NULL, 0, NULL}
};

static FMStructDescRec *input_format_list = datastream_format_list;
static FMStructDescRec *output_format_list = datastream_format_list;

extern handler_owner *client_data;
extern int whichnode;

typedef struct _dequeue_instr{
  int which_mstone;
  int num_jobs;

}dequeue_instr;

/***************************************************/
// dynamic and linkedlist-based queue implementation
struct JItem{
  int gid;
  int items; 
  int is_ready;
  void *cm;
  void *ds;
  struct list_head link;
};

struct Job{
  int jobs;
  struct list_head *jhead;
  struct list_head link;
};

struct Stream{
  int streamID;
  struct list_head *qhead;
  struct list_head link;
};

struct JQueues{
  pthread_mutex_t *qlocks;
  struct list_head *qheads;
};
extern struct JQueues *jqs;

struct JobsToProcess{
  int count;
  struct list_head **jobs; /* size = jobs */
};


// stream container
struct SBuffer{
  int streamID;
  struct list_head *dhead;
  struct list_head link;
};

// databuffer
struct DBuffer{ 
  void *ds;
  struct list_head link;
};

extern struct list_head *faultyhead;

//extern JobQueues **jqueues;
extern dequeue_instr *deq_instr; 
extern FILE **fp_deqslot, **fp_enqslot;
#if 0
extern int *fp_qlevel;
#endif
extern pthread_t adaptive_monitor_thread;
extern pthread_t statdat_download_thread;

extern int fp_p1_time, fp_p2_time, fp_p3_time;

extern int fp_wbuff_timer;
extern int fp_wobuff_timer;

// for logging global timestamp
extern int sockfd, n;
extern struct sockaddr_in servaddr, cliaddr;

// FD
extern int fp_final;
#endif
