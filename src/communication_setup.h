#ifndef COMMUNICATION_SETUP_H
#define COMMUNICATION_SETUP_H

#ifdef PLUGMPI
#include <mpi.h>
#endif
#include "evpath.h"
#include "ffs_datastream.h"

/*
 * to to inform the local connection info such as "stone ID" and "contact list"
 * that will be read by the upstream (the last resource station)
 *
 * @param: client_data - the global information carrier
 *
 * Note: This function is mainly invoked by the in-transit resource station, 
 *  assuming the existence of mstones.
 *  Furthermore, it assumes that each MPI rank will have only one local contact stone.
 *  However, it can be any mstone, according to the workflow execution from last resource
 *  station.
 *
 *  Therefore, the shared contact files has "num_multiactions" number of contacts per rank.
 *
 * */
#if 0
extern void 
log_localcontacts_mstones(handler_owner *client_data);
#endif

/*
 * to inform the local connection info such as "stone ID" and "contact list"
 * that will be read by the upstream (the last resource station)
 *
 * @param: client_data - the global information carrier
 *	   dshandlerPtr - the handler that is registered for the incoming datastream
 *
 * Note: This function is mainly invoked by the last resource station. 
 *  Furthermore, it assumes that each MPI rank will have only one local contact stone, 
 *  which has the action of EVassoc_terminal_action.
 *
 * */


//extern void 
//obtain_curclusters_contacts(handler_owner *gdata);

/*
 * to obtain the mstone ID and contact_strlist of each mstone of the resource station
 *
 * Note: This is mainly invoked by the control_party in order to send out 
 *  1) toexec_ctl_msg and 2) tooutput_ctl_msg
 * 
 * TODO: to extend to N group situation
 * */
#if 0
extern void 
obtain_mstone_connection_info(handler_owner *client_data);

extern void 
obtain_mstone_outputs_info(handler_owner *client_data);
#endif

/* 
 * to obtain the connection information for the next resource station
 * i.e., read through the contact_info.txt for 
 *	 1) the remote stone ID and 2) contact list 
 *	
 *	 to update 
 *	 1) client_data->remote_contacts
 *	 2) client_data->remote_contacts_size
 *
 * Note: In the current implementation, it assumes that each line of the contact_info.txt
 *  has remotestoneID:contact_strlist, which is the remote contact info per MPI group
 *
 * */
#if 0
extern void 
obtain_remote_connection_info(handler_owner *client_data);
#endif

/*
 * to init the internal map from groups (@upstream) to service mpirank (@downstream).
 * @params:
 * client_data - a information distributor
 *
 * Note that this func allocats the maximal service line and maps local rank to a remote rank.
 * In particular, there will be No. of remote contacts bridges inside per local rank
 *
 * 1) Bridge stone should be dynamically mapped to the remote multi-stones
 * according to the real-time executlation/resource sitation, i.e., workflow status
 * Or
 * 2) bridge stone is always bridged to the first mstone in the remote mapped rank,
 * the control stone is responsible for workflow status check to decide if some of the multistones
 * will be skipped
 * 2) is the current implementation since evpath requires handshake ahead of time for bridge stones
 *
 * This function needs more sophisticated mapping strategy!!!
 */
//extern void 
//connecto_curclusters(handler_owner *gdata);

extern void
obtain_localcontacts(handler_owner *gdata);

extern int
get_my_resourceID();

extern int
get_my_rankID();

extern int
get_my_laststageID();


void record_localcontacts(handler_owner *gdata, int clusterid);

void setup_finaloutput_actions(handler_owner *gdata, 
                              int (*dshandlerPtr)(CManager, void *, void *, attr_list), 
                              FMStructDescRec *msg_format_list);
#endif
