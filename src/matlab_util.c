/**
 * Helper functions for matlab
 * Wed Aug 20 22:28:02 EDT 2014
 */

/* C includes */
#include <assert.h>
#include <string.h>

/* Other packages includes */

/* Local includes */
#include "matlab_util.h"


int ml_marray2mx(marray *in, mxArray **out) {
  if (!in || !out) 
    return -1;
  if ((in->M < 1) || (in->N < 1)) 
    return -1;
  if (!in->data)
    return -1;

  *out = mxCreateDoubleMatrix(in->M, in->N, mxREAL); 
  if (!out)
    return -1;

  void *to = (void *)mxGetPr(*out);
  assert(to);
  size_t inbytes = mxGetElementSize(*out) * in->M * in->N;
  memcpy(to, (void *)in->data, inbytes);

  return 0;
}

