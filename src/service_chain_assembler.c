#define _GNU_SOURCE /* for syscall */
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>

#include <stdio.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <time.h>
#include <inttypes.h>
#include <errno.h>

#include <sys/stat.h>
#include <fcntl.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "comm_vars.h"
#include "evpath.h"
#include "service_chain_assembler.h"
#include "ffs_datastream.h"
#include "cod.h"
#include "straggler_ident.h"
#include "communication_setup.h"
#include "workflow.h"
#include "cod_funcs.h"

#define TIMING
#include "timer.h"

//#include "cloudupload.h"
//#include "cloudupload_supplement.h"

//int fp_datatimer = -1;

// the variable size of cloudaccess action
int VARSIZES[MSTONE_NUM] = {SIZES};

struct JQueues *jqs = NULL;
struct list_head *faultyhead = NULL; 

int sockfd,n;
struct sockaddr_in servaddr, cliaddr;

// the core and only computation thread (in charge of all steps)
pthread_t analysis_thread; 
pthread_t adaptive_monitor_thread;
pthread_t statdat_download_thread;

/* -begin-
 * to be used at taskready_handler, to enqueue complete groups */
#if 0
static void enqueue_this_stream(int qid, void *msg, JobQueues *jq, void *cm);
static void mark_readyjob(int qid, int jid, JobQueues *jq);

static int 
find_a_gslot(JobQueues *mqueues);
#endif

/* -end- */

/* -begin- 
 * to be used at adamsg receiver side, i.e., @adamsg_handler */
static  void
send_adamsg(adamsg *info, int to_whichrResrc, int to_whichrRank, 
    FMStructDescRec *msg_format_list);

static  void 
build_path_for_adaptive_mapping(int to_whichrResrc, int to_whichrRank, 
    FMStructDescRec *msg_format_list);
/* -end- */

/*=====================to be separate==========================================*/
/* TODO: what if more than one time calling this function?
 * i.e., more than one upstream contactor to the local resource
 * */
static int 
contactinfo_ready_handler(CManager cm, void *vevent, void *c_data, attr_list attrs);
static void 
update_system_connmaps(int msgsrcid, handler_owner *gdata);
static void 
obtain_dgprocs_contacts(handler_owner *gdata);
static void
build_ackpath_todgprocs(handler_owner *gdata);

static void
update_workflowprocs_contacts(handler_owner *gdata);
static void
build_broadcast_path(handler_owner *gdata, FMStructDescRec *msg_format_list);

/*====================for independent sys setup================================*/
/* to initialize containers for holding group arrival statistis */
static void 
init_groupstat();
/* to one-time setup expected group size 
 * (used later to check if one group is ready for analysis)
 */
static void 
setup_groupexec_instructions();
/*================================================================================*/
/*
 * to add a new multiq_stone into client_data->multiq_stones
 * Params: 1) queue_format_list the format list input of this multiq
 *	   2) phase_func the phase function description
 *	   4) client_data the global information distributor
 *
 * Note that no EVaction_set_output is enabled here.
 * And, client_data->num_multiactions will be updated by +1
 * */
static  void 
add_multiq_action(FMStructDescList *queue_format_list, 
    char *phase_func, 
    handler_owner *gdata);
static void 
init_globaldata(handler_owner *client_data, int rank, int size, CManager cm);
static void 
init_tracefile(int clusterid);
static void init_stragglermaps(int num_multistones);
/* internally called by init_globaldata 
 * */
static void setup_globaldata(handler_owner **gdata,
    int rank,
    int size, CManager cm);
/*
 * to set the control map, which is essentially a split stone
 * that will be outputed to all the existing multi-stones
 *
 */
static void 
setpath_to_localmstones(handler_owner *gdata);

/* setup for the following knowledge of the global client_data
 * setup terminal action for the following stones
 * 1. contactorready_stone
 * 2. taskready_stones (i.e., a group of streams have been ready as a task unit)
 * 3. adamsg_stones (i.e., to handle adaptation-related request)
 * 4. adamsg_ackinfo_stone (i.e., to handle the feedback info w.r.t adaptation-related request)
 * */
static void
setup_ctrlpaths(handler_owner *gdata);
/*================================================================================*/
/* to read in all the contact info (stoneID, str_list)
 * from all the resource locations & end site 
 * that are currently existing in the system
 * 
 * e.g., resource 1 can read in contact info of resource 2, resource N (End site) 
 * */
static void obtain_curclusters_contacts(handler_owner *gdata);
/* to be invoked inside in-transit resource location
 * 
 * 1) to set up the bridge to the end site
 * 2) to set up the bridge to one of the resource location
 *    For now, it will be chosen as the newly registered resource location
 *    in the system
 *    And the specific rank is RR_based.
 */
static void connecto_curclusters(handler_owner *gdata);

/*
 * to set the output of every mstone to either
 * 1) its following mstone
 * 2) bridge stone
 * 3) monitor stone
 *
 * Note: This is to build a complete map and the control stream from a third party
 *  can determine which output to choose at runtime.
 *  Furthermore, each output index has to be unique.
 *
 *  TODO: Assuming no threads involved for now
 *
 */
static  void
build_mstones_outputmap(handler_owner *gdata, int clusterid);
/*================================================================================*/
static void notify_newcontacts(contactsready_msg *ready_msg);
/*================================================================================*/
static  void
whisper_redirectreq(adamsg *info, FMStructDescRec *msg_format_list, 
    int which_resource, int which_rank);
/*==================end===========================================================*/
// to be only called in @upload_statdat. 
static void _construct_statinfo(struct StatusInfo* si);
static int _pick_an_offqueue(struct StatusInfo *si);

/*==================end===========================================================*/
static void _mark_a_readyjob(struct JItem *first, int qid);
static void init_jqueues(struct JQueues **jqs);
static struct JItem* assemble_an_jobitem(void *ds, void *cm, int gid);
static void init_job_container(struct Job **job, int gid);
static void _add_job_container(struct Job *job, struct list_head *qhead);
static void _add_an_item(struct JItem *item, struct list_head *qhead, int qid);

//static void _identify_upload_elements(int stage, int *start, int *elements);
/*=================================================================================*/
static int is_newstream(int streamID, int qid, struct JQueues *jqs);
static int _is_newstream(int streamID, struct list_head *qhead);
static void init_stream_container(struct Stream **stream, int streamID);
static void add_stream_container(struct Stream *stream, int qid, struct JQueues *jqs);
static void _add_stream_container(struct Stream *stream, struct list_head *qhead);
struct list_head* locate_streamqhead(int streamID, struct list_head* qhead);
static int is_newjob(int streamID, int gid, int qid, struct JQueues *jqs);
static int _is_newjob(int gid, struct list_head *qhead);

static int _delete_stream_jobs(struct JobsToProcess *j2p, struct list_head *qhead, int count);
static int _compute_qsize(struct list_head *qhead);
static void print_jqueue(int qid, struct JQueues *jqs);
static int _faulty_detection(struct list_head* dhead, void *ds);
/*=================================================================================*/


#if 0
// for setting up a global logging service
int init_udp(){
	sockfd=socket(AF_INET,SOCK_DGRAM,0);
  assert(sockfd > -1);

	memset(&servaddr,0,sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr=inet_addr("130.207.110.176"); // service IP
	servaddr.sin_port=htons(5555);

  return 0;
}

int request_ts(char *sentmsg){
  int ret = sendto(sockfd, sentmsg, strlen(sentmsg), 0, 
            (struct sockaddr *)&servaddr,sizeof(servaddr));

  return ret;
}
#endif


void init_system(int argc, char **argv,
    handler_owner **gdata,
    FMStructDescList *format_list,
    int clusterid)
{
  int rank = 0, size = 1, forked = 0;

  printf("My role is %d\n", clusterid);

#ifdef PLUGMPI
  init_mpirun(argc, argv, &rank, &size); // make mpi lib pluggable
#endif

#if 0
  MPI_Comm comm = MPI_COMM_WORLD;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &size);
#endif

  //printf("hereeee???\n");
  CManager cm;
  cm = CManager_create();
  //printf("Created evpath thread...\n");
  forked = CMfork_comm_thread(cm);
  assert(forked == 1);
  CMlisten(cm);

  //printf("setting up globaldata...\n");
  setup_globaldata(gdata, rank, size, cm);
  //init_udp();

  if(clusterid != DG){
    //printf("initing all multiqueue funcs...\n");
    init_all_multiqueue_funcs();
    add_multiq_action(format_list, multiqueue_action, *gdata);
    add_multiq_action(format_list, multiqueue_action_2, *gdata);
    add_multiq_action(format_list, multiqueue_action_3, *gdata);

    //printf("initing all groupstat...\n");
    init_groupstat();
    setup_groupexec_instructions();
    
    //printf("initing stragglermaps...\n");
    init_stragglermaps((*gdata)->num_multiactions);

    //init_myq(cm);
    init_jqueues(&jqs);
  }
  else{
    init_phasefunc(&multiqueue_action, "send_stream.cod");
    add_multiq_action(format_list, multiqueue_action, *gdata);
  }
  init_tracefile(clusterid);

  setup_ctrlpaths(*gdata);
  setpath_to_localmstones(*gdata);

  return;
}

#ifdef PLUGMPI
void init_mpirun(int argc, char **argv, int *rank, int *size)
{
  assert(argv && rank && size);

  MPI_Comm comm = MPI_COMM_WORLD;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(comm, rank);
  MPI_Comm_size(comm, size);

  return;
}
#endif

void start_matlabengine(handler_owner *gdata, int clusterid)
{
  assert(gdata);

  /*
   * Call engOpen with a NULL string. This starts a MATLAB process 
   * on the current host using the command "matlab".
   */
  if( !(gdata->ep = engOpen(""))){
    printf("\nCannot start Matlab engine @Rank#%d\n", gdata->rank);
    exit(0);
  }

  add_matlabfunc_path(gdata->ep);
  if(clusterid != DG){
    init_matlab(gdata->ep, gdata);
  }

  return;

}

void register_routines_toevpathspace(handler_owner *gdata, int clusterid)
{
  assert(gdata);

  if(clusterid != DG){
    static char extern_strings[] = "\
    void exit(int status);\n\
    int if_a_straggler_group(int groupID, straggler_map_t *straggler_info);\n\
    void ret_moutputs_mapinfo(mstone_outputs_t *mouts, int mstoneID);\n\
    int if_in_complset(int groupID, int *groupset);\n\
    int get_my_resourceID();\n\
    int get_my_rankID();\n\
    int get_my_laststageID();\n\
    void update_and_check_group_status(toexec_ctl_msg *toexec_ctl_data, int which_mstone, int which_group);\n\
                                  ";
  static cod_extern_entry externs_all[] = {
    {"exit", (void *)exit},
    {"if_a_straggler_group", (void *)if_a_straggler_group},
    {"ret_moutputs_mapinfo", (void *)ret_moutputs_mapinfo},
    {"if_in_complset", (void *)if_in_complset},
    {"get_my_resourceID", (void *)get_my_resourceID},
    {"get_my_rankID", (void *)get_my_rankID},
    {"get_my_laststageID", (void *)get_my_laststageID},
    {"update_and_check_group_status", (void *)update_and_check_group_status},
    {NULL, NULL}
    };
    EVadd_standard_routines(gdata->cm, extern_strings, externs_all);
  }
  else{
    static char extern_strings[] = "\
    void exit(int status);\n\
    void ret_moutputs_mapinfo(mstone_outputs_t *mouts, int mstoneID);\n\
    int get_my_resourceID();\n\
    int get_my_rankID();\n\
    int get_my_laststageID();\n\
    ";
  static cod_extern_entry externs_all[] = {
    {"exit", (void *)exit},
    {"ret_moutputs_mapinfo", (void *)ret_moutputs_mapinfo},
    {"get_my_resourceID", (void *)get_my_resourceID},
    {"get_my_rankID", (void *)get_my_rankID},
    {"get_my_laststageID", (void *)get_my_laststageID},
    {NULL, NULL}
    };
    EVadd_standard_routines(gdata->cm, extern_strings, externs_all);
  }

  return;
}

#if 0
void init_cloud(handler_owner* gdata, char **token, char **purl)
{
  assert(gdata && token && purl);

  _get_cloudinfo(gdata, token, purl);
  if(gdata->rank == 0){
    _create_datcontainers(*token, *purl, gdata);
    _create_statcontainers(*token, *purl);
  }
  return;
}


void _get_cloudinfo(handler_owner *gdata, char **token, char **purl)
{
  if(gdata->rank == 0){
    prepare_cloudaccess();
  }
#ifdef PLUGMPI
  else{
    *token = malloc(TOKEN_STR_SIZE*sizeof(char));
    *purl = malloc(URL_STR_SIZE*sizeof(char));
  }
  MPI_Bcast(*token, TOKEN_STR_SIZE, MPI_CHAR, 0, gdata->comm); 
  MPI_Bcast(*purl, URL_STR_SIZE, MPI_CHAR, 0, gdata->comm); 
  //printf("@rank%d: token = %s, purl = %s\n", gdata->rank, *token, *purl);
#endif

  return;
} 
#endif

#if 0
int _create_datcontainers(char* token, char* url, handler_owner* gdata)
{
  assert(token && url && gdata);

  char cname[CNAME_STR_SIZE];
  int i, j, ret;
  for(i=0; i<gdata->size; i++){
    memset(cname, 0, CNAME_STR_SIZE);
    for(j=0; j<=gdata->num_multiactions; j++){
      snprintf(cname, CNAME_STR_SIZE, "c%02dp%02ds%02d_con", gdata->itself, i, j);
      ret = create_container(token, url, cname);
      if(ret) return ret;

#if 0
      memset(cname, 0, CNAME_STR_SIZE);
      snprintf(cname, CNAME_STR_SIZE, "c%02dp%02ds%02d_faulty", gdata->itself, i, j);
      printf("creating resultcontainer: %s\n", cname);
      ret = create_container(token, url, cname);
      if(ret) return ret;
#endif
    }
  }

  return ret;
}

int _create_statcontainers(char* token, char* url)
{
  assert(token && url);

  char cname[CNAME_STR_SIZE];
  int ret;
  memset(cname, 0, CNAME_STR_SIZE);
  snprintf(cname, CNAME_STR_SIZE, "stat_con");
  ret = create_container(token, url, cname);
  if(ret) return ret;

  return ret;
}
#endif

void connect_system(handler_owner *gdata, int clusterid)
{
  assert(gdata);
  assert(clusterid >= 0);

  if(clusterid == ENDSITE){
    build_mstones_outputmap(gdata, clusterid);

    return;
  }

  //otherwise 
  obtain_curclusters_contacts(gdata);
  connecto_curclusters(gdata); 
  build_mstones_outputmap(gdata, clusterid);

  return;
}

void broadcast_contactreadymsg()
{
  contactsready_msg contactinfo_ready_msg;
  contactinfo_ready_msg.ready = TRUE;
  contactinfo_ready_msg.src = clusterid;
  notify_newcontacts(&contactinfo_ready_msg);

  return;

}


void ret_moutputs_mapinfo(mstone_outputs_t *mouts, int mstoneID)
{
  assert(mouts != NULL);

  mouts->to_contactready = client_data->mstone_outputs[mstoneID].to_contactready;
  mouts->to_nextphase = client_data->mstone_outputs[mstoneID].to_nextphase;
  mouts->to_endsite = client_data->mstone_outputs[mstoneID].to_endsite;
  mouts->to_nextsite = client_data->mstone_outputs[mstoneID].to_nextsite;
  mouts->to_taskready = client_data->mstone_outputs[mstoneID].to_taskready;
  mouts->to_adamsg = client_data->mstone_outputs[mstoneID].to_adamsg;
  mouts->to_finaloutput = client_data->mstone_outputs[mstoneID].to_finaloutput;
  mouts->num_mstones = client_data->num_multiactions;

  return;
}

#if 0
static void _identify_upload_elements(int stage, int *start, int *elements)
{
  assert(start && elements);

  // to identify which mdata[] for ds to upload
  *elements = VARSIZES[stage];
  switch(stage){
  case 0:
    *start = 0;
    break;
  case 1:
    *start = VARSIZES[stage-1];
    break;
  case 2:
    *start = VARSIZES[stage-1] + VARSIZES[stage-2];
    break;
  default:
    printf("should not research here (stage#%d). Please examine.\n", stage);
    exit(0);
  }

  return;
}
#endif

#if 0
void construct_dataobj(struct MemoryStruct* sdata, int start, int elements, datastream *ds)
{
  int i, M, N;
  char *data = NULL;
  size_t bytesize = 0, tbsize = 0;

  // fill in header info (M*N for each chunck)
  int *header = (int *)calloc(HEADER_SIZE, sizeof(int));
  assert(header);
  header[0] = start;
  header[1] = elements;
  for(i=0; i<elements; i++){ // to record meta data
    bytesize = ds->mdatas[start+i].tsize * sizeof(double);
    tbsize += bytesize; // to compute total mm for all data chuncks

    M = ds->mdatas[start+i].M;
    N = ds->mdatas[start+i].N;
    assert(M*N == ds->mdatas[start+i].tsize);
    header[(i+1)*2] = M;
    header[(i+1)*2 + 1] = N;
  }
  sdata->memory = (char *)header;
  sdata->bytesize = HEADER_SIZE * sizeof(int);

  sdata->memory = realloc(sdata->memory, sdata->bytesize + tbsize + 1);
  if (sdata->memory == NULL) {
    /* out of memory! */
    printf("not enough memory (realloc returned NULL)\n");
    exit(EXIT_FAILURE);
  }

  for(i=0; i<elements; i++){
    data = (char *)(ds->mdatas[start+i].data);
    bytesize = ds->mdatas[start+i].tsize * sizeof(double);

    memcpy(&(sdata->memory[sdata->bytesize]), data, bytesize);

    sdata->bytesize += bytesize;
    sdata->memory[sdata->bytesize] = 0; //for the following copy
  }

  return;
}
#endif

#if 0
// Note that this _onestap_upload is per dataouput basis
void upload_withbuff(const char *token, const char *url, const char *cname, const char *oname, struct MemoryStruct *sdata)
{
  char *rewindmm = NULL;
  rewindmm = sdata->memory;

  //decode_interdataset(sdata);
  upload_object(token, url, cname, oname, sdata);

  // to rewind the mm back to org, for next batch
  //  = (char *)calloc(BATCHSIZE, sizeof(char)); @analysis_engine
  sdata->bytesize = 0;
  sdata->memory = rewindmm;  
  memset(sdata->memory, 0, BATCHSIZE);

  return;
}

// Note that this _onestap_upload is per dataouput basis (nobuffer)
void upload_nobuff(const char *token, const char *url, const char *cname, const char *oname, struct MemoryStruct *sdata)
{
  char *freep = NULL;
  freep = sdata->memory;

  upload_object(token, url, cname, oname, sdata);
  //decode_interdata(sdata);

  //free(sdata->memory); // segfault because sdata->memory is modified
  //sdata->memory = NULL;
  sdata->bytesize = 0;
  sdata->memory = NULL;
  free(freep);
  freep = NULL;

  return;
}
#endif

/* to be invoked by upstream */
static void notify_newcontacts(contactsready_msg *ready_msg)
{
  assert(ready_msg != NULL);
  assert(ready_msg->ready == TRUE);

  int num_dsresource;
  int i, j;
  EVstone stone;
  EVaction action;
  attr_list remote_contact_list = NULL;
  EVsource source = NULL;

  num_dsresource = client_data->num_resources; 
  for(i=1; i<num_dsresource; i++){
    if(i == client_data->itself) continue;
    for(j=0; j<client_data->resource_mpisizes[i]; j++){
      remote_contact_list = attr_list_from_string(
          client_data->downstream_contacts[i][j].string_list); 
      stone = EValloc_stone(client_data->cm);
      action = EVassoc_bridge_action(
          client_data->cm, stone, 
          remote_contact_list,
          client_data->downstream_contacts[i][j].remote_stone);
      source = EVcreate_submit_handle(client_data->cm, stone, contactsready_msg_format_list);

      EVsubmit(source, ready_msg, NULL);
    }
  }

  return;
}


int
taskready_handler(CManager cm, void *vevent, void *c_data, attr_list attrs)
{
  assert(cm && vevent && c_data);
  if(EVtake_event_buffer(cm, vevent) == 0){
    printf("Error in CMtake_buffer.\n");
    exit(0);
  }
 
  int qid = *((int *)c_data);
  datastream *ds = (datastream *) (vevent);

  struct Stream *stream = NULL;
  if(is_newstream(ds->streamID, qid, jqs)){
    // to add a new list for the new stream to hold window-size buffer
    add_faulty_sbuffer(ds->streamID);

    init_stream_container(&stream, ds->streamID);
    add_stream_container(stream, qid, jqs);
  }

  struct Job *job = NULL;
  if(is_newjob(ds->streamID, ds->groupID, qid, jqs)){
    init_job_container(&job, ds->groupID);
    add_job_container(job, ds->streamID, qid, jqs);
  }

  struct JItem *item = assemble_an_jobitem(vevent, (void *)cm, ds->groupID);
  assert(item);
  add_an_item(item, qid, jqs);

  return 1;

}

// to obtain the decision on 
// 1) how many tasks to grab
// from
// 2) which queue
// Note that an advanced decision maker is expected 
static int TWORKLOAD = 1600;
static int QID = 0;
int obtain_processing_decision(int old, int *qid, int *num_tasks, int option, int faulty){
  assert(qid && num_tasks);

  /* to mimic a strategy to execute three queue One BY One. */
  // special case when reaches the last queue, since there may be more than TWORKLOAD tasks
  if (QID == 0){
    if(option == TWORKLOAD){
      *qid = ++QID;
      *num_tasks = 1;
    }
    else{
      *qid = QID;
      *num_tasks = 1;
    }
  }
  else if(QID == (MSTONE_NUM -1)){ 
    *qid = QID;
    *num_tasks = 1;
  }
  else{
    printf("Faulty No. = %d (option = %d, QID = %d)\n", faulty, option, QID);
    if(option == (TWORKLOAD - faulty)){
      *qid = ++QID;
      *num_tasks = 1;
    }
    else{
      *qid = QID;
      *num_tasks = 1;
    }
  }

  if(*qid != old) 
    return CHANGED;
  else
    return UNCHANGED; 
}

#if 0
//int apply_analysis(int qid, CompleteJobs *tasks, handler_owner *gdata)
int apply_analysis(int qid, struct JobsToProcess *j2p, handler_owner *gdata)
{
  if( (qid < 0) || (qid >= MSTONE_NUM) || (!j2p) || (!gdata) ){
    return -1;
  }
  if( j2p->count <= 0 ){
    return -1;
  }

  if(qid == 0){
    binarize_images(j2p, gdata);
  }
  else if(qid == 1){
    extract_flameedges(j2p, gdata);
  }
  else if(qid == 2){
    compute_curvatures(j2p, gdata);
  }

  return 0;
}

  void *
analysis_engine(void *in)
{
  int qid = -1, num_tasks, ret;
  struct JobsToProcess *j2p = NULL;

  struct MemoryStruct sdata[3]; // one per queue  (for interdata upload)
  int i;
  for(i=0; i<3; i++){
    sdata[i].memory = (char *)calloc(BATCHSIZE, sizeof(char));
    assert(sdata[i].memory != NULL);
    sdata[i].bytesize = 0;
  }

  struct MemoryStruct faulty_sdata; // for falutydata upload
  faulty_sdata.memory = (char *)calloc(BATCHSIZE, sizeof(char));
  assert(faulty_sdata.memory != NULL);
  faulty_sdata.bytesize = 0;
  static int faulty_count = 0;

  static int finished_tasks = 0; 
  int option = INVILID_INT;
  while(1){
    ret = obtain_processing_decision(qid, &qid, &num_tasks, finished_tasks, faulty_count);
    //printf("Current Decision: QID with NUM_tasks (%d,%d)\n", qid,num_tasks);
    if (ret == CHANGED){
      finished_tasks = 0;
      option = INVILID_INT;
    }

    j2p = active_dequeue(num_tasks, qid, jqs); 
    if(!j2p){
      sleep(2);
      continue;
    }
    finished_tasks += j2p->count;
    //printf("Current Finished tasks = %d\n", finished_tasks);
    if (qid == 0){
      if(finished_tasks == TWORKLOAD) option = LAST;
    }
    else{
      if(finished_tasks == (TWORKLOAD - faulty_count)) option = LAST;
    }

    printf("\nFinished #%d tasks and to check out next task\n", finished_tasks);
    apply_analysis(qid, j2p, client_data);

    if (qid == 0){
      // faulty image filter
      ret = faulty_detection(&faulty_sdata, j2p, qid, option);  

      if (ret == BAD) faulty_count += 1;
      //printf("(ret = %d) At-The-Moment faulty count = %d\n", ret, faulty_count);
    }
    if ( ((qid == 0)&&(ret == GOOD)) || (qid != 0) ) {
      disseminate_data(&(sdata[qid]), qid, j2p, client_data, option);
    }

  } 

  return NULL;
}

void disseminate_data(struct MemoryStruct* sdata, int qid, struct JobsToProcess *j2p, handler_owner *gdata, int option)
{
  assert(sdata && j2p && gdata);

  // control scheme on transferring intermediate result
  // 1. to_local  nextQ
  // 2. to_remote nextQ
  // 3. to_cloud  container
  int i;
  EVsource dest;
#if 0
  if(qid != (gdata->num_multiactions-1)) // !last_stage: 1
    dest = gdata->to_localmstones[qid+1];
  else{
    dest = gdata->toendsite_src; // 2
  }
#endif
    dest = gdata->toendsite_src; // 2

  int cur_opt = INVILID_INT;
  struct list_head *jhead = NULL;
  struct JItem *item = NULL;
  for(i=0; i<j2p->count; i++){
    //printf("Disseminating JOB #%d\n", i);
    if (i==(j2p->count-1)) cur_opt = option; // to mark the very last one

    jhead  = j2p->jobs[i];
    list_for_each_entry(item, jhead, link){
      if(item->ds == NULL){
        //printf("JOB#%d has %d items\n", item->gid, item->items);
        assert(item->is_ready);
        continue;
      }
      //printf("\n###Disseminating JOB#%d item\n", item->gid);
      //upload_interdata_withbuffer(token, purl, sdata, (datastream *)(item->ds), qid, gdata, BATCHSIZE, cur_opt); // 3
      //upload_interdata_nobuffer(token, purl, (datastream *)(item->ds), qid, gdata); // 3
      EVsubmit(dest, item->ds, NULL); // 1 or 2

      char msg[200];
      if (qid == 2){
        sprintf(msg, "end:image%d_R%drank%d", ((datastream *)item->ds)->srcID, client_data->itself, client_data->rank);
        //request_ts(msg);
      }

    }

  }

  // to release evpath space & my space
  free_processed_jobs(j2p, qid); 

  return;
}

int extend_dataobjs(struct MemoryStruct* sdata, int start, int elements, datastream *ds, size_t bufsize, int option)
{
  int i, M, N;
  char *data = NULL;
  size_t bytesize = 0, tbsize = 0;
  for(i=0; i<elements; i++){ // to record meta data
    bytesize = ds->mdatas[start+i].tsize * sizeof(double);
    tbsize += bytesize; // to compute total mm for all data chuncks
  }
  //printf("\n\n@@@@sdata->bytesize + (tbsize + HEADER_SIZE * sizeof(int)) = %lu\n", sdata->bytesize + (tbsize + HEADER_SIZE * sizeof(int)));
  if((sdata->bytesize + (tbsize + HEADER_SIZE * sizeof(int)))> bufsize) {
    printf("\n###current sdata->bytesize = %lu (THREASHL = %lu)\n", sdata->bytesize, bufsize);
    return UPLOAD;
  }

  // the very first dataoutput of the batch
  int *dataobjs = (int *)sdata->memory;
  if (sdata->bytesize == 0) {
    *dataobjs = 0;
    sdata->bytesize += sizeof(int);
  }
  *dataobjs += 1;

  char *newobj = sdata->memory + sdata->bytesize;
  int *header = (int *)newobj;
  header[0] = start;
  header[1] = elements;
  // fill in header info (M*N for each chunck)
  for(i=0; i<elements; i++){ // to record meta data
    bytesize = ds->mdatas[start+i].tsize * sizeof(double);
    tbsize += bytesize; // to compute total mm for all data chuncks

    M = ds->mdatas[start+i].M;
    N = ds->mdatas[start+i].N;
    assert(M*N == ds->mdatas[start+i].tsize);
    header[(i+1)*2] = M;
    header[(i+1)*2 + 1] = N;
  }
  sdata->bytesize += HEADER_SIZE * sizeof(int);

  for(i=0; i<elements; i++){
    //printf("@@@Composing %dth chunk into dataobject for cname %s\n", i, cname);
    data = (char *)(ds->mdatas[start+i].data);
    bytesize = ds->mdatas[start+i].tsize * sizeof(double);

    memcpy(&(sdata->memory[sdata->bytesize]), data, bytesize);

    sdata->bytesize += bytesize;
    //sdata->memory[sdata->bytesize] = 0; //for the following copy
  }
  printf("\n\n###packed sdata->bytesize = %lu\n", sdata->bytesize);
  printf("@@@current packed data objects NO. = %d\n", *dataobjs);
  
  if (option == LAST) return UPLOAD;
  return -1;
}
#endif


void free_processed_jobs(struct JobsToProcess *j2p, int qid)
{
  assert(j2p);

  int i;
  struct list_head *jhead = NULL;
  struct JItem *item = NULL, *tmp = NULL;
  for(i=0; i<j2p->count; i++){
    jhead  = j2p->jobs[i];
    list_for_each_entry_safe(item, tmp, jhead, link){
      if(item->ds == NULL){
        //printf("(@Free_JOB) JOB#%d has %d items\n", item->gid, item->items);
        assert(item->is_ready);
        list_del(&(item->link));
        free(item);
        item = NULL;
        continue;
      }

      list_del(&(item->link)); // normal item - assemble_an_jobitem
      if(qid == (get_my_laststageID() - 1)){ // last stage
        EVreturn_event_buffer((CManager)(item->cm), item->ds);
      } 
      free(item);
      item = NULL;
    }
    free(j2p->jobs[i]); //free: init_job_container-"(*job)->jhead
  }
  free(j2p);
  j2p = NULL;

  return;
} 

#if 0
void upload_interdata_withbuffer(char* token, char* url, struct MemoryStruct* sdata, datastream *ds, int stage, handler_owner* gdata, size_t bufsize, int option)
{
  char cname[CNAME_STR_SIZE];
  memset(cname, 0, CNAME_STR_SIZE);
  snprintf(cname, CNAME_STR_SIZE, "c%02dp%02ds%02d_con", gdata->itself, gdata->rank, stage+1);

  char oname[ONAME_STR_SIZE];
  memset(oname, 0, ONAME_STR_SIZE);
  snprintf(oname, ONAME_STR_SIZE, "data%sdatname", "TOFIGUREOUTLATER");

  upload_imagedata(token, url, sdata, cname, oname, ds, stage, gdata, bufsize, option);

  return;
}

void upload_faultydata_withbuffer(char* token, char* url, struct MemoryStruct* sdata, datastream *ds, int stage, handler_owner* gdata, size_t bufsize, int option){
  char cname[CNAME_STR_SIZE];
  memset(cname, 0, CNAME_STR_SIZE);
  snprintf(cname, CNAME_STR_SIZE, "c%02dp%02ds%02d_faulty", gdata->itself, gdata->rank, stage+1);

  char oname[ONAME_STR_SIZE];
  memset(oname, 0, ONAME_STR_SIZE);
  snprintf(oname, ONAME_STR_SIZE, "data%sdatname", "TOFIGUREOUTLATER");

  upload_imagedata(token, url, sdata, cname, oname, ds, stage, gdata, bufsize, option);

  return;
}

// "stage" is the step that has just finished
void upload_imagedata(char* token, char* url, struct MemoryStruct* sdata, char* cname, char* oname, datastream *ds, int stage, handler_owner* gdata, size_t bufsize, int option)
{
  assert(token && url && sdata && ds && gdata);
  TIMER_DECLARE1(wbuff_timer); // for overhead record
  uint64_t wbuff_clk = 0;

  int start, elements;
  _identify_upload_elements(stage, &start, &elements);

  int ret; 
  ret = extend_dataobjs(sdata, start, elements, ds, bufsize, option); 

  size_t size;
  if(ret == UPLOAD){
    size = sdata->bytesize;

    TIMER_START(wbuff_timer);
    upload_withbuff(token, url, cname, oname, sdata);
    TIMER_END(wbuff_timer, wbuff_clk);

    char msg[100];
    //sprintf(msg, "%ld:%llu\n", size, wbuff_clk);
    sprintf(msg, "%ld:%" PRIu64 "\n", size, wbuff_clk);
    write(fp_wbuff_timer, msg, strlen(msg));
    printf("@@@uploaded batchedObj (dsize = %lu)\n", size);
    
    if (option != LAST){
      printf("Option != LAST at this upload\n");
      // to store the current (not fit) dataoutput
      extend_dataobjs(sdata, start, elements, ds, bufsize, option);
    }
  }

  return;
}

// "stage" is the step that has just finished
void upload_interdata_nobuffer(char* token, char* url, datastream *ds, int stage, handler_owner* gdata)
{
  assert(token && url && ds && gdata);
  TIMER_DECLARE1(wobuff_timer); // for overhead record
  uint64_t wobuff_clk = 0;

  //upload_single_dataobj(token, url, cname, ds, stage);
  int start, elements;
  _identify_upload_elements(stage, &start, &elements);

  struct MemoryStruct sdata;
  construct_dataobj(&sdata, start, elements, ds);

  char cname[CNAME_STR_SIZE];
  memset(cname, 0, CNAME_STR_SIZE);
  snprintf(cname, CNAME_STR_SIZE, "c%02dp%02ds%02d_con", gdata->itself, gdata->rank, stage+1);
  char oname[ONAME_STR_SIZE];
  memset(oname, 0, ONAME_STR_SIZE);
  snprintf(oname, ONAME_STR_SIZE, "data%sdatname", ds->srcname);
  //printf("Uploading oname %s to cname %s bsize %lu\n", oname, cname, sdata.bytesize);

  size_t size; 
  size = sdata.bytesize;

  TIMER_START(wobuff_timer);
  upload_nobuff(token, url, cname, oname, &sdata);
  TIMER_END(wobuff_timer, wobuff_clk);

  char msg[100];
  //sprintf(msg, "%ld:%llu\n", size, wobuff_clk);
  sprintf(msg, "%ld:%" PRIu64 "\n", size, wobuff_clk);
  write(fp_wobuff_timer, msg, strlen(msg));

  return;
}
#endif

#if 0
void upload_statusdat(char* token, char* url, struct StatusInfo *si, struct MemoryStruct *sdata)
{
  char cname[] = "stat_con";

  char oname[ONAME_STR_SIZE];
  memset(oname, 0, ONAME_STR_SIZE);
  snprintf(oname, ONAME_STR_SIZE, "c%dp%d", si->cid, si->pid);
  upload_object(token, url, cname, oname, sdata);
  //printf("@@@Uploaded oname %s to cname %s\n", oname, cname);

  return;
}
#endif

/* to be invoked @adaptive capacity receiver side
 * e.g., a slower resource location
 * to respond with local group status & only be invoked in mstone0
 *
 * Instead of useing a dedicated thread to grab the local queues' status
 * The rational behind this is that the evpath (communication?) thread is probably
 * idle at the time of this message arrive
 *
 * TODO: may use the mechanism of a separate thread for this purpose
 *
 * */
  int
adamsg_responsor(CManager cm, void *vevent, void *c_data, attr_list attrs)
{
  assert( (vevent != NULL) );
  adamsg *event = (adamsg *)vevent;
  assert(event->type == REDIRECT_REQUEST);

  FMStructDescRec *msg_format_list = NULL;
  static int count_offworkload = 0;
  static int toffload = 0;

  int i; 
#ifdef DEBUG
  printf("\n!!!@(cid#%d, pi#%dd)response_to_workstealer: stealer CID#%d, PID#%d\n", client_data->itself, client_data->rank, event->resourceID, event->rankID);
#endif

  count_offworkload++;
  msg_format_list = datastream_format_list;

  struct JobsToProcess *j2p = NULL;
  j2p = passive_dequeue(jqs);
  if(j2p == NULL) {
    //printf("\n@@@@@@No streams to re-direct.\n");
    sleep(10);
    return 1;
  };

  struct list_head *jhead = NULL;
  struct JItem *item = NULL;
  for(i=0; i<j2p->count; i++){
    jhead = j2p->jobs[i];
    list_for_each_entry(item, jhead, link){
      if(item->ds == NULL) {
        printf("offloading JOB#%d (w/ %d items)\n", item->gid, item->items);
        continue;
      }

      response_with_workload((datastream *)(item->ds), event->resourceID, event->rankID, msg_format_list);
      toffload += 1;
    }
  }

  // to free space
  free_processed_jobs(j2p, (get_my_laststageID() - 1)); 
  j2p = NULL;

  return 1;
}

  void
response_with_workload(datastream *ds, int which_resource, int which_rank, 
    FMStructDescRec *msg_format_list)
{
  //TODO: to use which_resource, which_rank to identify which adaptive_source?
  assert(ds != NULL);

  build_path_for_adaptive_mapping(which_resource, which_rank, msg_format_list);
  EVsubmit(client_data->adaptive_source, ds, NULL);

  //printf("Return from Sending out the redirected workload\n");
  return;

}

/* to be invoked @adaptive capacity sender side
 * e.g., the faster resource location
 *
 * Can be called, e.g., no more streams is in local processing queue
 * */
  static  void
send_adamsg(adamsg *info, int to_whichrResrc, int to_whichrRank, 
    FMStructDescRec *msg_format_list)
{
  assert(info != NULL);
  assert(msg_format_list != NULL);

  build_path_for_adaptive_mapping(to_whichrResrc, to_whichrRank, msg_format_list);
  EVsubmit(client_data->adaptive_source, info, NULL);
  printf("R%d rank%d asking tasks from slow side\n\n", clusterid, client_data->rank);

  return;

}

/* to build a path on the fly, for the purpose of adaptive processing.
 *
 * Assuming that a single adaptive_stone per process!!!
 *
 * Can be invoked @ e.g., 
 * 1) the faster resource to build path for delivering capacity_info
 * 2) the slower resource to build path for dumping out queued datastream
 * */
  static  void 
build_path_for_adaptive_mapping(int to_whichrResrc, int to_whichrRank, 
    FMStructDescRec *msg_format_list)
{
  assert( (to_whichrResrc >= 0) && (to_whichrRank >= 0) );
  assert(msg_format_list != NULL);

  char *rstring = NULL;
  attr_list rcontact = NULL;
  EVstone stone = EValloc_stone(client_data->cm), rstone;

  // check out the contact info of the recever
  rstring = strdup(client_data->downstream_contacts[to_whichrResrc][to_whichrRank].string_list);
  assert(rstring != NULL);
  rcontact = attr_list_from_string(rstring);
  rstone = client_data->downstream_contacts[to_whichrResrc][to_whichrRank].remote_stone;
  //printf("@b_path_for_adaptive_mapping>>> R%d, r%d: remote contact info = %d:%s\n", clusterid, client_data->rank, rstone, rstring);
  EVassoc_bridge_action(client_data->cm, stone, rcontact, rstone);

  client_data->adaptive_source = EVcreate_submit_handle(client_data->cm, 
      stone, 
      msg_format_list);
  assert(client_data->adaptive_source != NULL);
  free(rstring);

  return;
}


/*
 * This thread will monitor queue levels from all the mstones
 * check every 1-second period: only one thread per process
 *
 * check all mstones' queue level & every resource location will have one such thread
 * VERY AGGRESSIVE
 * Next: send out the "IDLE" info to all its downstreams, but NOT
 *       end site
 *       Including: 1) intra-resource location processes
 *                  2) inter-resource location processes
 *
 * This is a thread. 
 * */
#if 0
  void *
upload_statdat(void *in)
{
  if( !getenv("ADAPTIVE") ){
    printf("*NONE ADAPTIVE MODE. @.@\n\n");
    return NULL;
  }

  /* The following is to demonstrate a simple adaptive strategy */
  // to monitor the workflow execution progress
  adamsg adamsg_info; 
  struct MemoryStruct sdata;
  struct StatusInfo si;
  while(1){
    construct_statobj(&sdata, &si);
    upload_statusdat(token, purl, &si, &sdata);
    ADA_MONUP_PERIOD;
  }

  return NULL;
}
#endif

void print_statusinfo(struct StatusInfo *si)
{
  printf("@(cid#%d, pid#%d) Qsize (%d, %d, %d)\n", \
      si->cid, si->pid, si->tasks[0], si->tasks[1], si->tasks[2]);
  return;
}

  static int
_pick_an_offqueue(struct StatusInfo *si)
{
  assert(si);

  print_statusinfo(si);
  int i, maxV = INVILID_INT, maxI = INVILID_INT;
  // based on tasks # (or estime #)
  for(i=0; i<MSTONE_NUM; i++){
    if(si->tasks[i] > maxV){
      maxV = si->tasks[i];
      maxI = i;
    }
  }

  return maxI;
}

  static int 
contactinfo_ready_handler(CManager cm, void *vevent, void *c_data, attr_list attrs)
{
  assert(vevent != NULL);
  contactsready_msg *event = (contactsready_msg *)vevent;
  update_system_connmaps(event->src, client_data);

  // once all resources join the system, broadcast path is to build 
  // (first_time is used in case a future new resource cluster joins in the system)
  static int first_time = TRUE;
  if( (first_time) && (client_data->num_resources == TOTAL_RESOURCES) ){ 
    build_broadcast_path(client_data, adamsg_format_list); 
    first_time = FALSE;
  }

  return 1;
}

/*====================setup system in case of new parties joining in the system====================*/
/* to obtain the upstream contactors that map their bridge to local rank
 * Note that this mapping information may dynamically change in the system runtime
 *
 * */
  static void 
obtain_dgprocs_contacts(handler_owner *gdata)
{
  assert(gdata != NULL);

  char temp_contact_info_filename[250];
  sprintf(temp_contact_info_filename, "%s", TEMP_CONTACT_INFO_FILE);
  FILE *fp_in = NULL;
  //char in_contact[250];
  //memset(in_contact, 0, 250);
  char in_contact[50] = "";
  int remote_stone, mapped_node, mapped_rank;
  int this_mpisize; 
  int ret, i, which_rank;

  // if too many processes open the file, 
  // may need to switch to rank 0 read and mpi broadcast
  fp_in = fopen(temp_contact_info_filename, "r");
  while(!fp_in){
    fp_in = fopen(temp_contact_info_filename, "r");
  }

  while( (ret = fscanf(fp_in, "%d:%d:%d:%s", &mapped_node, &mapped_rank, 
          &remote_stone, in_contact)) != EOF ){
    if(ret == 1){ // a block for a new resource location starts
      this_mpisize = mapped_node; 
      which_rank = 0;
    }
    if(ret == 4){ 
      if( (mapped_node == gdata->itself) && (mapped_rank == gdata->rank)){
        gdata->dgprocs_contacts[which_rank].remote_stone = remote_stone;
        gdata->dgprocs_contacts[which_rank].string_list =  strdup(in_contact);
        assert(gdata->dgprocs_contacts[which_rank].string_list); 
        gdata->num_dgprocs += 1;
      }
      which_rank++;
    }
  }
  fclose(fp_in);

  return;

}

  static void
update_workflowprocs_contacts(handler_owner *gdata)
{
  assert(gdata != NULL);

#ifdef DEBUG
  printf("@update_workflowprocs_contacts\n");
  printf("Current Num of resource locatons @system: %d\n", 
      gdata->num_resources);
#endif

  char temp_contact_info_filename[250];
  sprintf(temp_contact_info_filename, "%s", TEMP_CONTACT_INFO_FILE);
  FILE *fp_in = NULL;
  //char in_contact[250];
  //memset(in_contact, 0, 250);
  char in_contact[50] = "";
  int remote_stone, mapped_node, mapped_rank;
  int this_mpisize, cur_resourceID, cur_rankID; 
  int ret, i, j;

  // if too many processes open the file, 
  // may need to switch to rank 0 read and mpi broadcast
  fp_in = fopen(temp_contact_info_filename, "r");
  while(!fp_in){
    fp_in = fopen(temp_contact_info_filename, "r");
  }

  while( (ret = fscanf(fp_in, "%d:%d:%d:%s", &mapped_node, &mapped_rank, 
          &remote_stone, in_contact)) != EOF ){
    if(ret == 1){ // a block for a new resource location starts
      this_mpisize = mapped_node; 
      gdata->resource_mpisizes[gdata->num_resources] = this_mpisize;
      gdata->num_resources += 1;
      cur_rankID = 0;
    }
    else{ // still in the same block as the last resource
      cur_resourceID = gdata->num_resources - 1;
      gdata->downstream_contacts[cur_resourceID][cur_rankID].remote_stone = 
        remote_stone;
      gdata->downstream_contacts[cur_resourceID][cur_rankID].string_list = 
        strdup(in_contact);
      assert(gdata->downstream_contacts[cur_resourceID][cur_rankID].string_list 
          != NULL);
      gdata->reports += 1;
      cur_rankID += 1; 
    }
  }
  fclose(fp_in);

  return;

}

/* to setup the path from downstream to data_generator processes 
 * according to gdata->num_upstreamers, gdata->upstream_contacts
 *
 * Note: gdata: global data w.r.t per process 
 * */
  static void
build_ackpath_todgprocs(handler_owner *gdata)
{
  assert(gdata != NULL);

  int i;
  attr_list clist = NULL;
  EVstone stones[NUM_DG];
  for(i=0; i<NUM_DG; i++){
    if(gdata->dgprocs_contacts[i].remote_stone == INVILID_INT) continue;

    clist = attr_list_from_string(gdata->dgprocs_contacts[i].string_list);
    stones[i] = EValloc_stone(gdata->cm);
    EVassoc_bridge_action(gdata->cm, stones[i], 
        clist, gdata->dgprocs_contacts[i].remote_stone);
    gdata->acktodgprocs_srcs[i] = EVcreate_submit_handle(gdata->cm, stones[i], yesno_info_format_list);

    assert(gdata->acktodgprocs_srcs[i] != NULL);
    free(clist);
    clist = NULL;

  }

  return;
}

  static void
build_broadcast_path(handler_owner *gdata, FMStructDescRec *msg_format_list)
{
  assert(gdata != NULL); 

  char *rstring = NULL;
  attr_list rcontact = NULL;
  EVstone rstone;

  int i, j, ret;
  EVstone stones[NUM_RESOURCES][NUM_PROCESSES];
  for(i=1; i<gdata->num_resources; i++){ /* None end site */
    for(j=0; j<gdata->resource_mpisizes[i]; j++){
      stones[i][j] = EValloc_stone(gdata->cm);
      // check out the contact info of the recever
      rstring = strdup(gdata->downstream_contacts[i][j].string_list);
      assert(rstring != NULL);
      rcontact = attr_list_from_string(rstring);
      rstone = gdata->downstream_contacts[i][j].remote_stone;
      EVassoc_bridge_action(gdata->cm, stones[i][j], rcontact, rstone);

      // bridges for broadcast idlestat
      gdata->broadcast_srcs[i][j] = EVcreate_submit_handle(gdata->cm, 
          stones[i][j], 
          msg_format_list);
      assert(gdata->broadcast_srcs[i][j] != NULL);

      // bridges for sending ackinfo (future use)
      gdata->acktopeers_srcs[i][j] = EVcreate_submit_handle(gdata->cm, 
          stones[i][j], 
          yesno_info_format_list);
      assert(gdata->acktopeers_srcs[i][j] != NULL);
      free(rstring);
    }
  }

  return;

}

static void 
update_system_connmaps(int msgsrcid, handler_owner *gdata){
  assert(gdata != NULL);
  assert( (msgsrcid >=0) );

  if(msgsrcid != DG){
    update_workflowprocs_contacts(gdata);
  }
  if(msgsrcid == DG){
    obtain_dgprocs_contacts(gdata);
    build_ackpath_todgprocs(gdata); 
  }

  return;
}

/*=====================setup group arrival statistics====================================*/
  static void 
init_groupstat()
{
  int mstone_num = client_data->num_multiactions;
  int i, j, ret;
  group_stat = (group_map_t **)calloc(mstone_num, sizeof(group_map_t *));
  assert(group_stat != NULL);
  for(i=0; i<mstone_num; i++){
    group_stat[i] = (group_map_t *)calloc(GNUM, sizeof(group_map_t));
    assert(group_stat[i] != NULL);

    for(j=0; j<GNUM; j++){
      group_stat[i][j].groupID = INVILID_INT;
      group_stat[i][j].cur_gsize = 0;
      group_stat[i][j].if_straggler = FALSE;
      group_stat[i][j].mapped_rank = INVILID_INT;
      pthread_mutex_init(&group_stat[i][j].gstat_lock, NULL);
    }
  }

  return;
}

static void 
setup_groupexec_instructions(){
  assert(client_data != NULL);

  dfactors = (decision_factor_t **)calloc(client_data->num_multiactions, sizeof(decision_factor_t *));
  assert(dfactors != NULL);

  int mstone_num = client_data->num_multiactions;
  int i, j;
  for(i=0; i<mstone_num; i++){
    dfactors[i] = (decision_factor_t *)calloc(1, sizeof(decision_factor_t));
    assert(dfactors[i] != NULL);

    dfactors[i]->threshold = STRAGGLER_TIMEBOUND;
    dfactors[i]->mstoneID = i;
    dfactors[i]->groups_info = (gsize_t *)calloc(GNUM, sizeof(gsize_t));
    assert(dfactors[i]->groups_info);
    for(j=0; j<GNUM; j++){
      dfactors[i]->groups_info[j].groupID = j+1;
      dfactors[i]->groups_info[j].groupSize = PERGSIZE;
    }
  } 

  return;
}
/*==============================setup system pars==============================*/
  static  void 
add_multiq_action(FMStructDescList *queue_format_list, 
    char *phase_func, 
    handler_owner *gdata)
{
  assert(gdata != NULL);
  assert(queue_format_list != NULL);
  assert(phase_func != NULL);

  CManager cm = gdata->cm;

  EVstone multiq_stone = EValloc_stone(cm);
  char *multiq_spec = NULL;
  multiq_spec = create_multityped_action_spec(queue_format_list, phase_func);
  EVaction multiq_action = EVassoc_multi_action(cm, multiq_stone, multiq_spec, NULL);

  // starting from index 0
  int whichmstone = gdata->num_multiactions;
  gdata->multiq_stones[whichmstone] = multiq_stone;
  gdata->multiq_actions[whichmstone] = multiq_action;
  gdata->num_multiactions += 1;

  return;
}

  static void 
setup_globaldata(handler_owner **gdata,
    int rank,
    int size, CManager cm)
{
  assert(gdata != NULL);
  *gdata = (handler_owner *) calloc (1,sizeof(handler_owner));
  assert( (*gdata) != NULL);

  init_globaldata(*gdata, rank, size, cm);

  return;
}

  static void 
init_globaldata(handler_owner *gdata,
    int rank,
    int size,
    CManager cm)
{
  assert(gdata != NULL);
  assert(cm != NULL);

  int i, j;
  gdata->epars = NULL;
  gdata->ep = NULL;

#ifdef PLUGMPI
  gdata->comm = MPI_COMM_WORLD;
#endif

  gdata->itself = clusterid;
  gdata->rank = rank;
  gdata->size = size;
  gdata->map_size = 1;
  gdata->stages_count = 5;

  gdata->multiq_stones =(EVstone *) calloc(gdata->stages_count, sizeof(EVstone));
  gdata->multiq_actions = (EVaction *) calloc(gdata->stages_count, sizeof(EVaction));
  if(gdata->stages_count != 0){
    assert(gdata->multiq_stones != NULL);
    assert(gdata->multiq_actions != NULL);
  }

  gdata->contactorready_stone = INVILID_INT;

  gdata->src_contactstr = NULL; 
  gdata->mappedto_cluster = INVILID_INT;
  gdata->mappedto_rank = INVILID_INT;

  gdata->taskready_stones = NULL; 

  gdata->num_resources = 0;

  gdata->cm = cm;
  gdata->num_multiactions = 0;
  gdata->toendsite_stone = INVILID_INT;
  gdata->toendsite_src = NULL;
  gdata->to_nextsite = INVILID_INT;
  gdata->adamsg_stones = NULL;
  gdata->adaptive_source = NULL;
  gdata->mstone_outputs = NULL;

  gdata->to_localmstones = NULL;

  gdata->to_finaloutput = INVILID_INT;

  //gdata->adamsg_ackinfo_stone = INVILID_INT;

  for(i=0; i<NUM_RESOURCES; i++){
    for(j=0; j<NUM_PROCESSES; j++){
      gdata->broadcast_srcs[i][j] = NULL;
      // (for future use)
      gdata->acktopeers_srcs[i][j] = NULL;
    }
  }

  gdata->num_dgprocs= 0;
  for(i=0; i<NUM_DG; i++){
    gdata->dgprocs_contacts[i].remote_stone = INVILID_INT;
    gdata->dgprocs_contacts[i].string_list = NULL;
    gdata->acktodgprocs_srcs[i] = NULL;
  }

  gdata->epars = (exppars *)calloc(1, sizeof(exppars));
  assert(gdata->epars != NULL);
  init_exppars(gdata);

  gdata->reports = 0;

  return;
}

static void init_stragglermaps(int num_multistones)
{
  int i;

  if(num_multistones <= 0){
    return;
  }

  stragglers = (straggler_map_t **)calloc(num_multistones, sizeof(straggler_map_t));
  assert(stragglers != NULL);
  for(i=0; i<num_multistones; i++){
    stragglers[i] = (straggler_map_t *)calloc(1, sizeof(straggler_map_t));
    assert(stragglers[i]);

    stragglers[i]->groupIDs = (int *)calloc(1, sizeof(int));
    assert(stragglers[i]->groupIDs != NULL);
  }

  return;
}

  static void 
init_tracefile(int clusterid)
{
  char file_name[50];
  memset(&file_name[0], '\0', 50);
  int dg_procs = GNUM/OBJ_PER_STREAM;

  if(clusterid == ENDSITE){
    sprintf(file_name, "./gnum%d/final_data", dg_procs);
    fp_final = open(file_name, O_CREAT, O_RDWR|S_IRUSR|S_IWUSR);
    if(fp_final == -1){
      printf("fp_final open error! %s\n", strerror(errno));
    }
    assert(fp_final >= 0);

    return;
  }

  if (clusterid == DG){
    return;
  }

  // for pipeline clusters
  sprintf(file_name, "./gnum%d/R%drank%d_p1time", dg_procs, clusterid, client_data->rank);
  fp_p1_time = open(file_name, O_CREAT, O_RDWR|S_IRUSR|S_IWUSR);
  assert(fp_p1_time >= 0);

  memset(&file_name[0], '\0', 50);
  sprintf(file_name, "./gnum%d/R%drank%d_p2time", dg_procs, clusterid, client_data->rank);
  fp_p2_time = open(file_name, O_CREAT, O_RDWR|S_IRUSR|S_IWUSR);
  assert(fp_p2_time >= 0);

  memset(&file_name[0], '\0', 50);
  sprintf(file_name, "./gnum%d/R%drank%d_p3time", dg_procs, clusterid, client_data->rank);
  fp_p3_time = open(file_name, O_CREAT, O_RDWR|S_IRUSR|S_IWUSR);
  assert(fp_p3_time >= 0);

#if 0
  // to store the queuelevel with timestamp
  fp_qlevel =(int *)calloc(client_data->num_multiactions, sizeof(int));
  assert(fp_qlevel);
  int i;
  for(i=0; i<client_data->num_multiactions; i++){
    memset(&file_name[0], '\0', 50);
    sprintf(file_name, "./gnum%d/R%drank%dQ%d_level", dg_procs, clusterid, client_data->rank, i+1);
    fp_qlevel[i] = open(file_name, O_CREAT, O_RDWR|S_IRUSR|S_IWUSR);
    assert(fp_qlevel[i] >= 0);
  }


  memset(&file_name[0], '\0', 50);
  sprintf(&file_name[0], "./gnum%d/R%drank%d_datatimer", dg_procs, clusterid, client_data->rank);
  fp_datatimer = open(file_name, O_CREAT, O_RDWR|S_IRUSR|S_IWUSR);
  assert(fp_datatimer >= 0);

  memset(&file_name[0], '\0', 50);
  sprintf(&file_name[0], "./gnum%d/R%drank%d_interdataupload_withbuff_timer", dg_procs, clusterid, client_data->rank);
  fp_wbuff_timer = open(file_name, O_CREAT, O_RDWR|S_IRUSR|S_IWUSR);
  assert(fp_wbuff_timer >= 0);

  memset(&file_name[0], '\0', 50);
  sprintf(&file_name[0], "./gnum%d/R%drank%d_interdataupload_nobuff_timer", dg_procs, clusterid, client_data->rank);
  fp_wobuff_timer = open(file_name, O_CREAT, O_RDWR|S_IRUSR|S_IWUSR);
  assert(fp_wobuff_timer >= 0);
#endif

  return;
}

  static void
setup_ctrlpaths(handler_owner *gdata)
{
  assert(gdata != NULL);
  assert(gdata->num_multiactions > 0);
  int **which_stones = NULL, i; 

  // setup for contact file readiness-related mon-data
  gdata->contactorready_stone= EValloc_stone(gdata->cm);
  EVassoc_terminal_action(gdata->cm, 
      gdata->contactorready_stone, 
      contactsready_msg_format_list, contactinfo_ready_handler, NULL);

  // setup for queueing complete streams and 
  // then apply a phase funcs in a seperate thread
  gdata->taskready_stones = (EVstone *)calloc(gdata->num_multiactions, sizeof(EVstone));
  assert(gdata->taskready_stones != NULL);
  which_stones = (int **)calloc(gdata->num_multiactions, sizeof(int *));
  assert(which_stones != NULL);

  for(i=0; i<gdata->num_multiactions; i++){
    which_stones[i] = (int *)calloc(1, sizeof(int));
    assert(which_stones[i] != NULL);
    *(which_stones[i]) = i;
    gdata->taskready_stones[i] = EValloc_stone(gdata->cm);
    EVassoc_terminal_action(gdata->cm, 
        gdata->taskready_stones[i], 
        datastream_format_list, taskready_handler, 
        (void *)which_stones[i]);
  }

  // setup for adaptive cap info
  gdata->adamsg_stones = (EVstone *)calloc(gdata->num_multiactions, 
      sizeof(EVstone));
  assert(gdata->adamsg_stones != NULL);
  for(i=0; i<gdata->num_multiactions; i++){
    *(which_stones[i]) = i;
    gdata->adamsg_stones[i] = EValloc_stone(gdata->cm);
    EVassoc_terminal_action(gdata->cm,
        gdata->adamsg_stones[i], 
        adamsg_format_list, adamsg_responsor, (void *)which_stones[i]);
  }

  return;
}

  static void 
setpath_to_localmstones(handler_owner *gdata)
{
  assert(gdata != NULL);
  assert(gdata->num_multiactions > 0);

  gdata->to_localmstones = (EVsource *)calloc(gdata->num_multiactions, sizeof(EVsource));
  assert(gdata->to_localmstones != NULL);

  int i;
  EVstone stone;
  EVaction action;
  for(i=0; i<gdata->num_multiactions; i++){
    stone = EValloc_stone(gdata->cm);
    action = EVassoc_split_action(gdata->cm, stone, NULL);
    EVaction_add_split_target(gdata->cm, stone, action, gdata->multiq_stones[i]);    gdata->to_localmstones[i] = EVcreate_submit_handle(gdata->cm, 
        stone, datastream_format_list);
    assert(gdata->to_localmstones[i] != NULL);

  }

  return;

}
/*===================send the redirec request to the slower unti to balance load==================*/
  static  void
whisper_redirectreq(adamsg *info, FMStructDescRec *msg_format_list, 
    int which_resource, int which_rank)
{
  assert(info != NULL);
  send_adamsg(info, which_resource, which_rank, msg_format_list);

  return;
}

#if 0
  void *
download_statdat(void *in)
{
  char cname[] = "stat_con";
  struct list_head* olist = NULL;
  struct StatusInfo* si = NULL;
  int i;

  sleep(10);
  adamsg adamsg_info;
  int ql = 0;
  while(1){
    if(ret_localqstat() > QL_STEALER_THRESHOLD) {PULL_WAIT; continue;}
    olist = _download_statusobjs(token, purl, cname);
    if(!olist) {PULL_WAIT; continue;}
    si    = id_offloader(olist);
    if(!si) {PULL_WAIT; continue;}

    // to whisper
    printf("picked offloader cid#%d, pid#%d\n", si->cid, si->pid);
    for(i=0; i<MSTONE_NUM; i++){
      printf("tasks[%d], estime[%d] = %d, %zu\n", i, i, si->tasks[i], si->estime[i]);
    }
    adamsg_info.resourceID = client_data->itself;
    adamsg_info.rankID = client_data->rank;
    adamsg_info.type = REDIRECT_REQUEST;
    //printf("!!!resource %d, proc %d is sending out REDIRECT_REQUEST\n", adamsg_info.resourceID, adamsg_info.rankID);
    whisper_redirectreq(&adamsg_info, adamsg_format_list, si->cid, si->pid);
    // end

    free_objects(olist);
    olist = NULL;
    si    = NULL;

    ADA_MONDOWN_PERIOD;
  }

  return NULL;
}
#endif

/*=======================for upload_statdat===============================================*/
/* to decide if the local queuelevels run empty;
 * If so, initiate the idle_broadcast
 *
 * */
int ret_localqstat()
{
  struct StatusInfo si;
  _construct_statinfo(&si);   

  int i;
  int ql = 0;
  for(i=0; i<MSTONE_NUM; i++){
    ql += si.tasks[i];
  }
  //printf("\n@@@(cid %d, pid %d)Current localqstat = %d@@@\n", si.cid, si.pid, ql);

  return ql; 
}

// to compute the total jobs in a "qid" jqs (accouting for all the streams)
static int _compute_qsize(struct list_head *qhead)
{
  assert(qhead);
  struct list_head* sqhead = NULL;

  struct Stream* stream = NULL;
  struct Job* job = NULL;
  int jobs = 0;
  list_for_each_entry(stream, qhead, link){
    sqhead = stream->qhead; 

    list_for_each_entry(job, sqhead, link){
      if(job->jhead == NULL){
        jobs += job->jobs;
        break;
      }
    }
  }

  return jobs;
}

static void _construct_statinfo(struct StatusInfo* si)
{
  assert(si);

  int i, jobs = 0;
  for(i=0; i<client_data->num_multiactions; i++){
    pthread_mutex_lock(&(jqs->qlocks[i]));
    //si->tasks[i] = jqueues[i]->tasks;
    //si->estime[i] = jqueues[i]->tasks*2;
    #if 0
    qhead = &(jqs->qheads[i]);
    first = list_first_entry(qhead, struct Job, link);
    si->tasks[i] = first->jobs;
    si->estime[i] = first->jobs*2;
    #endif
    jobs = _compute_qsize(&(jqs->qheads[i]));

    si->tasks[i] = jobs;
    si->estime[i] = jobs*2;
    pthread_mutex_unlock(&(jqs->qlocks[i]));
  }
  si->cid = client_data->itself;
  si->pid = client_data->rank;

  return;
}

void construct_statobj(struct MemoryStruct* sdata, struct StatusInfo* si)
{
  assert(sdata && si);
  _construct_statinfo(si);

  sdata->memory = (char *)si;
  sdata->bytesize = sizeof(struct StatusInfo); // ?

  return;
}

/*=============================================================================*/
static void obtain_curclusters_contacts(handler_owner *gdata)
{
  assert(gdata != NULL);

#ifdef DEBUG
  fprintf(stderr, "@readingin existing clusters in the sys\n");
#endif

  char contact_info_filename[250];
  char contact_ready_filename[250];
  sprintf(contact_info_filename, "%s", CONTACT_INFO_FILE);
  sprintf(contact_ready_filename, "%s", FORUPSTREAM_CONTACT_READY_FILE);

  FILE *fp_in = NULL;
  //char in_contact[250];
  //memset(in_contact, 0, 250);
  char in_contact[50] = "";
  int rstone, num_procs, rank;
  int clustersize, clusteridx, rankidx; 
  int ret;

  // if too many processes open the file, 
  // may need to switch to rank 0 read and mpi broadcast
  fp_in = fopen(contact_ready_filename, "r");
  while(!fp_in){
    fprintf(stderr, "\nECHO: Waiting for downstream contact info to be ready\n");
    fp_in = fopen(contact_ready_filename, "r");
  }
  fclose(fp_in);

  fp_in = fopen(contact_info_filename, "r");
  while(!fp_in){
    fp_in = fopen(contact_info_filename, "r");
  }

  while( (ret = fscanf(fp_in, "%d:%d:%d:%s", &num_procs, &rank, 
          &rstone, in_contact)) != EOF ){
    if(ret == 1){ // a block for a new resource location starts
      clustersize = num_procs; 
      gdata->resource_mpisizes[gdata->num_resources] = clustersize;
      gdata->num_resources += 1;
      rankidx = 0;
    }
    else{ // still in the same block as the last resource
      clusteridx = gdata->num_resources - 1;
      gdata->downstream_contacts[clusteridx][rankidx].remote_stone = 
        rstone;
      gdata->downstream_contacts[clusteridx][rankidx].string_list = 
        strdup(in_contact);
      assert(gdata->downstream_contacts[clusteridx][rankidx].string_list 
          != NULL);
      if(clusteridx > 0){ // not counting the endsite
        gdata->reports += 1;
      }

      rankidx += 1; 
    }

  }

  fclose(fp_in);

  return;

}

/* to be invoked inside in-transit resource location
 * 
 * 1) to set up the bridge to the end site
 * 2) to set up the bridge to one of the resource location
 *    For now, it will be chosen as the newly registered resource location
 *    in the system
 *    And the specific rank is RR_based.
 *
 */
static  void 
connecto_curclusters(handler_owner *gdata)
{
  assert(gdata != NULL);

  int i, j, rc;
  int rank = gdata->rank;
  CManager cm = gdata->cm;
  int map_size = gdata->map_size;

  char *rstring = NULL;
  attr_list rcontact = NULL;
  EVstone rstone;
  int which_rank, which_cluster, clustersize, batch_size;

  // to set up the bridge to end site
  gdata->toendsite_stone = EValloc_stone(cm);
  clustersize = gdata->resource_mpisizes[ENDSITE]; // the end site block
  which_rank = rank % (clustersize);
  rstring = strdup(gdata->downstream_contacts[ENDSITE][which_rank].string_list);
  assert(rstring != NULL);
  rcontact = attr_list_from_string(rstring);
  rstone = gdata->downstream_contacts[ENDSITE][which_rank].remote_stone;
  EVassoc_bridge_action(cm, gdata->toendsite_stone, rcontact, rstone);
  gdata->toendsite_src = EVcreate_submit_handle(cm, 
      gdata->toendsite_stone, 
      datastream_format_list);
  assert(gdata->toendsite_src != NULL);
  free(rstring);

  // to set up the bridge to the next neighbout site's rank: RR-based
  rstring = NULL;
  rcontact = NULL;
  gdata->to_nextsite = EValloc_stone(cm);

  if(gdata->num_resources == 1){ /* the last resource station */
    batch_size = (gdata->size); 
    which_cluster = 0; /* 0 is endsite */
  }
  else {
    //batch_size = ceil( (1.0*(gdata->size)) / (gdata->num_resources - 1)); 
    batch_size = ( gdata->size%(gdata->num_resources - 1) == 0?(gdata->size/(gdata->num_resources - 1)):(gdata->size/(gdata->num_resources - 1)+1) );
    which_cluster = (gdata->rank) / batch_size + 1; /* 0 is endsite */
  }
  assert(which_cluster < gdata->num_resources);
  clustersize = gdata->resource_mpisizes[which_cluster];
  which_rank = (gdata->rank) % clustersize;
  assert(which_rank < gdata->resource_mpisizes[which_cluster]);

  gdata->mappedto_cluster = which_cluster;
  gdata->mappedto_rank = which_rank;
  rstring = strdup(gdata->downstream_contacts[which_cluster][which_rank].string_list);
  assert(rstring != NULL);
  rcontact = attr_list_from_string(rstring);
  rstone = gdata->downstream_contacts[which_cluster][which_rank].remote_stone;
  EVassoc_bridge_action(cm, gdata->to_nextsite, rcontact, rstone);
  free(rstring);

  return;
}

  static  void
build_mstones_outputmap(handler_owner *gdata, int clusterid)
{
  assert(gdata != NULL); 
  int num_multiactions = gdata->num_multiactions;

  int output_index = 0; 
  gdata->mstone_outputs = (mstone_outputs_t *) calloc(num_multiactions, 
      sizeof(mstone_outputs_t));
  assert(gdata->mstone_outputs != NULL);

  int i;
  if(clusterid == DG){
    for(i=0; i<num_multiactions; i++){
      gdata->mstone_outputs[i].to_nextsite = output_index;
      EVaction_set_output(gdata->cm, gdata->multiq_stones[i],
          gdata->multiq_actions[i], output_index++, 
          gdata->to_nextsite);
    }
    return;
  }

  // otherwise: not DG
  for(i=0; i<num_multiactions; i++){
    // extra info at ENDSITE
    if(clusterid == ENDSITE){ 
      gdata->mstone_outputs[i].to_finaloutput = output_index;
      EVaction_set_output(gdata->cm, gdata->multiq_stones[i],
          gdata->multiq_actions[i], output_index++, 
          gdata->to_finaloutput);
    }

    // only mstone 0 has thess control info
    if(i==0){
      gdata->mstone_outputs[0].to_contactready = output_index;
      EVaction_set_output(gdata->cm, gdata->multiq_stones[0],
          gdata->multiq_actions[0], output_index++, 
          gdata->contactorready_stone);

      gdata->mstone_outputs[i].to_adamsg = output_index;
      EVaction_set_output(gdata->cm, gdata->multiq_stones[i],
          gdata->multiq_actions[i], output_index++, 
          gdata->adamsg_stones[i]);
    }

    // for all mstones: the same
    gdata->mstone_outputs[i].to_taskready = output_index;
    EVaction_set_output(gdata->cm, gdata->multiq_stones[i],
        gdata->multiq_actions[i], output_index++, 
        gdata->taskready_stones[i]);
    if(clusterid != ENDSITE){
      gdata->mstone_outputs[i].to_endsite = output_index;
      EVaction_set_output(gdata->cm, gdata->multiq_stones[i],
          gdata->multiq_actions[i], output_index++, 
          gdata->toendsite_stone);
    }


    // for the mstones from 0 - (the second last): does have local next_stage
    if(i < (num_multiactions-1)){
      gdata->mstone_outputs[i].to_nextphase = output_index;
      EVaction_set_output(gdata->cm, gdata->multiq_stones[i],
          gdata->multiq_actions[i], output_index++, 
          gdata->multiq_stones[i+1]);

      if(clusterid != ENDSITE){
        gdata->mstone_outputs[i].to_nextsite = output_index;
        EVaction_set_output(gdata->cm, gdata->multiq_stones[i],
            gdata->multiq_actions[i], output_index++, 
            gdata->to_nextsite);
      }
    }
    else{ // the last stage: does not have a local next_stage
      if(clusterid != ENDSITE){
        gdata->mstone_outputs[i].to_nextphase = output_index;
        gdata->mstone_outputs[i].to_nextsite = output_index;
        EVaction_set_output(gdata->cm, gdata->multiq_stones[i],
            gdata->multiq_actions[i], output_index, 
            gdata->to_nextsite);
      }
    }

  }

  return;
}


/******************************************/
// routines for dynamic queue implementation
static void init_jqueues(struct JQueues **jqs)
{
#if 0
  if (!jqs || !*jqs)
    return -1;
#endif
  assert(jqs);
  *jqs = (struct JQueues*)calloc(1, sizeof(struct JQueues));
  assert((*jqs));

  int i, qcount = client_data->num_multiactions;
  (*jqs)->qlocks = (pthread_mutex_t *)calloc(qcount, sizeof(pthread_mutex_t));
  (*jqs)->qheads = (struct list_head *)calloc(qcount, sizeof(struct list_head));
  assert((*jqs)->qlocks && (*jqs)->qheads);

  struct Job *first;
  for(i=0; i<qcount; i++){
    printf("Initing Queue#%d Info\n", i);
    pthread_mutex_init(&((*jqs)->qlocks[i]), NULL);
    INIT_LIST_HEAD(&((*jqs)->qheads[i]));

#ifdef ONESTREMDESIGN
    // to init the very first and special element (counter)
    first = (struct Job *)calloc(1, sizeof(struct Job));
    assert(first);
    first->jobs = 0;
    first->jhead = NULL;
    list_add_tail(&(first->link), &((*jqs)->qheads[i])); // head->first->...
#endif
  }

  // init faultybuffer 
  faultyhead = (struct list_head*)calloc(1, sizeof(struct list_head));
  assert(faultyhead);
  INIT_LIST_HEAD(faultyhead);

  int ret;
  if(clusterid != ENDSITE){
    //ret = pthread_create(&adaptive_monitor_thread, NULL, upload_statdat, NULL);
    //assert(ret == 0);

    //ret = pthread_create(&statdat_download_thread, NULL, download_statdat, NULL);
    //assert(ret == 0);

    //ret = pthread_create(&analysis_thread, NULL, analysis_engine, NULL);
    //assert(ret == 0);
    printf("created complete_analysis_engine thread...\n");
    ret = pthread_create(&analysis_thread, NULL, complete_analysis_engine, NULL);
    assert(ret == 0);
  }

  return;
}

static int is_newjob(int streamID, int gid, int qid, struct JQueues *jqs)
{
  int ret;

  struct list_head *qhead = NULL;
  qhead = &(jqs->qheads[qid]);
  struct list_head *sqhead = NULL; // the stream struct->qhead

  pthread_mutex_lock(&(jqs->qlocks[qid]));

  sqhead = locate_streamqhead(streamID, qhead);
  ret = _is_newjob(gid, sqhead);

  pthread_mutex_unlock(&(jqs->qlocks[qid]));

  return ret; 
}

struct list_head* locate_streamqhead(int streamID, struct list_head* qhead)
{
  assert(qhead);
  struct list_head* sqhead = NULL;

  struct Stream* stream = NULL;
  list_for_each_entry(stream, qhead, link){
    if(stream->streamID == streamID){
      //printf("JOB located its StreamID #%d\n", streamID);
      sqhead = stream->qhead;
      break;
    }
  }

  return sqhead;
}

static int _is_newjob(int gid, struct list_head *qhead)
{
  int ret = TRUE;
  struct list_head *jhead = NULL;
  struct Job *job = NULL;
  struct JItem *first = NULL; 

  list_for_each_entry(job, qhead, link){
    if(job->jhead == NULL) continue; // skip the "first" element

    jhead = job->jhead;
    first = list_first_entry(jhead, struct JItem, link);
    assert(first->ds == NULL);
    if(first->gid == gid){
      //printf("@@@Job#%d NOT a new job with exisiting #%d members\n\n", first->gid, first->items);
      ret = FALSE;
      break;
    }
  }

  return ret;
}

static int is_newstream(int streamID, int qid, struct JQueues *jqs)
{
  assert(jqs);

  struct list_head *qhead = NULL;
  int ret;

  qhead = &(jqs->qheads[qid]);
  pthread_mutex_lock(&(jqs->qlocks[qid]));

  ret = _is_newstream(streamID, qhead);

  pthread_mutex_unlock(&(jqs->qlocks[qid]));

  return ret;
}

static int _is_newstream(int streamID, struct list_head *qhead)
{
  int ret = TRUE;
  struct Stream *stream = NULL;

  list_for_each_entry(stream, qhead, link){
    if(stream->streamID == streamID){
      //printf("@@@Stream#%d not a new stream\n", streamID);
      ret = FALSE;
      break;
    }
  }

  return ret;
}

static void init_stream_container(struct Stream **stream, int streamID)
{
  assert(stream);
  *stream = (struct Stream*)calloc(1, sizeof(struct Stream));
  assert(*stream);

  (*stream)->streamID = streamID;
  (*stream)->qhead = (struct list_head *)calloc(1, sizeof(struct list_head));
  assert((*stream)->qhead);
  INIT_LIST_HEAD((*stream)->qhead);

  struct Job *first = (struct Job*)calloc(1, sizeof(struct Job));
  assert(first);
  first = (struct Job *)calloc(1, sizeof(struct Job));
  assert(first);
  first->jobs = 0;
  first->jhead = NULL;

  list_add_tail(&(first->link), (*stream)->qhead);

  return;
}

static void add_stream_container(struct Stream *stream, int qid, struct JQueues *jqs)
{
  assert(stream && jqs);

  struct list_head *qhead = &(jqs->qheads[qid]);
  pthread_mutex_lock(&(jqs->qlocks[qid]));

  _add_stream_container(stream, qhead);

  pthread_mutex_unlock(&(jqs->qlocks[qid]));

  return;
}

static void _add_stream_container(struct Stream *stream, struct list_head *qhead)
{
  list_add_tail(&(stream->link), qhead);

  return;
}

// to be called every time when a new job arrives at the system
// to init the job "head" & "metadata element (first)"
static void init_job_container(struct Job **job, int gid)
{
  assert(job);
  *job =  (struct Job*)calloc(1, sizeof(struct Job));
  assert((*job));

  (*job)->jobs = INVILID_INT;
  (*job)->jhead = (struct list_head*)calloc(1, sizeof(struct list_head));
  assert((*job)->jhead);
  INIT_LIST_HEAD((*job)->jhead);

  struct JItem *first =(struct JItem*)calloc(1, sizeof(struct JItem));
  assert(first);

  first->gid = gid;
  first->items = 0;
  first->is_ready = 0;
  first->cm = NULL;
  first->ds = NULL;
  list_add_tail(&(first->link), (*job)->jhead); // head->first->...

  return;
}

// to add the job (container) to JQueues->qheads[this]
// unique job's gid is assumed
void add_job_container(struct Job *job, int streamID, int qid, struct JQueues *jqs)
{
  assert(job && jqs);

  struct list_head *sqhead = NULL;
  struct list_head *qhead = &(jqs->qheads[qid]);
  pthread_mutex_lock(&(jqs->qlocks[qid]));

  sqhead = locate_streamqhead(streamID, qhead);
  _add_job_container(job, sqhead);

  pthread_mutex_unlock(&(jqs->qlocks[qid]));

  return;
}

static void _add_job_container(struct Job *job, struct list_head *qhead)
{
  list_add_tail(&(job->link), qhead);

  struct Job *first = list_first_entry(qhead, struct Job, link);
  assert(first->jhead == NULL);
  first->jobs++;
  //printf("Queue holds #%d ready jobs now.\n", first->jobs);
  // may add a new field stating "all the existing jobs including not_ready"

  return;
}

// when a job is complete w.r.t all elements, it will update the "jobs" field at JQueues->jobs
void add_an_item(struct JItem *item, int qid, struct JQueues *jqs)
{
  assert(item && jqs);

  int streamID = ((datastream *)(item->ds))->streamID;
  struct list_head *sqhead = NULL, *qhead = NULL;
  qhead = &(jqs->qheads[qid]);

  pthread_mutex_lock(&(jqs->qlocks[qid]));

  sqhead = locate_streamqhead(streamID, qhead);
  _add_an_item(item, sqhead, qid);

  //print_jobqueue(qhead, qid);

  pthread_mutex_unlock(&(jqs->qlocks[qid]));

  return;
}

static void _add_an_item(struct JItem *item, struct list_head *qhead, int qid)
{
  struct list_head *jhead = NULL;
  struct Job *job = NULL;
  struct JItem *first = NULL; 

  list_for_each_entry(job, qhead, link){
    if(job->jhead == NULL) continue; // skip the "first" element

    jhead = job->jhead;
    first = list_first_entry(jhead, struct JItem, link);
    assert(first->ds == NULL);
    if(first->gid == item->gid){
      list_add_tail(&(item->link), jhead);

      first->items++;
      _mark_a_readyjob(first, qid);
      break;
    }
  }

  return;
}

static struct JItem* assemble_an_jobitem(void *ds, void *cm, int gid)
{
  assert(ds && cm);

  struct JItem *item = (struct JItem*)calloc(1, sizeof(struct JItem));
  assert(item);
  item->gid = gid;
  item->items = INVILID_INT;
  item->is_ready = INVILID_INT;
  item->cm = cm;
  item->ds = ds;

  return item;
}

struct JobsToProcess* passive_dequeue(struct JQueues *jqs)
{
  if(!jqs) return NULL;  // not ready yet

  // to decide which Q to offload
  struct StatusInfo si;
  int qid;
  _construct_statinfo(&si);
  qid = _pick_an_offqueue(&si);
  if (qid == 0){ 
    if (si.tasks[qid] == 0){
      return NULL;
    }
    else{
      printf("Should not have picked QID #%d\n", qid);
      exit(0); 
    }
  }
  //printf("@@@pick Qid #%d as offqueue\n", qid);

  // to decide how many to offload
  // do offload
  struct JobsToProcess *j2p = NULL;
  j2p = (struct JobsToProcess *)calloc(1, sizeof(struct JobsToProcess));
  assert(j2p);
  j2p->count = 0;

  struct list_head *sqhead = NULL, *qhead = &(jqs->qheads[qid]);
  struct Stream* stream = NULL;
  int count;
  int jobs;

  //printf("@@@Passively Dequeueing @Queue #%d\n", qid);
  pthread_mutex_lock(&(jqs->qlocks[qid]));
  
  jobs = _compute_qsize(qhead);
  count = jobs * QL_OFFLOADER_THRESHOLD;
  if(count == 0){
    pthread_mutex_unlock(&(jqs->qlocks[qid]));
    free(j2p);
    return NULL;
  }

  j2p->jobs = (struct list_head**)calloc(count, sizeof(struct list_head*));
  assert(j2p->jobs);
  list_for_each_entry(stream, qhead, link){
    sqhead = stream->qhead; 
    jobs = _delete_stream_jobs(j2p, sqhead, count);
    //printf("Stream #%d dequeued %d jobs\n", stream->streamID, jobs);
    if(jobs == count) break;
  }

  pthread_mutex_unlock(&(jqs->qlocks[qid]));

  if(j2p->count == 0){ // no dequeued jobs yet
    //printf("j2p->count = %d\n", j2p->count);
    free(j2p->jobs);
    free(j2p);
    return NULL;
  }

  return j2p;
}

struct JobsToProcess* active_dequeue(int count, int qid, struct JQueues *jqs)
{
  if(!jqs) return NULL;  // not ready yet

  struct JobsToProcess *j2p = NULL;
  j2p = (struct JobsToProcess *)calloc(1, sizeof(struct JobsToProcess));
  assert(j2p);
  j2p->jobs = (struct list_head**)calloc(count, sizeof(struct list_head*));
  assert(j2p->jobs);
  j2p->count = 0;

  // go through streamQ 1-by-1 and stop at "j2p->count <= count"
  struct list_head* qhead = &(jqs->qheads[qid]); 
  struct list_head* sqhead = NULL;
  struct Stream* stream = NULL;
  int jobs;

  //printf("@@@Dequeueing @Queue #%d\n", qid);
  pthread_mutex_lock(&(jqs->qlocks[qid]));

  list_for_each_entry(stream, qhead, link){
    sqhead = stream->qhead; // the qhead that points to "first" job

    jobs = _delete_stream_jobs(j2p, sqhead, count);
    //printf("Stream #%d dequeued %d jobs\n", stream->streamID, jobs);
    if(jobs == count) break;
  }

  pthread_mutex_unlock(&(jqs->qlocks[qid]));

  if(j2p->count == 0){ // no dequeued jobs yet
    //printf("j2p->count = %d\n", j2p->count);
    free(j2p->jobs);
    free(j2p);
    return NULL;
  }

  return j2p;
}

// delete jobs from a single streamQ
static int _delete_stream_jobs(struct JobsToProcess *j2p, struct list_head *qhead, int count)
{
  assert(j2p && qhead);

  struct Job *pos, *next, *qfirst;
  list_for_each_entry_safe(pos, next, qhead, link){
    if(pos->jhead == NULL) continue; // "first" metadata
    else{
      j2p->jobs[j2p->count] = pos->jhead; // record: init_job_container-"(*job)->jhead"
      j2p->count++;
      list_del(&(pos->link));
      free(pos); // free: init_job_container-"*job"

      qfirst = list_first_entry(qhead, struct Job, link);
      qfirst->jobs--;
      //printf("-->Remaining %d jobs\n", qfirst->jobs);

      if(j2p->count == count) break;
    }
  }

#if 0
  // for debugging purpose
  struct JItem *ifirst;
  if(!(*jhead)){
    printf("No available to de-queue.\n");
  }
  else{
    ifirst = list_first_entry(*jhead, struct JItem, link);
    assert(ifirst->is_ready);
    printf("De-queued JOB#%d (w/ %d items)\n", ifirst->gid, ifirst->items);
  }
#endif

  return j2p->count;
}

static void _mark_a_readyjob(struct JItem *first, int qid)
{
  assert(first);

  if(first->items == PERGSIZE){
    //printf("JOB#%d (w/ items #%d) turns ready @Q#%d\n", first->gid, first->items, qid);
    first->is_ready = 1;
  }

  return;
}

static void print_jqueue(int qid, struct JQueues *jqs)
{
  assert(jqs);

  struct list_head *qhead = NULL, *jhead = NULL;
  struct Job *job = NULL;
  struct JItem *first = NULL, *item = NULL;
  struct Stream* stream = NULL;
  struct list_head* sqhead = NULL;
  int count = 0; 

  qhead = &(jqs->qheads[qid]);
  pthread_mutex_lock(&(jqs->qlocks[qid]));

  list_for_each_entry(stream, qhead, link){
    sqhead = stream->qhead;

    list_for_each_entry(job, sqhead, link){
      if(job->jhead == NULL) {
        //printf("#####Queue#%d has #%d ready jobs now as JOBS.\n\n",qid, job->jobs);
        continue; 
      }

      jhead = job->jhead;
      list_for_each_entry(item, jhead, link){
        if(item->ds == NULL){
          //printf("@QID #%d: JOB #%d with %d items (is_ready = %d)\n", qid, item->gid, item->items, item->is_ready);
          continue;
        }
        count++;
        //printf("Iterating #%dth item\n", count);
      }
      count = 0;
    }

  }

  pthread_mutex_unlock(&(jqs->qlocks[qid]));

  return;
}

void *monitor_mmusage(void *in)
{
  return NULL;
}

struct _marray** decode_interdataset(struct MemoryStruct *sdata)
{
  if(!sdata) return NULL;

  int l;
  int *dataobjs = (int *)sdata->memory; 
  int dataset = *dataobjs;
  struct _marray **mdatas = (struct _marray**)calloc(dataset, sizeof(struct _marray*));
  if(!mdatas){
    printf("Memory calloc failure\n");
    exit(0);
  }
  for(l=0; l<dataset; l++){
    mdatas[l] = (struct _marray*)calloc(MAX_MFIELD, sizeof(struct _marray)); 
    if(!mdatas[l]){
      printf("Memory calloc failure\n");
      exit(0);
    }
  }

  int *header, start, freq;
  char *base;
  size_t hsize, offset = 0, *bsize_arr;
  double *data = NULL;
  int i, j, M, N, k, bidx = 0;

  // for each single data object
  base = sdata->memory + sizeof(int);
  for(l=0; l<dataset; l++){
    base += offset; 
    header = (int *)base;
    start = header[0];
    freq = header[1];
    bsize_arr = (size_t *)calloc(freq, sizeof(size_t));
    if(!bsize_arr){
      printf("Memory calloc failure\n");
      exit(0);
    }

    offset = 0; // reset offset
    bidx = 0;
    hsize = HEADER_SIZE * sizeof(int);
    offset += hsize;
    for(i=0; i<freq; i++){
      M = header[(i+1)*2];
      N = header[(i+1)*2 + 1];
      bsize_arr[i] = M * N * sizeof(double);
      offset +=  bsize_arr[i];

      if (i==0){
        bidx += hsize;
      }
      else{
        bidx += bsize_arr[i-1];
      }
      //data = (double *)(&sdata->memory[bidx]);
      data = (double *)(&base[bidx]);

      mdatas[l][start+i].data = data;
      mdatas[l][start+i].M = M;
      mdatas[l][start+i].N = N;
      mdatas[l][start+i].tsize = M*N;

      for(k=0; k<M*N; k++){
        printf("M, N = %d, %d, data[%d] = %.4f\n", M, N, k, mdatas[l][start+i].data[k]);
      }
    }
  }

  return mdatas;
}

/* auxiliary routines */
void print_jobqueue(struct list_head *qhead, int qid)
{
  struct list_head *jhead = NULL;
  struct Job  *job = NULL;
  struct JItem *first = NULL;

  list_for_each_entry(job, qhead, link){
    if(job->jhead == NULL) 
    {
      printf("Queue #%d has %d jobs\n", qid, job->jobs);
      continue;
    }

    jhead = job->jhead;
    first = list_first_entry(jhead, struct JItem, link);

    printf("####JOB ID %d has %d items\n", first->gid, first->items);
  }

  return;
}

// create faulty databuffer head
void add_faulty_sbuffer(int streamID) 
{
  struct SBuffer *sbuffer = (struct SBuffer*)calloc(1, sizeof(struct SBuffer));
  assert(sbuffer);

  sbuffer->streamID = streamID;
  sbuffer->dhead = (struct list_head*)calloc(1, sizeof(struct list_head));
  assert(sbuffer->dhead);
  INIT_LIST_HEAD(sbuffer->dhead);

  list_add_tail(&(sbuffer->link), faultyhead);

  return;
}

void add_faulty_dbuffer(void *ds, struct list_head* dhead)
{
  struct DBuffer *dbuffer = (struct DBuffer*)calloc(1, sizeof(struct DBuffer));
  assert(dbuffer);
  dbuffer->ds = ds;

  list_add_tail(&(dbuffer->link), dhead);

  return;
}

//fautly detection
//if <=2 images, assume they are corret, and just return the correct images for futher process
//if =3 images, calculate the intensity of the last image and compare it with the first 2 images
//and decide if the last image is good or bad
//if good, deleted the first image (2 images left in the list)
//and also return the first image;
//otherwise, label this last image as bad image and prepare to upload
//and also empty the list and return all the three image 
int faulty_detection(struct MemoryStruct* sdata, struct JobsToProcess *j2p, int qid, int option)
{
  assert(sdata && j2p);

  int i, streamID, count = 0, ret;
  struct list_head *jhead = NULL, *dhead = NULL;
  struct JItem *item = NULL;
  struct DBuffer *pos = NULL, *next = NULL;

  if (j2p->count > 1){
    printf("(a)Please fill in a new strategy for this situation which is not handled for now\n\n");
    exit(0);
  }

  printf("@@@@failure_detection: j2p->count = %d\n", j2p->count);
  for(i=0; i<j2p->count; i++){
    jhead = j2p->jobs[i];
    list_for_each_entry(item, jhead, link){
      if(item->ds == NULL){
        printf("JOB#%d has %d image items for failure detection\n", item->gid, item->items);
        assert(item->is_ready);
        if(item->items != 1) {
          printf("(b)Please fill in a new strategy for this situation which is not handled for now\n\n");
          exit(0);
        }
        continue;
      }
      
      streamID = ((datastream *)(item->ds))->streamID;
      dhead = locate_faulty_stream(streamID);
      assert(windowsize(dhead) <= 2);
      printf("StreamID %d with current Win Size %d\n", streamID, windowsize(dhead));

      if(windowsize(dhead) <= 1){
        printf("Adding dbufer...\n");
        add_faulty_dbuffer(item->ds, dhead);
        return GOOD;
      }
      if(windowsize(dhead) == 2){
        printf("Detecting fault...\n");
        ret = _faulty_detection(dhead, item->ds);

        if (ret == GOOD){ // good image
          // to move the window buffer forward
          list_for_each_entry_safe(pos, next, dhead, link){  
            list_del(&(pos->link));
            break;
          }
          printf("Adding dbufer...\n");
          add_faulty_dbuffer(item->ds, dhead); 
        }
        else{ // faulty image
          //printf("Fault detected: to upload faultydata with gid%d...\n", ((datastream *)(item->ds))->groupID);
          //upload_faultydata_withbuffer(token, purl, sdata, (datastream *)(item->ds), qid, client_data, BATCHSIZE, option);
          //printf("After packing/uploading fault data, Freeing processed jobs...\n");
        }
      }

    } 
  } 

  if (ret == BAD){
    free_processed_jobs(j2p, qid);
  }

  return ret; // only 1 item and 1 job
}

int windowsize(struct list_head* dhead)
{
  assert(dhead);

  int count = 0;
  struct DBuffer *dbuffer = NULL;
  list_for_each_entry(dbuffer, dhead, link){
    count += 1;
  }

  return count;
}

static int _faulty_detection(struct list_head* dhead, void *ds){
  assert(dhead && ds);
  
  datastream *object = (datastream *)ds;
  if (object->groupID %2 == 0)
  {
    //return BAD;
  }

  return GOOD;
}

struct list_head* locate_faulty_stream(int streamID)
{
  struct SBuffer *sb = NULL;
  list_for_each_entry(sb, faultyhead, link){
    if(sb->streamID == streamID){
      return sb->dhead;
    }
 }
  
  return NULL;
}


// end

#if 0
static int is_newjob(void *msg, int qid, struct JQueues *jqs);
static int _is_newjob(struct list_head *qhead, int gid);
// org
static int is_newjob(void *msg, int qid, struct JQueues *jqs)
{
  assert(msg && jqs);
  datastream *item = (datastream *)msg;

  int ret = 0;
  struct list_head *qhead = NULL;

  qhead = &(jqs->qheads[qid]);
  pthread_mutex_lock(&(jqs->qlocks[qid]));

  ret = _is_newjob(qhead, item->groupID);

  pthread_mutex_unlock(&(jqs->qlocks[qid]));

  return ret;
}

// org
static int _is_newjob(struct list_head *qhead, int gid)
{
  int ret = TRUE;
  struct list_head *jhead = NULL;
  struct Job *job = NULL;
  struct JItem *first = NULL; 

  list_for_each_entry(job, qhead, link){
    if(job->jhead == NULL) continue; // skip the "first" element

    jhead = job->jhead;
    first = list_first_entry(jhead, struct JItem, link);
    assert(first->ds == NULL);
    if(first->gid == gid){
      printf("@@@Job#%d NOT a new job with exisiting #%d members\n\n", first->gid, first->items);
      ret = FALSE;
      break;
    }
  }

  return ret;
}

static void _delete_jobs(struct JobsToProcess *j2p, struct list_head *qhead, int count);
static int _delete_a_job(struct list_head **jhead, struct list_head *qhead);

static void _delete_jobs(struct JobsToProcess *j2p, struct list_head *qhead, int count)
{
  int i;
  for(i=0; i<count; i++){
    if(_delete_a_job(&(j2p->jobs[i]), qhead)){
      j2p->count++;
    }
  }

  return;
}

static int _delete_a_job(struct list_head **jhead, struct list_head *qhead)
{
  assert(jhead && qhead);

  int ret = 0;
  struct Job *pos, *next, *qfirst;
  list_for_each_entry_safe(pos, next, qhead, link){
    if(pos->jhead == NULL) continue; // "first" metadata
    else{
      *jhead = pos->jhead; // record: init_job_container-"(*job)->jhead"
      list_del(&(pos->link));
      free(pos); // free: init_job_container-"*job"
      ret = 1;

      qfirst = list_first_entry(qhead, struct Job, link);
      qfirst->jobs--;
      printf("-->Remaining %d jobs\n", qfirst->jobs);
      break;
    }
  }

#if 0
  // for debugging purpose
  struct JItem *ifirst;
  if(!(*jhead)){
    printf("No available to de-queue.\n");
  }
  else{
    ifirst = list_first_entry(*jhead, struct JItem, link);
    assert(ifirst->is_ready);
    printf("De-queued JOB#%d (w/ %d items)\n", ifirst->gid, ifirst->items);
  }
#endif

  return ret;
}

#endif


#if 0
struct _marray* decode_interdata(struct MemoryStruct *sdata)
{
  if(!sdata) return NULL;
  int *header = (int *)sdata->memory;
  size_t hsize = HEADER_SIZE * sizeof(int);
  int start = header[0];
  int elements  = header[1];
  printf("@@@Expected data chucks: start, elements = %d, %d\n", start, elements);

  struct _marray *mdatas = (struct _marray*)calloc(MAX_MFIELD, sizeof(struct _marray));
  size_t *bsize_arr = (size_t *)calloc(elements, sizeof(size_t));
  if(!bsize_arr || !mdatas){
    printf("Memory calloc failure\n");
    exit(0);
  }

  double *data = NULL;
  int i, j, M, N, k;
  size_t bidx = 0;
  for(i=0; i<elements; i++){
    M = header[(i+1)*2];
    N = header[(i+1)*2 + 1];
    bsize_arr[i] = M * N * sizeof(double);

    if (i==0){
      bidx += hsize;
    }
    else{
      bidx += bsize_arr[i-1];
    }
    data = (double *)(&sdata->memory[bidx]);

    mdatas[start+i].data = data;
    mdatas[start+i].M = M;
    mdatas[start+i].N = N;
    mdatas[start+i].tsize = M*N;

    for(k=0; k<mdatas[start+i].tsize; k++){
      printf("M, N = %d, %d, data[%d] = %.4f\n", M, N, k, mdatas[start+i].data[k]);
    }
  }

  return mdatas;
}
#endif

#if 0 
// Note: sdata->bytesize in BYTES
void construct_dataobj(struct MemoryStruct* sdata, void* data, size_t size)
{
  assert(sdata && data);

  sdata->memory = (char *)data;
  sdata->bytesize = size * sizeof(double); 
  //printf("data addr %p, size %zu (bytesize = %zu)\n", data, size, sdata->bytesize);

  return;
}

// this is retired function by _onestep_upload
//static void _serial_upload(const char *token, const char *url, const char *cname, struct MemoryStruct *sdata, int start, int elements, datastream *ds);
// upload one object by one object
static void _serial_upload(const char *token, const char *url, const char *cname, struct MemoryStruct *sdata, int start, int elements, datastream *ds)
{
  int i;
  char oname[ONAME_STR_SIZE];
  for(i=0; i<elements; i++){
    construct_dataobj(sdata, (void *)ds->mdatas[start+i].data, ds->mdatas[start+i].tsize);
    memset(oname, 0, ONAME_STR_SIZE);
    snprintf(oname, ONAME_STR_SIZE, "data%sdatname%d", ds->srcname, i);
    printf("Uploading oname %s to cname %s\n", oname, cname);
    upload_object(token, url, cname, oname, sdata);
  }

  return;
}
#endif

#if 0
// to compose a single "complete data" with individual chuncks
// "start" is to point out the position of "mdata" in "ds" to upload
// "elements" is the number of elements of this "mdata" to upload
// Note that this _onestap_upload is per dataouput basis
static void _onestep_upload(const char *token, const char *url, const char *cname, struct MemoryStruct *sdata, int start, int elements, datastream *ds);
static void _onestep_upload(const char *token, const char *url, const char *cname, struct MemoryStruct *sdata, int start, int elements, datastream *ds)
{
  char *freep = NULL;
  char oname[ONAME_STR_SIZE];
  int i;

  construct_dataobj(sdata, start, elements, ds);

  freep = sdata->memory;
  memset(oname, 0, ONAME_STR_SIZE);
  snprintf(oname, ONAME_STR_SIZE, "data%sdatname", ds->srcname);
  printf("Uploading oname %s to cname %s bsize %lu\n", oname, cname, sdata->bytesize);
  upload_object(token, url, cname, oname, sdata);
  //printf("@@@@@@@@@@@@@@@@TESTING decoding...\n\n");
  //decode_interdata(sdata);

  //free(sdata->memory); // segfault because sdata->memory is modified
  //sdata->memory = NULL;
  free(freep);
  freep = NULL;

  return;
}
#endif

// for debugging
int apply_complete_analysis(int qid, struct JobsToProcess *j2p, handler_owner *gdata)
{
  if( (qid < 0) || (qid >= MSTONE_NUM) || (!j2p) || (!gdata) ){
    return -1;
  }
  if( j2p->count <= 0 ){
    return -1;
  }

  if(qid == 0){
    complete_workflow(j2p, gdata);
  }

  return 0;
}

  void *
complete_analysis_engine(void *in)
{
  // to readin all the image (mimic the receiver role from evpath way)
  printf("@complete_analysis_engine...\n");
  if(client_data->ep == NULL){
    sleep(50);
  }
  fake_taskready_handler(); 


  int qid = -1, num_tasks, ret;
  struct JobsToProcess *j2p = NULL;

  struct MemoryStruct sdata[3]; // one per queue  (for interdata upload)
  int i;
  for(i=0; i<3; i++){
    sdata[i].memory = (char *)calloc(BATCHSIZE, sizeof(char));
    assert(sdata[i].memory != NULL);
    sdata[i].bytesize = 0;
  }

  struct MemoryStruct faulty_sdata; // for falutydata upload
  faulty_sdata.memory = (char *)calloc(BATCHSIZE, sizeof(char));
  assert(faulty_sdata.memory != NULL);
  faulty_sdata.bytesize = 0;
  static int faulty_count = 0;

  static int finished_tasks = 0; 
  int option = INVILID_INT;
  while(1){
    ret = obtain_processing_decision(qid, &qid, &num_tasks, finished_tasks, faulty_count);
    //printf("Current Decision: QID with NUM_tasks (%d,%d)\n", qid,num_tasks);
    if (ret == CHANGED){
      finished_tasks = 0;
      option = INVILID_INT;
    }

    j2p = active_dequeue(num_tasks, qid, jqs); 
    if(!j2p){
      sleep(2);
      continue;
    }
    finished_tasks += j2p->count;
    //printf("Current Finished tasks = %d\n", finished_tasks);
    if (qid == 0){
      if(finished_tasks == TWORKLOAD) option = LAST;
    }
    else{
      if(finished_tasks == (TWORKLOAD - faulty_count)) option = LAST;
    }

    printf("\nFinished #%d tasks and to check out next task\n", finished_tasks);
    apply_complete_analysis(qid, j2p, client_data);

    if (qid == 0){
      // faulty image filter
      ret = faulty_detection(&faulty_sdata, j2p, qid, option);  

      if (ret == BAD) faulty_count += 1;
      //printf("(ret = %d) At-The-Moment faulty count = %d\n", ret, faulty_count);
    }
    if ( ((qid == 0)&&(ret == GOOD)) || (qid != 0) ) {
      fake_disseminate_data(&(sdata[qid]), qid, j2p, client_data, option);
    }

  } 

  return NULL;
}

void
//fake_taskready_handler(CManager cm, void *vevent, void *c_data, attr_list attrs)
fake_taskready_handler()
{
 
  int qid = 0, groupID;
  datastream *ds = NULL;
  int i;

  // assemble_datastream
  datastream *streamobj = NULL;
  streamobj = (datastream *)calloc((IMAGE_COUNT/client_data->size+1), sizeof(datastream));
  assert(streamobj != NULL);
  for(i=0; i <((GNUM)/(client_data->size)); i++) {
    groupID = client_data->rank*((GNUM)/(client_data->size)) + (i+1);

    //printf("Rank#%d start to init_stream %d with GID#%d\n", client_data->rank, i, groupID);
    fake_init_stream(&streamobj[i], client_data);

    printf("Rank#%d assembling %dth stream object/image\n", client_data->rank, groupID);
    assemble_streamObjs(client_data->ep, &streamobj[i], groupID);
  }

  for(i=0; i < ((GNUM)/(client_data->size)); i++) {
    fprintf(stderr, "client_data->rank%d sent out %dth datastreams srcID#%d, GID#%d\n", 
        client_data->rank, i, streamobj[i].srcID, streamobj[i].groupID);
    ds = &streamobj[i];

    struct Stream *stream = NULL;
    if(is_newstream(ds->streamID, qid, jqs)){
      // to add a new list for the new stream to hold window-size buffer
      add_faulty_sbuffer(ds->streamID);

      init_stream_container(&stream, ds->streamID);
      add_stream_container(stream, qid, jqs);
    }

    struct Job *job = NULL;
    if(is_newjob(ds->streamID, ds->groupID, qid, jqs)){
      init_job_container(&job, ds->groupID);
      add_job_container(job, ds->streamID, qid, jqs);
    }

    struct JItem *item = assemble_an_jobitem((void *)ds, (void *)client_data->cm, ds->groupID);
    assert(item);
    add_an_item(item, qid, jqs);
  }

  return;

}

void fake_disseminate_data(struct MemoryStruct* sdata, int qid, struct JobsToProcess *j2p, handler_owner *gdata, int option)
{
  assert(sdata && j2p && gdata);

  // control scheme on transferring intermediate result
  // 1. to_local  nextQ
  // 2. to_remote nextQ
  // 3. to_cloud  container
  int i;
  EVsource dest;
#if 0
  if(qid != (gdata->num_multiactions-1)) // !last_stage: 1
    dest = gdata->to_localmstones[qid+1];
  else{
    dest = gdata->toendsite_src; // 2
  }
#endif
    dest = gdata->toendsite_src; // 2

  int cur_opt = INVILID_INT;
  struct list_head *jhead = NULL;
  struct JItem *item = NULL;
  for(i=0; i<j2p->count; i++){
    //printf("Disseminating JOB #%d\n", i);
    if (i==(j2p->count-1)) cur_opt = option; // to mark the very last one

    jhead  = j2p->jobs[i];
    list_for_each_entry(item, jhead, link){
      if(item->ds == NULL){
        //printf("JOB#%d has %d items\n", item->gid, item->items);
        assert(item->is_ready);
        continue;
      }
      //printf("\n###Disseminating JOB#%d item\n", item->gid);
      //upload_interdata_withbuffer(token, purl, sdata, (datastream *)(item->ds), qid, gdata, BATCHSIZE, cur_opt); // 3
      //upload_interdata_nobuffer(token, purl, (datastream *)(item->ds), qid, gdata); // 3
      EVsubmit(dest, item->ds, NULL); // 1 or 2

      char msg[200];
      if (qid == 2){
        sprintf(msg, "end:image%d_R%drank%d", ((datastream *)item->ds)->srcID, client_data->itself, client_data->rank);
        //request_ts(msg);
      }

    }

  }

  // to release evpath space & my space
  fake_free_processed_jobs(j2p, qid); 

  return;
}

void fake_free_processed_jobs(struct JobsToProcess *j2p, int qid)
{
  assert(j2p);

  int i;
  struct list_head *jhead = NULL;
  struct JItem *item = NULL, *tmp = NULL;
  for(i=0; i<j2p->count; i++){
    jhead  = j2p->jobs[i];
    list_for_each_entry_safe(item, tmp, jhead, link){
      if(item->ds == NULL){
        //printf("(@Free_JOB) JOB#%d has %d items\n", item->gid, item->items);
        assert(item->is_ready);
        list_del(&(item->link));
        free(item);
        item = NULL;
        continue;
      }

      list_del(&(item->link)); // normal item - assemble_an_jobitem
      if(qid == (get_my_laststageID() - 1)){ // last stage
        //EVreturn_event_buffer((CManager)(item->cm), item->ds);
        free_ds_matdata((datastream *)(item->ds));
      } 
      free(item);
      item = NULL;
    }
    free(j2p->jobs[i]); //free: init_job_container-"(*job)->jhead
  }
  free(j2p);
  j2p = NULL;

  return;
} 

void free_ds_matdata(datastream *ds){
  int i;
  for(i=0; i<MAX_MFIELD; i++){
    free(ds->mdatas[i].data);
    free(ds->srcname);
    free(ds->workflow_nextstep);
    //free(ds->workflows)
    //free(ds->phases)
  }
}
