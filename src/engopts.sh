#
# engopts.sh	Shell script for configuring engine standalone applications.
#               These options were tested with the specified compiler.
#
# usage:        Do not call this file directly; it is sourced by the
#               mbuild shell script.  Modify only if you don't like the
#               defaults after running mbuild.  No spaces are allowed
#               around the '=' in the variable assignment.
#
# Note: For the version of system compiler supported with this release,
#       refer to the Supported and Compatible Compiler List at:
#       http://www.mathworks.com/support/compilers/current_release/
#
#
# SELECTION_TAGs occur in template option files and are used by MATLAB
# tools, such as mex and mbuild, to determine the purpose of the contents
# of an option file. These tags are only interpreted when preceded by '#'
# and followed by ':'.
#
#SELECTION_TAG_SA_OPT: Template Options file for building standalone engine applications
#
# Copyright 1984-2008 The MathWorks, Inc.
# $Revision: 1.30.4.13 $  $Date: 2008/11/04 19:40:05 $
#----------------------------------------------------------------------------
#
    #if ["$MATLAB" = ""]; then
    #  MATLAB=/sw/analysis-x64/matlab/8.0/sles11.1_binary
    #fi
	  #TMW_ROOT="$MATLAB"
    if [ "$TMW_ROOT" = "" ]; then
      TMW_ROOT="/opt/matlab/2013a";
#      TMW_ROOT="/opt/matlab/2010b";
    fi
#    MFLAGS="-I$TMW_ROOT/extern/include -I/sw/sith/ompi/1.6.5/rhel6_pgi13.4/include"
    MFLAGS="-I$TMW_ROOT/extern/include"
    MLIBS="-L$TMW_ROOT/bin/$Arch -leng -lmx"
#    MPIFLAGS="-I/sw/sith/ompi/1.6.5/rhel6_pgi13.4/include"
#    MPILIBS="-L/sw/sith/ompi/1.6.5/rhel6_pgi13.4/lib -lmpi"
#    MCXXFLAGS="-I$TMW_ROOT/extern/include/cpp $MFLAGS"
#    MCXXLIBS="$MLIBS"
    LDEXTENSION=''
    case "$Arch" in
        Undetermined)
#----------------------------------------------------------------------------
# Change this line if you need to specify the location of the MATLAB
# root directory.  The mex script needs to know where to find utility
# routines so that it can determine the architecture; therefore, this
# assignment needs to be done while the architecture is still
# undetermined.
#----------------------------------------------------------------------------
            MATLAB="$MATLAB"
            ;;
        glnxa64)
#----------------------------------------------------------------------------
           # RPATH="-Wl,-rpath-link,$TMW_ROOT/bin/$Arch"
            #CC='mpicc'
            CC='gcc'
#            CFLAGS='-ansi -D_GNU_SOURCE'
#            CFLAGS="$CFLAGS  -fexceptions"
#            CFLAGS="$MFLAGS $MPIFLAGS"
#            CFLAGS=" -std=c99"
#            CFLAGS="-std=standard"
#            CFLAGS="$MFLAGS -g -Wall  -pedantic"
            CFLAGS="$MFLAGS -g -Wall"
            CLIBS="$RPATH $MLIBS -lm"
#            COPTIMFLAGS='-O -DNDEBUG'
            CDEBUGFLAGS='-g'
#            CLIBS="$CLIBS -lstdc++"
#
            LD="$COMPILER"
            LDFLAGS=''
            LDOPTIMFLAGS='-O0'
            LDDEBUGFLAGS='-g'
#
            POSTLINK_CMDS=':'
#----------------------------------------------------------------------------
            ;;
    esac
#############################################################################
#############################################################################
#
# Architecture independent lines:
#
#     Set and uncomment any lines which will apply to all architectures.
#
#----------------------------------------------------------------------------
#           CC="$CC"
#           CFLAGS="$CFLAGS"
#           COPTIMFLAGS="$COPTIMFLAGS"
#           CDEBUGFLAGS="$CDEBUGFLAGS"
#           CLIBS="$CLIBS"
#
#           FC="$FC"
#           FFLAGS="$FFLAGS"
#           FOPTIMFLAGS="$FOPTIMFLAGS"
#           FDEBUGFLAGS="$FDEBUGFLAGS"
#           FLIBS="$FLIBS"
#
#           LD="$LD"
#           LDFLAGS="$LDFLAGS"
#           LDOPTIMFLAGS="$LDOPTIMFLAGS"
#           LDDEBUGFLAGS="$LDDEBUGFLAGS"
#           LDEXTENSION="$LDEXTENSION"
#----------------------------------------------------------------------------
#############################################################################
