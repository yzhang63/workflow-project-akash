#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <pthread.h>

#include "straggler_ident.h"
#include "ffs_datastream.h"
#include "comm_vars.h"

pthread_mutex_t *map_mutex = NULL;
straggler_map_t **stragglers = NULL;

group_map_t **group_stat = NULL;
pthread_t *gstat_mon_thread = NULL;

decision_factor_t **dfactors = NULL;

static void 
internal_update_group_stat(int which_mstone, int which_group);

static void 
internal_check_group_status(toexec_ctl_msg *toexec_ctl_data, decision_factor_t *df);



void
update_and_check_group_status(toexec_ctl_msg *toexec_ctl_data, int which_mstone, int which_group)
{
  assert(toexec_ctl_data != NULL);
  internal_update_group_stat(which_mstone, which_group);
  internal_check_group_status(toexec_ctl_data, dfactors[which_mstone]);

  return;
}

static void 
internal_update_group_stat(int which_mstone, int which_group)
{
  int i;
  //pthread_mutex_lock(&group_stat[which_mstone][which_group-1].gstat_lock);

  if(group_stat[which_mstone][which_group-1].groupID == INVILID_INT){
    group_stat[which_mstone][which_group-1].groupID = which_group;
  }
  group_stat[which_mstone][which_group-1].cur_gsize += 1;
  group_stat[which_mstone][which_group-1].start = time(NULL);
  
  printf("\n@rank%d - Updated group_map_t for GID#%d @mstone%d (cur_gsize=%d)\n", client_data->rank, which_group, which_mstone, group_stat[which_mstone][which_group-1].cur_gsize);
  //pthread_mutex_unlock(&group_stat[which_mstone][which_group-1].gstat_lock);
  //
  return;

}

static void 
internal_check_group_status(toexec_ctl_msg *toexec_ctl_data, decision_factor_t *df)
{
  assert(  (toexec_ctl_data != NULL) && (df != NULL) );

  int mstoneID = df->mstoneID;
  int count = 0, i = 0, complete = 0; /* TODO:count needs to be changed:static int count[num_mstones]*/
  int groupID, cur_gsize, idx=0;
  time_t cur, diff;
  time_t threshold = df->threshold;
  int enabled = FALSE;

  printf("@mstone %d: internal_check_group_status\n", mstoneID);
  idx = 0;
  for(i=0; i<GNUM; i++){
    toexec_ctl_data->groupID[i] = INVILID_INT;
    toexec_ctl_data->howmany_toexec[i] = INVILID_INT;
    toexec_ctl_data->output_target = INVILID_INT;
    toexec_ctl_data->srcID = INVILID_INT;
    toexec_ctl_data->srcrank = INVILID_INT;
  }
  for(i=0; i<GNUM; i++){
    //pthread_mutex_lock(&group_stat[mstoneID][i].gstat_lock);

    groupID = group_stat[mstoneID][i].groupID;
    cur_gsize = group_stat[mstoneID][i].cur_gsize;
    if(groupID != INVILID_INT){
      complete = if_a_complete_group(groupID, cur_gsize, df->groups_info);
      if(complete == TRUE){ 	  
#ifdef DEBUG
        fprintf(stderr, "@rank%d-mstone %d: group %d turns into a complete group @%d toexec info (cur_gsize=%d)\n", client_data->rank, mstoneID, groupID, count, cur_gsize);
#endif
        toexec_ctl_data->groupID[idx] = groupID;
        toexec_ctl_data->howmany_toexec[idx] = cur_gsize;
        idx++;

        group_stat[mstoneID][i].groupID = INVILID_INT;
        group_stat[mstoneID][i].cur_gsize = 0;
        enabled = TRUE;
      }
    }

    //pthread_mutex_unlock(&group_stat[mstoneID][i].gstat_lock);

  }

  toexec_ctl_data->output_target = client_data->mstone_outputs[mstoneID].to_nextsite;
  toexec_ctl_data->srcID = client_data->itself;
  toexec_ctl_data->srcrank = client_data->rank;

#ifdef DEBUG
  for(i=0; i<GNUM; i++){
    if(toexec_ctl_data->groupID[i] != INVILID_INT){
      printf("@rank%d-mstone %d: toexec GIDs%d\n", 
      client_data->rank, mstoneID, toexec_ctl_data->groupID[i]);
    }
  }
  printf("\n");
#endif

  return;

}

  int 
if_in_complset(int groupID, int *groupset)
{
  assert(groupset != NULL);

  int i;
  for(i=0; i<GNUM; i++){
    if(groupID == groupset[i]){
      return TRUE;
    }
  }

  return FALSE;
}

int if_a_straggler_group(int groupID, straggler_map_t *straggler_info)
{
  assert(straggler_info != NULL);
  assert(straggler_info->size > 0);

  int i;
  for(i=0; i<straggler_info->size; i++){
    if( groupID == straggler_info->groupIDs[i] ){
      return 1;
    }
  }
  return 0;

}

int if_a_complete_group(int groupID, int cur_gsize, gsize_t *e_gsize)
{
  assert(e_gsize != NULL);

  int i;
  for(i=0; i<GNUM; i++){
    if( (e_gsize[i].groupID == groupID) && (e_gsize[i].groupSize == cur_gsize) ){
      return TRUE;
    }
  }
  return FALSE;

}


