function [X,Y,Im,BWmeanC] = readin_imagedata(imageID)
%function [X,Y,I,Im,BWmeanC] = readin_imagedata(imageID)
name = ['B', sprintf('%0*d', 5, imageID), '.im7'];
disp('current image:');
disp(name);

%filedir = '/lustre/atlas/scratch/yzhang63/csc025/PIV/LSB';
filedir = '/net/hp100/ihpcae';
%filedir = '/tmp';
xfile = [filedir, '/image_data_112150_10000/X_data.',name];
yfile = [filedir, '/image_data_112150_10000/Y_data.', name];
imfile = [filedir, '/image_data_112150_10000/Im_data.', name];
bwmeancfile = [filedir, '/image_data_112150_10000/BWmeanC_data'];
X = dlmread(xfile);
Y = dlmread(yfile);
%I = dlmread(ifile);
Im = dlmread(imfile);
BWmeanC = dlmread(bwmeancfile);
%disp(BWmeanC);

end
