% Script that creates a figure illustrating the binarization process for a
% given image. The following images will be created and displayed
% side-by-side:
%   1. Raw, instantaneous image
%   2. Median-filtered image
%   3. Binarized image
%   4. Raw image with edge, fitted spline, and average progress variable contours overlaid

%% Input Parameters
setName = 'Cam_Date=121030_Time=112150';     % name of folder where dataset is located
inputParameters;                % script to define common input parameters
yl      = [0.7 1.3];            % y limits (nondimensional: y/d)
method  = 'graythresh';         % binarizing method: 'graythresh' or 'kmeans' or 'adaptivethreshold'
k1      = 5800;                 % image number to load (first)
k2      = 5800;                 % image number to load (last)
rc      = 0;
% fsize   = 18;

%% Edge Parameters
InputEdgeParameters;        % script to define edge parameters

%% Load mean progress variable field
if rc == 1
    finalFolders = [expdate, '\Reactant Conditioned'];
else
    finalFolders = expdate;
end
prevdir = cd;
currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
    int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
    fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', finalFolders];
cd(currdir);

finDir  = 'Final Results';
if ~exist(finDir, 'dir');
    mkdir(finDir);
end
currdir = [currdir, '\', finDir];
cd(currdir);

fnEnd   = [', shot ', int2str(frame), ', ', 'filt = ', int2str(filtSize(1)), ...
    ', ', method, ', images ', '1-', int2str(N)];
load(['data, delta_f = ', num2str(delta_f, 2), fnEnd, '.mat'], 'BWmeanC', 'X', 'Y');
XC      = X;
YC      = Y;
cd(prevdir);

for k = k1:k2
    %% Figure adjustment parameters
    xl      = [-1 1]*R*0.4;     % x limits (nondimensional: x/d)
%     xl      = [-1 1]*0.2;       % x limits (nondimensional: x/d)
    h       = zeros(1, 8);      % handle vector for figures
    
    %% Load image and find flame edge
    name                = ['B', sprintf('%0*d', 5, k), '.im7']; % image filename (e.g. 'B00001.im7')
    [A, X, Y, I, Im]    = ...
        loadImage(S, P, T, U, fuel, phi, angle, expdate, name, c, xl, yl, frame, ...
        'corrected', corrected, 'filtSize', filtSize);
    
    % Raw image
    h(1)    = figure;
    dispImage(A, X, Y, I, fsize, cmap);
    
    % Median-filtered image
    h(2)    = figure;
    dispImage(A, X, Y, Im, fsize, cmap);
    fname   = get(gcf, 'Name');
    set(gcf, 'Name', [fname ' filtered']);
    
    [BW, I1] = ...
        binarizeImages(X, Y, Im, [k k], S, P, T, U, fuel, phi, angle, expdate, ...
        c, xl, yl, frame, method, 'corrected', corrected, 'filtSize', filtSize);
    
    % Binarized image
    h(3)    = figure;
    dispImage(A, X, Y, BW, fsize, cmap);
    fname   = get(gcf, 'Name');
    set(gcf, 'Name', [fname ' binarized']);
    
    % Edge detection
    E       = flameEdge(BW, X, Y);
    Ec      = cell(1, 1);
    Ec{1}   = E;
    
    % Binarized image with edge overlaid
    h(4)    = figure;
    dispImage(A, X, Y, BW, fsize, cmap);
    fname   = get(gcf, 'Name');
    set(gcf, 'Name', [fname ' binarized with edge']);
    hold on
    plot(E(:, 1)/d, E(:, 2)/d, 'g', 'lineWidth', lwidth);
    hold off
    
    % Raw image with edge overlaid
    h(5)    = figure;
    dispImage(A, X, Y, I, fsize, cmap);
    fname   = get(gcf, 'Name');
    set(gcf, 'Name', [fname ' edge']);
    hold on;
    plot(E(:, 1)/d, E(:, 2)/d, 'g', 'lineWidth', lwidth);
    hold off
    
    % Curvature calculation
    [~, ~, ~, ~, x, y, ~, ~, ~, ~, ~, s, ~, ~, ~, ~] = ...
        Curvature(XC, YC, BWmeanC, Ec, 'delta_f', delta_f, 'fittype', ftype);
    x       = x{1};
    y       = y{1};
    s       = s{1};
    
    % Raw image with edge and fitted spline overlaid
    h(6)    = figure;
    dispImage(A, X, Y, I, fsize, cmap);
    fname   = get(gcf, 'Name');
    set(gcf, 'Name', [fname ' edge, spline']);
    hold on;
    plot(E(:, 1)/d, E(:, 2)/d, 'g', 'lineWidth', lwidth);
    plot(x/d, y/d, 'r--', 'lineWidth', lwidth);
    hold off
    
    % Raw image with edge, fitted spline, and leading point overlaid
    h(7)    = figure;
    dispImage(A, X, Y, I, fsize, cmap);
    fname   = get(gcf, 'Name');
    set(gcf, 'Name', [fname ' edge, spline, LP']);
    hold on;
    plot(E(:, 1)/d, E(:, 2)/d, 'g', 'lineWidth', lwidth);
    plot(x/d, y/d, 'r--', 'lineWidth', lwidth);
    [xLP, yLP, cLP, uLP, vLP] = ...
        LPvelocities(S, P, T, U, fuel, phi, angle, expdate, c, [k k], Ec, X, Y, BW, ...
        'vc7Folder', vc7Folder, 'rc', rc);
    plot(xLP/d, yLP/d, 'yx', 'lineWidth', lwidth, 'markerSize', msize);
    hold off
    
    % Raw image with edge, fitted spline, LP, and progress variable contours overlaid
    h(8)    = figure;
    dispImage(A, X, Y, I, fsize, cmap);
    fname   = get(gcf, 'Name');
    set(gcf, 'Name', [fname ' edge, spline, LP, c-contours']);
    hold on;
    [cc, hh] = contour(XC/d, YC/d, BWmeanC, [0.05 0.2:0.2:0.8 0.95], 'w', 'lineWidth', 1);
    clabel(cc, hh, 'FontSize', fsize-2, 'LabelSpacing', 450, 'color', 'w');
    plot(E(:, 1)/d, E(:, 2)/d, 'g', 'lineWidth', lwidth);
    plot(x/d, y/d, 'r--', 'lineWidth', lwidth);
    plot(xLP/d, yLP/d, 'yx', 'lineWidth', lwidth, 'markerSize', msize);
    hold off
    
    %% Save figures and data
    if corrected
        finalFolders = [expdate, '\Correction'];
    else
        finalFolders = expdate;
    end
    currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', finalFolders];
    cd(currdir);
    
    fnEnd   = [', delta_f = ', num2str(delta_f, 2), ', shot ', int2str(frame), ...
        ', filt = ', int2str(filtSize(1)), ', ', method, ', image ', int2str(k)];
    for i = length(h):-1:1
        fn          = get(h(i), 'name');
        fnFinal     = [fn, fnEnd];
        I           = find(fnFinal == '.');
        fnFinal(I)  = '_';
        saveas(h(i), fnFinal, 'fig');
        saveas(h(i), fnFinal, 'png');
        close(h(i));
    end
    % cd([currdir, '\masks']);
    % imwrite(BW, [name, ' mask.bmp'], 'BMP');
    cd(prevdir);
end