% Script to generate random splines and calculate curvatures of the splines
tic
%% Input Parameters
xm          = 128;                  % maximum x value (pixels)
sig         = 3;                    % standard deviation of splines or amplitudes of sines (pixels)
ybar        = 15*sig;               % mean of spline values or sine curves (pixels)
ym          = 2*ybar;               % maximum y value (pixels)
N           = 1600;                 % number of splines to generate
Xm          = 2.5;                  % maximum x value (mm)

% specific parameters according to curve type
curveType   = 'sin2';               % generate either a spline curve ('spline') or a sin curve ('sin')
switch curveType
    case 'spline'
        lambda      = NaN;          % dummy variable since lambda only has meaning for 'sin'
        lambdaS     = NaN;          % dummy variable since lambdaS only has meaning for 'sin2'
        nt          = NaN;          % dummy variable since nt only has meaning for 'sin'
    case 'sin'
        lambda      = 30;           % standard deviation of wavelength of sine curve (pixels)
        lambdaS     = NaN;          % dummy variable since lambdaS only has meaning for 'sin2'
        nt          = 5;            % number of terms in sine curve
    case 'sin2'
        lambda      = 240;          % mean wavelength of sine curve (pixels)
        lambdaS     = 20;           % standard deviation of wavelength of sine curve (pixels)
        nt          = 2;            % number of terms in sine curve
    otherwise
        error([curveType, ' is not a valid entry for the variable curveType']);
end

% Fit Parameters
ftype       = 'csaps';              % type of fit ('poly': polynomial, 'csaps': cubic smoothing spline)
DS          = 'on';                 % downsample ('off': no downsampling, 'on': downsample based on flame thickness)
switch ftype
    case 'poly'
        order       = 3:8;                  % order of polynomial (default: 4)
        pRepeat     = 2:4;                  % number of fits to perform (default: 2)
        n           = [7 13 25 49];         % number of points to fit over (default: 15)
        
        order       = 4;                    % order of polynomial to (default: 4)
        pRepeat     = 2;                    % number of fits to perform (default: 2)
        n           = 9;                    % number of points to fit over (default: 13)
    case 'csaps'
        order       = NaN;                  % dummy variable since order only has meaning for 'poly'
        pRepeat     = NaN;                  % dummy variable since pRepeat only has meaning for 'poly'
        n           = NaN;                  % dummy variable since n only has meaning for 'poly'
    otherwise
        error([ftype, ' is not a valid entry for the variable ftype']);
end

%% Figure Input Parameters
fsize       = 16;                   % font size
fname       = 'Times New Roman';    % font
lwidth      = 2;                    % line width (points)
clim        = [0 1];                % color axis limits (nondimensional: u/U)
cmap        = 'gray';               % colormap

%% Convert from pixels to mm
x           = (1:0.1:xm)';          % x vector of positions (pixels)
m           = 2*Xm/(x(end) - x(1)); % slope (mm/pixel)
Bx          = -(Xm + m);            % intercept (mm)
X           = m*x + Bx;             % x vector of positions (mm)

By          = -m;                   % y-intercept (mm)
Ybar        = m*ybar + By;          % mean of spline values or sine curves (mm)
Sig         = m*sig;                % standard deviation of spline values or amplitudes of sines (mm)
Ym          = 2*Ybar;               % maximum y value in mm

xd          = (1:xm)';              % x vector of positions (pixels) - digitized
yd          = (1:ym)';              % y vector of positions (pixels) - digitized
Xd          = m*xd + Bx;            % x vector of positions (mm) - digitized

Yed         = cell(N, 1);

for l = 1:length(lambda)
    Lambda      = m*lambda(l);          % for sin/sin2, std/mean of wavelength of sine curve (mm)
    LambdaS     = m*lambdaS;            % for sin2, std of wavelength of sine curve (mm)
    Ks          = 2*pi/Lambda;          % for sin/sin2, std/mean of wavenumber of sine curve (1/mm)
    KsS         = 2*pi/LambdaS;         % for sin2, std of wavenumber of sine curve (1/mm)
    
    %% Generate splines and calculate curvatures
    switch curveType
        case 'spline'
            [Ye, kappaTrue] = pseudoCurvature(X, Ybar, Sig, N);
        case 'sin'
            [Ye, kappaTrue] = pseudoCurvatureSin(X, Ybar, Sig, Ks, nt, N);
        case 'sin2'
            [Ye, kappaTrue] = pseudoCurvatureSin2(X, Ybar, Sig, Ks, KsS, nt, N);
        otherwise
            error([curveType, ' is not a valid entry for the variable curveType']);
    end
    
    %% Generate binarized images from extracted edges
    for i = 1:N
        Yed{i}  = interp1(X, Ye{i}, Xd);    % interpolate y-values of edge onto digitized x-space (mm)
    end
    clear Ye;
    % convert edge locations in physical space to pixel space -
    % this is where the digitization of the y-value of the edge occurs
    yed         = cellfun(@(p) round((p - By)/m), Yed, 'uniformOutput', 0);
    clear Yed;
    BWs         = pseudoBinarize(xm, ym, yed);
    Xd          = Xd';                      % x vector of positions (mm) - digitized
    Yd          = fliplr((m*yd + By)');     % y vector of positions (mm) - digitized
    
    %% Find edge using flameEdges code
    E           = flameEdges(BWs, Xd, Yd);
    
    %% Generate average progress variable, <c>, contour plot
    BWmean      = mean(BWs, 3);
    
    figure;
    dispImageNoA(Xd, Yd, BWmean, fsize, cmap);
    colorbar;
    colormap(cmap);
    caxis(clim);
    set(gca, 'FontSize', fsize, 'FontName', fname);
    set(gcf, 'Name', 'Average Progress Variable');
    
    %% Make folder for figures and data
    prevdir = cd;
    switch curveType
        case 'spline'
            currdir = ['\\Vboxsvr\piv\LSB\reacting\pseudocurvature\sigma = ', num2str(Sig, 2), ...
                ' mm\N = ', int2str(N), '\res = ', num2str(m, 2), ' mm per pixel\', ...
                curveType];
        case 'sin'
            currdir = ['\\Vboxsvr\piv\LSB\reacting\pseudocurvature\sigma = ', num2str(Sig, 2), ...
                ' mm\N = ', int2str(N), '\res = ', num2str(m, 2), ' mm per pixel\', ...
                curveType, '\num. terms = ', int2str(nt), '\S_k = ', num2str(Ks, 2), ' mm-1'];
        case 'sin2'
            currdir = ['\\Vboxsvr\piv\LSB\reacting\pseudocurvature\sigma = ', num2str(Sig, 2), ...
                ' mm\N = ', int2str(N), '\res = ', num2str(m, 2), ' mm per pixel\', ...
                curveType, '\num. terms = ', int2str(nt), '\k = ', num2str(Ks, 2), ' mm-1', ...
                '\S_k = ', num2str(KsS, 2), ' mm-1'];
        otherwise
            error([curveType, ' is not a valid entry for the variable curveType']);
    end
    
    if ~exist(currdir, 'dir');
        mkdir(currdir);
    end
    cd(currdir);
    
    %% Save average progress variable figure as fig and png
    fn      = get(gcf, 'Name');
    I       = fn == '.';
    fn(I)   = '_';
    I       = fn == '<' | fn == '>';
    fn(I)   = [];
    saveas(gcf, [fn, ', pseudo, ', curveType], 'fig');
    saveas(gcf, [fn, ', pseudo, ', curveType], 'png');
    close;
    clear BWmean
    
    %% Curvature Calculations
    for i = 1:length(order)
        for j = 1:length(pRepeat)
            for k = 1:length(n)
                %             disp([i; j; k]);
                [~, ~, ~, ~, ~, ~, ~, ~, ~, ~, kappa, ~] = ...
                    Curvature(Xd, Yd, BWs, E, 1, ['pseudo, ', curveType], ...
                    'order', order(i), 'polyRepeats', pRepeat(j), 'fitPoints', n(k), ...
                    'downSample', DS, 'fitType', ftype);
                [~, ~, ~, ~, ~, ~, ~, ~, ~, ~, kappae, ~] = ...
                    Curvature(Xd, Yd, BWs, E, 1, ['pseudo, ', curveType], ...
                    'order', order(i), 'polyRepeats', pRepeat(j), 'fitPoints', n(k), ...
                    'downSample', 'off', 'fitType', ftype);
                
                %% Calculate standard deviation and relative error in standard deviation
                kappaF  = outliers(cell2mat(kappa));            % remove outliers
                kappaF(isnan(kappaF)) = [];                     % remove NaNs
                kappaeF = outliers(cell2mat(kappae));           % remove outliers
                kappaeF(isnan(kappaeF)) = [];                   % remove NaNs
                SkdTrue = m*cumStd(cell2mat(kappaTrue));        % cumulative std of real curvature
                Skd     = m*cumStd(cell2mat(kappa));            % cum. std of alg. curv., DS, no OR
                Sked    = m*cumStd(cell2mat(kappae));           % cum. std of alg. curv., no DS, no OR
                SkdF    = m*cumStd(kappaF);                     % cum. std of alg. curv., DS, OR
                SkedF   = m*cumStd(kappaeF);                    % cum. std of alg. curv., no DS, OR
                estd    = 100*(SkdTrue(end) - Skd(end))/...
                    SkdTrue(end);                               % error in std of alg., DS, no OR
                estde   = 100*(SkdTrue(end) - Sked(end))/...
                    SkdTrue(end);                               % error in std of alg., no DS, no OR
                estdF   = 100*(SkdTrue(end) - SkdF(end))/...
                    SkdTrue(end);                               % error in std of alg., DS, OR
                estdeF  = 100*(SkdTrue(end) - SkedF(end))/...
                    SkdTrue(end);                               % error in std of alg., no DS, no OR
                
                switch ftype
                    case 'poly'
                        fParam  = ['order = ', int2str(order(i)), ...
                            ', pRepeat = ', int2str(pRepeat(j)), ', n = ', int2str(n(k))];
                    case 'csaps'
                        fParam  = 'cubic smoothing splines';
                    otherwise
                        error([ftype, ' is not a valid entry for the variable ftype']);
                end
                disp(fParam);
                disp(['DS, No OR: percent error in standard deviation is: ', ...
                    num2str(estd, 2), '%']);
                disp(['No DS, No OR: percent error in standard deviation is: ', ...
                    num2str(estde, 2), '%']);
                disp(['DS, OR: percent error in standard deviation is: ', ...
                    num2str(estdF, 2), '%']);
                disp(['No DS, OR: percent error in standard deviation is: ', ...
                    num2str(estdeF, 2), '%']);
                
                %% Generate plot of cumulative standard deviations
                figure;
                set(gcf, 'color', 'w');
                set(gcf, 'Name', 'Cumulative Standard Deviations');
                plot(1:length(SkdTrue), SkdTrue, 'b', 1:length(Skd), Skd, 'r', ...
                    1:length(Sked), Sked, 'r--', 1:length(SkdF), SkdF, 'g', ...
                    1:length(SkedF), SkedF, 'g--', 'LineWidth', lwidth);
                set(gca, 'FontSize', fsize, 'FontName', fname);
                xlabel('{\itN}');
                ylabel('{\it\sigma}');
                switch ftype
                    case 'poly'
                        fParam2 = ['{\ito} = ', int2str(order(i)), ...
                            ', {\itp} = ', int2str(pRepeat(j)), ...
                            ', {\itn} = ', int2str(n(k))];
                    case 'csaps'
                        fParam2 = ftype;
                    otherwise
                        error([ftype, ' is not a valid entry for the variable ftype']);
                end
                legend(['{\it\sigma_a} /{\it\delta} = ', int2str(sig), ... Real curvature
                    ', {\itn_t} = ', int2str(nt), ...
                    ', {\it\sigma_\lambda} /{\it\delta} = ', int2str(lambda)], ...
                    [fParam2, ', {\ite_\sigma} = ', num2str(estd, 2), '%', ', DS, no OR'], ...
                    [fParam2, ', {\ite_\sigma} = ', num2str(estde, 2), '%', ', no DS, no OR'], ...
                    [fParam2, ', {\ite_\sigma} = ', num2str(estdF, 2), '%', ', DS, OR'], ...
                    [fParam2, ', {\ite_\sigma} = ', num2str(estdeF, 2), '%', ', no DS, OR'], ...
                    'location', 'best');
                
                %% Save figures and data
                fn      = get(gcf, 'Name');
                I       = fn == '.';
                fn(I)   = '_';
                I       = fn == '<' | fn == '>';
                fn(I)   = [];
                fnFinal = [fn, ', ', fParam, ', pseudo, ', curveType];
                saveas(gcf, fnFinal, 'fig');
                saveas(gcf, fnFinal, 'png');
                close
                
                %% Generate histogram plot of true curvature
                figure;
                histPlot(m*cell2mat(kappaTrue), 'fontSize', fsize, 'varname', 'kappadelta', ...
                    'xlabel', '{\kappa\delta}', 'plotType', 'stairs');
                XL      = xlim;
                YL      = ylim;
                
                %% Histogram plot of algorithm generated curvature
                hold on
                histPlot(m*cell2mat(kappa), 'fontSize', fsize, 'varname', 'kappadelta', ...
                    'xlabel', '{\kappa\delta}', 'plotType', 'stairs', ...
                    'lmstyle', 'r');
                
                %% Histogram plot of algorithm generated curvature 
                % s is not downsampled
                hold on
                histPlot(m*cell2mat(kappae), 'fontSize', fsize, 'varname', 'kappadelta', ...
                    'xlabel', '{\kappa\delta}', 'plotType', 'stairs', ...
                    'lmstyle', 'r--');
                
                %% Histogram plot of algorithm generated curvature with outliers removed
                histPlot(m*kappaF, 'fontSize', fsize, 'varname', 'kappadelta', ...
                    'xlabel', '{\kappa\delta}', 'plotType', 'stairs', ...
                    'lmstyle', 'g');
                
                %% Histogram plot of algorithm generated curvature with outliers removed 
                % s is not downsampled
                histPlot(m*kappaeF, 'fontSize', fsize, 'varname', 'kappadelta', ...
                    'xlabel', '{\kappa\delta}', 'plotType', 'stairs', ...
                    'lmstyle', 'g--');
                xlim(XL);
                ylim(YL);
                
                %% Add legend to histogram plot
                legend(['{\it\sigma_a} /{\it\delta} = ', int2str(sig), ... Real curvature
                    ', {\itn_t} = ', int2str(nt), ...
                    ', {\it\sigma_\lambda} /{\it\delta} = ', int2str(lambda)], ...
                    [fParam2, ', {\ite_\sigma} = ', num2str(estd, 2), '%', ', DS, no OR'], ...
                    [fParam2, ', {\ite_\sigma} = ', num2str(estde, 2), '%', ', no DS, no OR'], ...
                    [fParam2, ', {\ite_\sigma} = ', num2str(estdF, 2), '%', ', DS, OR'], ...
                    [fParam2, ', {\ite_\sigma} = ', num2str(estdeF, 2), '%', ', no DS, OR'], ...
                    'location', 'best');
                
                %% Save figures and data
                fn      = get(gcf, 'Name');
                I       = fn == '.';
                fn(I)   = '_';
                I       = fn == '<' | fn == '>';
                fn(I)   = [];
                fnFinal = [fn, ', ', fParam, ', pseudo, ', curveType];
                saveas(gcf, fnFinal, 'fig');
                saveas(gcf, fnFinal, 'png');
                save(['data', ', ', fParam, ', pseudo, ', curveType, '.mat']);
                close
            end
        end
    end
    cd(prevdir);
end
toc