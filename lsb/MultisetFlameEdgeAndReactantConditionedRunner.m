clear;
% Define a structure containing the name of each dataset. The name is used
% to define the input parameters for each dataset.
setNames    = ...
    {'Cam_Date=121030_Time=112150';...  30 m/s, 50-50 H2-CO, phi = 0.55, angle = 0
    'Cam_Date=121030_Time=113727';...   30 m/s, 50-50 H2-CO, phi = 0.55, angle = 24
    'Cam_Date=121030_Time=170514';...   30 m/s, 70-30 H2-CO, phi = 0.51, angle = 0
    'Cam_Date=121030_Time=172305';...   30 m/s, 70-30 H2-CO, phi = 0.51, angle = 24
    'Cam_Date=121103_Time=112726';...   30 m/s, 100 H2, phi = 0.46, angle = 0
    'Cam_Date=121103_Time=114443';...   30 m/s, 100 H2, phi = 0.46, angle = 24
    'Cam_Date=121031_Time=195417';...   50 m/s, 50-50 H2-CO, phi = 0.55, angle = 0
    'Cam_Date=121031_Time=201041';...   50 m/s, 50-50 H2-CO, phi = 0.55, angle = 24
    'Cam_Date=121101_Time=190834';...   50 m/s, 70-30 H2-CO, phi = 0.51, angle = 0
    'Cam_Date=121101_Time=193502';...   50 m/s, 70-30 H2-CO, phi = 0.51, angle = 24
    'Cam_Date=121103_Time=121008';...   50 m/s, 100 H2, phi = 0.46, angle = 0
    'Cam_Date=121103_Time=125656'};   % 50 m/s, 100 H2, phi = 0.46, angle = 24

%% FlameEdgeRunner - Primarily used to determine curvatures along the flame front
for m = 1:length(setNames)
    setName = setNames{m};
    disp(setName);
    tic
    FlameEdgeRunner;
    toc
    disp(' ');
    clearvars -except setNames
end

%% ReactantConditionedRunner - Primarily used to determine tangential strain rates along the flame front
for m = 1:length(setNames)
    setName = setNames{m};
    disp(setName);
    tic
    ReactantConditionedRunner;
    toc
    disp(' ');
    clearvars -except setNames
end

%% jointPDFRunner - Primarily used to analyze joint statistics of curvature and tangential strain rate
for m = 1:length(setNames)
    setName = setNames{m};
    disp(setName);
    tic
    jointPDFRunner;
    toc
    disp(' ');
    clearvars -except setNames
end

%% LSBRunner - Primarily used to determine mean and RMS velocity fields and generate plots
for m = 1:length(setNames)
    setName = setNames{m};
    disp(setName);
    tic
    LSBRunner;
    toc
    disp(' ');
    clearvars -except setNames
end