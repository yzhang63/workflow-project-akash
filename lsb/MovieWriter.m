% Script to create video file from im7 images

%% Input Parameters
setName     = 'Cam_Date=121103_Time=125656';            % name of folder where dataset is located
inputParameters;                                        % script to define common input parameters
% yl          = [0.1 1.5];                                % y limits (nondimensional: y/d)
method      = 'graythresh';                             % binarizing method

%% Movie Input Parameters
F           = 5000;                                     % index of first im7 file
L           = 6000;                                     % index of last im7 file
shot        = 1;                                        % image frame (1 or 2)
enlarge     = 1.2;                                      % multiplying factor to determine figure size
bits        = 8;                                        % number of bits for image resolution

%% Figure adjustment parameters
xl          = [-1 1]*R*0.4;                             % x limits (nondimensional: x/d)

%% Prepare the video file
vName       = ['B', sprintf('%0*d', 5, F), '-', ...
    'B', sprintf('%0*d', 5, L), '_', int2str(shot), ...
    '.avi'];                                            % filename (e.g. 'B00001-B00500_1.avi')
writObj     = VideoWriter(vName);                       % initialize writer object
open(writObj);                                          % open writer object

%% Binarize the images and find the flame edge for each image
fn                  = ['B', sprintf('%0*d', 5, F)];    % im7 filename (e.g. 'B00001.im7')
[A, X, Y, I, Im]    = loadImage(S, P, T, U, fuel, phi, angle, expdate, [fn, '.im7'], c, ...
    xl, yl, shot, 'corrected', corrected, 'filtSize', filtSize);
[BWs, I1]           = binarizeImages(X, Y, Im, [F, L], S, P, T, U, fuel, phi, angle, expdate, ...
    c, xl, yl, shot, method, 'corrected', corrected, 'filtSize', filtSize);
E                   = flameEdges(BWs, X, Y);

%% Generate figure and set axes and figure properties
dispImage(A, X, Y, I, fsize, 'gray');
posIni              = get(gcf, 'Position');
xFin                = posIni(1);
yFin                = posIni(2)+posIni(4)-round(enlarge*length(I(:, 1)));
wFin                = round(enlarge*length(I(1, :)));
gap                 = wFin - length(I(1, :));
hFin                = length(I(:, 1)) + gap;
posFin              = [xFin, yFin, wFin, hFin];
set(gcf, 'Position', posFin);

R                   = F:L;
for i = 1:length(R)
    fn                  = ['B', sprintf('%0*d', 5, R(i))];  % im7 filename (e.g. 'B00001.im7')
    [A, X, Y, I, Im]    = loadImage(S, P, T, U, fuel, phi, angle, expdate, [fn, '.im7'], c, ...
        xl, yl, shot, 'corrected', corrected, 'filtSize', filtSize);
    
    %% Scale data
    I = scaleImage(I, bits);
    
    %% Match histogram across images
    if R(i) == F
        J   = I;
    else
        I   = imhistmatch(I, J);
    end
    
    %% Write video file
    dispImage(A, X, Y, I, 16, cmap);
    hold on
    plot(E{i}(:, 1)/d, E{i}(:, 2)/d, 'g', 'lineWidth', lwidth);
    frame   = getframe(gcf);
    writeVideo(writObj, frame);
end
close(writObj);