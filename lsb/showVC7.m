% Script to show a specific vc7 file

%% Input Parameters
setName = 'Cam_Date=121031_Time=195417';    % name of folder where dataset is located
inputParameters;                            % script to define common input parameters
n       = 5000;                             % image number
rc      = 1;                                % 1 = reactant-conditioned, 0 = unconditioned

%% Extract velocities and plot on a quiver plot
A                   = vc7extract(S, P, T, U, fuel, phi, angle, expdate, n, rc, 'vc7Folder', mvc7Folder);
[Xv, Yv, VX, VY]    = prepareIMX(A, c);

h = figure;
quiverProfile(A, Xv, Yv, VX, VY, U, d, dim, yl, 1.4*clim, fsize);