#include <stdio.h>
#include <assert.h>
#include <stdlib.h>


int main(){
  int a = 0, b = -1, c= 0, i = 100;

  while(a != b){
    printf("(start) a = %d, b = %d, c = %d\n", a, b, c);
    assert(( 0 <= a) &&(a <= (i-1)));
    assert(( -1 <= b) &&(b <= (i-1)));
    assert((0 <= c ) && (c <= i));
    b = a;
    c = c+ 1;


    if(c<i){
      a = a+1;
    }


    printf("(end) a = %d, b = %d, c = %d\n", a, b, c);
  }
}
