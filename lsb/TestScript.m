% j       = 1;
% f{1}    = bound(1, :);
% for i = 2:length(bound(:, 1))
%     dx = bound(i, 2) - bound(i-1, 2);   % delta-x
%     dy = bound(i, 1) - bound(i-1, 1);   % delta-y
%     if abs(dx) > 1 || abs(dy) > 1       % check to see if any pixels are jumped
%         j       = j + 1;                % if a pixel is jumped, start a new cell
%         f{j}    = [];
%     end
%     f{j} = cat(1, f{j}, bound(i, :));   % place into cell array
% end
% [~, k]  = max(cellfun('size', f, 1));   % find the longest continuous boundary
% g       = f{k};                         % use only the longest continuous boundary

dx      = zeros(length(bound(:, 1))-1, 1);
dy      = dx;
for i = 2:length(bound(:, 1))
    dx(i-1) = bound(i, 2) - bound(i-1, 2);      % delta-x
    dy(i-1) = bound(i, 1) - bound(i-1, 1);      % delta-y
end
ii      = find((abs(dx) > 1) | (abs(dy) > 1));  % find indices where pixel jumps occur
ii      = [0; ii; length(bound(:, 1))];         % add start and end values
dii     = zeros(length(ii)-1, 1);
for i = 2:length(ii)
    dii(i-1) = ii(i) - ii(i-1);                 % find lengths of continuous flame edges
end
[~, jj] = max(dii);                             % find longest continuous flame edge
h       = bound(ii(jj)+1:ii(jj+1), :);          % extract longest continuous flame edge