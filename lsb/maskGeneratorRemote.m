% Script that creates a figure illustrating the binarization process for a
% given image. The following images will be created and displayed
% side-by-side: 
%   1. Raw, instantaneous image
%   2. Median-filtered image
%   3. Binarized image

%% Input Parameters
inputParameters;            % script to define common input parameters
yl      = [-1 3];        % y limits (nondimensional: y/d)
bs      = 32;               % interrogation window binsize in pixels
ol      = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
method  = 'graythresh';     % binarizing method: 'graythresh' or 'kmeans'
level   = 434;              % threshold level for binarizing images (currently does nothing)
imfn    = 'B00001.im7';     % image filename

%% Figure adjustment parameters
xl      = [-1 1]*R*5;     % x limits (nondimensional: x/d)
h       = zeros(1, 3);      % handle vector for figures

%% Load images and find flame edge
[A X Y I Im]    = loadImageRemote(S, P, T, U, fuel, phi, angle, expdate, imfn, c, xl, yl, frame);
[BWs I1]        = binarizeImagesRemote(X, Y, Im, N, level, S, P, T, U, fuel, phi, angle, expdate, c, xl, yl, frame, method);

%% Save mask files
prevdir = cd;
currdir = ['Z:\PIV\LSB\reacting\S = ', num2str(S), '\', ...
    int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
    fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', expdate];
cd([currdir, '\masks2']);
parfor i = 1:N
    name    = ['B', sprintf('%0*d', 5, i), '.im7'];     % image filename (e.g. 'B00001.im7')
    BW      = imcomplement(BWs(:, :, i));
    imwrite(BW, [name, ' mask.bmp'], 'BMP');
end
cd(prevdir);