% Script to plot ST,LD data

fsize   = 18;                           % font size
fname   = 'Times New Roman';            % font name
lwidth  = 2;                            % line width
msize   = 10;                           % marker size

%% 20 m/s
figure;
[STLD, uy]  = loadSTLD(0.57, 1, 300, 20, '100 CH4', 0.9, 0, '20120215a', 'linearfit');
SL          = 0.34;                         % unstretched laminar flame speed (m/s)
plotSTLD(STLD, uy, SL, 'g+', lwidth, msize, fsize);
hold on

angle       = [0 12 24];
SL          = 0.34;                         % unstretched laminar flame speed (m/s)
STLD        = zeros(1, length(angle));
uy          = STLD;
for i = 1:length(angle)
    [STLD(i), uy(i)]  = loadSTLD(0.57, 1, 300, 20, '100 CH4', 0.9, angle(i), '20120215', 'linearfit');
end
plotSTLD(STLD, uy, SL, 'g+', lwidth, msize, fsize);

% load(['\\Vboxsvr\piv\LSB\reacting\S = 0.57\1 atm\300 K\20 mps\100 CH4\phi = 0.9\', ...
%     '24 deg\20120301\PIV_MP(32x32_50%ov)_PostProc\TimeMeanQF_Vector\data.mat']);
% SL      = 0.34;                         % unstretched laminar flame speed (m/s)
% 
% plot(uy/SL, STLD/SL, 'g+', 'LineWidth', lwidth, 'MarkerSize', msize);

%% 30 m/s
% angle       = 24;
% SL          = 0.34;                         % unstretched laminar flame speed (m/s)
% STLD        = zeros(1, length(angle));
% uy          = STLD;
% for i = 1:length(angle)
%     [STLD(i) uy(i)]   = loadSTLD(0.57, 1, 300, 30, '100 CH4', 0.9, angle(i), '20120222', 'linearfit');
% end
% plotSTLD(STLD, uy, SL, 'r*', lwidth, msize, fsize);

angle       = [0 24];
SL          = 0.34;                         % unstretched laminar flame speed (m/s)
STLD        = zeros(1, length(angle));
uy          = STLD;
for i = 1:length(angle)
    [STLD(i), uy(i)]  = loadSTLD(0.57, 1, 300, 30, '50-50 H2-CH4', 0.73, angle(i), '20120301', 'linearfit');
end
plotSTLD(STLD, uy, SL, 'r*', lwidth, msize, fsize);

angle       = 0;
SL          = 0.34;                         % unstretched laminar flame speed (m/s)
STLD        = zeros(1, length(angle));
uy          = STLD;
for i = 1:length(angle)
    [STLD(i), uy(i)]  = loadSTLD(0.6, 1, 300, 30, '75-25 H2-CH4', 0.63, angle(i), '20120402', 'linearfit');
end
plotSTLD(STLD, uy, SL, 'rd', lwidth, msize, fsize);

%% 40 m/s
angle       = 24;
SL          = 0.34;                         % unstretched laminar flame speed (m/s)
STLD        = zeros(1, length(angle));
uy          = STLD;
for i = 1:length(angle)
    [STLD(i), uy(i)]  = loadSTLD(0.57, 1, 300, 40, '100 CH4', 0.9, angle(i), '20120222', 'linearfit');
end
plotSTLD(STLD, uy, SL, 'c+', lwidth, msize, fsize);

angle       = [0 24];
SL          = 0.34;                         % unstretched laminar flame speed (m/s)
STLD        = zeros(1, length(angle));
uy          = STLD;
for i = 1:length(angle)
    [STLD(i), uy(i)]  = loadSTLD(0.58, 1, 300, 40, '50-50 H2-CH4', 0.73, angle(i), '20120301', 'linearfit');
end
plotSTLD(STLD, uy, SL, 'c*', lwidth, msize, fsize);

angle       = 0;
SL          = 0.34;                         % unstretched laminar flame speed (m/s)
STLD        = zeros(1, length(angle));
uy          = STLD;
for i = 1:length(angle)
    [STLD(i), uy(i)]  = loadSTLD(0.6, 1, 300, 40, '60-40 H2-CH4', 0.69, angle(i), '20120402', 'linearfit');
end
plotSTLD(STLD, uy, SL, 'cs', lwidth, msize, fsize);

angle       = 0;
SL          = 0.34;                         % unstretched laminar flame speed (m/s)
STLD        = zeros(1, length(angle));
uy          = STLD;
for i = 1:length(angle)
    [STLD(i), uy(i)]  = loadSTLD(0.6, 1, 300, 40, '75-25 H2-CH4', 0.63, angle(i), '20120402', 'linearfit');
end
plotSTLD(STLD, uy, SL, 'cd', lwidth, msize, fsize);

x1 = xlim;
xlim([0 x1(2)]);
y1 = ylim;
ylim([0 y1(2)]);
x2 = xlim;
STSL    = @(uySL) 1 + 1.73*uySL;
plot(x2, STSL(x2), 'k--', 'LineWidth', lwidth);