%% Average data over entire set of images
% start by initializing variables
EC          = cell(N, 1);
xC          = EC;
yC          = EC;
cC          = EC;
sC          = EC;
kappaC      = EC;
nxC         = EC;
nyC         = EC;
BWmeanC     = zeros(size(BWmean));
kappaLPC    = zeros(N, 1);
xLPC        = kappaLPC;
yLPC        = kappaLPC;
cLPC        = kappaLPC;
uLPC        = xLPC;
vLPC        = xLPC;
XLPC        = 0;
YLPC        = 0;
CLPC        = 0;
ULPC        = 0;
VLPC        = 0;
upLPC       = 0;
vpLPC       = 0;

if rc
    uLPaveC = xLPC;
    vLPaveC = xLPC;
    uUPC    = xLPC;
    vUPC    = xLPC;
    ULPaveC = 0;
    VLPaveC = 0;
    ucC     = zeros(N, length(cbars));
    vcC     = ucC;
    ucpC    = zeros(size(ucp));
    vcpC    = ucpC;
end
kappaFC = NaN(500*N, 1);                        % Make very large then shorten by removing NaNs
xFC     = kappaFC;
yFC     = kappaFC;
cFC     = kappaFC;

i1      = 1;
for j = 1:numSets
    %% Load data
    F           = M(j) + 1;                             % index of first image
    L           = M(j + 1);                             % index of last image
    if rc == 1
        finalFolders = [expdate, '\Reactant Conditioned'];
    else
        finalFolders = expdate;
    end
    currdir     = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', finalFolders];
    cd(currdir);
    
    fnEnd       = [', shot ', int2str(frame), ', ', 'filt = ', int2str(filtSize(1)), ...
        ', ', method, ', images ', int2str(F), '-', int2str(L)];
    if exist('setNames', 'var')
        temp        = setNames;
    end
    load(['data, delta_f = ', num2str(delta_f, 2), fnEnd, '.mat']);
    if exist('setNames', 'var')
        setNames    = temp;
    end
    
    %% Combine data from different sets
    % (combined variables will have the same variable name as the original
    % variables except that an uppercase C will be appended to the end)
    EC(F:L)         = E;
    xC(F:L)         = x;
    yC(F:L)         = y;
    cC(F:L)         = c_;
    sC(F:L)         = s;
    kappaC(F:L)     = kappa;
    nxC(F:L)        = nx;
    nyC(F:L)        = ny;
    kappaLPC(F:L)   = kappaLP;
    xLPC(F:L)       = xLP;
    yLPC(F:L)       = yLP;
    cLPC(F:L)       = cLP;
    uLPC(F:L)       = uLP;
    vLPC(F:L)       = vLP;
    XLPC            = XLPC + XLP;
    YLPC            = YLPC + YLP;
    CLPC            = CLPC + CLP;
    ULPC            = ULPC + ULP;
    VLPC            = VLPC + VLP;
    upLPC           = upLPC + upLP;
    vpLPC           = vpLPC + vpLP;
    
    if rc
        uLPaveC(F:L)    = uLPave;
        vLPaveC(F:L)    = vLPave;
        ucC(F:L, :)     = uc;
        vcC(F:L, :)     = vc;
        uUPC(F:L)       = uUP;
        vUPC(F:L)       = vUP;
        ULPaveC         = ULPaveC + ULPave;
        VLPaveC         = VLPaveC + VLPave;
        ucpC            = ucpC + ucp;
        vcpC            = vcpC + vcp;
    end
    i2              = i1 + length(kappaF) - 1;
    kappaFC(i1:i2)  = kappaF;
    xFC(i1:i2)      = xF;
    yFC(i1:i2)      = yF;
    cFC(i1:i2)      = cF;
    i1              = i2 + 1;
end

%% Generate average progress variable, <c>, contour plot
clear h
h(1)            = figure;
dispImage(A, X, Y, BWmeanC, fsize);
colorbar;
caxis(clim);
set(gca, 'FontSize', fsize, 'FontName', fname);
set(gcf, 'Name', 'Average Progress Variable');

%% Generate 1-D plot of progress variable as a function of axial distance
x1              = 0;                % radial position (nondimensional: x/d)

h(end+1)        = figure; %#ok<*SAGROW>
[cf, gf, Co]    = progressVariablePlot(A, X, Y, BWmeanC, x1, 'kx', fsize);
set(gcf, 'Name', ['Average Progress Variable, x = ', num2str(x1)]);

%% Find axial location of a given value of the average progress variable
Cfun    = @(y) Co(1)*(erf(Co(2)*(y - Co(3)))) + Co(4) - cbar;
y1C     = fzero(Cfun, 0.6*d);

%% Calculate cumulative standard deviations of curvature
Inan            = isnan(kappaFC);
kappaFC(Inan)   = [];                                   % remove NaNs
xFC(Inan)       = [];
yFC(Inan)       = [];
cFC(Inan)       = [];
SkdFC           = cumStd(kappaFC);              % cum. std of curv., DS, OR

%% Calculate progress variable values at location of each curvature value
% Nkappa      = length(kappaFC);
% cFC         = NaN(Nkappa, 1);
% parfor i = 1:Nkappa
%     cFC(i)  = interp2(X, Y, BWmeanC, xFC(i), yFC(i));
% end

%% Generate plot of cumulative standard deviations of curvature
h(end+1)                = figure;
set(gcf, 'color', 'w');
set(gcf, 'Name', ['Cumulative Standard Deviations, ', ftype, ', delta_f = ', num2str(delta_f, 2)]);
plot(1:length(SkdFC), SkdFC, 'g', 'LineWidth', lwidth);
set(gca, 'FontSize', fsize, 'FontName', fname);
xlabel('{\itN}');
ylabel('{\it\sigma} (1/mm)');
legend([ftype, ', OR'], 'location', 'best');

%% PDF of curvature
h(end+1)    = figure;
histPlot(kappaFC, 'fontSize', fsize, 'varname', 'kappa', 'units', '1/mm', ...
    'xlabel', '{\kappa}', 'plotType', 'stairs');
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);
KAPPAFC     = nanmean(kappaFC);     % mean value of curvature (1/mm)
kappapFC    = nanstd(kappaFC);      % standard deviation of curvature (1/mm)
kappasFC    = skewness(kappaFC);    % skewness of curvature

delta_p     = X(2) - X(1);                              % pixel size (mm)
h(end+1)    = figure;
histPlot(kappaFC*delta_p, 'fontSize', fsize, 'varname', 'kappadeltap', ...
    'xlabel', '{\kappa\Delta}', 'plotType', 'stairs');
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);

h(end+1)    = figure;
histPlot(kappaFC*delta_f0, 'fontSize', fsize, 'varname', 'kappadeltaf0', ...
    'xlabel', '{\kappa\delta_f_,_0}', 'plotType', 'stairs');
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);

h(end+1)    = figure;
histPlot(kappaFC*delta_SLmax, 'fontSize', fsize, 'varname', 'kappadeltafSLmax', ...
    'xlabel', '{\kappa\delta_{f,S_{L,max}}}', 'plotType', 'stairs');
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);

%% PDF of curvature conditioned on progress variable value
cbin        = 0.1;              % Width of progress variable bins
cvals       = 0:cbin:1;         % Progress variable bins
kappac      = cell(length(cvals)-1, 1);
KAPPAC      = NaN(length(cvals)-1, 1);
CFC         = KAPPAC;
legEntry    = kappac;
n           = kappac;
n2          = kappac;
kout        = kappac;
colOrd      = jet(length(cvals)-1);
h(end+1)    = figure;
hold on
for i = 1:length(cvals)-1
    c1          = cvals(i);
    c2          = cvals(i+1);
    Ic          = cFC >= c1 & cFC < c2;     % Indices of c1 <= cFC < c2
    kappac{i}   = kappaFC(Ic);              % curvatures satisfying c1 <= cFC < c2
    CFC(i)      = nanmean(cFC(Ic));         % mean progress variable satisfying c1 <= cFC < c2
    KAPPAC(i)   = nanmean(kappac{i});       % mean curvatures satisfying c1 <= cFC < c2
    legEntry{i} = [num2str(cvals(i)), ' {\leq} {\it<c>} < ', num2str(cvals(i+1))];
    [n{i}, kout{i}] = histPlot(kappac{i}, 'fontSize', fsize, 'varname', 'kappaC', 'units', '1/mm', ...
        'xlabel', '{\kappa}', 'plotType', 'stairs', 'color', colOrd(i, :));
    w           = kout{i}(2) - kout{i}(1);  % bin width (1/mm)
    n2{i}       = n{i}.*abs(kout{i});
    n2{i}       = n2{i}/(w*sum(n2{i}));     % normalize height such that area under histogram is 1
end
legend(legEntry, 'location', 'best', 'fontSize', fsize-2);
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2), ...
    ', cbin = ', num2str(cbin)]);
hold off

h(end+1)    = figure;
set(gcf, 'color', 'w');
set(gcf, 'Name', 'Normalized arc-angle-weighted histogram of kappaC');
hold on
for i = 1:length(kappac)
    stairs(kout{i}, n2{i}, 'LineWidth', lwidth, 'color', colOrd(i, :));
end
set(gca, 'FontSize', fsize, 'FontName', fname);
xlim([-5 5]);
xlabel('{\it\kappa} (1/mm)');
ylabel('{\itPDF}');
legend(legEntry, 'location', 'best', 'fontSize', fsize-2);
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2), ...
    ', cbin = ', num2str(cbin)]);
hold off

h(end+1)    = figure;
set(gcf, 'color', 'w');
set(gcf, 'Name', 'kappabar as a function of progress variable');
plot(CFC, KAPPAC, 'bx', 'LineWidth', lwidth, 'MarkerSize', msize);
set(gca, 'FontSize', fsize, 'FontName', fname);
xlabel('{\it<c>}');
ylabel('$\bar{\kappa}$ (1/mm)', 'interpreter', 'latex', 'FontSize', fsize+2);
xlim([0 1]);
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2), ...
    ', cbin = ', num2str(cbin)]);

%% Calculate statistics at the leading points of the flame
XLPC    = XLPC/numSets;
YLPC    = YLPC/numSets;
CLPC    = CLPC/numSets;
ULPC    = ULPC/numSets;
VLPC    = VLPC/numSets;
upLPC   = upLPC/numSets;
vpLPC   = vpLPC/numSets;

%% Generate PDFs of leading points properties
% Curvatures
kappaFLPC       = outliers(kappaLPC);           % remove outliers
Inan            = isnan(kappaFLPC);             % indices of NaNs
kappaFLPC(Inan) = [];                           % remove NaNs
KAPPAFLPC       = mean(kappaFLPC);              % mean curvature (1/mm) of instantaneous leading points
kappapFLPC      = std(kappaFLPC);               % standard deviation of curvature (1/mm) of ILPs
h(end+1)    = figure;
[nLP, kLP]  = histPlot(kappaFLPC, 'fontSize', fsize, 'varname', 'kappaLP', 'units', '1/mm', ...
    'xlabel', '{\kappa_L_P}', 'plotType', 'stairs');
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);
hold on
Ipos        = kappaFLPC >= 0 & kappaFLPC < 5;
PD          = fitdist(kappaFLPC(Ipos), 'gamma');
YPD         = pdf(PD, 0:0.1:5);
plot(0:0.1:5, YPD, 'r', 'LineWidth', lwidth);
xlim([0 5]);
hold off

h(end+1)    = figure;
histPlot(kappaFLPC*delta_p, 'fontSize', fsize, 'varname', 'kappaLPdeltap', ...
    'xlabel', '{\kappa_L_P\Delta}', 'plotType', 'stairs');
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);

h(end+1)    = figure;
histPlot(kappaFLPC*delta_f0, 'fontSize', fsize, 'varname', 'kappaLPdeltaf0', ...
    'xlabel', '{\kappa_L_P\delta_{f,0}}', 'plotType', 'stairs');
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);

h(end+1)    = figure;
histPlot(kappaFLPC*delta_SLmax, 'fontSize', fsize, 'varname', 'kappaLPdeltafSLmax', ...
    'xlabel', '{\kappa\delta_{f,S_{L,max}}}', 'plotType', 'stairs');
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);

% Positions
h(end+1)    = figure;
histPlot(xLPC, 'fontSize', fsize, 'varname', 'x_L_P', 'units', 'mm');

h(end+1)    = figure;
histPlot(yLPC, 'fontSize', fsize, 'varname', 'y_L_P', 'units', 'mm');

h(end+1)    = figure;
histPlot(cLPC, 'fontSize', fsize, 'varname', 'c_L_P', 'plotType', 'stairs');
xlim([0 1]);

% Velocities
h(end+1)    = figure;
f1_         = length(h);
histPlot(uLPC-ULPC, 'fontSize', fsize, 'varname', 'u''_L_P', 'units', 'm/s', ...
    'plotType', 'stairs', 'lmStyle', 'r');

h(end+1)    = figure;
f2_         = length(h);
histPlot(vLPC-VLPC, 'fontSize', fsize, 'varname', 'v''_L_P', 'units', 'm/s', ...
    'plotType', 'stairs', 'lmStyle', 'r');

if rc
    %% Find velocities at the average position of the leading points
    ULPaveC = ULPaveC/numSets;
    VLPaveC = VLPaveC/numSets;
    
    %% Find velocities at given progress variable values and upstream of flame
    ucpC    = ucpC./numSets;
    vcpC    = vcpC./numSets;
    
    h(end+1)    = figure;
    set(gcf, 'color', 'w');
    set(gcf, 'Name', 'turbulence intensity at different progress variables');
    plot(cbars, vcpC/U, 'bo', cbars, ucpC/U, 'go', 'LineWidth', lwidth);
    set(gca, 'FontSize', fsize, 'FontName', fname);
    xlabel('{\it<c>}');
    ylabel('{\itu} / {\itU}_0');
    legend('{\itu''_a_x}', '{\itu''_r_a_d}', 'location', 'best');
    
    %% Generate histograms of leading points properties
    figure(h(f1_));
    hold on
    histPlot(uLPaveC-ULPaveC, 'fontSize', fsize, 'varname', 'u''', 'units', 'm/s', ...
        'plotType', 'stairs', 'lmstyle', 'b');
    hold on
    histPlot(uUPC-mean(uUPC), 'fontSize', fsize, 'varname', 'u''', 'units', 'm/s', ...
        'plotType', 'stairs', 'lmstyle', 'g');
    legend('{\itu''_L_P}', ['{\itu''}({\it<c>} = ', num2str(CLP, 2), ')'], '{\itu''_u_p}', ...
        'location', 'best');
    
    figure(h(f2_));
    hold on
    histPlot(vLPaveC-VLPaveC, 'fontSize', fsize, 'varname', 'v''', 'units', 'm/s', ...
        'plotType', 'stairs', 'lmstyle', 'b');
    hold on
    histPlot(vUPC-mean(vUPC), 'fontSize', fsize, 'varname', 'v''', 'units', 'm/s', ...
        'plotType', 'stairs', 'lmstyle', 'g');
    legend('{\itv''_L_P}', ['{\itv''}({\it<c>} = ', num2str(CLP, 2), ')'], '{\itv''_u_p}', ...
        'location', 'best');
    
    legEntry    = cell(size(cbars));
    h(end+1)    = figure;
    histPlot(ucC-ones(length(ucC(:, 1)), 1)*mean(ucC, 1), 'fontSize', fsize, ...
        'varname', 'u''_c', 'xlabel', 'u''', 'units', 'm/s', ...
        'plotType', 'stairs', 'lineWidth', lwidth);
    for i = 1:length(legEntry)
        legEntry{i} = ['{\it<c>} = ', num2str(cbars(i))];
    end
    legend(legEntry, 'location', 'best');
    
    h(end+1)    = figure;
    histPlot(vcC-ones(length(vcC(:, 1)), 1)*mean(vcC, 1), 'fontSize', fsize, ...
        'varname', 'v''_c', 'xlabel', 'v''', 'units', 'm/s', ...
        'plotType', 'stairs', 'lineWidth', lwidth);
    legend(legEntry, 'location', 'best');
    
    h(end+1)    = figure;
    histPlot(cLPC, 'fontSize', fsize, 'varname', '<c>_L_P', 'xlim', [0, 1]);
end

%% Find ST,LD, u'axial and other flame parameters
[meanV, rmsV]       = meanRMSextract(S, P, T, U, fuel, phi, angle, expdate, rc, ...
    'vc7Folder', vc7Folder);
[Xv, Yv, VX, VY]    = prepareIMX(meanV, c);
[~, ~, vx, vy]      = prepareIMX(rmsV, c);
[clx, clsx]         = confidenceLevel(vx, N);
[cly, clsy]         = confidenceLevel(vy, N);

h(end+1)    = figure;
K           = axialPlot(meanV, Xv, Yv, VY, cly, U, d, x1, dim, [yl(1) 2], 1, 'b-', fsize);
hold on
[~, J]      = min(abs(Yv(1, :) - y1C));
plot(Yv(1, J)/d, VY(K, J)/U, 'rx', 'lineWidth', lwidth, 'MarkerSize', msize);
STLD        = VY(K, J);             % local displacement flame speed (m/s)
uy          = vy(K, J);             % axial turbulence intensity (m/s)
ux          = vx(K, J);             % radial turbulence intensity (m/s)
yf          = Yv(K, J);             % flame axial location (mm)
axialPlot(meanV, Xv, Yv, VX, clx, U, d, x1, 0, [yl(1) 2], 1, 'g-', fsize);
set(gcf, 'Name', [meanV.Source ' x = ' num2str(x1) ', VY, VX']);
legend('{\itU_a_x}', '{\itS_T_,_L_D}', '{\itU_r_a_d}', 'location', 'best');

h(end+1)    = figure;
axialPlot(rmsV, Xv, Yv, vy, clsy, U, d, x1, 0, [yl(1) 2], 0.5, 'b-', fsize);
hold on
axialPlot(rmsV, Xv, Yv, vx, clsx, U, d, x1, 0, [yl(1) 2], 0.5, 'g-', fsize);
plot(Yv(1, J)/d, vy(K, J)/U, 'rx', 'LineWidth', lwidth, 'MarkerSize', msize);
set(gcf, 'Name', [rmsV.Source ' x = ' num2str(x1) ', vy, vx']);
legend('{\itu''_a_x}', '{\itu''_r_a_d}', 'location', 'best');

%% Save figures and data
if rc == 1
    finalFolders = [expdate, '\Reactant Conditioned'];
else
    finalFolders = expdate;
end
currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
    int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
    fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', finalFolders];
cd(currdir);

finDir  = 'Final Results';
if ~exist(finDir, 'dir');
    mkdir(finDir);
end
currdir = [currdir, '\', finDir];
cd(currdir);

fnEnd   = [', shot ', int2str(frame), ', ', 'filt = ', int2str(filtSize(1)), ...
    ', ', method, ', images ', '1-', int2str(N)];
for i = length(h):-1:1
    fn      = get(h(i), 'Name');
    I       = fn == '.';
    fn(I)   = '_';
    I       = fn == '<' | fn == '>';
    fn(I)   = [];
    fnFinal = [fn, fnEnd];
    saveas(h(i), fnFinal, 'fig');
    saveas(h(i), fnFinal, 'png');
    close(h(i));
end

save(['data, delta_f = ', num2str(delta_f, 2), fnEnd, '.mat'], '-v7.3');
cd(prevdir);