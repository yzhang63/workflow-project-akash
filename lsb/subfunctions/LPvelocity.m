function [uLP, vLP] = LPvelocity(xLP, yLP, X, Y, VX, VY)
%LPVELOCITY determines the velocity at the leading point of a flame
%   LPVELOCITY(XLP, YLP, X, Y) determines the x and y velocity components 
%   at the leading point of a flame.
%   Inputs:
%       xLP: x-coordinate of the leading point of the flame (units specified in A.UnitX)
%       yLP: y-coordinate of the leading point of the flame (units specified in A.UnitY)
%       X: array of x-positions (units specified in A.UnitX)
%       Y: array of y-positions (units specified in A.UnitY)
%       VX: vector of x-coordinate velocities (units specified in A.UnitX)
%       VY: vector of y-coordinate velocities (units specified in A.UnitY)
%   Outputs:
%       uLP: x-component velocity at the leading point (m/s)
%       vLP: y-component velocity at the leading point (m/s)
%
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

[~, Ix] = min(abs(X(:, 1) - xLP));  % index of x-coordinate of leading point
Yu      = yLP - Y(1, :);            % Y-values shifted by location of leading point
Ip      = Yu >= 0;                  % indices of positive values (y-values upstream of leading point)
Iy      = find(Ip, 1);              % index of first y-position upstream of the leading point

uLP     = VX(Ix, Iy);
vLP     = VY(Ix, Iy);
end