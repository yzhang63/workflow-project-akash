function [X, Y, I] = prepareImage(A, frame, c)
%PREPAREIMAGE Creates image data from data structure
%   PREPAREIMAGE(A, frame, c) takes the data structure generated from READIMX 
%   for .im7 files and generates x and y position vectors and the intensity 
%   array.
%   Inputs:
%       A: structure generated from READIMX for .im7 files
%       frame: shot 1 or shot 2
%       c: position of the center of the burner in mm found from inspection
%       of .im7 image files.
%   Output:
%       X: vector of x-positions (units specified in A.UnitX)
%       Y: vector of y-positions (units specified in A.UnitY)
%       I: 2-D array of image intensities (units specified in
%       A.UnitI)
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

nx      = A.Nx;
ny      = A.Ny;

%% Set data range
XRange  = 1:nx;
YRange  = 1:ny;

%% Initialize X and Y values
X       = double((XRange - 0.5)*A.Grid*A.ScaleX(1) + A.ScaleX(2) - c); % x-range
Y       = double((YRange - 0.5)*A.Grid*A.ScaleY(1) + A.ScaleY(2)); % y-range

%% Calculate vector position and components
% Calculate frame range
if frame > 0 && frame < A.Nf,
    YRange = YRange + frame*ny;
end
I       = double(A.Data(:, YRange)');
end