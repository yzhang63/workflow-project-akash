function [X, N] = outlierRemove(X)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

E = 1;
N = 0;
while E > 0.01
    sigma           = std(X);
    X               = outliers(X);
    X(isnan(X))     = [];     % remove NaNs
    % figure; plot(Xf);
    E               = abs(std(X) - sigma)/std(X);
    N               = N + 1;
end
end