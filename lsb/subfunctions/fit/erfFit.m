function [cf gof] = erfFit(Y, C)
%ERFFIT Fit the error function to the average progress variable along
%the centerline
%   ERFFIT(Y, C) Creates a fit of the error function to the average
%   progress variable along the centerline, C. 
%   Inputs:
%       Y: vector of y-positions (units specified in A.UnitY)
%       C: average progress variable along the centerline
%   Outputs:
%       cf: curvefit object
%       gof: goodness-of-fit

% --- Create fit "error function"
fo_         = fitoptions('method', 'NonlinearLeastSquares', 'Lower', [0 0 0 0], 'Upper', [1  1 50  1]);
ok_         = isfinite(Y) & isfinite(C);

if ~all( ok_ )
    warning( 'GenerateMFile:IgnoringNansAndInfs',...
        'Ignoring NaNs and Infs in data.' );
end

st_ = [0.5 0.22 24 0.5];
set(fo_,'Startpoint',st_);
ft_         = fittype('a*(erf(b*(x-c)))+d',...
    'dependent',{'y'},'independent',{'x'},...
    'coefficients',{'a', 'b', 'c', 'd'});

% Fit this model using new data
[cf gof]    = fit(Y(ok_), C(ok_), ft_, fo_);
end