function [fo gof] = axialFit(X, Y, u, x, yl)
%AXIALFIT Generates a linear fit for u as a function of axial distance
%   AXIALFIT(X, Y, u, U, d, x, yl, dim) generates a linear fit for u as a
%   function of axial distance at a specified radial position over a
%   specified range.
%   Inputs:
%       X: 2-D array of x-positions (units specified in A.UnitX)
%       Y: 2-D array of y-positions (units specified in A.UnitY)
%       u: 2-D array of velocities (units specified in A.UnitI)
%       d: burner diameter (mm)
%       x: radial position of axial cut (dimensional)
%       yl: limits over which to perform linear fit ([ymin ymax]) (dimensional)
%   Outputs
%       fo: fit object (linear fit)
%       gof: goodness-of-fit structure
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

[~, I]      = min(abs(X(:, 1) - x));
[~, J1]     = min(abs(Y(1, :) - yl(1)));
[~, J2]     = min(abs(Y(1, :) - yl(2)));

[fo, gof]   = fit(Y(I, J1:-1:J2)', u(I, J1:-1:J2)', 'poly1');

end