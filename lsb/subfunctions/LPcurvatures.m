function [xLP, yLP, sLP, kappaLP] = LPcurvatures(x, y, s, kappa)
%LPCURVATURES Finds curvatures at the leading points of the edge
%   LPCURVATURES(x, y, s, kappa) finds the curvature, kappa, at the leading
%   points of the edges in x, y, s
%   Inputs:
%       x: Nx1 cell array of x-coordinates of the edge (N: number of edges)
%       y: Nx1 cell array of y-coordinates of the edge
%       s: Nx1 cell array of s-coordinates of the edge
%       kappa: Nx1 cell array of kappa values

%% Initialize variables
N       = length(x);              % number of edges
xLP     = NaN(N, 1);
yLP     = xLP;
sLP     = xLP;
kappaLP = xLP;

%% Find curvatures at leading points
parfor i = 1:N
    if ~isempty(x{i})
        [yLP(i), ILP]   = min(y{i});
        if ILP == 1 || ILP == length(x{i}) % if LP is at edge of domain
            yLP(i)          = NaN;
        else
            xLP(i)          = x{i}(ILP);
            sLP(i)          = s{i}(ILP);
            kappaLP(i)      = kappa{i}(ILP);
        end
    end
end
end

