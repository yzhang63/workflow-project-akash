function [xLP, yLP, cLP, uLP, vLP] = ...
    LPvelocities(S, P, T, U, fuel, phi, angle, expdate, c, N, E, X, Y, C, varargin)
%LPVELOCITIES Calculates velocities at the leading points of flames
%   LPVELOCITIES(S, P, T, U, fuel, phi, angle, expdate, N, E) determines
%   the x and y velocity components at the leading points of N flames.
%   Inputs:
%       S: swirl number
%       P: pressure (atm)
%       T: reactant temperature (K)
%       U: mean flow velocity (m/s)
%       fuel: fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore this variable if phi = 0)
%       phi: equivalence ratio (use 0 for nonreacting)
%       angle: turbulence generator angle
%       expdate: string of the experiment date in form: 'yyyymmdd'
%       c: position of the center of the burner in mm found from inspection
%       of im7 image files.
%       N: number of images or 2-element vector range of images (i.e., N =
%           [F L], where F is the first image index and L is the last image
%           index
%       E: cell array of flame edge positions found from BINARIZEIMAGES
%       X: vector of x-positions (units specified in A.UnitX)
%       Y: vector of y-positions (units specified in A.UnitY)
%       C: 2-D array of the average progress variable
%   Outputs:
%       xLP: column vector of x-components of the leading points (mm)
%       yLP: column vector of y-components of the leading points (mm)
%       cLP: column vector of c-values of the leading points (mm)
%       uLP: column vector of x-component velocities at the leading points (m/s)
%       vLP: column vector of y-component velocities at the leading points (m/s)
%
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought:
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

%% Assign variables to varargin cell values
% default values
vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';   % folder name for processed vc7 files
rc          = 1;                                % 1: reactant-conditioned, 0: unconditioned
for i = 1:2:length(varargin)
    switch lower(varargin{i})
        case 'vc7folder'
            vc7Folder   = varargin{i+1};
        case 'rc'
            rc          = varargin{i+1};
        otherwise
            error('Invalid property name');
    end
end

%% Set range of vc7's to be extracted and initialize variables
if length(N) == 1
    N = [1 N];
end
F   = N(1);                               % index of first image
L   = N(2);                               % index of last image
N   = L - F + 1;                          % number of images to be analyzed

uLP             = NaN(N, 1);
vLP             = uLP;
xLP             = uLP;
yLP             = uLP;
cLP             = uLP;

%% Extract vector information from vc7 files
A               = vc7extract(S, P, T, U, fuel, phi, angle, expdate, 1, rc, 'vc7Folder', vc7Folder);
[Xv, Yv, ~, ~]  = prepareIMX(A, c);
parfor i = 1:N
    if ~isempty(E{i}) && ~isnan(mean(mean(E{i})))
        [ylp, ILP]          = min(E{i}(:, 2));
        % No leading point if minimum is first or last index (left or right side of image)
        if ILP == 1 || ILP == length(E{i}(:, 2))
            yLP(i)  = NaN;
        else
            xlp                 = E{i}(ILP, 1);
            clp                 = C(Y == ylp, X == xlp); %#ok<PFBNS>
            A                   = vc7extract(S, P, T, U, fuel, phi, angle, expdate, i+F-1, rc, ...
                'vc7Folder', vc7Folder);
            [~, ~, VX, VY]      = prepareIMX(A, c);
            VX(VX == 0)         = NaN;                  % replace zeros with NaNs
            VY(VY == 0)         = NaN;
            VX                  = inpaint_nans(VX);     % interpolate NaNs to get values right at flame
            VY                  = inpaint_nans(VY);
            xLP(i)              = xlp;
            yLP(i)              = ylp;
            cLP(i)              = clp;
            [uLP(i), vLP(i)]    = LPvelocity(xlp, ylp, Xv, Yv, VX, VY);
        end
    end
end
end