function [y, kappa] = pseudoCurvatureSin2(x, a0, as, k, ks, n, N)
%PSEUDOCURVATURESIN2 Finds curvatures of pseudorandom sine curves
%   PSEUDOCURVATURESIN2(x, a0, as, k, ks, n, N) generates pseudorandom sine 
%   curves for a given set of x-values and then calculates curvatures for 
%   the sine curve
%   Inputs:
%       x: column vector of x-values
%       a0: desired mean y-value for the sine curve
%       as: desired standard deviation in y for the sine curve
%       k: desired mean wavenumber for the sine curve
%       ks: desired standard deviation of the wavenumber for the sine curve
%       n: number of terms to use in sine curve
%       N: number of splines to generate
%   Outputs:
%       y: cell array of y-values for the splines
%       kappa: cell array of curvatures for the splines

x       = x(:)';                    % make sure that x is a row vector
%% Initialize values
kappa   = cell(N, 1);
y       = kappa;

%% Perform curvature calculations
parfor i = 1:N
    %% Find spline values
    [y{i}, ai, ki]  = randSin2(x, a0, as, k, ks, n);
    AI              = ai*ones(1, length(x));
    yp              = (ki')*(AI.*cos(ki*x));        % first derivative
    ypp             = -(ki').^2*(AI.*sin(ki*x));    % second derivative
    yp              = yp';
    ypp             = ypp';
    %% Find the curvature
    kappa{i}        = ypp./(1 + yp.^2).^(3/2);      % curvature
end
end