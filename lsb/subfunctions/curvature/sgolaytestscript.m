function [yf, dyf, d2yf] = sgolayFit(y, dx, k, f)
% SGOLAYFIT uses the Savitzky-Golay smoothing filter to fit data
%   SGOLAYFIT(x, y, dx, k, f) uses the Savitzky-Golay smoothing filter to 
%   perform a local polynomial regression (of degree k) on a series 
%   Inputs:
%       y: vector of y-positions
%       dx: delta-x of independent variable
%       k: order of polynomial for fitting
%       f: window size over which to perform fit
%   Outputs:
%       yf: vector of fitted y data
%       dyf: vector of fitted y' data
%       d2yf: vector of fitted y'' data

[~, g]  = sgolay(k, f);                         % Calculate S-G coefficients
HW      = (f + 1)/2 - 1;                        % Half window

% initialize variables
yf      = NaN(size(x));
dyf     = yf;
d2yf    = yf;

Y       = circulant(y, -1);
Y       = Y(1:end-2*HW, 1:length(g(:, 1)));

yf(HW+1:end-HW)     = Y*g(:, 1);                % smoothed data
dyf(HW+1:end-HW)    = Y*g(:, 2);                % 1st differential
d2yf(HW+1:end-HW)   = 2*Y*g(:, 3);              % 2nd differential

dyf     = dyf/dx;                               % 1st derivative
d2yf    = d2yf/dx^2;                            % 2nd derivative
end