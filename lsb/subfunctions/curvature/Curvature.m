function [xDS, yDS, cDS, sDS, mx, my, mc, xp, yp, xpp, ypp, ms, mkappa, R, nx, ny] = ...
    Curvature(X, Y, C, Emat)
%disp('Emat class');
%disp(class(Emat));
E = cell(1,1);
E{1} = Emat;
%xDS = 0;
%yDS = 0;
%cDS = 0;
%sDS = 0;
%mx = 0;
%my = 0;
%mc = 0;
%xp = 0;
%yp = 0;
%xpp = 0;
%ypp = 0;
%ms = 0;
%mkappa = 0;
%R = 0;
%nx = 0;
%ny = 0;

%disp(E);
%disp(E{1});

%disp(X);
%disp(Y);
%disp(C);
%disp(Emat);

%function [xDS, yDS, cDS, sDS, x, y, c, xp, yp, xpp, ypp, s, kappa, R, nx, ny] = ...
%    Curvature(X, Y, C, E, varargin)
% CURVATURE calculates curvature along edges E
%   CURVATURE(X, Y, BW, E) calculates the curvature along the edges E
%   Inputs:
%       X: vector of x-positions (units specified in A.UnitX)
%       Y: vector of y-positions (units specified in A.UnitY)
%       C: average progress variable field
%       E: Nx1 cell array, where N is the number of edges. Each cell within
%       the array contains a 2 column array of x and y positions of the
%       edge (dimensions: mm)
%   Outputs:
%       xDS: downsampled x-coordinate of the edge
%       yDS: downsampled y-coordinate of the edge
%       cDS: downsampled c-coordinate of the edge
%       sDS: downsampled arc length along the edge
%       x: x-coordinate of the fitted edge
%       y: y-coordinate of the fitted edge
%       c: c-coordinate of the fitted edge
%       xp: x' of the fitted edge
%       yp: y' of the fitted edge
%       xpp: x'' of the fitted edge
%       ypp: y'' of the fitted edge
%       s: s-coordinate of the fitted edge
%       kappa: curvature of the fitted edge
%       R: radius of curvature of the fitted edge
%       nx: x-component of the unit normal vector
%       ny: y-component of the unit normal vector

%% Assign variables to varargin cell values
% default values
delta_f = 0.5;          % flame thickness (mm)
ftype   = 'csaps';      % type of fit ('poly': polynomial, 'csaps': cubic smoothing spline)
DS      = 'on';         % downsample ('off': no downsampling, 'on': downsample based on flame thickness)

% Polynomial fit parameters
k       = 4;            % order of polynomial fit
polyRep = 2;            % number of times to repeat the polynomial edge smoothing
f       = 15;           % number of points to fit over --> use odd number

%for i = 1:2:length(varargin)
%    switch lower(varargin{i})
%        case 'delta_f'
%            delta_f = varargin{i+1};
%        case 'downsample'
%            DS      = varargin{i+1};
%        case 'fittype'
%            ftype   = varargin{i+1}
%        case 'order'
%            k       = varargin{i+1};
%        case 'polyrepeats'
%            polyRep = varargin{i+1};
%        case 'fitpoints'
%            f       = varargin{i+1};
%        otherwise
%            error('Invalid property name');
%    end
%end
% to replace varargin pars
%delta_f0 = 0.605988764963387; %changed with different dataset
delta_f0 = 0.60599; %changed with different dataset
delta_f = delta_f0;
%SL0 = 34.568990000000000; %inputParameters.m
SL0 = 34.56899; %inputParameters.m
ftype = 'csaps'; %cscaps or poly: type of fit ('poly': polynomial, 'csaps': cubic smoothing spline

%% Calculate the minimum resolution that can be resolved
delta_s = 0.5*pi*delta_f;   % Arc length resampling interval (mm)
delta_p = X(2) - X(1);      % pixel size (mm)
switch DS
    case 'on'
        ds_norm = delta_s/delta_p;  % Normalized arc length resampling interval (pixels)
    case 'off'
        ds_norm = 1;
    otherwise
        error('Invalid argument for propertyValue downsample');
end

%disp('test1');
%% Initialize cell arrays
xDS     = cell(size(E));    % downsampled x-coordinate of the edge
yDS     = xDS;              % downsampled y-coordinate of the edge
cDS     = xDS;              % downsampled c-coordinate of the edge
sDS     = xDS;              % downsampled arc length along the edge
x       = xDS;              % x-coordinate of the fitted edge
y       = xDS;              % y-coordinate of the fitted edge
c       = xDS;              % c-coordinate of the fitted edge
xp      = xDS;              % x' of the fitted edge
yp      = xDS;              % y' of the fitted edge
xpp     = xDS;              % x'' of the fitted edge
ypp     = xDS;              % y'' of the fitted edge
s       = xDS;              % s-coordinate of the fitted edge
kappa   = xDS;              % curvature of the fitted edge
R       = xDS;              % radius of curvature of the fitted edge
nx      = xDS;              % x-component of the unit normal vector
ny      = xDS;              % y-component of the unit normal vector

%% Perform calculations for each edge
%sDS_= (1:1:10)';
%parfor i = 1:length(E)
for i = 1:length(E)
    if ~isempty(E{i}) && ~isnan(mean(mean(E{i})))
        %% Curvature calculations
        % Calculate the distance along the flame contour
        xe = E{i}(:, 1);
        ye = E{i}(:, 2);
        se = arcLength(xe, ye);
        % Adjust starting point if downsampling in order to generate values at every point on the edge
        for j = 1:floor(ds_norm)
            % Downsample the edge on to a fewer number of points
            switch DS
                case 'on'
                    startpt = (j - 1)*delta_p;
                    sDS_    = (startpt:delta_s:max(se))';
                    xDS_    = interp1(se, xe, sDS_);
                    yDS_    = interp1(se, ye, sDS_);
                case 'off'
                    sDS_    = se;
                    xDS_    = xe;
                    yDS_    = ye;
                otherwise
                    error('Invalid argument for propertyValue downsample');
            end
            % Perform low order fit to the data. This is done by fitting a polynomial x = x(s) and y = y(s)
            switch ftype
                case 'poly'
                    [x_, y_, xp_, yp_, xpp_, ypp_, s_]  = ...
                        PolynomialFit(xDS_, yDS_, sDS_, f, k, polyRep);
                case 'csaps'
                    %disp('test2: @case csaps');
                    [x_, y_, xp_, yp_, xpp_, ypp_, s_]  = SmoothingSplineFit(xDS_, yDS_, sDS_);
                case 'sgolay'
                    [x_, y_, xp_, yp_, xpp_, ypp_, s_]  = SavitzkyGolayFit(xDS_, yDS_, sDS_, k, f);
                otherwise
                    error('Invalid argument for propertyValue ftype');
            end
            % Combine the different curves generated at different starting locations
            % (do not include endpoints since spline derivatives are not physical at these locations)
            sDS{i}  = [sDS{i}; sDS_(2:end-1)];
            xDS{i}  = [xDS{i}; xDS_(2:end-1)];
            yDS{i}  = [yDS{i}; yDS_(2:end-1)];
            x{i}    = [x{i}; x_(2:end-1)];
            y{i}    = [y{i}; y_(2:end-1)];
            s{i}    = [s{i}; s_(2:end-1)];
            xp{i}   = [xp{i}; xp_(2:end-1)];
            yp{i}   = [yp{i}; yp_(2:end-1)];
            xpp{i}  = [xpp{i}; xpp_(2:end-1)];
            ypp{i}  = [ypp{i}; ypp_(2:end-1)];
        end
        % Sort values by increasing arc length
        [sDS{i}, I] = sort(sDS{i});
        xDS{i}      = xDS{i}(I);
        yDS{i}      = yDS{i}(I);
        [s{i}, J]   = sort(s{i});
        x{i}        = x{i}(J);
        y{i}        = y{i}(J);
        xp{i}       = xp{i}(J);
        yp{i}       = yp{i}(J);
        xpp{i}      = xpp{i}(J);
        ypp{i}      = ypp{i}(J);
        
        % Calculate progress variable values
        cDS_        = zeros(size(xDS{i}));
        c_          = cDS_;
        for j = 1:length(cDS_)
            cDS_(j) = interp2(X, Y, C, xDS{i}(j), yDS{i}(j));
            c_(j)   = interp2(X, Y, C, x{i}(j), y{i}(j));
        end
        cDS{i}      = cDS_;
        c{i}        = c_;
        
        % Calculate the curvature
        kappa{i}    = (yp{i}.*xpp{i} - xp{i}.*ypp{i})./(xp{i}.^2 + yp{i}.^2).^(3/2);
        R{i}        = 1./kappa{i};   % Radius of curvature, mm
        
        % Calculate the unit normal vector
        Nx          = yp{i}.^2.*xpp{i} - xp{i}.*yp{i}.*ypp{i};
        Ny          = xp{i}.^2.*ypp{i} - yp{i}.*xp{i}.*xpp{i};
        nx{i}       = Nx./sqrt(Nx.^2 + Ny.^2);
        ny{i}       = Ny./sqrt(Nx.^2 + Ny.^2);
    end
end
%disp('sizeof(xDS)');
%size(xDS)
%class(xDS)
%disp('sizeof(yDS)');
%size(yDS)
%class(yDS)
%disp('sizeof(cDS)');
%size(cDS)
%class(cDS)
%disp('sizeof(sDS)');
%size(sDS)
%class(sDS)
%disp('sizeof(x)');
%size(x)
%class(x)
%disp('sizeof(xp)');
%size(xp)
%class(xp)
%disp('sizeof(xpp)');
%size(xpp)
%class(xpp)
%disp('sizeof(kappa)');
%size(kappa)
%class(kappa)
%for k=1:length(kappa)
%  mm = kappa{k}
%end
%nn = cell2mat(kappa)
%size(nn)

mkappa = cell2mat(kappa);
mx = cell2mat(x);
my = cell2mat(y);
mc = cell2mat(c);
ms = cell2mat(s);

disp('@matlab func, listing resulting vars info:')
disp(size(mkappa));
disp(mkappa(1));
disp(size(mx));
disp(mx(1));
disp(size(my));
disp(my(1));
disp(size(mc));
disp(mc(1));
disp(size(ms));
disp(ms(1));
save curvature.mat

disp('Existing Curvature matlab func...')

end
