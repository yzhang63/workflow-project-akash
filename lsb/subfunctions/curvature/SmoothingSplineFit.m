function [xf, yf, xp, yp, xpp, ypp, s] = SmoothingSplineFit(x, y, s)
% SMOOTHINGSPLINEFIT fits a cubic smoothing spline to the data
%   SMOOTHINGSPLINEFIT(x, y, s, n, order, polyRepeats) fits a cubic 
%   smoothing spline to the points in x and y
%   Inputs:
%       x: vector of x-positions
%       y: vector of y-positions
%       s: vector of arc lengths
%   Outputs:
%       xf: vector of fitted x data
%       yf: vector of fitted y data
%       xp: vector of fitted x' data
%       yp: vector of fitted y' data
%       xpp: vector of fitted x'' data
%       ypp: vector of fitted y'' data
%       s: vector of fitted s data

x   = x(:);                 % ensures x is a column vector
y   = y(:);
s   = s(:);

px  = csaps(s, x);          % cubic smoothing spline fit x(s) in ppform
py  = csaps(s, y);          % cubic smoothing spline fit y(s) in ppform
xf  = fnval(px, s);
yf  = fnval(py, s);
xp  = fnval(fnder(px), s);
yp  = fnval(fnder(py), s);
xpp = fnval(fnder(px, 2), s);
ypp = fnval(fnder(py, 2), s);
end