function [p, y] = genRandPolyn(n, sigma, x)
%GENRANDPOLYN Generate a random polynomial
%   GENRANDPOLYN(n, range) Generate an n-th order polynomial over a
%   specified x-range
%   Inputs:
%       n: order of the polynomial
%       sigma: standard deviation of polynomial coefficients
%       x: x-values
%   Outputs:
%       p: polynomial coefficients
%       y: y-values

p = sigma*randn(1, n);
y = polyval(p, x);
end