function [y, p] = randSpline(x, ybar, sig)
%RANDSPLINE Generate a random spline
%   RANDSPLINE(x, ybar, sigma) Generate an n-th order polynomial over a
%   specified x-range
%   Inputs:
%       x: x-values
%       ybar: mean of y-values
%       sigma: standard deviation of y-values
%   Outputs:
%       y: y-values

a   = min(x);
b   = max(x);
dx  = x(2) - x(1);
nDS = round(length(x)/20);              % length of downsampled x-values
xDS = linspace(a, b, nDS);
xDS = xDS';
ex  = 1 + dx*randn(size(xDS));          % pseudorandom error to be added to xDS
xDS = xDS.*ex;                          % add pseudorandom error to x-values
yDS = ybar + sig*randn(size(xDS));      % downsampled y-values
yDS = [0; yDS; 0];
p   = spline(xDS, yDS);                 % generate spline curve
y   = ppval(p, x);
end