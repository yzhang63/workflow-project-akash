function [xDS, yDS, sDS, x, y, xp, yp, xpp, ypp, s, kappa, R] = CurvatureNoDS(X, Y, BW, E, frame, method, varargin)
% CURVATURE calculates curvature along edges E
%   CURVATURE(X, Y, BW, E) calculates the curvature along the edges E
%   Inputs:
%       X: vector of x-positions (units specified in A.UnitX)
%       Y: vector of y-positions (units specified in A.UnitY)
%       BW: 3D array of binarized images
%       E: Nx1 cell array, where N is the number of edges. Each cell within
%       the array contains a 2 column array of x and y positions of the
%       edge (dimensions: mm)
%       frame: shot 1 or shot 2
%       method: binarization method
%   Outputs:
%       xDS: downsampled x-coordinate of the edge
%       yDS: downsampled y-coordinate of the edge
%       sDS: downsampled arc length along the edge
%       x: x-coordinate of the fitted polynomials
%       y: y-coordinate of the fitted polynomials
%       xp: x' of the fitted polynomials
%       yp: y' of the fitted polynomials
%       xpp: x'' of the fitted polynomials
%       ypp: y'' of the fitted polynomials
%       s: s-coordinate of the fitted polynomials
%       kappa: curvature of the fitted polynomials
%       R: radius of curvature of the fitted polynomials

%% Assign variables to varargin cell values
d       = 36.3;         % burner diameter (mm)
msize   = 10;           % marker size
lwidth  = 3;            % marker size
% delta_f = 0.5;          % flame thickness (mm)
res     = 600;          % resolution (dpi)
cmap    = 'gray';       % colormap

% Polynomial fit parameters
order   = 4;            % order of polynomial fit
polyRep = 2;            % number of times to repeat the polynomial edge smoothing
n       = 15;           % number of points to fit over --> use odd number

for i = 1:2:length(varargin)
    switch lower(varargin{i})
        case 'linewidth'
            lwidth  = varargin{i+1};
        case 'markersize'
            msize   = varargin{i+1};
%         case 'delta_f'
%             delta_f = varargin{i+1};
        case 'order'
            order   = varargin{i+1};
        case 'polyrepeats'
            polyRep = varargin{i+1};
        case 'fitpoints'
            n       = varargin{i+1};
        case 'colormap'
            cmap    = varargin{i+1};
        otherwise
            error('Invalid property name');
    end
end

%% Calculate the minimum resolution that can be resolved
% delta_s = ((2/3)*pi*delta_f)/4;

%% Create a directory to store the processed data and the images
folderName      = 'Curvatures';
if ~exist(folderName, 'dir');
    mkdir(folderName);
end
currdir         = cd;
curvatureDir    = [currdir, '\', folderName];
cd(curvatureDir);

%% Initialize cell arrays

xDS     = cell(size(E));    % downsampled x-coordinate of the edge
yDS     = xDS;              % downsampled y-coordinate of the edge
sDS     = xDS;              % downsampled arc length along the edge
x       = xDS;              % x-coordinate of the fitted polynomials
y       = xDS;              % y-coordinate of the fitted polynomials
xp      = xDS;              % x' of the fitted polynomials
yp      = xDS;              % y' of the fitted polynomials
xpp     = xDS;              % x'' of the fitted polynomials
ypp     = xDS;              % y'' of the fitted polynomials
s       = xDS;              % s-coordinate of the fitted polynomials
kappa   = xDS;              % curvature of the fitted polynomials
R       = xDS;              % radius of curvature of the fitted polynomials

%% Perform calculations for each edge
parfor i = 1:length(E)
    if ~isempty(E{i}) && ~isnan(mean(mean(E{i})))
        figure;
        dispImageNoA(X, Y, BW(:, :, i), 16, 'gray');
        hold on
        plot(E{i}(:, 1)/d, E{i}(:, 2)/d, 'g', 'LineWidth', 2);
        xe = E{i}(:, 1);
        ye = E{i}(:, 2);
        
        %% Curvature calculations
        % Calculate the distance along the flame contour
        se = arcLength(xe, ye);
        
%         % Downsample the edge on to a fewer number of points
%         sDS{i} = (0:delta_s:max(se))';
%         xDS{i} = interp1(se, xe, sDS{i});
%         yDS{i} = interp1(se, ye, sDS{i});
        
        % Next three lines are used to see the effect of not downsampling
        sDS{i} = se;
        xDS{i} = xe;
        yDS{i} = ye;
        
        % Perform low order fit to the data. This is done by fitting a polynomial x = x(s) and y = y(s)
        [x{i}, y{i}, xp{i}, yp{i}, xpp{i}, ypp{i}, s{i}] = PolynomialFit(xDS{i}, yDS{i}, sDS{i}, n, order, polyRep);
        
        % Calculate the curvature
        kappa{i}    = (yp{i}.*xpp{i} - xp{i}.*ypp{i})./(xp{i}.^2 + yp{i}.^2).^(3/2);
        R{i}        = 1./kappa{i};   % Radius of curvature, mm
        
        zeroCurvature   = find(abs(kappa{i}) < 0.001);
        negCurvature    = find(kappa{i} < -0.001);
        posCurvature    = find(kappa{i} > 0.001);
        
        plot(x{i}(zeroCurvature)/d, y{i}(zeroCurvature)/d, 'mx', ...
            x{i}(negCurvature)/d, y{i}(negCurvature)/d, 'gx', ...
            x{i}(posCurvature)/d, y{i}(posCurvature)/d, 'cx', ...
            'MarkerSize', msize, 'LineWidth', lwidth);
        hold off
        
        set(gcf, 'Name', ['Curvature, order = ', int2str(order), ...
            ', pRepeat = ', int2str(polyRep), ', n = ', int2str(n), ', ', int2str(i), ', no DS']);
        colormap(cmap);
        
        fn      = get(gcf, 'Name');
        I       = fn == '.';
        fn(I)   = '_';
        I       = fn == '<' | fn == '>';
        fn(I)   = [];
        fn      = [fn, ', shot ', int2str(frame), ', ' method];
        saveas(gcf, fn, 'fig');
        print(gcf, fullfile(curvatureDir, [fn, '.png']), '-dpng', ['-r', num2str(res)], '-painters');
        close;
    end
end
cd(currdir);
end