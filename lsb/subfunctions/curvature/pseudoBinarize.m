function BWs = pseudoBinarize(x, y, ye)
%PSEUDOBINARIZE Generates binary images from edge coordinates
%   PSEUDOBINARIZE(x, y, N) generates binary images from edge coordinates
%   given by x and y. All values above the edge are set to 1, values below
%   the edge are set to zero. The edge must be an explicit function of the
%   variable x, i.e. y = f(x).
%   Inputs:
%       x: maximum x-value (pixels)
%       y: maximum y-value (pixels)
%       ye: cell array of y-values for the edge for each image (pixels)

N   = length(ye);                        % number of edges
BWs = zeros(y, x, N);
for i = 1:N
    for j = 1:x
        BWs(1:ye{i}(j), j, i) = 1;
    end
end
end