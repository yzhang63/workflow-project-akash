function [y, kappa] = pseudoCurvatureSin(x, a0, as, ws, n, N)
%PSEUDOCURVATURE Finds curvatures of pseudorandom splines
%   PSEUDOCURVATURE(x, ybar, sigma, N) generates pseudorandom spline curves
%   for a given set of x-values and then calculates curvatures for the
%   splines
%   Inputs:
%       x: column vector of x-values
%       ybar: desired mean y-value for the splines
%       sigma: desired standard deviation in y for the splines
%       N: number of splines to generate
%   Outputs:
%       y: cell array of y-values for the splines
%       kappa: cell array of curvatures for the splines

x       = x(:)';                    % make sure that x is a row vector
%% Initialize values
kappa   = cell(N, 1);
y       = kappa;

%% Perform curvature calculations
parfor i = 1:N
    %% Find spline values
    [y{i}, ai, wi]  = randSin(x, a0, as, ws, n);
    AI              = ai*ones(1, length(x));
    yp              = (wi')*(AI.*cos(wi*x));        % first derivative
    ypp             = -(wi').^2*(AI.*sin(wi*x));    % second derivative
    yp              = yp';
    ypp             = ypp';
    %% Find the curvature
    kappa{i}        = ypp./(1 + yp.^2).^(3/2);      % curvature
end
end