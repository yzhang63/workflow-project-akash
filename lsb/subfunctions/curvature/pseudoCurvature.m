function [y, kappa] = pseudoCurvature(x, ybar, sig, N)
%PSEUDOCURVATURE Finds curvatures of pseudorandom splines
%   PSEUDOCURVATURE(x, ybar, sigma, N) generates pseudorandom spline curves
%   for a given set of x-values and then calculates curvatures for the
%   splines
%   Inputs:
%       x: column vector of x-values
%       ybar: desired mean y-value for the splines
%       sigma: desired standard deviation in y for the splines
%       N: number of splines to generate
%   Outputs:
%       y: cell array of y-values for the splines
%       kappa: cell array of curvatures for the splines

%% Initialize values
kappa   = cell(N, 1);
y       = kappa;

%% Perform curvature calculations
parfor i = 1:N
    %% Find spline values
    [y{i}, p]   = randSpline(x, ybar, sig);     % spline values and coefficients
    pp          = fnder(p);                     % first derivative coefficients
    yp          = ppval(pp, x);                 % first derivative values
    ppp         = fnder(p, 2);                  % second derivative coefficients
    ypp         = ppval(ppp, x);                % second derivative values
    
    %% Find the curvature
    kappa{i}    = ypp./(1 + yp.^2).^(3/2);      % curvature
end
end