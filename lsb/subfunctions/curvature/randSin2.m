function [y, ai, ki] = randSin2(x, a0, as, k, ks, n)
%RANDSIN2 Generates a random combination of sin functions
%   RANDSIN2(x, a0, as, k, ks, n) Generates a random combination of n sin 
%   terms of the following form:
%   
%       y = a0 + sum(ai*sin(ki*x))
%   
%   Inputs:
%       x: x-values
%       a0: mean of y-values
%       as: standard deviation of ai
%       k: mean value of ki
%       ks: standard deviation of ki
%       n: number of terms
%   Outputs:
%       y: y-values
%       ai: column vector of pseudorandom amplitudes, ai
%       ki: column vector of pseudorandom wavenumbers, ki

x   = x(:)';                    % make sure that x is a row vector
ai  = as*randn(n, 1);           % column vector of random ai
ki  = k + ks*randn(n, 1);       % column vector of random wi
AI  = ai*ones(1, length(x));    % matrix of ai with form: [ai, ai, ... ai] dim: length(ai) x length(x)
y   = a0 + sum(AI.*sin(ki*x));
y   = y';
end