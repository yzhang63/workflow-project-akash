function [xDS, yDS, sDS, x, y, xp, yp, xpp, ypp, s, kappa, R] = ...
    Curvature(X, Y, BW, E, frame, method, filt, varargin)
% CURVATURE calculates curvature along edges E
%   CURVATURE(X, Y, BW, E) calculates the curvature along the edges E
%   Inputs:
%       X: vector of x-positions (units specified in A.UnitX)
%       Y: vector of y-positions (units specified in A.UnitY)
%       BW: 3D array of binarized images
%       E: Nx1 cell array, where N is the number of edges. Each cell within
%       the array contains a 2 column array of x and y positions of the
%       edge (dimensions: mm)
%       frame: shot 1 or shot 2
%       method: binarization method
%       filt: filter size (pixels)
%   Outputs:
%       xDS: downsampled x-coordinate of the edge
%       yDS: downsampled y-coordinate of the edge
%       sDS: downsampled arc length along the edge
%       x: x-coordinate of the fitted polynomials
%       y: y-coordinate of the fitted polynomials
%       xp: x' of the fitted polynomials
%       yp: y' of the fitted polynomials
%       xpp: x'' of the fitted polynomials
%       ypp: y'' of the fitted polynomials
%       s: s-coordinate of the fitted polynomials
%       kappa: curvature of the fitted polynomials
%       R: radius of curvature of the fitted polynomials

%% Assign variables to varargin cell values
% default values
d       = 36.3;         % burner diameter (mm)
msize   = 6;            % marker size
lwidth  = 3;            % marker size
delta_f = 0.5;          % flame thickness (mm)
res     = 600;          % resolution (dpi)
cmap    = 'gray';       % colormap
ftype   = 'poly';       % type of fit ('poly': polynomial, 'csaps': cubic smoothing spline)
DS      = 'on';         % downsample ('off': no downsampling, 'on': downsample based on flame thickness)
F       = 1;            % initial image index (affects filename used when saving fig and png files)

% Polynomial fit parameters
order   = 4;            % order of polynomial fit
polyRep = 2;            % number of times to repeat the polynomial edge smoothing
n       = 15;           % number of points to fit over --> use odd number

for i = 1:2:length(varargin)
    switch lower(varargin{i})
        case 'linewidth'
            lwidth  = varargin{i+1};
        case 'markersize'
            msize   = varargin{i+1};
        case 'delta_f'
            delta_f = varargin{i+1};
        case 'downsample'
            DS      = varargin{i+1};
        case 'fittype'
            ftype   = varargin{i+1};
        case 'order'
            order   = varargin{i+1};
        case 'polyrepeats'
            polyRep = varargin{i+1};
        case 'fitpoints'
            n       = varargin{i+1};
        case 'colormap'
            cmap    = varargin{i+1};
        case 'initialindex'
            F       = varargin{i+1};
        otherwise
            error('Invalid property name');
    end
end

%% Calculate the minimum resolution that can be resolved
delta_s = 0.5*pi*delta_f;   % Arc length resampling interval (mm)
delta_p = X(2) - X(1);      % pixel size (mm)
switch DS
    case 'on'
        ds_norm = delta_s/delta_p;  % Normalized arc length resampling interval (pixels)
        if ds_norm < 1
            error('Arc length resampling interval is smaller than one pixel');
        end
    case 'off'
        ds_norm = 1;
    otherwise
        error('Invalid argument for propertyValue downsample');
end

%% Create a directory to store the processed data and the images
folderName      = 'Curvatures';
if ~exist(folderName, 'dir');
    mkdir(folderName);
end
currdir         = cd;
curvatureDir    = [currdir, '\', folderName];
cd(curvatureDir);

%% Initialize cell arrays
xDS         = cellfun(@(x) NaN*x(:, 1), E, 'UniformOutput', false); 
                                % downsampled x-coordinate of the edge
yDS         = xDS;              % downsampled y-coordinate of the edge
sDS         = xDS;              % downsampled arc length along the edge
x           = xDS;              % x-coordinate of the fitted polynomials
y           = xDS;              % y-coordinate of the fitted polynomials
xp          = xDS;              % x' of the fitted polynomials
yp          = xDS;              % y' of the fitted polynomials
xpp         = xDS;              % x'' of the fitted polynomials
ypp         = xDS;              % y'' of the fitted polynomials
s           = xDS;              % s-coordinate of the fitted polynomials
kappa       = xDS;              % curvature of the fitted polynomials
R           = xDS;              % radius of curvature of the fitted polynomials

%% Perform calculations for each edge
parfor i = 1:length(E)
    if ~isempty(E{i}) && ~isnan(mean(mean(E{i})))
        figure;
        dispImageNoA(X, Y, BW(:, :, i), 16, 'gray');
        hold on
        plot(E{i}(:, 1)/d, E{i}(:, 2)/d, 'g', 'LineWidth', 2);
        xe = E{i}(:, 1);
        ye = E{i}(:, 2);
        
        %% Curvature calculations
        % Calculate the distance along the flame contour
        se = arcLength(xe, ye);
        
        % Adjust starting point if downsampling in order to generate values at every point on the edge
        i1 = 1;
        for j = 1:floor(ds_norm)
            % Downsample the edge on to a fewer number of points
            switch DS
                case 'on'
                    startpt = (j - 1)*delta_p;
                    sDS_    = (startpt:delta_s:max(se))';
                    xDS_    = interp1(se, xe, sDS_);
                    yDS_    = interp1(se, ye, sDS_);
                case 'off'
                    sDS_    = se;
                    xDS_    = xe;
                    yDS_    = ye;
                otherwise
                    error('Invalid argument for propertyValue downsample');
            end
            % Perform low order fit to the data. This is done by fitting a polynomial x = x(s) and y = y(s)
            switch ftype
                case 'poly'
                    [x_, y_, xp_, yp_, xpp_, ypp_, s_] = ...
                        PolynomialFit(xDS_, yDS_, sDS_, n, order, polyRep);
                case 'csaps'
                    [x_, y_, xp_, yp_, xpp_, ypp_, s_] = ...
                        SmoothingSplineFit(xDS_, yDS_, sDS_);
                otherwise
                    error('Invalid argument for propertyValue ftype');
            end
            % Combine the different curves generated at different starting locations
            % (do not include endpoints since spline derivatives are not physical at these locations)
            i2              = i1 + length(sDS_(2:end-1)) - 1;
            sDS{i}(i1:i2)   = sDS_(2:end-1);
            xDS{i}(i1:i2)   = xDS_(2:end-1);
            yDS{i}(i1:i2)   = yDS_(2:end-1);
            x{i}(i1:i2)     = x_(2:end-1);
            y{i}(i1:i2)     = y_(2:end-1);
            s{i}(i1:i2)     = s_(2:end-1);
            xp{i}(i1:i2)    = xp_(2:end-1);
            yp{i}(i1:i2)    = yp_(2:end-1);
            xpp{i}(i1:i2)   = xpp_(2:end-1);
            ypp{i}(i1:i2)   = ypp_(2:end-1);
            i1              = i2 + 1;
        end
        % Remove NaNs
        Inan            = isnan(sDS{i});   % logical indices of NaNs
        sDS{i}(Inan)    = [];
        xDS{i}(Inan)    = [];
        yDS{i}(Inan)    = [];
        x{i}(Inan)      = [];
        y{i}(Inan)      = [];
        s{i}(Inan)      = [];
        xp{i}(Inan)     = [];
        yp{i}(Inan)     = [];
        xpp{i}(Inan)    = [];
        ypp{i}(Inan)    = [];
        kappa{i}(Inan)  = [];
        R{i}(Inan)      = [];
        
        % Calculate the curvature
        kappa{i}    = (yp{i}.*xpp{i} - xp{i}.*ypp{i})./(xp{i}.^2 + yp{i}.^2).^(3/2);
        R{i}        = 1./kappa{i};   % Radius of curvature, mm
        
        zeroCurvature   = find(abs(kappa{i}) < 0.01);
        negCurvature    = find(kappa{i} < -0.01);
        posCurvature    = find(kappa{i} > 0.01);
        
        plot(x{i}(zeroCurvature)/d, y{i}(zeroCurvature)/d, 'mx', ...
            x{i}(negCurvature)/d, y{i}(negCurvature)/d, 'gx', ...
            x{i}(posCurvature)/d, y{i}(posCurvature)/d, 'cx', ...
            'MarkerSize', msize, 'LineWidth', lwidth);
        hold off
        
        switch ftype
            case 'poly'
                set(gcf, 'Name', ['Curvature, order = ', int2str(order), ...
                    ', pRepeat = ', int2str(polyRep), ', n = ', int2str(n), ', ', ...
                    int2str(i + F - 1), ', DS = ', DS, ', ', ftype]);
            case 'csaps'
                set(gcf, 'Name', ['Curvature, ', int2str(i + F - 1), ', DS = ', DS, ', ', ftype]);
            otherwise
                error('Invalid argument for propertyValue downsample');
        end
        colormap(cmap);
        
        fn      = get(gcf, 'Name');
        fn      = [fn, ', delta_f = ', num2str(delta_f, 2), ', shot ', int2str(frame), ...
            ', filt = ', int2str(filt), ', ' method];
        I       = fn == '.';
        fn(I)   = '_';
        I       = fn == '<' | fn == '>';
        fn(I)   = [];
        saveas(gcf, fn, 'fig');
        print(gcf, fullfile(curvatureDir, [fn, '.png']), '-dpng', ['-r', num2str(res)], '-painters');
        close;
    end
end
cd(currdir);
end