function [xp, yp, pxp, pyp] = FirstDerivative(px, py, s)
% This is a function to compute the first derivative from a MATLAB polyfit

pxp = polyder(px);
pyp = polyder(py);

xp = polyval(pxp, s);
yp = polyval(pyp, s);
end