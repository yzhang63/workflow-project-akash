function [y, ai, wi] = randSin(x, a0, as, ws, n)
%RANDSIN Generates a random combination of sin functions
%   RANDSIN(x, a0, as, ws, n) Generates a random combination of n sin terms
%   of the following form:
%   
%       y = a0 + sum(ai*sin(wi*x))
%   
%   Inputs:
%       x: x-values
%       a0: mean of y-values
%       as: standard deviation of ai
%       ws: standard deviation of wi
%       n: number of terms
%   Outputs:
%       y: y-values
%       ai: column vector of pseudorandom amplitudes, ai
%       wi: column vector of pseudorandom frequencies, wi

x   = x(:)';                    % make sure that x is a row vector
ai  = as*randn(n, 1);           % column vector of random ai
wi  = ws*randn(n, 1);           % column vector of random wi
AI  = ai*ones(1, length(x));    % matrix of ai with form: [ai, ai, ... ai] dim: length(ai) x length(x)
y   = a0 + sum(AI.*sin(wi*x));
y   = y';
end