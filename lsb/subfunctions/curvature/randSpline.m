function [y, p] = randSpline(x, ybar, sig)
%RANDSPLINE Generate a random spline
%   RANDSPLINE(x, ybar, sigma) Generate an n-th order polynomial over a
%   specified x-range
%   Inputs:
%       x: x-values
%       ybar: mean of y-values
%       sigma: standard deviation of y-values
%   Outputs:
%       y: y-values

a   = min(x);
b   = max(x);
nDS = round(length(x)/200);           % length of downsampled x-values
xDS = linspace(a, b, nDS);
xDS = xDS';
yDS = ybar + sig*randn(size(xDS));      % downsampled y-values
yDS = [0; yDS; 0];
p   = spline(xDS, yDS);                 % generate spline curve
y   = ppval(p, x);
end