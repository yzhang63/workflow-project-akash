function s = arcLength(x, y)
% ARCLENGTH calculates arc length
%   ARCLENGTH(x, y) calculates arc length along the path defined
%   by x and y
%   Inputs:
%       x: vector of x-positions
%       y: vector of y-positions
%   Outputs:
%       s: vector of arc lengths

s           = zeros(length(x), 1);
dx          = x(2:end) - x(1:end-1);
dy          = y(2:end) - y(1:end-1);
ds          = sqrt(dx.^2 + dy.^2);
s(2:end)    = cumsum(ds);
end