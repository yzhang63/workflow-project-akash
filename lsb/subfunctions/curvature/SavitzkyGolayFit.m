function [xf, yf, xp, yp, xpp, ypp, s] = SavitzkyGolayFit(x, y, s, k, f)
% SAVITZKYGOLAYFIT uses the Savitzky-Golay smoothing filter to fit data
%   SAVITZKYGOLAYFIT(x, y, s) uses the Savitzky-Golay smoothing filter to
%   perform a local polynomial regression (of degree k) on a series 
%   Inputs:
%       x: vector of x-positions
%       y: vector of y-positions
%       ds: delta-s of arc length vector
%       k: order of polynomial for fitting
%       f: window size over which to perform fit
%   Outputs:
%       xf: vector of fitted x data
%       yf: vector of fitted y data
%       xp: vector of fitted x' data
%       yp: vector of fitted y' data
%       xpp: vector of fitted x'' data
%       ypp: vector of fitted y'' data

x               = x(:);                     % ensures x is a column vector
y               = y(:);
s               = s(:);
ds              = s(2) - s(1);              % delta-s of arc length

[xf, xp, xpp]   = sgolayFit(x, ds, k, f);
[yf, yp, ypp]   = sgolayFit(y, ds, k, f);
end