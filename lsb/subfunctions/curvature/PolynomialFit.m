function [x, y, xp, yp, xpp, ypp, s] = PolynomialFit(x, y, s, f, k, polyRepeats)
% POLYNOMIALFIT fits a polynomial to the data
%   POLYNOMIALFIT(x, y, s, n, order, polyRepeats) fits a polynomial to the points in 
%   x and y over n points centered around i
%   Inputs:
%       x: vector of x-positions
%       y: vector of y-positions
%       s: vector of arc lengths
%       f: number of points over which to perform the fit
%       order: order of the polynomial
%   Outputs:
%       x: vector of fitted x data
%       y: vector of fitted y data
%       xp: vector of fitted x' data
%       yp: vector of fitted y' data
%       xpp: vector of fitted x'' data
%       ypp: vector of fitted y'' data
%       s: vector of fitted s data

x   = x(:);             % ensures x is a column vector
y   = y(:);
s   = s(:);
hp  = round(f/2) - 1;   % halfway point of n
xf  = zeros(size(x));
yf  = xf;
for j = 1:polyRepeats
    px  = zeros(length(s), k + 1);      % polynomial coefficients of x(s_hat) where s_hat = (s - mu(1))/mu(2)
    py  = px;                           % polynomial coefficients of y(s_hat) where s_hat = (s - mu(1))/mu(2)
    mux = zeros(length(s), 2);          % mean and standard deviation of s
    muy = mux;                          % identical to mux
    for i = 1:length(s)
        if i <= hp
            [px(i, :), ~, mux(i, :)] = polyfit(s(1:f), x(1:f), k);
            [py(i, :), ~, muy(i, :)] = polyfit(s(1:f), y(1:f), k);
        elseif i > hp && i <= length(x) - hp
            [px(i, :), ~, mux(i, :)] = polyfit(s(i-hp:i+hp), x(i-hp:i+hp), k);
            [py(i, :), ~, muy(i, :)] = polyfit(s(i-hp:i+hp), y(i-hp:i+hp), k);
        elseif i > length(x) - hp
            [px(i, :), ~, mux(i, :)] = polyfit(s(end-f+1:end), x(end-f+1:end), k);
            [py(i, :), ~, muy(i, :)] = polyfit(s(end-f+1:end), y(end-f+1:end), k);
        end
        xf(i) = polyval(px(i, :), s(i), [], mux(i, :));
        yf(i) = polyval(py(i, :), s(i), [], muy(i, :));
    end
    s = arcLength(xf, yf);              % arc length for new fitted xf and yf values
    x = xf;                             % set x to xf and repeat fitting to smooth further
    y = yf;                             % set y to yf and repeat fitting to smooth further
end

xp  = zeros(size(x));                   % x'(s)
yp  = xp;                               % y'(s)
xpp = xp;                               % x''(s)
ypp = xp;                               % y''(s)
sh  = (s - mux(:, 1))./mux(:, 2);       % s_hat = (s - mean(s))/std(s)
for i = 1:length(s)
    [xp(i), yp(i), pxp, pyp]    = FirstDerivative(px(i, :), py(i, :), sh(i));   % x'(sh), y'(sh)
    [xpp(i), ypp(i)]            = FirstDerivative(pxp, pyp, sh(i));             % x''(sh), y''(sh)
end
xp  = xp./mux(:, 2);                    % x'(s)     = x'(sh)/std(sh)
yp  = yp./muy(:, 2);                    % y'(s)     = y'(sh)/std(sh)
xpp = xpp./mux(:, 2).^2;                % x''(s)    = x''(sh)/std(sh)^2
ypp = ypp./muy(:, 2).^2;                % y''(s)    = y''(sh)/std(sh)^2
end