%% Input Parameters
sigma       = 0.78;             % standard deviation of spline values (mm)
N           = 100;              % number of splines to generate
res         = 0.039;            % resolution (mm/pixel)
curveType   = 'sin';            % type of pseudorandom curve: 'spline' or 'sin'

%% Fit Parameters
orders      = 3:8;              % order of fitting polynomial to binarized images (default: 4)
pRepeats    = 2:4;              % number of fits to perform (default: 2)
ns          = [7 13 25 49];     % number of points to fit over (default: 15)

%% Load mat files of data
prevdir = cd;
currdir = ['\\Vboxsvr\piv\LSB\reacting\pseudocurvature\sigma = ', num2str(sigma), ...
    ' mm\N = ', int2str(N), '\res = ', num2str(res), ' mm per pixel'];
cd(currdir);

switch curveType
    case 'spline'
        method = 'pseudo';
    case 'sin'
        method = 'pseudo, sin';
    otherwise
        error([curveType, ' is not a valid entry for the variable curveType']);
end

estds   = zeros(length(orders), length(pRepeats), length(ns));
emeans  = estds;
eskews  = estds;
emeanF  = estds;
estdF   = estds;
eskewF  = estds;
olegend = cell(length(pRepeats), length(ns));
plegend = cell(length(orders), length(ns));
nlegend = cell(length(orders), length(pRepeats));
for i = 1:length(orders)
    for j = 1:length(pRepeats)
        for k = 1:length(ns)
            load(['data, order = ', int2str(orders(i)), ', pRepeat = ', int2str(pRepeats(j)), ...
                ', n = ', int2str(ns(k)), ', ', method, '.mat'], 'estd', 'kappaTrue', 'kappa');
            emeans(i, j, k) = 100*(mean(cell2mat(kappaTrue)) - mean(cell2mat(kappa)))/...
                                mean(cell2mat(kappaTrue));
            estds(i, j, k)  = estd;
            eskews(i, j, k) = 100*(skewness(cell2mat(kappaTrue)) - skewness(cell2mat(kappa)))/...
                                skewness(cell2mat(kappaTrue));
            kappaF          = outliers(cell2mat(kappa));    % remove outliers
            kappaF(isnan(kappaF)) = [];                     % remove NaNs
            emeanF(i, j, k) = 100*(mean(cell2mat(kappaTrue)) - mean(kappaF))/...
                                mean(cell2mat(kappaTrue));
            estdF(i, j, k)  = 100*(std(cell2mat(kappaTrue)) - std(kappaF))/...
                                std(cell2mat(kappaTrue));
            eskewF(i, j, k) = 100*(skewness(cell2mat(kappaTrue)) - skewness(kappaF))/...
                                skewness(cell2mat(kappaTrue));
            olegend{j, k}   = ['{\itp} = ', int2str(pRepeats(j)), ', {\itn} = ', int2str(ns(k))];
            plegend{i, k}   = ['{\ito} = ', int2str(orders(i)), ', {\itn} = ', int2str(ns(k))];
            nlegend{i, j}   = ['{\ito} = ', int2str(orders(i)), ', {\itp} = ', int2str(pRepeats(j))];
        end
    end
end
cd(prevdir);

%% Figure Input Parameters
fsize   = 18;                   % font size
fname   = 'Times New Roman';    % font
lwidth  = 2;                    % line width
msize   = 8;                    % marker size
lmstyle = 'o--';               % line and marker style and color

%% Plot error in standard deviation as function of polynomial order
clear h
h       = zeros(size(ns));
for k = 1:length(ns)
    h(k)    = figure;
    set(gcf, 'color', 'w');
    set(gcf, 'Name', ['e_std = f(order), n = ', int2str(ns(k))]);
    
    plot(orders, estds(:, :, k)', lmstyle, 'LineWidth', lwidth, 'MarkerSize', msize);
    
    set(gca, 'FontSize', fsize, 'FontName', fname);
    xlabel('Polynomial Order');
    ylabel('{\ite_\sigma} (%)');
    ylim([-50 50]);
    legend(olegend{:, k}, 'location', 'best');
end

%% Save figures and data
cd(currdir);
for i = length(h):-1:1
    fn      = get(h(i), 'Name');
    I       = fn == '.';
    fn(I)   = '_';
    I       = fn == '<' | fn == '>';
    fn(I)   = [];
    saveas(h(i), [fn, ', ', method], 'fig');
    saveas(h(i), [fn, ', ', method], 'png');
end
cd(prevdir);
close all

%% Plot error in standard deviation as function of number of points, n
clear h
h       = zeros(size(orders));
for i = 1:length(orders)
    h(i)    = figure;
    set(gcf, 'color', 'w');
    set(gcf, 'Name', ['e_std = f(n), order = ', int2str(orders(i))]);
    
    plot(ns, squeeze(estds(i, :, :)), lmstyle, 'LineWidth', lwidth, 'MarkerSize', msize);
    
    set(gca, 'FontSize', fsize, 'FontName', fname);
    xlabel('fit points');
    ylabel('{\ite_\sigma} (%)');
    ylim([-50 50]);
    legend(nlegend{i, :}, 'location', 'best');
end

%% Save figures and data
cd(currdir);
for i = length(h):-1:1
    fn      = get(h(i), 'Name');
    I       = fn == '.';
    fn(I)   = '_';
    I       = fn == '<' | fn == '>';
    fn(I)   = [];
    saveas(h(i), [fn, ', ', method], 'fig');
    saveas(h(i), [fn, ', ', method], 'png');
end
cd(prevdir);
close all

%% Plot error in standard deviation as function of polynomial repeats
clear h
h       = zeros(size(orders));
for i = 1:length(orders)
    h(i)    = figure;
    set(gcf, 'color', 'w');
    set(gcf, 'Name', ['e_std = f(p), order = ', int2str(orders(i))]);
    
    plot(pRepeats, squeeze(estds(i, :, :))', lmstyle, 'LineWidth', lwidth, 'MarkerSize', msize);
    
    set(gca, 'FontSize', fsize, 'FontName', fname);
    xlabel('polynomial fit repeats');
    ylabel('{\ite_\sigma} (%)');
    ylim([-50 50]);
    legend(plegend{i, :}, 'location', 'best');
end

%% Save figures and data
cd(currdir);
for i = length(h):-1:1
    fn      = get(h(i), 'Name');
    I       = fn == '.';
    fn(I)   = '_';
    I       = fn == '<' | fn == '>';
    fn(I)   = [];
    saveas(h(i), [fn, ', ', method], 'fig');
    saveas(h(i), [fn, ', ', method], 'png');
end
cd(prevdir);
close all

%% Repeat all plots using errors after removing outliers

%% Plot error in standard deviation as function of polynomial order
clear h
h       = zeros(size(ns));
for k = 1:length(ns)
    h(k)    = figure;
    set(gcf, 'color', 'w');
    set(gcf, 'Name', ['e_stdF = f(order), n = ', int2str(ns(k))]);
    
    plot(orders, estdF(:, :, k)', lmstyle, 'LineWidth', lwidth, 'MarkerSize', msize);
    
    set(gca, 'FontSize', fsize, 'FontName', fname);
    xlabel('Polynomial Order');
    ylabel('{\ite_\sigma_,_F} (%)');
    ylim([-50 50]);
    legend(olegend{:, k}, 'location', 'best');
end

%% Save figures and data
cd(currdir);
for i = length(h):-1:1
    fn      = get(h(i), 'Name');
    I       = fn == '.';
    fn(I)   = '_';
    I       = fn == '<' | fn == '>';
    fn(I)   = [];
    saveas(h(i), [fn, ', ', method], 'fig');
    saveas(h(i), [fn, ', ', method], 'png');
end
cd(prevdir);
close all

%% Plot error in standard deviation as function of number of points, n
clear h
h       = zeros(size(orders));
for i = 1:length(orders)
    h(i)    = figure;
    set(gcf, 'color', 'w');
    set(gcf, 'Name', ['e_stdF = f(n), order = ', int2str(orders(i))]);
    
    plot(ns, squeeze(estdF(i, :, :)), lmstyle, 'LineWidth', lwidth, 'MarkerSize', msize);
    
    set(gca, 'FontSize', fsize, 'FontName', fname);
    xlabel('fit points');
    ylabel('{\ite_\sigma_,_F} (%)');
    ylim([-50 50]);
    legend(nlegend{i, :}, 'location', 'best');
end

%% Save figures and data
cd(currdir);
for i = length(h):-1:1
    fn      = get(h(i), 'Name');
    I       = fn == '.';
    fn(I)   = '_';
    I       = fn == '<' | fn == '>';
    fn(I)   = [];
    saveas(h(i), [fn, ', ', method], 'fig');
    saveas(h(i), [fn, ', ', method], 'png');
end
cd(prevdir);
close all

%% Plot error in standard deviation as function of polynomial repeats
clear h
h       = zeros(size(orders));
for i = 1:length(orders)
    h(i)    = figure;
    set(gcf, 'color', 'w');
    set(gcf, 'Name', ['e_stdF = f(p), order = ', int2str(orders(i))]);
    
    plot(pRepeats, squeeze(estdF(i, :, :))', lmstyle, 'LineWidth', lwidth, 'MarkerSize', msize);
    
    set(gca, 'FontSize', fsize, 'FontName', fname);
    xlabel('polynomial fit repeats');
    ylabel('{\ite_\sigma_,_F} (%)');
    ylim([-50 50]);
    legend(plegend{i, :}, 'location', 'best');
end

%% Save figures and data
cd(currdir);
for i = length(h):-1:1
    fn      = get(h(i), 'Name');
    I       = fn == '.';
    fn(I)   = '_';
    I       = fn == '<' | fn == '>';
    fn(I)   = [];
    saveas(h(i), [fn, ', ', method], 'fig');
    saveas(h(i), [fn, ', ', method], 'png');
end
cd(prevdir);
close all