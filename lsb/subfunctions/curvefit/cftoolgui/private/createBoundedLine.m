function h = createBoundedLine(fit, bndsonoff, clev, userargs, varargin)
% createBoundedLine   Create a "bounded line" object
%
%   H = createBoundedLine(FIT, BNDSONOFF, CLEV, USERARGS, ...) is a bounded line
%   object.
%
%   FIT is a CFTOOL.FIT object.
%
%   BNDSONOFF is either the string 'on' or the string 'off'.
%
%   CLEV is a number between 0 and 100 representing the confidence level to show.
%
%   USERARGS is a cell array of user arguments. Usually this is {FIT.dataset
%   FIT.dshandle}
%
%   The variable part of the argument list is parameter-value pairs for
%   properties of a line. These include line specification and parent.

%   Copyright 2010-2013 The MathWorks, Inc.

if feature('HGUsingMATLABClasses')
    warning( 'curvefit:createBoundedLine:NotSupportedWithHGUsingMATLABClasses', ...
        'cftool.boundedline is not supported with HGUsingMATLABClasses.' );
end

h = cftool.boundedline( fit, bndsonoff, clev, '-userargs', userargs, varargin{:} );

end
