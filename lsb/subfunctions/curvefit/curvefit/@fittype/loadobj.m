function obj = loadobj(obj)
%LOADOBJ Method to post-process FITTYPE objects after loading

%   Copyright 2001-2013 The MathWorks, Inc.

% If the loaded version is a structure then we need to convert it to
% an object
if isstruct( obj )
    if obj.version == 1.0
        obj = versionTwoFromVersionOne( obj );
    end
    obj = copyFittypeProperties( fittype, obj );
end

% Restore function handles that had to be removed during save
if ~isequal(category(obj),'custom')
    libname = obj.fType;
    if ~isempty(libname)
        obj = sethandles(libname,obj);
    end
end

% Attempt to fix the anonymous function if it has been stripped out (R2013a or
% earlier)
obj = iFixAnonymousFunction( obj );

end

function obj = versionTwoFromVersionOne( obj )
% VERSIONTWOFROMVERSIONONE -- Convert the old fieldnames to the new names
obj = changeFieldname( obj, 'category',        'fCategory' );
obj = changeFieldname( obj, 'constants',       'fConstants' );
obj = changeFieldname( obj, 'feval',           'fFeval' );
obj = changeFieldname( obj, 'fitoptions',      'fFitoptions' );
obj = changeFieldname( obj, 'nonlinearcoeffs', 'fNonlinearcoeffs' );
obj = changeFieldname( obj, 'startpt',         'fStartpt' );
obj = changeFieldname( obj, 'type',            'fType' );
obj = changeFieldname( obj, 'typename',        'fTypename' );
obj.version = 2.0;
end

function s = changeFieldname( s, old, new )
s.(new) = s.(old);
s = rmfield( s, old );
end

function obj = iFixAnonymousFunction(obj)
if obj.fFeval && isempty( obj.expr )
    obj.fFeval = false;
    obj.expr = obj.defn;
    % Test that we can evaluate the fittype -- test works for fittype but not for
    % subclasses of fittype.
    ft = fittype( obj );
    try
        iTestCustomModelEvaluation( ft );
    catch e
        bugReport = '<a href="http://www.mathworks.com/support/bugreports/968079">968079</a>';
        warning( message( 'curvefit:fittype:cannotLoadAnonFunction', ...
            obj.defn, bugReport, e.message ) );
    end
end
end

function iTestCustomModelEvaluation( obj )
% iTestCustomModelEvaluation   Test that when a custom model is evaluated it does
% not throw an error.

% Create example values for input arguments
independentValues = iExampleIndependentValues( obj );
problemValues = iExampleProblemValues( obj );
coefficients = iExampleCoefficients( obj );

iAssertCustomEquationIsEvaluable(obj, problemValues, independentValues, coefficients);

dependentValues = iEvaluateCustomEquation(obj, problemValues, independentValues, coefficients);

iAssertCustomEquationIsVectorized(obj, dependentValues, independentValues);
end

function r = iArbitrary( k )
% iArbitrary( K ) is a row vector of K arbitrary values. This can be
% thought of as a substitute for RAND( 1, K ) when the "random" aspect
% is not required.
r = (1:k)/(k+1);
end

function values = iExampleIndependentValues( obj )
% iExampleIndependentValues   A cell array of example independent variables for
% the given fittype object.

ARBITRARY_NUM_VALUES = 7;
if numindep( obj ) == 1
    values = {iArbitrary( ARBITRARY_NUM_VALUES ).'}; 
else % numindep( obj ) == 2
    values = {iArbitrary( ARBITRARY_NUM_VALUES ).', 1-iArbitrary( ARBITRARY_NUM_VALUES ).'}; 
end
end

function probparams = iExampleProblemValues(obj)
% iExampleProblemValues   A cell array of example problem parameters (all
% scalars) for the given fittype object
numProblemParameters = numargs(obj)-obj.numCoeffs-numindep( obj );
probparams = num2cell( iArbitrary( numProblemParameters ) );
end

function coefficients = iExampleCoefficients(obj)
% iExampleCoefficients   A cell array of example coefficients (all scalars) for
% the given fittype object

if iHasValidStartPoint( obj )
    coefficients = num2cell( obj.fFitoptions.StartPoint );
else
    coefficients = num2cell( iArbitrary( obj.numCoeffs ) );
end
end

function tf = iHasValidStartPoint( obj )
% iHasValidStartPoint   True for a fittype with a valid start point, i.e., the
% fit options has a start point field and the number of elements of that field
% matches the number of coefficients
tf = ~isempty(findprop(obj.fFitoptions, 'StartPoint')) && ...
    isequal(length(obj.fFitoptions.StartPoint),obj.numCoeffs);
end

function iAssertCustomEquationIsVectorized(obj, dependentValues, independentValues)
% iAssertCustomEquationIsVectorized   Throws an error if the independent
% values and dependent values of the custom equation indicate that the
% function is not vectorized.
if ~isequal(size(dependentValues), size(independentValues{1}))
    newError = curvefit.exception('curvefit:fittype:nonVectorOutput', obj.formula);
    throwAsCaller( newError );
end
end

function iAssertCustomEquationIsEvaluable(obj, problemValues, independentValues, coefficients)
% iAssertCustomEquationIsEvaluable   Throws an error if the custom equation
% cannot be evaluated
try
    % Try to evaluate the model
    if islinear( obj ) 
        % Also eval to get coefficient matrix
        [~] = getcoeffmatrix( obj, problemValues{:}, independentValues{:} );
    end
    [~] = iEvaluateCustomEquation(obj, problemValues, independentValues, coefficients);
    
catch caughtError
    newError = curvefit.exception( 'curvefit:fittype:invalidExpression', ...
        obj.defn, caughtError.message );
    
    newError = addCause( newError, caughtError ); 
    throwAsCaller( newError );
end
end

function dependentValues = iEvaluateCustomEquation(obj,problemValues,independentValues,coefficients)
% iEvaluateCustomEquation   Calculate the dependent values of a custom
% equation from the coefficients, problem values and independent values
dependentValues = feval( obj, coefficients{:}, problemValues{:}, independentValues{:} );
end
