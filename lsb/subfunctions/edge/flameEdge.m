function E = flameEdge(BW, X, Y)
%FLAMEEDGE Finds the flame edge from a binarized image
%   FLAMEEDGE(BW, X, Y) finds the flame edge from a binarized PIV image
%   Inputs:
%       BW: binarized flame image
%       X: vector of x-positions (units specified in A.UnitX)
%       Y: vector of y-positions (units specified in A.UnitY)

disp('inside flameEdge: length X&Y');
disp(length(X));
disp(length(Y));

d           = 36.3;                         %#ok<NASGU> % burner diameter (mm)
B           = bwboundaries(BW, 'noholes');  % trace the boundaries without including holes
[~, J]      = max(cellfun('size', B, 1));   % find the longest continuous boundary
bound       = B{J};                         % use only the longest continuous boundary
K1          = bound(:, 1) == 1;             % find all edges detected along the left border
bound(K1,:) = [];                           % eliminate edges along the left border
K2          = bound(:, 1) == length(Y);     % find all edges detected along the right border
bound(K2,:) = [];                           % eliminate edges along the right border
M1          = bound(:, 2) == 1;             % find all edges detected along the bottom border
bound(M1,:) = [];                           % eliminate edges along the bottom border
M2          = bound(:, 2) == length(X);     % find all edges detected along the top border
bound(M2,:) = [];                           % eliminate edges along the top border

% Find jumps in pixel values
dx      = zeros(length(bound(:, 1))-1, 1);
dy      = dx;
for i = 2:length(bound(:, 1))
    dx(i-1) = bound(i, 2) - bound(i-1, 2);      % delta-x
    dy(i-1) = bound(i, 1) - bound(i-1, 1);      % delta-y
end
ii      = find((abs(dx) > 1) | (abs(dy) > 1));  % find indices where pixel jumps occur
ii      = [0; ii; length(bound(:, 1))];         % add start and end values
dii     = zeros(length(ii)-1, 1);
for i = 2:length(ii)
    dii(i-1) = ii(i) - ii(i-1);                 % find lengths of continuous flame edges
end
[~, jj] = max(dii);                             % find longest continuous flame edge
bound   = bound(ii(jj)+1:ii(jj+1), :);          % extract longest continuous flame edge

% plot(X(bound(:, 2))/d, Y(bound(:, 1))/d, 'g', 'LineWidth', 2);
Xedge           = X(bound(:, 2))';
Yedge           = Y(bound(:, 1))';
disp('Xedge*Yedge');
disp(size(Xedge));
disp(size(Yedge))
[Xedge, Yedge]  = removeRepeatPt(Xedge, Yedge);
E               = [Xedge Yedge];
disp('sizeof & class');
disp(size(E));
disp(class(E));
end
