function ImE = edgeValue(X, Y, Im, E, x, y)
%EDGEVALUE Interpolates values of a 2D field, Im, along an edge E
%   EDGEVALUE(X, Y, Im, E, x, y) calculates values of the 2D field, Im, 
%   along the edge defined by the x and y coordinates. This is done by 
%   upsampling the 2D field Im to the grid resolution of the edge 
%   coordinates E. E is not required to be monotonic in either x or y, but 
%   must have come from an underlying uniformly gridded 2D field.
%   Inputs:
%       X: x-location at which data Im is given
%       Y: y-location at which data Im is given
%       Im: 2D array of data
%       E: Nx2 array of x and y coordinates of an edge
%       x: x coordinates of the fitted, smoothed edge
%       y: y coordinates of the fitted, smoothed edge
%   Outputs:
%       ImE: Nx1 vector of interpolated values of Im at edge locations E

%% Find the necessary grid spacing for the edge E
dx          = E(2:end, 1) - E(1:end-1, 1);
dy          = E(2:end, 2) - E(1:end-1, 2);
dx(dx == 0) = [];
dy(dy == 0) = [];
dx          = abs(dx);
dy          = abs(dy);
dx          = min(dx);
dy          = min(dy);

%% Find the x and y limits to put a box around the edge E
x1          = min(E(:, 1));
x2          = max(E(:, 1));
y1          = min(E(:, 2));
y2          = max(E(:, 2));
% If the fitted edge goes outside of this region, expand the box
if min(x) < x1
    n   = ceil((x1 - min(x))/dx);
    x1  = x1 - n*dx;
end
if max(x) > x2
    n   = ceil((max(x) - x2)/dx);
    x2  = x2 + n*dx;
end
if min(y) < y1
    n   = ceil((y1 - min(y))/dy);
    y1  = y1 - n*dy;
end
if max(y) > y2
    n   = ceil((max(y) - y2)/dy);
    y2  = y2 + n*dy;
end

XE          = x1:dx:x2;
YE          = (y2:-dy:y1)';

%% Crop the image Im to the size of the box defined by XE and YE
dX1         = X - x1;
J1          = dX1 <= 0;         % find all points to the left of x1
X1          = max(dX1(J1));     % find the closest point to the left of x1
L           = dX1 == X1;        % find the index of this point in X
L           = find(L);
dX2         = X - x2;
J2          = dX2 >= 0;         % find all points to the right of x2
X2          = min(dX2(J2));     % find the closest point to the right of x2
R           = dX2 == X2;        % find the index of this point in X
R           = find(R);

dY1         = Y - y1;
J1          = dY1 <= 0;         % find all points below y1
Y1          = max(dY1(J1));     % find the closest point below y1
B           = dY1 == Y1;        % find the index of this point in Y
B           = find(B);
dY2         = Y - y2;
J2          = dY2 >= 0;         % find all points above y2
Y2          = min(dY2(J2));     % find the closest point above y2
T           = dY2 == Y2;        % find the index of this point in Y
T           = find(T);

Im          = Im(T:B, L:R);     % crop the image
X           = X(L:R);           % crop the X vector
Y           = Y(T:B);           % crop the Y vector

%% Interpolate original 2D field onto grid defined by XE and YE
% if the image contains NaNs, use inpaint_nans to interpolate them out
Im          = inpaint_nans(Im);
ImInterp    = interp2(X, Y, Im, XE, YE);
xEpixels    = round(1 + (x - XE(1))/(XE(2) - XE(1)));
yEpixels    = round(1 + (y - YE(1))/(YE(2) - YE(1)));

%% Calculate values along the smoothed edge defined by x and y
ImE         = NaN(length(xEpixels), 1);
for i = 1:length(xEpixels)
    ImE(i)  = ImInterp(yEpixels(i), xEpixels(i));
end
end