function [x, y] = removeRepeatPt(x, y)
%REMOVEREPEATPT Removes points that repeat consecutively
%   REMOVEREPEATPT(xi, yi) finds points that are repeated consecutively
%   (i.e. that occur at index i and i + 1 for any index i) and removes one 
%   of the pair from the vectors xi and yi. 
%   Inputs:
%       xi: initial vector of x positions
%       yi: initial vector of y positions
%   Outputs:
%       x: final vector of x positions
%       y: final vector of y positions

I       = diff(x) == 0 & diff(y) == 0;
x(I)    = [];
y(I)    = [];
end