function BW = binarizeImage(Im, method)
%BINARIZEIMAGE Binarizes a PIV image through particle density gradients
%   BINARIZEIMAGE(X, Y, N, Im) loads a PIV image and binarizes it into 0
%   for reactants and 1 for products. The boundary between reactants and
%   products is found through a jump in particle density.
%   Inputs:
%       Im: 2-D array of median filtered image intensities (units specified in A.UnitI)
%       method: binarizing method: 'graythresh' or 'kmeans'
%   Outputs:
%       BW: binarized image
%       I: locations where binarization fails

%disp('size of 2-D array of the median filtered image: ')
%disp(size(Im))

%disp('Im: ')
%disp(Im)

switch method
    case 'graythresh' % current in-use method 06/01/2014 can be checked at binarizeImages.m
        Im      = (Im - min(min(Im)))./(max(max(Im)) - min(min(Im)));      % Scale intensity between [0, 1]
        Im      = Im.*(2^16 - 1);                                          % Scale to precision of 16 bits
        Im      = uint16(Im);
        level   = graythresh(Im);
        BW      = im2bw(Im, level);
        BW      = imcomplement(BW);
        %disp('displaying binarzied image: ');
        %disp(BW);
        %disp('size(BW): ');
        %disp(size(BW));
    case 'kmeans'
        BW      = reshape(kmeans(Im(:), 2), size(Im)) - 1;
        if mean(BW(end, :)) > 0.5
            BW  = imcomplement(BW);
        end
    case 'adaptivethreshold'
        BW      = adaptivethreshold(Im, 64, 0.01, 1);
    otherwise
        error('Invalid method');
end
end
