function [BWs I] = binarizeImagesRemote(X, Y, Im, N, level, S, P, T, U, fuel, phi, angle, expdate, c, xl, yl, frame, method)
%BINARIZEIMAGES Binarizes PIV images through particle density gradients
%   BINARIZEIMAGES(X, Y, N, Im) loads PIV images and binarizes them into 0
%   for reactants and 1 for products. The boundary between reactants and
%   products is found through a jump in particle density.
%   Inputs:
%       X: vector of x-positions (units specified in A.UnitX)
%       Y: vector of y-positions (units specified in A.UnitY)
%       Im: 2-D array of median filtered image intensities (units specified in A.UnitI)
%       N: number of images
%       level: threshold level for binarizing images
%       S: swirl number
%       P: pressure (atm)
%       T: reactant temperature (K)
%       U: mean flow velocity (m/s)
%       fuel: fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore this variable if phi = 0)
%       phi: equivalence ratio (use 0 for nonreacting)
%       angle: turbulence generator angle
%       expdate: string of the experiment date in form: 'yyyymmdd'
%       c: position of the center of the burner in mm found from inspection
%       of im7 image files.
%       xl: x limits (nondimensional: x/d)
%       yl: y limits (nondimensional: y/d)
%       frame: shot 1 or shot 2
%       method: binarizing method: 'graythresh' or 'kmeans'
%   Outputs:
%       BWs: 3-D array of binarized images
%       I: locations where binarization fails

BWs         = zeros(length(Y), length(X), N);

switch method
    case 'graythresh'
        Im          = uint8(Im);
        level       = graythresh(Im);
        BW          = im2bw(Im, level);
        BWs(:,:,1)  = imcomplement(BW);
    case 'kmeans'
        BW          = reshape(kmeans(Im(:), 2), size(Im))-1;
        if mean(BW(end, :)) > 0.5
            BWs(:,:,1)  = imcomplement(BW);
        else
            BWs(:,:,1)  = BW;
        end
    otherwise
        error('Invalid method');
end

parfor i = 2:N
    name                = ['B', sprintf('%0*d', 5, i), '.im7'];     % image filename (e.g. 'B00001.im7')
    [~, ~, ~, ~, Im]    = loadImageRemote(S, P, T, U, fuel, phi, angle, expdate, name, c, xl, yl, frame);
%     figure;
%     dispImage(A, X, Y, I, fsize);
%     figure;
%     dispImage(A, X, Y, Im, fsize);
    switch method
        case 'graythresh'
            Im          = uint8(Im);
            level       = graythresh(Im);
            BW          = im2bw(Im, level);
            BWs(:,:,i)  = imcomplement(BW);
        case 'kmeans'
            BW          = reshape(kmeans(Im(:), 2, 'emptyaction', 'drop'), size(Im))-1;
            if mean(BW(end, :)) > 0.5
                BWs(:,:,i)  = imcomplement(BW);
            else
                BWs(:,:,i)  = BW;
            end
        otherwise
            error('Invalid method');
    end
%     figure;
%     dispImage(A, X, Y, BW, fsize);
%     hold on;
%     flameEdge(BW, X, Y);
%     close
end
% find images where binarization produced only zeros or ones
Z               = mean(BWs, 1);
Z               = mean(Z, 2);
% BWs(:, :, I)    = [];
% find images where binarization only picks up swirling region
SR              = mean(BWs(1, :, :));
I               = Z == 0 | Z == 1 | SR < 0.5;
% BWs(:, :, J)    = [];
end