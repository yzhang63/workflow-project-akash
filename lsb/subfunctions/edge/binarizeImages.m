function [BWs, I] = binarizeImages(X, Y, Im, N, S, P, T, U, phi, angle, R, c, frame, varargin)
%disp('inside binarize images - sizeof(X, Y, Im):');
%disp(size(X));
%disp(size(Y));
%disp(size(Im));
fuel = '50-50 H2-CO';
method = 'graythresh';
expdate = 'Cam_Date=121030_Time=112150';
yl = [0.4 1.5];
xl = [-1 1]*R*0.4;

%function [BWs, I] = binarizeImages(X, Y, Im, N, S, P, T, U, fuel, phi, angle, expdate, ...
%    c, xl, yl, frame, method, varargin)
%BINARIZEIMAGES Binarizes PIV images through particle density gradients
%   BINARIZEIMAGES(X, Y, N, Im) loads PIV images and binarizes them into 0
%   for reactants and 1 for products. The boundary between reactants and
%   products is found through a jump in particle density.
%   Inputs:
%       X: vector of x-positions (units specified in A.UnitX)
%       Y: vector of y-positions (units specified in A.UnitY)
%       Im: 2-D array of median filtered image intensities (units specified in A.UnitI)
%       N: number of images or 2-element vector range of images (i.e., N =
%           [F L], where F is the first image index and L is the last image
%           index
%       level: threshold level for binarizing images
%       S: swirl number
%       P: pressure (atm)
%       T: reactant temperature (K)
%       U: mean flow velocity (m/s)
%       fuel: fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore this variable if phi = 0)
%       phi: equivalence ratio (use 0 for nonreacting)
%       angle: turbulence generator angle
%       expdate: string of the experiment date in form: 'yyyymmdd'
%       c: position of the center of the burner in mm found from inspection
%       of im7 image files.
%       xl: x limits (nondimensional: x/d)
%       yl: y limits (nondimensional: y/d)
%       frame: shot 1 or shot 2
%       method: binarizing method: 'graythresh' or 'kmeans'
%       cb: pixel value where burner reflection ends in order to zero out
%           this section of the image before binarizing. This is an optional
%           input, but recommended when not cropping out burner using yl.
%   Outputs:
%       BWs: 3-D array of binarized images
%       I: locations where binarization fails
%       E: cell array of flame edge positions

%% Assign variables to varargin cell values
% default values
corrected   = 0;            % 1 = world coordinates, 0 = raw coordinates
filtSize    = [8, 8];       % size of median filter
cb          = 1;            % crop burner pixel location
cropb       = 0;            % 1 = crop, 0 = no cropping

%length(varargin)
%for i = 1:2:length(varargin)
%    mystr = varargin{i}
%    switch varargin{i}
%    switch lower(varargin{i})
%        case 'cropburner'
%            cb          = varargin{i+1};
%            cropb       = 1;
%        case 'corrected'
%            corrected   = varargin{i+1};
%        case 'filtsize'
%            filtSize    = varargin{i+1};
%        otherwise
%            error('Invalid property name');
%    end
%end

corrected = 0;
filtSize = [5 5];


%% Setup image variables
if length(N) == 1
    N = [1 N];
end
F   = N(1);                               % index of first image
L   = N(2);                               % index of last image
N   = L - F + 1;                          % number of images to be analyzed

%disp('length(Y), length(X), N:');
%disp(length(Y));
%disp(length(X));
%disp(N);
%disp('size of Im in binarize image func: ');
%disp(size(Im));
%BWs = zeros(length(Y), length(X), N);
BWs = zeros(size(Im,1), size(Im,2));
% Crop the brightly illuminated burner out of the image
if cropb
    Im(cb:end, :)   = 0;
end

%% Binarize images
BWs(:,:,1)  = binarizeImage(Im, method);

if N >= 2
    parfor i = 2:N
        name                = ['B', sprintf('%0*d', 5, i+F-1), '.im7']; % filename (e.g. 'B00001.im7')
        [~, ~, ~, ~, Im]    = loadImage(S, P, T, U, fuel, phi, angle, expdate, name, ...
            c, xl, yl, frame, 'corrected', corrected, 'filtSize', filtSize);
        if cropb
            Im(cb:end, :)   = 0;
        end
        BWs(:,:,i)  = binarizeImage(Im, method);
    end
end
% find images where binarization produced only zeros or ones
Z               = mean(BWs, 1);
Z               = mean(Z, 2);
% BWs(:, :, I)    = [];
% find images where binarization only picks up swirling region
SR              = mean(BWs(1, :, :));
I               = Z == 0 | Z == 1 | SR < 0.5;
disp('class of I');
disp(class(I));

% BWs(:, :, J)    = [];

%size(BWs)
%disp('size of I:');
%disp(size(I));
disp('Exiting binarizeImages matlab function...');

end
