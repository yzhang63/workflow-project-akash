%% Edge finding and curvature parameters
global phi;
global ftype;
global delta_f;
global delta_f0;

if phi ~= 0
    ftype   = 'csaps';          % fitting method for flame edge ('poly' or 'csaps')
    delta_f = delta_f0;      % unstretched: delta_f0, sretched: delta_SLmax

%    disp('Exiting InputEdgeParameters...');
end
