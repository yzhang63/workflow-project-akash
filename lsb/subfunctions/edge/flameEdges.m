function [Ecell,Esize,Emat] = flameEdges(BWs, X, Y)
save flameEdges.mat
%disp('inside flameEdges step');
%FLAMEEDGES runs FLAMEEDGE on a 3D matrix of images
%   FLAMEEDGES(BWs, X, Y) runs FLAMEEDGE on a 3D matrix of binarized
%   images, BWs. See the help file of FLAMEEDGE for more details.
%   Inputs:
%       BWs: 3D matrix of binarized images
%       X: vector of x-positions (units specified in A.UnitX)
%       Y: vector of y-positions (units specified in A.UnitY)
%   Outputs:
%       E: cell array of flame edge coordinates

N       = length(BWs(1, 1, :));
% find images where binarization produced only zeros or ones
Z       = mean(BWs, 1);
Z       = mean(Z, 2);
% find images where binarization only picks up swirling region
SR      = mean(BWs(1, :, :));
I       = Z == 0 | Z == 1 | SR < 0.5;
I2      = reshape(I, N, 1, 1);
% set the edge values at indices I2 == 1 to be NaN
E       = cell(N, 1);
if mean(I2) ~= 0
    I3      = zeros(size(I2));
    I3(I2)  = NaN;
    I3      = num2cell(I3);
    E       = I3;
end
% generate array of indices that does not include I2 == 1
J       = 1:length(BWs(1, 1, :));
J(I2)   = [];
%disp('J');
%disp(J);
save flameEdges_mid.mat
for i = J
    disp('Entering flameEdge subfunc');
    disp(i);
    disp('length X&Y');
    disp(length(X));
    disp(length(Y));
    E{i} = flameEdge(BWs(:, :, i), X, Y);
    disp('Exiting flameEdge subfunc');
    %disp('class(E{i})');
    %disp(class(E{i}));
end
save flameEdges_mid_2.mat
%disp('E class & size');
%disp(class(E));
%disp(size(E));
%disp('N');
%disp(N);
%for i=1:length(E)
%  disp('sizeof E{i}');
%  disp(size(E{i}));
%end

% to report un-expected situation  for DEBUGGING
% i.e., length(E) > 1 (N > 1)
Esize = max(size(E,1), size(E,2)); %length(E)
if Esize > 1
  disp('cell array E - Unexpected forms!!! @flameEdges');
  exit;
end
Ecell = E;
Emat = E{1}; % use a trick here
%disp('reading Emat');
disp(size(Emat));
%disp(Emat(1,1));
%disp('size of Ecell:');
%disp(size(Ecell));
%disp('size of Emat:');
%disp(size(Emat));
save flameEdges_end.mat
disp('Exiting flameedges matlab func');

end
