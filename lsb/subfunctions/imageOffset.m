function os = imageOffset(A, c, Xr, Yr)
%IMAGEOFFSET calculates the offsets of a cropped PIV image from the
%original
%   IMAGEOFFSET(A, c, X, Y) calculates the x and y offsets of a cropped PIV
%   image from the original image. Offsets are given as pixels from the
%   top left corner of the original image. 0 corresponds to no offset.
%   Inputs:
%       A: structure generated from readimx for .im7 files
%       c: position of the center of the burner in mm found from inspection
%       Xr: vector of x-positions (units specified in A.UnitX) from
%           LOADIMAGE
%       Yr: vector of y-positions (units specified in A.UnitY) from
%           LOADIMAGE
%   Outputs:
%       os: x and y offsets (x_offset, y_offset) in pixels

[X, Y, ~]   = prepareImage(A, 1, c);    % prepare the image
I           = find(X == Xr(1));
J           = find(Y == Yr(1));
os(1)       = I - 1;
os(2)       = J - 1;
end