function I = scaleImage(I, bits)
%SCALEIMAGE scale images to a specific bit depth
%   SCALEIMAGE(I, bits) scales the 2D matrix I to image depth specified by
%   bits
%   Inputs:
%       I: 2D matrix to be scaled
%       bits: bit depth to scale to (8 bit or 16 bit)
%   Outputs:
%       I: 2D scaled matrix

I   = (I - min(min(I)))./(max(max(I)) - min(min(I)));   % Scale intensity between [0, 1]
I   = I.*(2^bits - 1);                                  % Scale to precision of bits
switch bits
    case 8
        I   = uint8(I);
    case 16
        I   = uint16(I);
    otherwise
        error('unsupported precision value for variable bits');
end
end