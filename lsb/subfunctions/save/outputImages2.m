function [] = outputImages2(Z1, Z2, flnm)
%OUTPUTIMAGES2 Creates uncompressed tif image files
%   Takes processed images and saves the data as uncompressed tif image
%   files suitable for DaVis processing.
%   
%   Inputs:
%       Z1: shot 1
%       Z2: shot 2
%       flnm: string of the base of the filename for the output images
%       without the .tif extension on the end.
% Updated: 10/12/11 by Andrew Marshall

bits = 16;

%% Scale data
Z1          = scaleImage(Z1, bits);
Z2          = scaleImage(Z2, bits);

%% Write image
imwrite(Z1, [flnm, '_1.tif'], 'tiff', 'Compression', 'none');
imwrite(Z2, [flnm, '_2.tif'], 'tiff', 'Compression', 'none');
end