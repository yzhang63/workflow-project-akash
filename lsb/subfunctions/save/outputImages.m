function [] = outputImages(Z1, Z2, flnm)
%OUTPUTIMAGES Creates uncompressed tif image files
%   Takes processed images and saves the data as uncompressed tif image
%   files suitable for DaVis processing.
%   
%   Inputs:
%       Z1: shot 1
%       Z2: shot 2
%       flnm: string of the base of the filename for the output images
%       without the .tif extension on the end.
% Updated: 10/12/11 by Andrew Marshall

bits = 16;

%% Scale and concatenate data
Z1          = scaleImage(Z1, bits);
Z2          = scaleImage(Z2, bits);
Z           = cat(1, Z2, Z1);

%% Write image
imwrite(Z, [flnm, '.tif'], 'tiff', 'Compression', 'none');
end