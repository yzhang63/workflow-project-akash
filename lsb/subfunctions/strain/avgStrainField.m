function [X, Y, M, varargout] = ...
    avgStrainField(S, P, T, U, fuel, phi, angle, expdate, c, N, EijFolder, varargin)
%AVGSTRAINFIELD computes the average strain field Eij from DaVis compressed images
%   AVGSTRAINFIELD(S, P, T, U, fuel, phi, angle, expdate, c, N, EijFolder, varargin)
%   computes the average strain field Eij from DaVis compressed images
%   Inputs:
%       S: swirl number
%       P: pressure (atm)
%       T: reactant temperature (K)
%       U: mean flow velocity (m/s)
%       fuel: fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore this variable if phi = 0)
%       phi: equivalence ratio (use 0 for nonreacting)
%       angle: turbulence generator angle
%       expdate: string of the experiment date in form: 'yyyymmdd'
%       c: position of the center of the burner in mm found from inspection
%           of im7 image files.
%       N: number of images or 2-element vector range of images (i.e., N =
%           [F L], where F is the first image index and L is the last image
%           index
%       EijFolder: string specifying strain field to extract (e.g. 'Exx'
%                   for du/dx, 'Eyx' for dv/dx, etc.)
%   Outputs:
%       X: vector of x-positions (units specified in A.UnitX)
%       Y: vector of y-positions (units specified in A.UnitY)
%       M: 2-D array of average strain rates (units specified in A.UnitI)
%       s: 2-D array of sample standard deviations
%       SE: 2-D array of standard errors of the means

%% Assign variables to varargin cell values
% default values
vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';   % folder name for processed vc7 files
rc          = 1;                                % 1: reactant-conditioned, 0: unconditioned
for i = 1:2:length(varargin)
    switch lower(varargin{i})
        case 'vc7folder'
            vc7Folder   = varargin{i+1};
        case 'rc'
            rc          = varargin{i+1};
        otherwise
            error('Invalid property name');
    end
end

[X, Y, Eij, A]  = loadStrainFields(S, P, T, U, fuel, phi, angle, expdate, c, N, EijFolder, ...
    'rc', rc, 'vc7Folder', vc7Folder);
M               = nanmean(Eij, 3);

if nargout >= 4
    varargout{1} = nanstd(Eij, 0, 3);
end
if nargout >= 5
    I               = ~isnan(Eij);
    n               = sum(I, 3);
    varargout{2}    = varargout{1}./sqrt(n);
end
if nargout >= 6
    varargout{3}    = A;
end
end