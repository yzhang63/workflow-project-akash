function kappa_s = ...
    tangentialStrainRates(S, P, T, U, fuel, phi, angle, expdate, c, N, E, x, y, nx, ny, varargin)
%TANGENTIALSTRAINRATES Calculates tangential strain rate along a given edge
%   TANGENTIALSTRAINRATES(S, P, T, U, fuel, phi, angle, expdate, c, N, E, x, y, nx, ny, varargin)
%   calculates the tangential strain rate at locations along an edge.
%   Inputs:
%       S: swirl number
%       P: pressure (atm)
%       T: reactant temperature (K)
%       U: mean flow velocity (m/s)
%       fuel: fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore this variable if phi = 0)
%       phi: equivalence ratio (use 0 for nonreacting)
%       angle: turbulence generator angle
%       expdate: string of the experiment date in form: 'yyyymmdd'
%       c: position of the center of the burner in mm found from inspection
%       of im7 image files.
%       N: number of images or 2-element vector range of images (i.e., N =
%           [F L], where F is the first image index and L is the last image
%           index
%       E: cell array of flame edge positions found from BINARIZEIMAGES
%       x: x coordinates of the fitted, smoothed edge
%       y: y coordinates of the fitted, smoothed edge
%       nx: x-component of the unit normal vector
%       ny: y-component of the unit normal vector
%   Outputs:
%       kappa_s: tangential strain rate (1/s)

%% Assign variables to varargin cell values
% default values
vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';   % folder name for processed vc7 files
rc          = 1;                                % 1: reactant-conditioned, 0: unconditioned
for i = 1:2:length(varargin)
    switch lower(varargin{i})
        case 'vc7folder'
            vc7Folder   = varargin{i+1};
        case 'rc'
            rc          = varargin{i+1};
        otherwise
            error('Invalid property name');
    end
end

%% Setup image variables
if length(N) == 1
    N = [1 N];
end
F   = N(1);                               % index of first image
L   = N(2);                               % index of last image
N   = L - F + 1;                          % number of images to be analyzed

kappa_s         = cell(N, 1);
name            = ['B', sprintf('%0*d', 5, F), '.im7'];
[~, Xs, Ys, ~]  = loadStrainField(S, P, T, U, fuel, phi, angle, expdate, name, c, 'Exx', ...
    'rc', rc, 'vc7Folder', vc7Folder);
parfor i = 1:N
    name            = ['B', sprintf('%0*d', 5, i+F-1), '.im7'];
    [~, ~, ~, Exx]  = loadStrainField(S, P, T, U, fuel, phi, angle, expdate, name, c, 'Exx', ...
        'rc', rc, 'vc7Folder', vc7Folder);
    Exx(Exx == 0)   = NaN;
    [~, ~, ~, Exy]  = loadStrainField(S, P, T, U, fuel, phi, angle, expdate, name, c, 'Exy', ...
        'rc', rc, 'vc7Folder', vc7Folder);
    Exy(Exy == 0)   = NaN;
    [~, ~, ~, Eyx]  = loadStrainField(S, P, T, U, fuel, phi, angle, expdate, name, c, 'Eyx', ...
        'rc', rc, 'vc7Folder', vc7Folder);
    Eyx(Eyx == 0)   = NaN;
    [~, ~, ~, Eyy]  = loadStrainField(S, P, T, U, fuel, phi, angle, expdate, name, c, 'Eyy', ...
        'rc', rc, 'vc7Folder', vc7Folder);
    Eyy(Eyy == 0)   = NaN;
    ExxE            = edgeValue(Xs, Ys, Exx, E{i}, x{i}, y{i});
    ExyE            = edgeValue(Xs, Ys, Exy, E{i}, x{i}, y{i});
    EyxE            = edgeValue(Xs, Ys, Eyx, E{i}, x{i}, y{i});
    EyyE            = edgeValue(Xs, Ys, Eyy, E{i}, x{i}, y{i});
    kappa_s{i}      = tangentialStrainRate(ExxE, ExyE, EyxE, EyyE, nx{i}, ny{i});
end
end