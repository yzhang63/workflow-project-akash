function kappa_s = tangentialStrainRate(Exx, Exy, Eyx, Eyy, nx, ny)
%TANGENTIALSTRAINRATE Calculates the 2D tangential strain rate
%   TANGENTIALSTRAINRATE(Exx, Exy, Eyx, Eyy, nx, ny) calculates the 2D
%   tangential strain rate, kappa_s, along an edge with unit normal 
%   n = (nx, ny).
%   Inputs:
%       Exx: compression/expansion, du/dx (1/s)
%       Exy: shear, du/dy (1/s)
%       Eyx: shear, dv/dx (1/s)
%       Eyy: compression/expansion, dv/dy (1/s)
%       nx: x-component of the unit normal vector
%       ny: y-component of the unit normal vector
%   Outputs:
%       kappa_s: tangential strain rate (1/s)

kappa_s = (1 - nx.^2).*Exx - nx.*ny.*(Exy + Eyx) + (1 - ny.^2).*Eyy;
end

