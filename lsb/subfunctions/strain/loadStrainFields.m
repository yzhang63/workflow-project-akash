function [X, Y, Eij, varargout] = ...
    loadStrainFields(S, P, T, U, fuel, phi, angle, expdate, c, N, EijFolder, varargin)
%LOADSTRAINFIELDS loads strain fields Eij from DaVis compressed images
%   LOADSTRAINFIELDS(S, P, T, U, fuel, phi, angle, expdate, c, N, EijFolder, varargin)
%   loads strain fields Eij from DaVis im7 image files 
%   Inputs:
%       S: swirl number
%       P: pressure (atm)
%       T: reactant temperature (K)
%       U: mean flow velocity (m/s)
%       fuel: fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore this variable if phi = 0)
%       phi: equivalence ratio (use 0 for nonreacting)
%       angle: turbulence generator angle
%       expdate: string of the experiment date in form: 'yyyymmdd'
%       c: position of the center of the burner in mm found from inspection
%           of im7 image files.
%       N: number of images or 2-element vector range of images (i.e., N =
%           [F L], where F is the first image index and L is the last image
%           index
%       EijFolder: string specifying strain field to extract (e.g. 'Exx'
%                   for du/dx, 'Eyx' for dv/dx, etc.)
%   Outputs:
%       X: vector of x-positions (units specified in A.UnitX)
%       Y: vector of y-positions (units specified in A.UnitY)
%       Eij: 2-D array of strain rates (units specified in A.UnitI)
%       A: structure generated from READIMX for .vc7 files
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

%% Assign variables to varargin cell values
% default values
vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';   % folder name for processed vc7 files
rc          = 1;                                % 1: reactant-conditioned, 0: unconditioned
for i = 1:2:length(varargin)
    switch lower(varargin{i})
        case 'vc7folder'
            vc7Folder   = varargin{i+1};
        case 'rc'
            rc          = varargin{i+1};
        otherwise
            error('Invalid property name');
    end
end

%% Setup image variables
if length(N) == 1
    N = [1 N];
end
F   = N(1);                               % index of first image
L   = N(2);                               % index of last image
N   = L - F + 1;                          % number of images to be analyzed

name            = ['B', sprintf('%0*d', 5, F), '.im7'];
[A, X, Y, ~]    = loadStrainField(S, P, T, U, fuel, phi, angle, expdate, name, c, EijFolder, ...
    'rc', rc, 'vc7Folder', vc7Folder);
Eij             = NaN(length(Y), length(X), N);
parfor i = 1:N
    name                    = ['B', sprintf('%0*d', 5, i+F-1), '.im7'];
    [~, ~, ~, Eij(:, :, i)] = loadStrainField(S, P, T, U, fuel, phi, angle, expdate, name, c, ...
        EijFolder, 'rc', rc, 'vc7Folder', vc7Folder);
end
Eij(Eij == 0)   = NaN;
if nargout >= 4
    varargout{1} = A;
end
end