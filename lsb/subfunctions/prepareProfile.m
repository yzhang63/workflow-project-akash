function [X, Y, vx, vy] = prepareProfile(A, c)
%PREPAREPROFILE Creates meshgrid of profile data from data structure
%   PREPAREPROFILE(A, c) takes the data structure generated from READIMX 
%   for .vc7 files and generates arrays of positions and velocities. These 
%   outputs can be used with the QUIVER function to generate 2-D velocity 
%   arrow plots.
%   Inputs:
%       A: structure generated from READIMX for .vc7 files
%       c: position of the center of the burner in mm found from inspection
%       of im7 image files.
%   Output:
%       X: 2-D array of x-positions (units specified in A.UnitX)
%       Y: 2-D array of y-positions (units specified in A.UnitY)
%       vx: 2-D array of x-component velocities (units specified in
%       A.UnitI)
%       vy: 2-D array of y-component velocities (units specified in
%       A.UnitI)
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

nx      = A.Nx;
ny      = A.Ny;

%% Set data range
XRange  = 1:nx;
YRange  = 1:ny;

%% Initialize X and Y values
X       = double((XRange - 0.5)*A.Grid*A.ScaleX(1) + A.ScaleX(2) - c); % x-range
Y       = double((YRange - 0.5)*A.Grid*A.ScaleY(1) + A.ScaleY(2)); % y-range

%% Calculate vector position and components
[X, Y]  = ndgrid(X, Y);
vx      = double(A.Data(:, YRange     ))*A.ScaleI(1) + A.ScaleI(2);
vy      = double(A.Data(:, YRange + ny))*A.ScaleI(1) + A.ScaleI(2);

if A.ScaleY(1) < 0
    vy  = -vy;
end
end