function [n, xout] = hist3n(z1, z2)
%HISTN Normalized bivariate histogram
%   HIST3N(z) generates the data necessary to generate a normalized
%   bivariate histogram plot. This approach uses the Freedman-Diaconis 
%   choice for calculating the histogram bin width.
%   Inputs:
%       z1: column vector of data for variable 1
%       z2: column vector of data for variable 2
%   Outputs:
%       n: normalized frequency counts in each bin (normalized by the total
%           number of counts multiplied by the bin width h)
%       xout: bin locations

z1l         = [nanmean(z1)-6*nanstd(z1), nanmean(z1)+6*nanstd(z1)];         % +/- 6 standard deviations
z2l         = [nanmean(z2)-6*nanstd(z2), nanmean(z2)+6*nanstd(z2)];         % +/- 6 standard deviations

I1          = z1 < z1l(1) | z1 > z1l(2);            % data outside of 6 standard deviations
I2          = z2 < z2l(1) | z2 > z2l(2);
I           = I1 | I2;
z1(I)       = [];
z2(I)       = [];

N1          = length(z1);                           % number of elements in z1
h1          = 2*iqr(z1)/N1^(1/3);                   % histogram bin width (Freedman-Diaconis' choice)
k1          = ceil((max(z1) - min(z1))./h1);        % number of histogram bins

N2          = length(z2);                           % number of elements in z2
h2          = 2*iqr(z2)/N2^(1/3);                   % histogram bin width (Freedman-Diaconis' choice)
k2          = ceil((max(z2) - min(z2))./h2);        % number of histogram bins

[n, xout]   = hist3([z1, z2], [k1, k2]);            % frequency counts and bin locations
n           = n./((N1 + N2)*h1*h2);                 % normalized frequency counts (total area = 1)
end