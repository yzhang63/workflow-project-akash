function [n, xout] = histn(z)
%HISTN Normalized histogram
%   HISTN(z) generates the data necessary to generate a normalized
%   histogram plot. This approach uses the Freedman-Diaconis choice for
%   calculating the histogram bin width.
%   Inputs:
%       z: vector of data
%   Outputs:
%       n: normalized frequency counts in each bin (normalized by the total
%           number of counts multiplied by the bin width h)
%       xout: bin locations

N           = length(z(:, 1));              % number of elements in z
h           = 2*iqr(z)/N^(1/3);             % histogram bin width (Freedman-Diaconis' choice)
k           = ceil((max(z) - min(z))./h);   % number of histogram bins

n           = cell(1, length(z(1, :)));
xout        = n;
for i = 1:length(z(1, :))
    [n{i}, xout{i}]  = hist(z(:, i), k(i));  % frequency counts and bin locations
    n{i}            = n{i}./(N*h(i));       % normalized frequency counts
end
if length(z(1, :)) == 1
    n       = n{1};
    xout    = xout{1};
end
end