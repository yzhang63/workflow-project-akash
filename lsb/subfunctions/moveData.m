%% Input parameters
dirProj     = [dirProjs, '\', proj];            % Project directory

%% setup character string for destination directory
if phi == 0
    destDir = [dirData, '\nonreacting\S = ', num2str(S), '\', int2str(P), ' atm\', int2str(T), ' K\', ...
        int2str(U), ' mps\', int2str(angle), ' deg\', setName];
else
    destDir = [dirData, '\reacting\S = ', num2str(S), '\', int2str(P), ' atm\', int2str(T), ' K\', ...
        int2str(U), ' mps\', fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', setName];
end

%% cd to directory containing data to be moved
if rc
    dirSource   = [dirProj, '\', setName, '\masked images'];
    destDir     = [destDir, '\masked images'];  % modify destination directory for masked images
else
    dirSource   = [dirProj, '\', setName];
end
prevdir = cd;
cd(dirSource);

%% cd to folder containing data to be moved
if strcmp(dirSource, [dirProj, '\', dirName])
    % do nothing - this means that the data is im7 files located in the directory dirSource
elseif exist(dirName, 'dir')                    % check if folder containing data exists at this level
    cd(dirName);                                % cd into folder containing data
    destDir     = [destDir, '\', dirName];      % modify destination directory to folder containing data
else                                            % no folder, thus, subfolder of velocity vectors folder
    if rc
        cd(mvc7Folder);                         % cd into folder containing masked velocity vectors
        destDir = [destDir, '\', mvc7Folder];   % modify destination directory to folder containing data
    else
        cd(vc7Folder);                          % cd into folder containing masked velocity vectors
        destDir = [destDir, '\', vc7Folder];    % modify destination directory to folder containing data
    end
    cd(dirName);                                % cd into folder containing data to be moved
    destDir     = [destDir, '\', dirName];      % modify destination directory to folder containing data
end

%% create destination folder and move data
% only move data if it has not been moved
if ~isempty(dir('B*7'))
    if ~exist(destDir, 'dir')
        mkdir(destDir)
    end
    movefile('B*7', destDir);
    disp([dirName, ' moved to the destination directory']);
elseif ~isempty(dir([destDir, '\', 'B*7']))
    disp([dirName, ' has already been moved to the destination directory']);
else
    disp([dirName, ' does not exist']);
end
cd(prevdir);