function [STLD, varargout] = ...
    loadSTLD(S, P, T, U, fuel, phi, angle, expdate, method, frame, varargin) %#ok<STOUT>
%LOADSTLD loads flame speed data
%   LOADSTLD(S, P, T, U, angle, expdate) loads flame speed data from a
%   specific folder. Loaded data includes STLD and uy.
%   Inputs:
%       S: swirl number
%       P: pressure (atm)
%       T: reactant temperature (K)
%       U: mean flow velocity (m/s)
%       fuel: fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore this variable if phi = 0)
%       phi: equivalence ratio (use 0 for nonreacting)
%       angle: turbulence generator angle
%       expdate: string of the experiment date in form: 'yyyymmdd'
%       method: 'flamebrush' = STLD calculated from FlameEdgeRunner.m
%               'linearfit' = STLD calculated from LSBRunner.m
%       frame: shot 1 or shot 2 (ignored for method = 'linearfit')
%   Outputs:
%       STLD: turbulent local displacement flame speed
%       uy: axial turbulence intensity at the location of STLD

%% Assign variables to varargin cell values
% default values
vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';   % folder name for processed vc7 files
filtSize    = 5;                                % median filter size
N           = 10000;                            % number of images

for i = 1:2:length(varargin)
    switch lower(varargin{i})
        case 'vc7folder'
            vc7Folder   = varargin{i+1};
        case 'delta_f'
            delta_f     = varargin{i+1};
        case 'filtsize'
            filtSize    = varargin{i+1};
        case 'numimages'
            N           = varargin{i+1};
        otherwise
            error('Invalid property name');
    end
end

%% Load data
switch method
    case {'graythresh', 'kmeans'}
        load(['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
            int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
            fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', expdate, ...
            '\Final Results\data, delta_f = ', num2str(delta_f, 2), ', shot ', int2str(frame), ...
            ', filt = ', int2str(filtSize(1)), ', ', method, ', images 1-', int2str(N), '.mat'], ...
            'STLD', 'uy', 'SL0', 'SLmax');
    case 'linearfit'
        load(['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
            int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
            fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', expdate, ...
            '\', vc7Folder, '\TimeMeanQF_Vector\data.mat'], ...
            'STLD', 'uy', 'SL0', 'SLmax');
    otherwise
        error([method, ' is not a valid method']);
end
varargout       = cell(1, nargout - 1);
switch length(varargout)
    case 1
        varargout{1}    = uy;
    case 2
        varargout{1}    = uy;
        varargout{2}    = SL0;
    case 3
        varargout{1}    = uy;
        varargout{2}    = SL0;
        varargout{3}    = SLmax;
end
end