function [A, X, Y, I] = loadFullImage(S, P, T, U, fuel, phi, angle, expdate, name, c, frame)
%LOADFULLIMAGE loads a specific image from a specific PIV experiment
%   LOADIMAGE(S, P, T, U, fuel, phi, angle, expdate, name, c, frame) loads and
%   displays a specific image from a specific PIV experiment.
%   Inputs:
%       S: swirl number
%       P: pressure (atm)
%       T: reactant temperature (K)
%       U: mean flow velocity (m/s)
%       fuel: fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore this variable if phi = 0)
%       phi: equivalence ratio (use 0 for nonreacting)
%       angle: turbulence generator angle
%       expdate: string of the experiment date in form: 'yyyymmdd'
%       name: string of the name of the image to be displayed (e.g.
%       'B00001.im7')
%       c: position of the center of the burner in mm found from inspection
%       frame: shot 1 or shot 2
%   Outputs:
%       A: structure generated from readimx for .im7 files
%       X: vector of x-positions (units specified in A.UnitX)
%       Y: vector of y-positions (units specified in A.UnitY)
%       I: 2-D array of image intensities (units specified in A.UnitI)
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

prevdir = cd;
if phi == 0
    currdir = ['\\Vboxsvr\piv\LSB\nonreacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        int2str(angle), ' deg\', expdate];
else
    currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', ...
        expdate];
end
cd(currdir);
A           = readimx(name);
A.LabelX    = 'x';
A.LabelY    = 'y';
A.UnitX(1)      = '(';
A.UnitX(end)    = ')';
A.UnitY(1)      = '(';
A.UnitY(end)    = ')';
cd(prevdir);

[X, Y, I] = prepareImage(A, frame, c);    % prepare the image
end