function [A, X, Y, ux, uy] = ...
loadVectorField(S, P, T, U, fuel, phi, angle, expdate, name, c, varargin)
%LOADVECTORFIELD Loads vector fields from DaVis vc7 files
%   LOADVECTORFIELD(S, P, T, U, fuel, phi, angle, expdate, name, c, varargin)
%   loads vector fields from DaVis vc7 files.
%   Inputs:
%       S: swirl number
%       P: pressure (atm)
%       T: reactant temperature (K)
%       U: mean flow velocity (m/s)
%       fuel: fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore this variable if phi = 0)
%       phi: equivalence ratio (use 0 for nonreacting)
%       angle: turbulence generator angle
%       expdate: string of the experiment date in form: 'yyyymmdd'
%       name: string of the name of the image to be displayed (e.g.
%       'B00001.im7')
%       c: position of the center of the burner in mm found from inspection
%       of im7 image files.
%   Outputs:
%       A: structure generated from readimx for .im7 files
%       X: vector of x-positions (units specified in A.UnitX)
%       Y: vector of y-positions (units specified in A.UnitY)
%       ux: 2-D array of x-component velocities (units specified in A.UnitI)
%       uy: 2-D array of y-component velocities (units specified in A.UnitI)
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

%% Assign variables to varargin cell values
% default values
rc          = 0;                                % 1 = reactant-conditioned, 0 = unconditioned
vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';   % folder name for processed vc7 files

for i = 1:2:length(varargin)
    switch lower(varargin{i})
        case 'rc'
            rc          = varargin{i+1};
        case 'vc7folder'
            vc7Folder   = varargin{i+1};
        otherwise
            error('Invalid property name');
    end
end

%% Change to directory containing im7 file
prevdir = cd;

if rc == 1
    finalFolders = [expdate, '\masked images'];
else
    finalFolders = expdate;
end

if phi == 0
    currdir = ['\\Vboxsvr\piv\LSB\nonreacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        int2str(angle), ' deg\', finalFolders, '\', vc7Folder];
else
    currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', ...
        finalFolders, '\', vc7Folder];
end
cd(currdir);

%% Load image file and make a few changes to the structure
A               = readimx(name);
A.LabelX        = 'x';
A.LabelY        = 'y';
A.UnitX(1)      = '(';
A.UnitX(end)    = ')';
A.UnitY(1)      = '(';
A.UnitY(end)    = ')';
A.UnitI         = ['(', A.UnitI, ')'];
cd(prevdir);

%% Prepare the image
[X, Y, ux, uy]  = prepareIMX(A, c);      % prepare the image
end