function [A, X, Y, Eij] = ...
loadStrainField(S, P, T, U, fuel, phi, angle, expdate, name, c, EijFolder, varargin)
%LOADSTRAINFIELD Loads strain field Eij from DaVis compressed images
%   LOADSTRAINFIELD(S, P, T, U, fuel, phi, angle, expdate, name, c, EijFolder, varargin)
%   loads strain field Eij from DaVis im7 image files. 
%   Inputs:
%       S: swirl number
%       P: pressure (atm)
%       T: reactant temperature (K)
%       U: mean flow velocity (m/s)
%       fuel: fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore this variable if phi = 0)
%       phi: equivalence ratio (use 0 for nonreacting)
%       angle: turbulence generator angle
%       expdate: string of the experiment date in form: 'yyyymmdd'
%       name: string of the name of the image to be displayed (e.g.
%       'B00001.im7')
%       c: position of the center of the burner in mm found from inspection
%       of im7 image files.
%       EijFolder: string specifying strain field to extract (e.g. 'Exx'
%                   for du/dx, 'Eyx' for dv/dx, etc.)
%   Outputs:
%       A: structure generated from readimx for .im7 files
%       X: vector of x-positions (units specified in A.UnitX)
%       Y: vector of y-positions (units specified in A.UnitY)
%       Eij: 2-D array of strain rates (units specified in A.UnitI)
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

%% Assign variables to varargin cell values
% default values
rc          = 0;                                % 1 = reactant-conditioned, 0 = unconditioned
vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';   % folder name for processed vc7 files

for i = 1:2:length(varargin)
    switch lower(varargin{i})
        case 'rc'
            rc          = varargin{i+1};
        case 'vc7folder'
            vc7Folder   = varargin{i+1};
        otherwise
            error('Invalid property name');
    end
end

%% Change to directory containing im7 file
prevdir = cd;

if rc == 1
    finalFolders = [expdate, '\masked images'];
else
    finalFolders = expdate;
end

if phi == 0
    currdir = ['\\Vboxsvr\piv\LSB\nonreacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        int2str(angle), ' deg\', finalFolders, '\', vc7Folder, '\', EijFolder];
else
    currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', ...
        finalFolders, '\', vc7Folder, '\', EijFolder];
end
cd(currdir);

%% Load image file and make a few changes to the structure
A               = readimx(name);
A.LabelX        = 'x';
A.LabelY        = 'y';
A.UnitX(1)      = '(';
A.UnitX(end)    = ')';
A.UnitY(1)      = '(';
A.UnitY(end)    = ')';
A.UnitI         = ['(', A.UnitI, ')'];
cd(prevdir);

%% Prepare the image, perform median filtering, and crop the image down
[X, Y, Eij]     = prepareIMX(A, c);      % prepare the image
end