function [X, Y, I, Im] = loadImage(S, P, T, U, fuel, phi, angle, expdate, name, c, xl, yl, frame, varargin)
%function [A, X, Y, I, Im] = loadImage(S, P, T, U, fuel, phi, angle, expdate, name, c, xl, yl, frame, varargin)
%LOADIMAGE loads and displays a specific image from a specific PIV experiment
%   LOADIMAGE(S, P, T, U, fuel, phi, angle, expdate, name, c, xl, yl, frame) loads and
%   displays a specific image from a specific PIV experiment.
%   Inputs:
%       S: swirl number
%       P: pressure (atm)
%       T: reactant temperature (K)
%       U: mean flow velocity (m/s)
%       fuel: fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore this variable if phi = 0)
%       phi: equivalence ratio (use 0 for nonreacting)
%       angle: turbulence generator angle
%       expdate: string of the experiment date in form: 'yyyymmdd'
%       name: string of the name of the image to be displayed (e.g.
%       'B00001.im7')
%       c: position of the center of the burner in mm found from inspection
%       of im7 image files.
%       xl: x limits (nondimensional: x/d)
%       yl: y limits (nondimensional: y/d)
%       frame: shot 1 or shot 2
%   Outputs:
%       A: structure generated from readimx for .im7 files
%       X: vector of x-positions (units specified in A.UnitX)
%       Y: vector of y-positions (units specified in A.UnitY)
%       I: 2-D array of image intensities (units specified in A.UnitI)
%       Im: median filtered image
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

%% Assign variables to varargin cell values
% default values
d           = 36.3;         % burner diameter (mm)
corrected   = 0;            % 1 = world coordinates, 0 = raw coordinates
filtSize    = [8, 8];       % size of median filter

for i = 1:2:length(varargin)
    switch lower(varargin{i})
        case 'corrected'
            corrected   = varargin{i+1};
        case 'filtsize'
            filtSize    = varargin{i+1};
        otherwise
            error('Invalid property name');
    end
end

%% Change to directory containing im7 file
if corrected
    expdate = [expdate, '/Correction'];
end

prevdir = cd;
%disp(prevdir);
%disp(expdate);

datahomedir = '/lustre/atlas/scratch/yzhang63/csc025/PIV';
%datahomedir = '/lustre/widow/scratch/yzhang63/PIV';
if phi == 0
    currdir = [datahomedir, '/LSB/nonreacting/S = ', num2str(S), '/', ...
        int2str(P), ' atm/', int2str(T), ' K/', int2str(U), ' mps/', ...
        int2str(angle), ' deg/', expdate];
else
    currdir = [datahomedir, '/LSB/reacting/S = ', num2str(S), '/', ...
        int2str(P), ' atm/', int2str(T), ' K/', int2str(U), ' mps/', ...
        fuel, '/phi = ', num2str(phi), '/', int2str(angle), ' deg/', ...
        expdate];
end
%disp(currdir);
cd(currdir);

% one way to work around this is to
% run the code on wins first, and store X, Y, I;
% and in linux matlab just read in X, Y, I, instead of 
% calling readimx & prepareImage
%% Load image file and make a few changes to the structure
%A               = readimx(name);
%%[status, result] = system('ReadIMX');
%%disp(status);
%%disp(result);
%A.LabelX        = 'x';
%A.LabelY        = 'y';
%A.UnitX(1)      = '(';
%A.UnitX(end)    = ')';
%A.UnitY(1)      = '(';
%A.UnitY(end)    = ')';
%cd(prevdir);
%disp('Current dir:');
%disp(cd);

%% Prepare the image, perform median filtering, and crop the image down
%[X, Y, I]   = prepareImage(A, frame, c);    % prepare the image
filedir = '/lustre/atlas/scratch/yzhang63/csc025/PIV/LSB';
xfile = [filedir, '/in_data/X_data.', name];
yfile = [filedir, '/in_data/Y_data.', name];
ifile = [filedir, '/in_data/I_data.', name];
X = dlmread(xfile);
Y = dlmread(yfile);
I = dlmread(ifile);

Im          = medfilt2(I, filtSize);        % median filtered image

[~, I1]     = min(abs(X - xl(1)*d));        % index of lower x limit
[~, I2]     = min(abs(X - xl(2)*d));        % index of upper x limit
X           = X(I1:I2);                     % reduced X range
[~, J1]     = min(abs(Y - yl(1)*d));        % index of lower y limit
[~, J2]     = min(abs(Y - yl(2)*d));        % index of upper y limit
Y           = Y(J2:J1);                     % reduced Y range
I           = I(J2:J1, I1:I2);              % reduced image range
Im          = Im(J2:J1, I1:I2);             % reduced filtered image range

xfile = [filedir, '/inter_data/X_data.',name];
yfile = [filedir, '/inter_data/Y_data.', name];
ifile = [filedir, '/inter_data/I_data.', name];
imfile = [filedir, '/inter_data/Im_data.', name];
dlmwrite(xfile, X);
dlmwrite(yfile, Y);
dlmwrite(ifile, I);
dlmwrite(imfile, Im);

end
