function [A, X, Y, I] = loadImageBasic(S, frame)
%LOADIMAGEBASIC loads and displays a specific image from a specific PIV experiment
%   LOADIMAGE(S) loads and displays a specific image from a specific PIV experiment.
%   Inputs:
%       S: Matlab string of a valid filename
%       frame: shot 1 or shot 2
%   Outputs:
%       A: structure generated from readimx for .im7 files
%       X: vector of x-positions (units specified in A.UnitX)
%       Y: vector of y-positions (units specified in A.UnitY)
%       I: 2-D array of image intensities (units specified in A.UnitI)
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

A               = readimx(S);
A.LabelX        = 'x';
A.LabelY        = 'y';
A.UnitX(1)      = '(';
A.UnitX(end)    = ')';
A.UnitY(1)      = '(';
A.UnitY(end)    = ')';
[X, Y, I]       = prepareImage(A, frame, 0);    % prepare the image
end