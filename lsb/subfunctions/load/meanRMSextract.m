function [meanV, rmsV] = meanRMSextract(S, P, T, U, fuel, phi, angle, expdate, rc, varargin)
%MEANRMSEXTRACT Extract mean and RMS vc7 vector files
%   MEANRMSEXTRACT(S, P, T, U, fuel, phi, angle, expdate) extracts PIV Profile data 
%   from .vc7 files. 
%   Inputs:
%       S: swirl number
%       P: pressure (atm)
%       T: reactant temperature (K)
%       U: mean flow velocity (m/s)
%       fuel: fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore this variable if phi = 0)
%       phi: equivalence ratio (use 0 for nonreacting)
%       angle: turbulence generator angle
%       expdate: string of the experiment date in form: 'yyyymmdd'
%       rc: 1 = reactant conditioned, 0 = unconditioned
%   Output:
%       meanV: structure extracted from mean velocity vc7 vector file
%       rmsV: structure extracted from RMS velocity vc7 vector file
%       
%       Details on the fields of the output structure can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

%% Assign variables to varargin cell values
% default values
vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';   % folder name for processed vc7 files

for i = 1:2:length(varargin)
    switch lower(varargin{i})
        case 'vc7folder'
            vc7Folder   = varargin{i+1};
        otherwise
            error('Invalid property name');
    end
end

%% Change to directory containing vc7 files
prevdir = cd;

if rc == 1
    expdate = [expdate, '\masked images'];
end

if phi == 0
    currdir = ['\\Vboxsvr\piv\LSB\nonreacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        int2str(angle), ' deg\', expdate, ...
        '\', vc7Folder, '\TimeMeanQF_Vector'];
else
    currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', ...
        expdate, '\', vc7Folder, '\TimeMeanQF_Vector'];
end
cd(currdir);

%% Extract mean and RMS vc7 files
meanV   = readimx('B00001_Avg V.VC7');
meanV.LabelX = 'x';
meanV.LabelY = 'y';
rmsV    = readimx('B00002_RMS V.VC7');
rmsV.LabelX = 'x';
rmsV.LabelY = 'y';
cd(prevdir);
end