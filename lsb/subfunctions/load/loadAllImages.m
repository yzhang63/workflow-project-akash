function I = loadAllImages(N, S, P, T, U, fuel, phi, angle, expdate, c, frame)
%LOADALLIMAGES loads all PIV images for a given set of conditions
%   LOADALLIMAGES(N, S, P, T, U, fuel, phi, angle, expdate, c) loads PIV 
%   images and binarizes them into 0 for reactants and 1 for products. The 
%   boundary between reactants and products is found through a jump in 
%   particle density.
%   Inputs:
%       N: number of images or 2-element vector range of images (i.e., N =
%           [F L], where F is the first image index and L is the last image
%           index
%       S: swirl number
%       P: pressure (atm)
%       T: reactant temperature (K)
%       U: mean flow velocity (m/s)
%       fuel: fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore this variable if phi = 0)
%       phi: equivalence ratio (use 0 for nonreacting)
%       angle: turbulence generator angle
%       expdate: string of the experiment date in form: 'yyyymmdd'
%       c: position of the center of the burner in mm found from inspection
%           of im7 image files.
%       frame: shot 1 or shot 2
%   Outputs:
%       I: 3D matrix of images

if length(N) == 1
    N = [1 N];
end
F = N(1);                               % index of first image
L = N(2);                               % index of last image
N = L - F + 1;                          % number of images to be analyzed

name            = ['B', sprintf('%0*d', 5, F), '.im7'];     % image filename (e.g. 'B00001.im7')
[~, X, Y, J]    = loadFullImage(S, P, T, U, fuel, phi, angle, expdate, name, c, frame);
I               = zeros(length(Y), length(X), N);
I(:, :, 1)      = J;

parfor i = 2:N
    name            = ['B', sprintf('%0*d', 5, i+F-1), '.im7'];
    [~, ~, ~, J]    = loadFullImage(S, P, T, U, fuel, phi, angle, expdate, name, c, frame);
    I(:, :, i)      = J;
end
end