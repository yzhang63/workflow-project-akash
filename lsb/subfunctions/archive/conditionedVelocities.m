function [X1r Y1r vx1rc vy1rc] = conditionedVelocities(S, P, T, U, fuel, phi, angle, expdate, c, BW, I, bs, ol, os)
%CONDITIONEDVELOCITIES Finds conditioned velocities using binarized PIV
%images
%   CONDITIONEDVELOCITIES(S, P, T, U, fuel, phi, angle, expdate, c, BW, bs, ol, os)
%   calculates reactant conditioned velocities from binarized PIV images by
%   creating a binarized matrix of reactants and products and multiplying
%   this by the vector field at the same conditions.
%   Inputs:
%       S: swirl number
%       P: pressure (atm)
%       T: reactant temperature (K)
%       U: mean flow velocity (m/s)
%       fuel: fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore this variable if phi = 0)
%       phi: equivalence ratio (use 0 for nonreacting)
%       angle: turbulence generator angle
%       expdate: string of the experiment date in form: 'yyyymmdd'
%       c: position of the center of the burner in mm found from inspection
%           of im7 image files.
%       BW: 3-D array of binarized images
%       I: locations where binarization failed
%       bs: binsize of the interrogation window in pixels (i.e. 32)
%       ol: window overlap (0 - 1) (ol = 0.5 corresponds to 50% overlap)
%       os: image offset found from IMAGEOFFSET
%   Outputs:
%       

N               = length(BW(1, 1, :));
J               = find(~I, 1);                              % find first binarization that didn't fail
V               = vc7extract(S, P, T, U, fuel, phi, angle, expdate, J);
[X1 Y1 vx1 vy1] = prepareIMX(V, c);
mn              = ceil(os/((1 - ol)*bs));                   % multipliers to determine offset of first interrogation window
ab              = (1 - ol)*bs*mn - os;                      % amount of offset for first interrogation window

[H W]           = size(BW(:, :, 1));
col1            = (1 + ab(1)):(1 - ol)*bs:(W - bs + 1);     % column start position vector
row1            = (1 + ab(2)):(1 - ol)*bs:(H - bs + 1);     % row start position vector
BWm             = binarizedMatrix(BW(:, :, J), bs, row1, col1);
op              = size(BWm);
X1r             = X1(mn(1)+1:mn(1)+op(2), mn(2)+1:mn(2)+op(1));
Y1r             = Y1(mn(1)+1:mn(1)+op(2), mn(2)+1:mn(2)+op(1));
vx1r            = vx1(mn(1)+1:mn(1)+op(2), mn(2)+1:mn(2)+op(1));
vy1r            = vy1(mn(1)+1:mn(1)+op(2), mn(2)+1:mn(2)+op(1));
vx1rc           = zeros(op(2), op(1), N);
vy1rc           = vx1rc;
BWm             = imcomplement(BWm);                        % reactants = 1, products = 0
BWm(BWm == 0)   = NaN;                                      % reactants = 1, products = NaN
vx1rc(:, :, J)  = BWm'.*vx1r;                               % reactant-conditioned vx1r
vy1rc(:, :, J)  = BWm'.*vy1r;                               % reactant-conditioned vy1r

ind             = 1:N;
ind(I)          = [];
ind(1)          = [];
for i = ind
    V               = vc7extract(S, P, T, U, fuel, phi, angle, expdate, i);
    [~,~, vx1, vy1] = prepareIMX(V, c);
%     figure;
%     quiverProfile(V, X1, Y1, vx1, vy1, U, d, 0, [yl(1) 1.5], 1.4*clim, fsize);
    BWm             = binarizedMatrix(BW(:, :, i), bs, row1, col1);
    vx1r            = vx1(mn(1)+1:mn(1)+op(2), mn(2)+1:mn(2)+op(1));
    vy1r            = vy1(mn(1)+1:mn(1)+op(2), mn(2)+1:mn(2)+op(1));
%     figure;
%     dispImageNoA(X1r(:, 1), Y1r(1, :), BWm, 16);
%     figure;
%     quiverProfile(V, X1r, Y1r, vx1r, vy1r, U, d, 0, [yl(1) 1.5], 1.4*clim, fsize);
    BWm             = imcomplement(BWm);    % reactants = 1, products = 0
    BWm(BWm == 0)   = NaN;                  % reactants = 1, products = NaN
    vx1rc(:, :, i)  = BWm'.*vx1r;           % reactant-conditioned vx1r
    vy1rc(:, :, i)  = BWm'.*vy1r;           % reactant-conditioned vy1r
%     figure;
%     quiverProfile(V, X1r, Y1r, vx1rc, vy1rc, U, d, 0, [yl(1) 1.5], 1.4*clim, fsize);
end
end