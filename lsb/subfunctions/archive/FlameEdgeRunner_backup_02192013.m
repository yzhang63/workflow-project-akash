% Script to track flame edge and generate flame brush from Mie scattering
% images
tic
%% Save current workspace to temp mat file and clear workspace
save temp.mat
clear

%% Input Parameters
setName = 'Cam_Date=121103_Time=112726';    % name of folder where dataset is located
inputParameters;            % script to define common input parameters
method  = 'graythresh';     % binarizing method: 'graythresh' or 'kmeans'
cbar    = 0.05;             % progress variable value
B       = 1000;             % number of images to run in each for loop to keep memory from filling up

%% Edge Parameters
InputEdgeParameters;        % script to define edge parameters

%% Figure adjustment parameters
xl      = [-1 1]*R*0.4;     % x limits (nondimensional: x/d)

%% Determine number of for loops and their indices
L       = ceil(N/B);        % determines number of for loops to run to keep memory from filling up
M       = 0:B:B*L;          % for loop indices
M(end)  = N;                % force final index to be N
numSets = length(M) - 1;    % number of image sets

%% Load images and find flame edge
for j = 1:numSets
    F   = M(j) + 1;                           % index of first image
    L   = M(j + 1);                           % index of last image
    Q   = L - F + 1;                          % number of images to be analyzed
    
    prevdir = cd;
    if rc == 1
        finalFolders = [expdate, '\Reactant Conditioned'];
    else
        finalFolders = expdate;
    end
    
    currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', finalFolders];
    cd(currdir);
    fnEnd       = [', shot ', int2str(frame), ', ', 'filt = ', int2str(filtSize(1)), ...
        ', ', method, ', images ', int2str(F), '-', int2str(L)];
    existData   = exist(['data, delta_f = ', num2str(delta_f, 2), fnEnd, '.mat'], 'file');
    switch existData
        case 2
            % Load data if binarization has already been completed
            load(['data, delta_f = ', num2str(delta_f, 2), fnEnd, '.mat']);
            InputEdgeParameters;
        otherwise
            % Load images, binarize and find flame edge
            name                = ['B', sprintf('%0*d', 5, M(j)+1), '.im7'];
            [A, X, Y, I, Im]    = loadImage(S, P, T, U, fuel, phi, angle, expdate, name, ...
                c, xl, yl, frame, 'corrected', corrected, 'filtSize', filtSize);
            [BWs, I1]           = binarizeImages(X, Y, Im, [F, L], S, P, T, U, fuel, phi, angle, ...
                expdate, c, xl, yl, frame, method, 'corrected', corrected, 'filtSize', filtSize);
            E                   = flameEdges(BWs, X, Y);
    end
    cd(prevdir);
    
    %% Generate average progress variable, <c>, contour plot
    BWmean      = mean(BWs(:, :, ~I1), 3);
    
    clear h
    h(1)        = figure;
    dispImage(A, X, Y, BWmean, fsize);
    colorbar;
    caxis(clim);
    set(gca, 'FontSize', fsize, 'FontName', fname);
    set(gcf, 'Name', 'Average Progress Variable');
    
    %% Generate 1-D plot of progress variable as a function of axial distance
    x1              = 0;                % radial position (nondimensional: x/d)
    
    h(end+1)        = figure; %#ok<*SAGROW>
    [cf, gf, Co]    = progressVariablePlot(A, X, Y, BWmean, x1, 'kx', fsize);
    set(gcf, 'Name', ['Average Progress Variable, x = ', num2str(x1)]);
    
    %% Find axial location of a given value of the average progress variable
    Cfun    = @(y) Co(1)*(erf(Co(2)*(y - Co(3)))) + Co(4) - cbar;
    y1      = fzero(Cfun, 0.6*d);
    
    %% Curvature Calculations
    prevdir = cd;
    if corrected
        finalFolders = [expdate, '\Correction'];
    else
        finalFolders = expdate;
    end
    
    currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', finalFolders];
    cd(currdir);
    [xDS, yDS, sDS, x, y, xp, yp, xpp, ypp, s, kappa, R] = ...
        Curvature(X, E, 'delta_f', delta_f, 'fittype', ftype);
    cd(prevdir);
    
    %% Calculate cumulative standard deviations of curvature
    kappaF                  = outliers(cell2mat(kappa));    % remove outliers
    kappaF(isnan(kappaF))   = [];                           % remove NaNs
    Skd                     = cumStd(cell2mat(kappa));      % cum. std of curv., DS, no OR
    SkdF                    = cumStd(kappaF);               % cum. std of curv., DS, OR
    
    %% Generate plot of cumulative standard deviations
    h(end+1)                = figure;
    set(gcf, 'color', 'w');
    set(gcf, 'Name', ['Cumulative Standard Deviations, ', ftype, ', delta_f = ', num2str(delta_f, 2)]);
    plot(1:length(Skd), Skd, 'r', 1:length(SkdF), SkdF, 'g', 'LineWidth', lwidth);
    set(gca, 'FontSize', fsize, 'FontName', fname);
    xlabel('{\itN}');
    ylabel('{\it\sigma} (1/mm)');
    legend([ftype, ', no OR'], [ftype, ', OR'], 'location', 'best');
    
    %% Histogram plot of curvature
    h(end+1)    = figure;
    histPlot(kappaF, 'fontSize', fsize, 'varname', 'kappa', 'units', '1/mm', 'xlabel', '{\kappa}', ...
        'plotType', 'stairs');
    set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);
    
    delta_p     = X(2) - X(1);                              % pixel size (mm)
    h(end+1)    = figure;
    histPlot(kappaF*delta_p, 'fontSize', fsize, 'varname', 'kappadeltap', ...
        'xlabel', '{\kappa\Delta}', 'plotType', 'stairs');
    set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);
    
    h(end+1)    = figure;
    histPlot(kappaF*delta_f0, 'fontSize', fsize, 'varname', 'kappadeltaf0', ...
        'xlabel', '{\kappa\delta_f_,_0}', 'plotType', 'stairs');
    set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);
    
    h(end+1)    = figure;
    histPlot(kappaF*delta_SLmax, 'fontSize', fsize, 'varname', 'kappadeltafSLmax', ...
        'xlabel', '{\kappa\delta_{f,S_{L,max}}}', 'plotType', 'stairs');
    set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);
    
    %% Calculate quantities at the leading points of the flame
    [xLPk, yLPk, sLP, kappaLP]  = LPcurvatures(x, y, s, kappa);
    [xLP, yLP, cLP, uLP, vLP]   = LPvelocities(S, P, T, U, fuel, phi, angle, expdate, c, ...
        [F, L], E, X, Y, BWmean, 'vc7Folder', vc7Folder, 'rc', rc);
    
    %% Calculate statistics at the leading points of the flame
    KAPPALP = nanmean(kappaLP);                 % mean curvature at the leading points
    XLP     = nanmean(xLP);                     % mean x position of leading points
    YLP     = nanmean(yLP);                     % mean y position of leading points
    CLP     = nanmean(cLP);                     % mean <c> value of leading points
    ULP     = nanmean(uLP);                     % mean x component velocity of leading points
    VLP     = nanmean(vLP);                     % mean y component velocity of leading points
    upLP    = nanstd(uLP);                      % rms x component velocity of leading points
    vpLP    = nanstd(vLP);                      % rms y component velocity of leading points
    
    %% Generate histograms of leading points properties
    % Curvatures
    h(end+1)    = figure;
    histPlot(kappaLP, 'fontSize', fsize, 'varname', 'kappaLP', 'units', '1/mm', ...
        'xlabel', '{\kappa_L_P}', 'plotType', 'stairs');
    set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);
    
    h(end+1)    = figure;
    histPlot(kappaLP*delta_p, 'fontSize', fsize, 'varname', 'kappaLPdeltap', ...
        'xlabel', '{\kappa_L_P\Delta}', 'plotType', 'stairs');
    set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);
    
    h(end+1)    = figure;
    histPlot(kappaLP*delta_f0, 'fontSize', fsize, 'varname', 'kappaLPdeltaf0', ...
        'xlabel', '{\kappa_L_P\Delta}', 'plotType', 'stairs');
    set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);
    
    h(end+1)    = figure;
    histPlot(kappaLP*delta_SLmax, 'fontSize', fsize, 'varname', 'kappaLPdeltafSLmax', ...
        'xlabel', '{\kappa_L_P\Delta}', 'plotType', 'stairs');
    set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);
    
    % Positions
    h(end+1)    = figure;
    histPlot(xLP, 'fontSize', fsize, 'varname', 'x_L_P', 'units', 'mm');
    
    h(end+1)    = figure;
    histPlot(yLP, 'fontSize', fsize, 'varname', 'y_L_P', 'units', 'mm');
    
    % Fluctuating velocities
    h(end+1)    = figure;
    f1_         = length(h);
    histPlot(uLP-ULP, 'fontSize', fsize, 'varname', 'u''_L_P', 'units', 'm/s', ...
        'plotType', 'stairs', 'lmStyle', 'r');
    
    h(end+1)    = figure;
    f2_         = length(h);
    histPlot(vLP-VLP, 'fontSize', fsize, 'varname', 'v''_L_P', 'units', 'm/s', ...
        'plotType', 'stairs', 'lmStyle', 'r');
    
    %% Reactant-Conditioned Statistics
    if rc
        %% Find velocities at the average position of the leading points
        [uLPave, vLPave]    = ...
            velocitiesGivenLoc(S, P, T, U, fuel, phi, angle, expdate, c, [F, L], XLP, YLP);
        ULPave              = mean(uLPave);
            % mean x component velocity at the average position of the leading points
        VLPave              = mean(vLPave);
            % mean y component velocity at the average position of the leading points
        
        %% Find velocities at given progress variable values and upstream of flame
        cbars   = 0.1:0.2:0.6;
        uc      = zeros(Q, length(cbars));
        vc      = uc;
        
        for i = 1:length(cbars)
            Cfuns   = @(y) Co(1)*(erf(Co(2)*(y - Co(3)))) + Co(4) - cbars(i);
            ys      = fzero(Cfuns, 0.6*d);
            [uc(:, i), vc(:, i)] = ...
                velocitiesGivenLoc(S, P, T, U, fuel, phi, angle, expdate, c, [F, L], 0, ys);
        end
        clear Cfuns ys
        ucp         = std(uc);          % rms x component velocity at given progress variable values
        vcp         = std(vc);          % rms y component velocity at given progress variable values
        [uUP, vUP]  = velocitiesGivenLoc(S, P, T, U, fuel, phi, angle, expdate, c, ...
            [F, L], 0, y1/2);           % velocity halfway between burner exit and <c> = 0.05 contour
        
        %% Generate histograms of leading points properties
        figure(h(f1_));
        hold on
        histPlot(uLPave-ULPave, 'fontSize', fsize, 'varname', 'u''', 'units', 'm/s', ...
            'plotType', 'stairs', 'lmstyle', 'b');
        hold on
        histPlot(uUP-mean(uUP), 'fontSize', fsize, 'varname', 'u''', 'units', 'm/s', ...
            'plotType', 'stairs', 'lmstyle', 'g');
        legend('{\itu''_L_P}', ['{\itu''}({\it<c>} = ', num2str(CLP, 2), ')'], '{\itu''_u_p}', ...
            'location', 'best');
        
        figure(h(f2_));
        hold on
        histPlot(vLPave-VLPave, 'fontSize', fsize, 'varname', 'v''', 'units', 'm/s', ...
            'plotType', 'stairs', 'lmstyle', 'b');
        hold on
        histPlot(vUP-mean(vUP), 'fontSize', fsize, 'varname', 'v''', 'units', 'm/s', ...
            'plotType', 'stairs', 'lmstyle', 'g');
        legend('{\itv''_L_P}', ['{\itv''}({\it<c>} = ', num2str(CLP, 2), ')'], '{\itv''_u_p}', ...
            'location', 'best');
        
        legEntry    = cell(size(cbars));
        h(end+1)    = figure;
        histPlot(uc-ones(length(uc(:, 1)), 1)*mean(uc, 1), 'fontSize', fsize, ...
            'varname', 'u''_c', 'xlabel', 'u''', 'units', 'm/s', ...
            'plotType', 'stairs', 'lineWidth', lwidth);
        for i = 1:length(legEntry)
            legEntry{i} = ['{\it<c>} = ', num2str(cbars(i))];
        end
        legend(legEntry, 'location', 'best');
        
        h(end+1)    = figure;
        histPlot(vc-ones(length(vc(:, 1)), 1)*mean(vc, 1), 'fontSize', fsize, ...
            'varname', 'v''_c', 'xlabel', 'v''', 'units', 'm/s', ...
            'plotType', 'stairs', 'lineWidth', lwidth);
        legend(legEntry, 'location', 'best');
        
        h(end+1)    = figure;
        histPlot(cLP, 'fontSize', fsize, 'varname', '<c>_L_P', 'xlim', [0, 1]);
        
        %% Plot turbulence intensity at different progress variable values
        h(end+1)    = figure;
        set(gcf, 'color', 'w');
        set(gcf, 'Name', 'turbulence intensity at different progress variables');
        plot(cbars, vcp/U, 'bo', cbars, ucp/U, 'go', 'LineWidth', lwidth);
        set(gca, 'FontSize', fsize, 'FontName', fname);
        xlabel('{\it<c>}');
        ylabel('{\itu} / {\itU}_0');
        legend('{\itu''_a_x}', '{\itu''_r_a_d}', 'location', 'best');
    end
    
    % %% Display some binarized images to visually make sure nothing strange is happening
    % for i = 1:floor(length(BWs(1, 1, :))/50):length(BWs(1, 1, :))
    %     figure;
    %     dispImageNoA(X, Y, BWs(:,:,i), 16, cmap);
    %     hold on
    %     plot(E{i}(:, 1)/d, E{i}(:, 2)/d, 'g', 'lineWidth', lwidth);
    %     if rc
    %         plot(xLP(i)/d, yLP(i)/d, 'gx', 'lineWidth', lwidth);
    %     end
    % end
    
    %% Save figures and data
    prevdir = cd;
    if rc == 1
        finalFolders = [expdate, '\Reactant Conditioned'];
    else
        finalFolders = expdate;
    end
    
    currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', finalFolders];
    cd(currdir);
    
    if ParamStudy == 1
        folderName = 'Parameter Study';
        if ~exist(folderName, 'dir');
            mkdir(folderName);
        end
        paramDir        = [currdir, '\', folderName];
        cd(paramDir);
    end
        
    fnEnd   = [', shot ', int2str(frame), ', ', 'filt = ', int2str(filtSize(1)), ...
        ', ', method, ', images ', int2str(F), '-', int2str(L)];
    for i = length(h):-1:1
        fn      = get(h(i), 'Name');
        I       = fn == '.';
        fn(I)   = '_';
        I       = fn == '<' | fn == '>';
        fn(I)   = [];
        fnFinal = [fn, fnEnd];
        saveas(h(i), fnFinal, 'fig');
        saveas(h(i), fnFinal, 'png');
        close(h(i));
    end
    
    save(['data, delta_f = ', num2str(delta_f, 2), fnEnd, '.mat']);
    cd(prevdir);
end

%% Average data over entire set of images
if ~ParamStudy
    FlameEdgeRunnerCombineAllSets;
end

%% Clear workspace, load temp.mat, and delete temp.mat
clear
load temp.mat
delete temp.mat
toc