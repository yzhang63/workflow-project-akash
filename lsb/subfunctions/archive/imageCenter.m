function I = imageCenter(A)
%IMAGECENTER finds the center of a symmetric image
%   IMAGECENTER(A) finds the center of a vertically symmetric image by 
%   computing the correlation between two sides by flipping one half and 
%   comparing the correlation between the two sides.
%   Inputs:
%       A: image matrix
%   Outputs:
%       I: index of the centerline

D   = size(A);

for i = 1:D(2)
    if i <= D(2)/2
        j1  = 1;
        j2  = 2*i;
    else
        j1  = 2*i - D(2) + 1;
        j2  = D(2);
    end
    C   = xcorr2(A(:, i:-1:j1), A(:, i+1:j2));
end
end