%% Input Parameters
setName     = 'Cam_Date=121031_Time=195417';     % name of folder where dataset is located
inputParameters;                % script to define common input parameters
method      = 'graythresh';     % binarizing method: 'graythresh' or 'kmeans'

%% Edge Parameters
InputEdgeParameters;            % script to define edge parameters

%% Load unconditioned data
currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', int2str(P), ' atm\', ...
    int2str(T), ' K\', int2str(U), ' mps\', fuel, '\phi = ', num2str(phi), '\', ...
    int2str(angle), ' deg\', expdate, '\Final Results'];
cd(currdir);
fnEnd   = [', shot ', int2str(frame), ', ', 'filt = ', int2str(filtSize(1)), ...
    ', ', method, ', images ', '1-', int2str(N)];
load(['data, delta_f = ', num2str(delta_f, 2), fnEnd, '.mat']);
cd(prevdir);

%% Additional Input Parameters
rc          = 1;                % reactant-conditioned

%% Load images and find flame edge
for j = 1:numSets
    F   = M(j) + 1;                           % index of first image
    L   = M(j + 1);                           % index of last image
    Q   = L - F + 1;                          % number of images to be analyzed
    currdir     = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', expdate];
    cd(currdir);
    fnEnd       = [', shot ', int2str(frame), ', filt = ', int2str(filtSize(1)), ...
        ', ', method, ', images ', int2str(F), '-', int2str(L)];
    load(['data, delta_f = ', num2str(delta_f, 2), fnEnd, '.mat'], 'E');
    cd(prevdir);
    
    %% Calculate quantities at the leading points of the flame
    [~, ~, ~, uLP, vLP]   = LPvelocities(S, P, T, U, fuel, phi, angle, expdate, c, ...
        [F, L], E, X, Y, BWmeanC, 'vc7Folder', mvc7Folder, 'rc', rc);
    
    %% Calculate statistics at the leading points of the flame
    ULP     = nanmean(uLP);                     % mean x component velocity of leading points
    VLP     = nanmean(vLP);                     % mean y component velocity of leading points
    upLP    = nanstd(uLP);                      % rms x component velocity of leading points
    vpLP    = nanstd(vLP);                      % rms y component velocity of leading points
    
    %% Generate histograms of leading points properties
    clear h
    h(1)        = figure;
    f1_         = length(h);
    histPlot(uLP-ULP, 'fontSize', fsize, 'varname', 'u''_L_P', 'units', 'm/s', ...
        'plotType', 'stairs', 'lmStyle', 'r');
    
    h(end+1)    = figure; %#ok<*SAGROW>
    f2_         = length(h);
    histPlot(vLP-VLP, 'fontSize', fsize, 'varname', 'v''_L_P', 'units', 'm/s', ...
        'plotType', 'stairs', 'lmStyle', 'r');
        
    %% Find velocities at the average position of the leading points
    [uLPave, vLPave]    = velocitiesGivenLoc(S, P, T, U, fuel, phi, angle, expdate, ...
        c, [F, L], XLPC, YLPC, 'vc7Folder', mvc7Folder);
    ULPave              = nanmean(uLPave);
    % mean x component velocity at the average position of the leading points
    VLPave              = nanmean(vLPave);
    % mean y component velocity at the average position of the leading points
    
    %% Find velocities at given progress variable values and upstream of flame
    cbars   = 0.1:0.2:0.6;
    uc      = zeros(Q, length(cbars));
    vc      = uc;
    
    for i = 1:length(cbars)
        Cfuns   = @(y) Co(1)*(erf(Co(2)*(y - Co(3)))) + Co(4) - cbars(i);
        ys      = fzero(Cfuns, 0.6*d);
        [uc(:, i), vc(:, i)] = ...
            velocitiesGivenLoc(S, P, T, U, fuel, phi, angle, expdate, c, [F, L], 0, ys);
    end
    clear Cfuns ys
    ucp         = nanstd(uc);       % rms x component velocity at given progress variable values
    vcp         = nanstd(vc);       % rms y component velocity at given progress variable values
    % find velocity halfway between burner exit and <c> = 0.05 contour
    [uUP, vUP]  = velocitiesGivenLoc(S, P, T, U, fuel, phi, angle, expdate, c, [F, L], 0, y1C/2);
    
    %% Generate histograms of leading points properties
    figure(h(f1_));
    hold on
    histPlot(uLPave-ULPave, 'fontSize', fsize, 'varname', 'u''', 'units', 'm/s', ...
        'plotType', 'stairs', 'lmstyle', 'b');
    hold on
    histPlot(uUP-nanmean(uUP), 'fontSize', fsize, 'varname', 'u''', 'units', 'm/s', ...
        'plotType', 'stairs', 'lmstyle', 'g');
    legend('{\itu''_L_P}', ['{\itu''}({\it<c>} = ', num2str(CLPC, 2), ')'], '{\itu''_u_p}', ...
        'location', 'best');
    
    figure(h(f2_));
    hold on
    histPlot(vLPave-VLPave, 'fontSize', fsize, 'varname', 'v''', 'units', 'm/s', ...
        'plotType', 'stairs', 'lmstyle', 'b');
    hold on
    histPlot(vUP-nanmean(vUP), 'fontSize', fsize, 'varname', 'v''', 'units', 'm/s', ...
        'plotType', 'stairs', 'lmstyle', 'g');
    legend('{\itv''_L_P}', ['{\itv''}({\it<c>} = ', num2str(CLPC, 2), ')'], '{\itv''_u_p}', ...
        'location', 'best');
    
    legEntry    = cell(size(cbars));
    h(end+1)    = figure;
    histPlot(uc-ones(length(uc(:, 1)), 1)*nanmean(uc, 1), 'fontSize', fsize, ...
        'varname', 'u''_c', 'xlabel', 'u''', 'units', 'm/s', ...
        'plotType', 'stairs', 'lineWidth', lwidth);
    for i = 1:length(legEntry)
        legEntry{i} = ['{\it<c>} = ', num2str(cbars(i))];
    end
    legend(legEntry, 'location', 'best');
    
    h(end+1)    = figure;
    histPlot(vc-ones(length(vc(:, 1)), 1)*nanmean(vc, 1), 'fontSize', fsize, ...
        'varname', 'v''_c', 'xlabel', 'v''', 'units', 'm/s', ...
        'plotType', 'stairs', 'lineWidth', lwidth);
    legend(legEntry, 'location', 'best');
    
    h(end+1)    = figure;
    histPlot(cLPC, 'fontSize', fsize, 'varname', '<c>_L_P', 'xlim', [0, 1]);
    
    %% Plot turbulence intensity at different progress variable values
    h(end+1)    = figure;
    set(gcf, 'color', 'w');
    set(gcf, 'Name', 'turbulence intensity at different progress variables');
    plot(cbars, vcp/U, 'bo', cbars, ucp/U, 'go', 'LineWidth', lwidth);
    set(gca, 'FontSize', fsize, 'FontName', fname);
    xlabel('{\it<c>}');
    ylabel('{\itu} / {\itU}_0');
    legend('{\itu''_a_x}', '{\itu''_r_a_d}', 'location', 'best');
    
    
    %% Save figures and data
    currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', expdate, '\masked images'];
    cd(currdir);
    
    for i = length(h):-1:1
        fn      = get(h(i), 'Name');
        I       = fn == '.';
        fn(I)   = '_';
        I       = fn == '<' | fn == '>';
        fn(I)   = [];
        fnFinal = [fn, fnEnd];
        saveas(h(i), fnFinal, 'fig');
        saveas(h(i), fnFinal, 'png');
        close(h(i));
    end
    
    save(['data, delta_f = ', num2str(delta_f, 2), fnEnd, '.mat'], '-v7.3');
    cd(prevdir);
end

%% Average data over entire set of images
% if ~ParamStudy
%     FlameEdgeRunnerCombineAllSets;
% end