% Script to track flame edge and generate flame brush from Mie scattering
% images

%% Input Parameters
inputParameters;            % script to define common input parameters
yl      = [0.3 1.2];        % y limits (nondimensional: y/d)
bs      = 32;               % interrogation window binsize in pixels
ol      = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
method  = 'graythresh';     % binarizing method: 'graythresh' or 'kmeans'
level   = 434;              % threshold level for binarizing images (currently does nothing)

%% Figure adjustment parameters
xl      = [-1 1]*R*0.5;     % x limits (nondimensional: x/d)
h       = zeros(1, 4);      % handle vector for figures

%% Load images and find flame edge
[A X Y I Im]    = loadImage(S, P, T, U, fuel, phi, angle, expdate, 'B00001.im7', c, xl, yl, frame);
[BWs I1]        = binarizeImages(X, Y, Im, N, level, S, P, T, U, fuel, phi, angle, expdate, c, xl, yl, frame, method);

%% Display some binarized images to visually make sure nothing strange is happening
for i = 1:floor(length(BWs(1, 1, :))/50):length(BWs(1, 1, :))
    figure;
    dispImageNoA(X, Y, BWs(:,:,i), 16);
end

%% Multiply binarized images by instantaneous vector fields to obtain velocities conditioned to the reactants
% os                      = imageOffset(A, c, X, Y);
% [X1r Y1r vx1rc vy1rc]   = conditionedVelocities(S, P, T, U, fuel, phi, angle, expdate, c, BWs, I1, bs, ol, os);
% vx1rcmean               = nanmean(vx1rc(:, :, ~I1), 3);     % reactant-conditioned mean velocity in the x-direction
% vy1rcmean               = nanmean(vy1rc(:, :, ~I1), 3);     % reactant-conditioned mean velocity in the y-direction
% 
% figure;
% quiverProfileNoA(X1r, Y1r, vx1rcmean, vy1rcmean, U, d, dim, [yl(1) 1.5], 1.4*clim, fsize);
% 
% vx1rcrms                = nanstd(vx1rc(:, :, ~I1), 0, 3);
% vy1rcrms                = nanstd(vy1rc(:, :, ~I1), 0, 3);
% 
% figure;
% contourProfileNoA(X1r, Y1r, vx1rcrms, U, d, dim, [yl(1) 1.5], 0.3*clim, fsize);
% if dim
%     cb  = ylabel(colorbar, '{\itu''_r_a_d}'); %#ok<UNRCH>
% else
%     cb  = ylabel(colorbar, '{\itu''_r_a_d} / {\itU}_0');
% end
% set(gca, 'FontSize', fsize, 'FontName', fname);
% set(cb, 'FontSize', fsize-2, 'FontName', fname);
% 
% figure;
% contourProfileNoA(X1r, Y1r, vy1rcrms, U, d, dim, [yl(1) 1.5], 0.3*clim, fsize);
% if dim
%     cb  = ylabel(colorbar, '{\itu''_a_x}'); %#ok<UNRCH>
% else
%     cb  = ylabel(colorbar, '{\itu''_a_x} / {\itU}_0');
% end
% set(gca, 'FontSize', fsize, 'FontName', fname);
% set(cb, 'FontSize', fsize-2, 'FontName', fname);
% 
% [clrcx clsrcx]          = confidenceLevel(vx1rcrms, N);
% [clrcy clsrcy]          = confidenceLevel(vx1rcrms, N);

%% Generate average progress variable, <c>, contour plot
BWmean  = mean(BWs(:, :, ~I1), 3);

h(1)    = figure;
dispImage(A, X, Y, BWmean, fsize);
colorbar;
caxis(clim);
set(gca, 'FontSize', fsize, 'FontName', fname);
set(gcf, 'Name', 'Average Progress Variable');

%% Generate 1-D plot of progress variable as a function of axial distance
x           = 0;                % radial position (nondimensional: x/d)

h(2)        = figure;
[cf gf Co]  = progressVariablePlot(A, X, Y, BWmean, x, 'kx', fsize);
set(gcf, 'Name', ['Average Progress Variable, x = ', num2str(x)]);

%% Find axial location of a given value of the average progress variable
cb      = 0.05;             % progress variable value

Cfun    = @(y) Co(1)*(erf(Co(2)*(y - Co(3)))) + Co(4) - cb;
y       = fzero(Cfun, 0.6*d);

%% Find ST,LD and u'axial and other flame parameters (reactant-conditioned)

% h (3)   = figure;
% K       = axialPlotNoA(X1r, Y1r, vy1rcmean, clrcy, U, d, x, 0, [0.2141 2], 1, 'b-', fsize);
% hold on
% [~, L]  = min(abs(Y1r(1, :) - y));
% plot(Y1r(1, L)/d, vy1rcmean(K, L)/U, 'rx', 'LineWidth', 2, 'MarkerSize', 10);
% STLD    = vy1rcmean(K, L);      % local displacement flame speed (m/s)
% uy      = vy1rcrms(K, L);       % axial turbulence intensity (m/s)
% ux      = vx1rcrms(K, L);       % radial turbulence intensity (m/s)
% yf      = Y1r(K, L);            % flame axial location (mm)
% axialPlotNoA(X1r, Y1r, vx1rcmean, clrcx, U, d, x, 0, [0.2141 2], 1, 'g-', fsize);
% set(gcf, 'Name', ['x = ' num2str(x) ', c = ', num2str(cb), ', VY, VX']);
% legend('{\itU_a_x}', '{\itS_T_,_L_D}', '{\itU_r_a_d}', 'location', 'best');
% 
% h(4)    = figure;
% axialPlotNoA(X1r, Y1r, vy1rcrms, clsrcy, U, d, x, 0, [0.2141 2], 0.5, 'b-', fsize);
% hold on
% axialPlotNoA(X1r, Y1r, vx1rcrms, clsrcx, U, d, x, 0, [0.2141 2], 0.5, 'g-', fsize);
% plot(Y1r(1, L)/d, vy1rcrms(K, L)/U, 'rx', 'LineWidth', 2, 'MarkerSize', 10);
% set(gcf, 'Name', ['x = ' num2str(x) ', c = ', num2str(cb), ', vyrms, vxrms']);
% legend('{\itu''_a_x}', '{\itu''_r_a_d}', 'location', 'best');

[meanV rmsV]    = meanRMSextract(S, P, T, U, fuel, phi, angle, expdate);
[Xv Yv VX VY]   = prepareProfile(meanV, c);
[~, ~, vx vy]   = prepareProfile(rmsV, c);
[clx clsx]      = confidenceLevel(vx, N);
[cly clsy]      = confidenceLevel(vy, N);

h (3)   = figure;
K       = axialPlot(meanV, Xv, Yv, VY, cly, U, d, x, 0, [0.2141 2], 1, 'b-', fsize);
hold on
[~, L]  = min(abs(Yv(1, :) - y));
plot(Yv(1, L)/d, VY(K, L)/U, 'rx', 'LineWidth', 2, 'MarkerSize', 10);
STLD    = VY(K, L);             % local displacement flame speed (m/s)
uy      = vy(K, L);             % axial turbulence intensity (m/s)
ux      = vx(K, L);             % radial turbulence intensity (m/s)
yf      = Yv(K, L);             % flame axial location (mm)
axialPlot(meanV, Xv, Yv, VX, clx, U, d, x, 0, [0.2141 2], 1, 'g-', fsize);
set(gcf, 'Name', [meanV.Source ' x = ' num2str(x) ', VY, VX']);
legend('{\itU_a_x}', '{\itS_T_,_L_D}', '{\itU_r_a_d}', 'location', 'best');

h(4)    = figure;
axialPlot(rmsV, Xv, Yv, vy, clsy, U, d, x, 0, [0.2141 2], 0.5, 'b-', fsize);
hold on
axialPlot(rmsV, Xv, Yv, vx, clsx, U, d, x, 0, [0.2141 2], 0.5, 'g-', fsize);
plot(Yv(1, L)/d, vy(K, L)/U, 'rx', 'LineWidth', 2, 'MarkerSize', 10);
set(gcf, 'Name', [rmsV.Source ' x = ' num2str(x) ', vy, vx']);
legend('{\itu''_a_x}', '{\itu''_r_a_d}', 'location', 'best');

%% Save figures and data
prevdir = cd;
currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
    int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
    fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', expdate];
cd(currdir);

for i = length(h):-1:1
    fn      = get(h(i), 'Name');
    I       = find(fn == '.');
    fn(I)   = '_';
    saveas(h(i), [fn, ', shot ', int2str(frame), ', ' method], 'fig');
    saveas(h(i), [fn, ', shot ', int2str(frame), ', ' method], 'png');
end

save(['data, shot ', int2str(frame), ', ' method, ', c = ', num2str(cb), '.mat']);
cd(prevdir);