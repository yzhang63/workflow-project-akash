N               = length(BWs(1, 1, :));
J               = find(~I1, 1);                             % find first binarization that didn't fail
V               = vc7extract(S, P, T, U, fuel, phi, angle, expdate, J);
[meanV rmsV]    = meanRMSextract(S, P, T, U, fuel, phi, angle, expdate);
[X1 Y1 vx1 vy1] = prepareIMX(V, c);
mn              = ceil(os/((1 - ol)*bs));                   % multipliers to determine offset of first interrogation window
ab              = (1 - ol)*bs*mn - os;                      % amount of offset for first interrogation window

[H W]           = size(BWs(:, :, 1));
col1            = (1 + ab(1)):(1 - ol)*bs:(W - bs + 1);     % column start position vector
row1            = (1 + ab(2)):(1 - ol)*bs:(H - bs + 1);     % row start position vector
BWm             = binarizedMatrix(BWs(:, :, J), bs, row1, col1);
op              = size(BWm);
X1r             = X1(mn(1)+1:mn(1)+op(2), mn(2)+1:mn(2)+op(1));
Y1r             = Y1(mn(1)+1:mn(1)+op(2), mn(2)+1:mn(2)+op(1));
vx1r            = vx1(mn(1)+1:mn(1)+op(2), mn(2)+1:mn(2)+op(1));
vy1r            = vy1(mn(1)+1:mn(1)+op(2), mn(2)+1:mn(2)+op(1));
vx1rc           = zeros(op(2), op(1), N);
vy1rc           = vx1rc;
BWm             = imcomplement(BWm);                        % reactants = 1, products = 0
BWm(BWm == 0)   = NaN;                                      % reactants = 1, products = NaN
vx1rc(:, :, J)  = BWm'.*vx1r;                               % reactant-conditioned vx1r
vy1rc(:, :, J)  = BWm'.*vy1r;                               % reactant-conditioned vy1r

ind             = 1:N;
ind(I1)         = [];
ind(1)          = [];
for i = 1000
    V               = vc7extract(S, P, T, U, fuel, phi, angle, expdate, i);
    [~,~, vx1, vy1] = prepareIMX(V, c);
    figure;
    quiverProfile(V, X1, Y1, vx1, vy1, U, d, 0, [yl(1) 1.5], 1.4*clim, fsize);
    figure;
    dispImageNoA(X, Y, BWs(:,:,i), 16);
    BWm             = binarizedMatrix(BWs(:, :, i), bs, row1, col1);
    vx1r            = vx1(mn(1)+1:mn(1)+op(2), mn(2)+1:mn(2)+op(1));
    vy1r            = vy1(mn(1)+1:mn(1)+op(2), mn(2)+1:mn(2)+op(1));
    figure;
    dispImageNoA(X1r(:, 1), Y1r(1, :), BWm, 16);
    figure;
    quiverProfile(V, X1r, Y1r, vx1r, vy1r, U, d, 0, [yl(1) 1.5], 1.4*clim, fsize);
    BWm             = imcomplement(BWm);    % reactants = 1, products = 0
    BWm(BWm == 0)   = NaN;                  % reactants = 1, products = NaN
    vx1rc(:, :, i)  = BWm'.*vx1r;           % reactant-conditioned vx1r
    vy1rc(:, :, i)  = BWm'.*vy1r;           % reactant-conditioned vy1r
    figure;
    quiverProfile(V, X1r, Y1r, vx1rc(:, :, i), vy1rc(:, :, i), U, d, 0, [yl(1) 1.5], 1.4*clim, fsize);
end