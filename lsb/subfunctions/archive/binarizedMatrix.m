function BWm = binarizedMatrix(BW, bs, row1, col1)
%BINARIZEDMATRIX Converts a binarized PIV image into a binarized matrix
%   BINARIZEDMATRIX(stuff) takes a binarized PIV image obtained from
%   BINARIZEIMAGES and creates a binarized matrix with the same dimensions
%   as the .vc7 vector files from the same PIV data. This matrix can be
%   used to generate conditioned velocities
%   Inputs:
%       BW: binarized PIV image
%       bs: bin size in pixels
%       row1: row start position vector
%       col1: column start position vector
%   Output:
%       BWm: matrix of binarized values for vc7 vector files
% Updated: 4/23/12 by Andrew Marshall

col2    = col1 + bs - 1;                            % column end position vector
row2    = row1 + bs - 1;                            % row end position vector
M       = length(row1);
N       = length(col1);
BWm     = zeros(M, N);

for i = 1:M
    BWr     = mean(BW(row1(i):row2(i), :));
    for j = 1:N
        BWs         = mean(BWr(col1(j):col2(j)));
        if BWs > 0.1            % only count as reactant velocity if >90% of pixels are zero
            BWm(i, j) = 1;
        else
            BWm(i, j) = 0;
        end
    end
end
end