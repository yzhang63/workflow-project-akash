% Script to track flame edge and generate flame brush from Mie scattering
% images

%% Input Parameters
inputParameters;            % script to define common input parameters
yl      = [0.3 1.2];        % y limits (nondimensional: y/d)
bs      = 32;               % interrogation window binsize in pixels
method  = 'graythresh';     % binarizing method: 'graythresh' or 'kmeans'

%% Figure adjustment parameters
xl      = [-1 1]*R*0.5;     % x limits (nondimensional: x/d)
h       = zeros(1, 2);      % handle vector for figures

%% Generate 1-D plot of progress variable as a function of axial distance
x           = 0;                % radial position (nondimensional: x/d)
% 
% h(2)        = figure;
% [cf gf Co]  = progressVariablePlot(A, X, Y, BWmean, x, 'kx', fsize);
% set(gcf, 'Name', ['Average Progress Variable, x = ', num2str(x)]);

%% Find axial location of a given value of the average progress variable
cb      = 0.05;             % progress variable value
% 
% Cfun    = @(y) Co(1)*(erf(Co(2)*(y - Co(3)))) + Co(4) - cb;
% y       = fzero(Cfun, 0.6*d);

%% Find ST,LD and u'axial and other flame parameters (reactant-conditioned)

[meanV, rmsV]       = meanRMSextract(S, P, T, U, fuel, phi, angle, expdate, 1);
[meanV1, rmsV1]     = meanRMSextract(S, P, T, U, fuel, phi, angle, expdate);
[Xv, Yv, VX, VY]    = prepareProfile(meanV, c);
[~, ~, vx, vy]      = prepareProfile(rmsV, c);
[clx, clsx]         = confidenceLevel(vx, N);
[cly, clsy]         = confidenceLevel(vy, N);

h(1)    = figure;
K       = axialPlot(meanV, Xv, Yv, VY, cly, U, d, x, 0, [0.2141 2], 1, 'b-', fsize);
hold on
% [~, L]  = min(abs(Yv(1, :) - y));
% plot(Yv(1, L)/d, VY(K, L)/U, 'rx', 'LineWidth', 2, 'MarkerSize', 10);
% STLD    = VY(K, L);             % local displacement flame speed (m/s)
% uy      = vy(K, L);             % axial turbulence intensity (m/s)
% ux      = vx(K, L);             % radial turbulence intensity (m/s)
% yf      = Yv(K, L);             % flame axial location (mm)
axialPlot(meanV, Xv, Yv, VX, clx, U, d, x, 0, [0.2141 2], 1, 'g-', fsize);
set(gcf, 'Name', [meanV.Source ' x = ' num2str(x) ', VY, VX']);
legend('{\itU_a_x}', '{\itU_r_a_d}', 'location', 'best');       % '{\itS_T_,_L_D}', 

h(2)    = figure;
axialPlot(rmsV, Xv, Yv, vy, clsy, U, d, x, 0, [0.2141 2], 0.5, 'b-', fsize);
hold on
axialPlot(rmsV, Xv, Yv, vx, clsx, U, d, x, 0, [0.2141 2], 0.5, 'g-', fsize);
% plot(Yv(1, L)/d, vy(K, L)/U, 'rx', 'LineWidth', 2, 'MarkerSize', 10);
set(gcf, 'Name', [rmsV.Source ' x = ' num2str(x) ', vy, vx']);
legend('{\itu''_a_x}', '{\itu''_r_a_d}', 'location', 'best');

%% Save figures and data
prevdir = cd;
currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
    int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
    fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', expdate, ...
    '\Reactant Conditioned'];
cd(currdir);

for i = length(h):-1:1
    fn      = get(h(i), 'Name');
    I       = find(fn == '.');
    fn(I)   = '_';
    saveas(h(i), [fn, ', shot ', int2str(frame), ', ' method], 'fig');
    saveas(h(i), [fn, ', shot ', int2str(frame), ', ' method], 'png');
end

save(['data, shot ', int2str(frame), ', ' method, ', c = ', num2str(cb), '.mat']);
cd(prevdir);