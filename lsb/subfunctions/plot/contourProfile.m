function [] = contourProfile(A, X, Y, u, U, d, dim, yl, clim, fsize)
%CONTOURPROFILE Generates a contour plot of profile data from PREPAREPROFILE
%   CONTOURPROFILE(A, X, Y, u, U, d, dim, yl, clim, fsize) takes the data 
%   structure generated from READIMX for .vc7 files and the outputs of 
%   PREPAREPROFILE and generates a contour plot.
%   Inputs:
%       A: structure generated from READIMX for .vc7 files
%       X: 2-D array of x-positions (units specified in A.UnitX)
%       Y: 2-D array of y-positions (units specified in A.UnitY)
%       u: 2-D array of velocities (units specified in A.UnitI)
%       U: mean flow velocity (m/s)
%       d: burner diameter (mm)
%       dim: 0 = nondimensional (normalized by d, U), 1 = dimensional
%       yl: limits to plot (nondimensional: y/d)
%       clim: color axis limits (nondimensional: u/U)
%       fsize: font size
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

fname   = 'Times New Roman';            % font name

set(gcf, 'color', 'w');
set(gcf, 'Name', A.Source);

if dim == 0
    X           = X/d;
    Y           = Y/d;
    u           = u/U;
else
    yl      = yl*d;
    clim    = clim*U;
end
% X       = interp2(X, 5);
% Y       = interp2(Y, 5);
% u       = interp2(u, 5);
[~, J]  = min(abs(Y(1, :) - yl(1)));

%% Create contour plot and adjust figure properties
contourf(X(:, 1:J), Y(:, 1:J), u(:, 1:J), 500, 'linestyle', 'none');

% pcolor(X(:, 1:J), Y(:, 1:J), u(:, 1:J));
% shading flat
colorbar;

set(gca, 'FontSize', fsize, 'FontName', fname);
axis image
if dim
    A.UnitX(1)      = '(';
    A.UnitX(end)    = ')';
    A.UnitY(1)      = '(';
    A.UnitY(end)    = ')';
    xlabel(['{\it' A.LabelX '} ' A.UnitX]);
    ylabel(['{\it' A.LabelY '} ' A.UnitY]);
    xlim([-d d]);
else
    xlabel(['{\it' A.LabelX '} / {\itd}']);
    ylabel(['{\it' A.LabelY '} / {\itd}']);
    xlim([-1 1]);
end
axis tight
ylim([0 yl(2)]);
caxis(clim);
% title ([A.LabelI ' ' A.UnitI]);

if A.ScaleY(1) >= 0.0
    axis ij;
else
    axis xy;
end
end