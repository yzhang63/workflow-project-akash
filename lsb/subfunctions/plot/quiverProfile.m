function [] = quiverProfile(A, X, Y, vx, vy, U, d, dim, yl, clim, fsize)
%QUIVERPROFILE Generates quiver plot of profile data from PREPAREPROFILE
%   QUIVERPROFILE(A, X, Y, vx, vy, U, d, dim) takes the data structure 
%   generated from READIMX for .vc7 files and the outputs of PREPAREPROFILE
%   and generates a quiver plot.
%   Inputs:
%       A: structure generated from READIMX for .vc7 files
%       X: 2-D array of x-positions (units specified in A.UnitX)
%       Y: 2-D array of y-positions (units specified in A.UnitY)
%       vx: 2-D array of x-component velocities (units specified in
%       A.UnitI)
%       vy: 2-D array of y-component velocities (units specified in
%       A.UnitI)
%       U: mean flow velocity (m/s)
%       d: burner diameter (mm)
%       dim: 0 = nondimensional (normalized by d, U), 1 = dimensional
%       yl: limits to plot (nondimensional: y/d)
%       clim: color axis limits (nondimensional: u/U)
%       fsize: font size
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

fname   = 'Times New Roman';            % font name
lwidth  = 2;                            % line width

if dim == 0
    X       = X/d;
    Y       = Y/d;
    vx      = vx/U;
    vy      = vy/U;
else
    yl      = yl*d;
    clim    = clim*U;
end

[~, J]  = min(abs(Y(1, :) - yl(1)));

% v = sqrt(vx.^2 + vy.^2);
% contourf(X(:, 1:J), Y(:, 1:J), v(:, 1:J), 500, 'linestyle', 'none');
% hold on
quiverc(X(:, 1:J), Y(:, 1:J), vx(:, 1:J), vy(:, 1:J), 3, 'k');
set(gcf, 'color', 'w');
set(gcf, 'Name', A.Source);

set(gca, 'FontSize', fsize, 'FontName', fname);
axis image
if dim
    A.UnitX(1)      = '(';
    A.UnitX(end)    = ')';
    A.UnitY(1)      = '(';
    A.UnitY(end)    = ')';
    A.UnitI         = ['(', A.UnitI, ')'];
    xlabel(['{\it' A.LabelX '} ' A.UnitX]);
    ylabel(['{\it' A.LabelY '} ' A.UnitY]);
    h = ylabel(colorbar, ['{\itu} ' A.UnitI]);
    xlim([-d d]);
else
    xlabel(['{\it' A.LabelX '} / {\itd}']);
    ylabel(['{\it' A.LabelY '} / {\itd}']);
    h = ylabel(colorbar, '{\itu} / {\itU}_0');
    xlim([-1 1]);
end
caxis(clim);
set(gca, 'FontSize', fsize, 'FontName', fname);
set(h, 'FontSize', fsize-2, 'FontName', fname);

if A.ScaleY(1) >= 0.0
    axis ij;
else
    axis xy;
end
axis tight
ylim([0 yl(2)]);
% hold off
end