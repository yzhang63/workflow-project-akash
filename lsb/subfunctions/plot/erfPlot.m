function [Coeff Cfit] = erfPlot(cf, Y, lStyle)
%ERFPLOT Creates a plot of the error function fit as a function of axial distance
%   ERFPLOT(cf, Y, U, d, dim, lineStyle) creates a plot of the linear fit
%   to the axial velocity as a function of axial distance. 
%   Inputs:
%       cf: fit object generated from ERFFIT
%       Y: 2-D array of y-positions (units specified in A.UnitY)
%       U: mean flow velocity (m/s)
%       d: burner diameter (mm)
%       dim: 0 = nondimensional (normalized by d, U), 1 = dimensional
%       lineStyle: string of line/marker style for plotting
%   Outputs:
%       ay: normalized mean axial strain rate (1/mm)
%       y0: virtual origin (mm)
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

d       = 36.3;                         % burner diameter (mm)
Coeff   = coeffvalues(cf);
Cfit    = @(y) Coeff(1)*(erf(Coeff(2)*(y - Coeff(3)))) + Coeff(4);

lwidth  = 2;                            % line width
plot(Y/d, Cfit(Y), lStyle, 'LineWidth', lwidth);
end