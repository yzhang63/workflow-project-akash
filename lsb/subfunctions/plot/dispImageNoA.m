function [] = dispImageNoA(X, Y, I, fsize, cmap)
%DISPIMAGENOA Displays an image
%   DISPIMAGE(X, Y, I) displays an image
%   Inputs:
%       X: vector of x-positions (units specified in A.UnitX)
%       Y: vector of y-positions (units specified in A.UnitY)
%       I: 2-D array of image intensities (units specified in A.UnitI)
%       fsize: font size
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

if nargin == 4
    cmap = colormap;
end

fname   = 'Times New Roman';            % font name
d       = 36.3;                         % burner diameter (mm)

imagesc(X/d, Y/d, I);
colormap(cmap);
% Set label, axis and figure properties
set(gcf, 'color', 'w');
set(gca, 'FontSize', fsize, 'FontName', fname);
xlabel('{\itx} / {\itd}');
ylabel('{\ity} / {\itd}');

axis xy;
axis equal
axis tight
end