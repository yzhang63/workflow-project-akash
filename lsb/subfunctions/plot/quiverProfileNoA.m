function [] = quiverProfileNoA(X, Y, vx, vy, U, d, dim, yl, clim, fsize)
%QUIVERPROFILE Generates quiver plot of profile data from PREPAREPROFILE
%   QUIVERPROFILE(X, Y, vx, vy, U, d, dim) takes the data structure 
%   generated from READIMX for .vc7 files and the outputs of PREPAREPROFILE
%   and generates a quiver plot.
%   Inputs:
%       X: 2-D array of x-positions (units specified in A.UnitX)
%       Y: 2-D array of y-positions (units specified in A.UnitY)
%       vx: 2-D array of x-component velocities (units specified in
%       A.UnitI)
%       vy: 2-D array of y-component velocities (units specified in
%       A.UnitI)
%       U: mean flow velocity (m/s)
%       d: burner diameter (mm)
%       dim: 0 = nondimensional (normalized by d, U), 1 = dimensional
%       yl: limits to plot (nondimensional: y/d)
%       clim: color axis limits (nondimensional: u/U)
%       fsize: font size
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

fname   = 'Times New Roman';            % font name
lwidth  = 1;                            % line width

if dim == 0
    X       = X/d;
    Y       = Y/d;
    vx      = vx/U;
    vy      = vy/U;
else
    yl      = yl*d;
    clim    = clim*U;
end

[~, J]  = min(abs(Y(1, :) - yl(1)));

v = sqrt(vx.^2 + vy.^2);
contourf(X(:, 1:J), Y(:, 1:J), v(:, 1:J), 'linestyle', 'none');
set(gcf, 'color', 'w');
set(gcf, 'Name', ['quiver profile, U = ', num2str(U), ' m/s']);
hold on
quiver(X(:, 1:J), Y(:, 1:J), vx(:, 1:J), vy(:, 1:J), 'LineWidth', lwidth);

set(gca, 'FontSize', fsize, 'FontName', fname);
axis image
if dim
    xlabel('{\itx} (mm)');
    ylabel('{\ity} (mm)');
    h = ylabel(colorbar, '({\itU_a_x}^2 + {\itU_r_a_d}^2)^1^/^2');
    xlim([-d d]);
else
    xlabel('{\itx} / {\itd}');
    ylabel('{\ity} / {\itd}');
    h = ylabel(colorbar, '({\itU_a_x}^2 + {\itU_r_a_d}^2)^1^/^2 / {\itU}_0');
    xlim([-1 1]);
end
ylim([0 yl(2)]);
caxis(clim);
set(gca, 'FontSize', fsize, 'FontName', fname);
set(h, 'FontSize', fsize-2, 'FontName', fname);

axis xy;
hold off
end