function [] = plotSTLD(STLD, uy, SL, s, lwidth, msize, fsize, varargin)
%PLOTSTLD plots STLD as a function of u'_axial, both normalized by SL
%   PLOTSTLD(STLD, uy, SL, s, lwidth, msize, fsize) plots STLD/SL as a
%   function of uy/SL.
%   Inputs:
%       STLD: turbulent local displacement flame speed
%       uy: turbulence intensity in the axial direction
%       SL: laminar flame speed
%       s: character string designating line type, plot symbol, and color
%       lwidth: line width
%       msize: marker size
%       fsize: font size

%% Assign variables to varargin cell values
% default values
normType    = 'SL0';    % parameter for normalizing flame speed data ('SL0' or 'SLmax')

for i = 1:2:length(varargin)
    switch lower(varargin{i})
        case 'normtype'
            normType = varargin{i+1};
        otherwise
            error('Invalid property name');
    end
end

%% Plot ST,LD as a function of turbulence intensity, normalized by the laminar flame speed

fname   = 'Times New Roman';            % font name
set(gcf, 'color', 'w');

plot(uy./SL, STLD./SL, s, 'LineWidth', lwidth, 'MarkerSize', msize);
set(gca, 'FontSize', fsize, 'FontName', fname);
switch normType
    case 'SL0'
        xlabel('{\itu''_a_x / S_L_,}_0');
        ylabel('{\itS_T_,_L_D / S_L_,}_0');
    case 'SLmax'
        xlabel('{\itu''_a_x / S_{L,max}}');
        ylabel('{\itS_T_,_L_D / S_{L,max}}');
    otherwise
        error('Invalid string for variable normType');
end
end