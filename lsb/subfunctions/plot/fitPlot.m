function [Ufit, ay, y0] = fitPlot(fo, Y, U, d, dim, lineStyle)
%FITPLOT Creates a plot of the linear fit as a function of axial distance
%   FITPLOT(fo, Y, U, d, dim, lineStyle) creates a plot of the linear fit
%   to the axial velocity as a function of axial distance. 
%   Inputs:
%       fo: fit object generated from AXIALFIT
%       Y: 2-D array of y-positions (units specified in A.UnitY)
%       U: mean flow velocity (m/s)
%       d: burner diameter (mm)
%       dim: 0 = nondimensional (normalized by d, U), 1 = dimensional
%       lineStyle: string of line/marker style for plotting
%   Outputs:
%       ay: normalized mean axial strain rate (1/mm)
%       y0: virtual origin (mm)
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

C       = coeffvalues(fo);
ay      = 1/U*C(1);
y0      = (1 - C(2)/U)/ay;

Ufit    = @(y) U*(1 + ay*(y - y0));

lwidth  = 2;                            % line width
if dim == 0
    plot(Y(1, :)/d, Ufit(Y(1, :))/U, lineStyle, 'LineWidth', lwidth);
else
    plot(Y(1, :), Ufit(Y(1, :)), lineStyle, 'LineWidth', lwidth);
end
end