function A = showImage(S, P, T, U, fuel, phi, angle, expdate, name, frame)
%SHOWIMAGE show a specific image from a specific PIV experiment
%   SHOWIMAGE(S, P, T, U, angle, expdate, name) shows a specific image from
%   a specific PIV experiment.
%   Inputs:
%       S: swirl number
%       P: pressure (atm)
%       T: reactant temperature (K)
%       U: mean flow velocity (m/s)
%       fuel: fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore this variable if phi = 0)
%       phi: equivalence ratio (use 0 for nonreacting)
%       angle: turbulence generator angle
%       expdate: string of the experiment date in form: 'yyyymmdd'
%       name: string of the name of the image to be displayed (e.g.
%       'B00001.im7')
%       frame: shot 1 or shot 2

prevdir = cd;
if phi == 0
    currdir = ['\\Vboxsvr\piv\LSB\nonreacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        int2str(angle), ' deg\', expdate];
else
    currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', ...
        expdate];
end
cd(currdir);
A               = readimx(name);
A.LabelX        = 'x';
A.LabelY        = 'y';
A.UnitX(1)      = '(';
A.UnitX(end)    = ')';
A.UnitY(1)      = '(';
A.UnitY(end)    = ')';
showimx(A, frame);
cd(prevdir);
end