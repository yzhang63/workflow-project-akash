function I = radPlot(X, Y, Z, y, varargin)
%RADPLOT Generates a plot of Z as a function of radial distance
%   RADPLOT(X, Y, Z, y, 'PropertyName', PropertyValue, ...) plots z as a
%   function of radial distance X at a fixed axial location y
%   Inputs:
%       X: 2-D array of x-positions (units specified in A.UnitX)
%       Y: 2-D array of y-positions (units specified in A.UnitY)
%       Z: 2-D array of z-values (units specified in A.UnitI)
%       y: axial position of radial cut (nondimensional: y/d)

%% Assign variables to varargin cell values
zs      = 'z';                              % character string of name of Z
fsize   = 16;                               % font size
lwidth  = 2;                                % line width
lmstyle = 'b';                              % line and marker style
dim     = 0;                                % 0: dimensionless, 1: dimensional
d       = 36.3;                             % burner diameter (mm)
Z0      = max(max(Z));                      % normalizing parameter
xl      = [min(min(X)), max(max(X))]/d;     % nondimensional plot limits for x (x/d)
zl      = [min(min(Z)), max(max(Z))]/Z0;    % nondimensional plot limits for z (z/Z0)
xunit   = 'mm';                             % default x unit
zunit   = 'm/s';                            % default z unit
zlab    = zs;                               % z-axis label

for i = 1:2:length(varargin)
    switch lower(varargin{i})
        case 'varname'
            zs      = varargin{i+1};
        case 'fontsize'
            fsize   = varargin{i+1};
        case 'linewidth'
            lwidth  = varargin{i+1};
        case 'lmstyle'
            lmstyle = varargin{i+1};
        case 'dim'
            dim     = varargin{i+1};
        case 'burnerdiameter'
            d       = varargin{i+1};
        case 'normparam'
            Z0      = varargin{i+1};
        case 'xlim'
            xl      = varargin{i+1};
        case 'zlim'
            zl      = varargin{i+1};
        case 'xunit'
            xunit   = varargin{i+1};
        case 'zunit'
            zunit   = varargin{i+1};
        case 'zlabel'
            zlab    = varargin{i+1};
        case 'confidencelevel'
            cl      = varargin{i+1};
        otherwise
            error('Invalid property name');
    end
end

%% Plot Parameters
fname   = 'Times New Roman';                    % font name
set(gcf, 'color', 'w');
set(gcf, 'Name', ['radial plot of ', zs, ' at y = ', num2str(y)]);

%% Calculate plot parameters
if dim == 0
    X       = X/d;
    Y       = Y/d;
    Z       = Z/Z0;
    if exist('cl', 'var')
        cl  = cl/Z0;
    end
else
    y       = y*d;
    xl      = xl*d;
    zl      = zl*Z0;
end

[~, I]  = min(abs(Y(1, :) - y));

%% Plot
if exist('cl', 'var')
    errorbar(X(:, I), Z(:, I), cl(:, I), lmstyle, 'LineWidth', lwidth);
else
    plot(X(:, I), Z(:, I), cl(:, I), lmstyle, 'LineWidth', lwidth);
end
set(gca, 'FontSize', fsize, 'FontName', fname);

if dim
    xlabel(['{\itx} (' xunit, ')']);
    ylabel(['{\it', zs, '} (', zunit, ')']);
else
    xlabel('{\itx} / {\itd}');
    ylabel(zlab);
end
xlim(xl);
ylim(zl);
end