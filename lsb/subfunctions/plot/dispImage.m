function [] = dispImage(A, X, Y, I, fsize, cmap)
%DISPIMAGE Displays an image
%   DISPIMAGE(A, X, Y, I) displays an image
%   Inputs:
%       A: structure generated from READIMX for .im7 files
%       X: vector of x-positions (units specified in A.UnitX)
%       Y: vector of y-positions (units specified in A.UnitY)
%       I: 2-D array of image intensities (units specified in A.UnitI)
%       fsize: font size
%       cmap: colormap string, optional input (e.g. 'gray')
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

if nargin == 5
    cmap = colormap;
end

fname   = 'Times New Roman';            % font name
d       = 36.3;                         % burner diameter (mm)

imagesc(X/d, Y/d, I);
colormap(cmap);
% Set label, axis and figure properties
set(gcf, 'Name', A.Source);
set(gcf, 'color', 'w');
set(gca, 'FontSize', fsize, 'FontName', fname);
xlabel(['{\it' A.LabelX '} / {\itd}']);
ylabel(['{\it' A.LabelY '} / {\itd}']);

if A.ScaleY(1) >= 0
    axis ij;
else
    axis xy;
end
axis equal
axis tight
end