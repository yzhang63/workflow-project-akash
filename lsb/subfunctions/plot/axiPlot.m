function I = axiPlot(X, Y, Z, x, varargin)
%AXIPLOT Generates a plot of Z as a function of axial distance
%   AXIPLOT(X, Y, Z, x, 'PropertyName', PropertyValue, ...) plots z as a
%   function of axial distance Y at a fixed radial location x
%   Inputs:
%       X: 2-D array of x-positions (units specified in A.UnitX)
%       Y: 2-D array of y-positions (units specified in A.UnitY)
%       Z: 2-D array of z-values (units specified in A.UnitI)
%       x: radial position of axial cut (nondimensional: x/d)

%% Assign variables to varargin cell values
zs      = 'z';                              % character string of name of Z
fsize   = 16;                               % font size
lwidth  = 2;                                % line width
lmstyle = 'b';                              % line and marker style
dim     = 0;                                % 0: dimensionless, 1: dimensional
d       = 36.3;                             % burner diameter (mm)
Z0      = max(max(Z));                      % normalizing parameter
yl      = [min(min(Y)), max(max(Y))]/d;     % nondimensional plot limits for y (y/d)
zl      = [min(min(Z)), max(max(Z))]/Z0;    % nondimensional plot limits for z (z/Z0)
yunit   = 'mm';                             % default y unit
zunit   = 'm/s';                            % default z unit
zlab    = zs;                               % z-axis label

for i = 1:2:length(varargin)
    switch lower(varargin{i})
        case 'varname'
            zs      = varargin{i+1};
        case 'fontsize'
            fsize   = varargin{i+1};
        case 'linewidth'
            lwidth  = varargin{i+1};
        case 'lmstyle'
            lmstyle = varargin{i+1};
        case 'dim'
            dim     = varargin{i+1};
        case 'burnerdiameter'
            d       = varargin{i+1};
        case 'normparam'
            Z0      = varargin{i+1};
        case 'ylim'
            yl      = varargin{i+1};
        case 'zlim'
            zl      = varargin{i+1};
        case 'yunit'
            yunit   = varargin{i+1};
        case 'zunit'
            zunit   = varargin{i+1};
        case 'zlabel'
            zlab    = varargin{i+1};
        case 'confidencelevel'
            cl      = varargin{i+1};
        otherwise
            error('Invalid property name');
    end
end

%% Plot Parameters
fname   = 'Times New Roman';                    % font name
set(gcf, 'color', 'w');
set(gcf, 'Name', ['axial plot of ', zs, ' at x = ', num2str(x)]);

%% Calculate plot parameters
if dim == 0
    X       = X/d;
    Y       = Y/d;
    Z       = Z/Z0;
    if exist('cl', 'var')
        cl  = cl/Z0;
    end
else
    x       = x*d;
    yl      = yl*d;
    zl      = zl*Z0;
end

[~, I]  = min(abs(X(:, 1) - x));
[~, J]  = min(abs(Y(1, :) - yl(1)));

%% Plot
if exist('cl', 'var')
    errorbar(Y(I, 1:J), Z(I, 1:J), cl(I, 1:J), lmstyle, 'LineWidth', lwidth);
else
    plot(Y(I, 1:J), Z(I, 1:J), lmstyle, 'LineWidth', lwidth);
end
set(gca, 'FontSize', fsize, 'FontName', fname);

if dim
    xlabel(['{\ity} (' yunit, ')']);
    ylabel(['{\it', zs, '} (', zunit, ')']);
else
    xlabel('{\ity} / {\itd}');
    ylabel(zlab);
end
xlim([0 yl(2)]);
ylim(zl);
end