function [cf, gf, Co] = progressVariablePlot(A, X, Y, c, x, lineStyle, fsize)
%PROGRESSVARIABLEPLOT Generates a 1-D plot of progress variable as a function of axial distance
%   PROGRESSVARIABLEPLOT(A, X, Y, u, U, d, x, dim, ymax, umax, lineStyle) 
%   generates a plot of c as a function of axial distance at a specified 
%   radial position over a specified range.
%   Inputs:
%       A: structure generated from READIMX for .vc7 files
%       X: vector of x-positions (units specified in A.UnitX)
%       Y: vector of y-positions (units specified in A.UnitY)
%       c: 2-D array of the average progress variable
%       x: radial position of axial cut (nondimensional: x/d)
%       lineStyle: string of line/marker style for plotting
%       fsize: font size
%   Outputs:
%       cf: curve fit of the error function to the average progress
%       variable
%       gf: goodness-of-fit
%       Co: coefficients of the fit
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

d       = 36.3;                         % burner diameter (mm)
[~, I]  = min(abs(X - x*d));
C       = c(:, I)';

fname   = 'Times New Roman';            % font name
lwidth  = 2;                            % line width
msize   = 8;                            % marker size

set(gcf, 'color', 'w');
set(gcf, 'Name', [A.Source ' x = ' num2str(x)]);

plot(Y/d, C, lineStyle, 'LineWidth', lwidth, 'MarkerSize', msize);
set(gca, 'FontSize', fsize, 'FontName', fname);

xlabel(['{\it' A.LabelY '} / {\itd}']);
ylabel('{<\itc}>');
ylim([0 1]);
[cf, gf] = erfFit(Y', C');
hold on
[Co, ~] = erfPlot(cf, Y, 'r-');
hold off
end