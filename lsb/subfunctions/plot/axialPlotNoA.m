function I = axialPlotNoA(X, Y, u, cl, U, d, x, dim, yl, umax, lineStyle, fsize)
%AXIALPLOT Generates a plot of u as a function of axial distance
%   AXIALPLOT(X, Y, u, U, d, x, dim, ymax, umax, lineStyle) generates a
%   plot of u as a function of axial distance at a specified radial
%   position over a specified range.
%   Inputs:
%       X: 2-D array of x-positions (units specified in A.UnitX)
%       Y: 2-D array of y-positions (units specified in A.UnitY)
%       u: 2-D array of velocities (units specified in A.UnitI)
%       cl: 2-D array of confidence levels (found from CONFIDENCELEVEL)
%       U: mean flow velocity (m/s)
%       d: burner diameter (mm)
%       x: radial position of axial cut (nondimensional: x/d)
%       dim: 0 = nondimensional (normalized by d, U), 1 = dimensional
%       yl: plot limits for y (nondimensional: y/d)
%       umax: max velocity to plot in 1-D plots (nondimensional: u/U)
%       lineStyle: string of line/marker style for plotting
%       fsize: font size
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

if dim == 0
    X       = X/d;
    Y       = Y/d;
    u       = u/U;
    cl      = cl/U;
else
    x       = x*d;
    yl      = yl*d;
    umax    = umax*U;
end

[~, I]  = min(abs(X(:, 1) - x));
[~, J]  = min(abs(Y(1, :) - yl(1)));

fname   = 'Times New Roman';            % font name
lwidth  = 2;                            % line width

set(gcf, 'color', 'w');
set(gcf, 'Name', [' x = ' num2str(x)]);

errorbar(Y(I, 1:J), u(I, 1:J), cl(I, 1:J), lineStyle, 'LineWidth', lwidth);

set(gca, 'FontSize', fsize, 'FontName', fname);
if dim
    xlabel('{\ity} (mm)');
    ylabel('{\itu} (m/s)');
else
    xlabel(['{\ity} / {\itd}']);
    ylabel('{\itu} / {\itU}_0');
end
xlim([0 yl(2)]);
ylim([0 umax]);
end