function varargout = hist3Plot(z1, z2, varargin)
%HIST3PLOT Plots a normalized bivariate histogram
%   HIST3PLOT(z1, z2, 'PropertyName', PropertyValue, ...) plots a 
%   normalized (total area = 1) bivariate histogram.
%   Inputs:
%       z1: column vector of data for variable 1
%       z2: column vector of data for variable 2
%       vs: variable name as a string (PropertyName: 'varname')
%       fsize: font size (PropertyName: 'fontSize')
%       ptype: plot type (PropertyName: 'plotType') (values: 'bar', 'line')
%       lmstyle: line type, plot symbol, and/or color designator
%       (PropertyName: 'lmStyle') (see "help plot" for designators)
%       lwidth: line width (PropertyName: 'lineWidth')
%       units: units of z as a string (PropertyName: 'units')
%   Outputs:
%       n: normalized frequency counts in each bin
%       xo: bin locations

%% Assign variables to varargin cell values
vs1     = 'z1';
vs2     = 'z2';
fsize   = 16;
ptype   = 'contourf';
cmap    = colormap;
x1l     = [nanmean(z1)-2*nanstd(z1), nanmean(z1)+2*nanstd(z1)];         % +/- 2 standard deviations
x2l     = [nanmean(z2)-2*nanstd(z2), nanmean(z2)+2*nanstd(z2)];         % +/- 2 standard deviations
smth    = 'none'; % 'none' = no smoothing, 'csaps' = cubic smoothing splines, 'medfilt' = median filter

for i = 1:2:length(varargin)
    switch lower(varargin{i})
        case 'var1name'
            vs1     = varargin{i+1};
        case 'var2name'
            vs2     = varargin{i+1};
        case 'fontsize'
            fsize   = varargin{i+1};
        case 'units1'
            units1  = varargin{i+1};
        case 'units2'
            units2  = varargin{i+1};
        case 'plottype'
            ptype   = varargin{i+1};
        case 'colormap'
            cmap    = varargin{i+1};
        case 'x1label'
            x1lab   = varargin{i+1};
        case 'x2label'
            x2lab   = varargin{i+1};
        case 'x1lim'
            x1l     = varargin{i+1};
        case 'x2lim'
            x2l     = varargin{i+1};
        case 'smooth'
            smth    = varargin{i+1};
        otherwise
            error('Invalid property name');
    end
end

%% Plot Parameters
fname   = 'Times New Roman';                        % font name
set(gcf, 'color', 'w');
set(gcf, 'Name', ['JPDF of ', vs1, ' and ', vs2]);

%% Calculate plot parameters
[n, xo] = hist3n(z1, z2);                           % normalized histogram data

if nargout ~= 0
    varargout{1} = n;
    varargout{2} = xo;
end

I1          = xo{1} < x1l(1) | xo{1} > x1l(2);          % bins outside of plot limits
I2          = xo{2} < x2l(1) | xo{2} > x2l(2);
xo{1}(I1)   = [];
xo{2}(I2)   = [];
n(I1, :)    = [];
n(:, I2)    = [];
switch smth
    case 'none'
        
    case 'csaps'
        n       = fnval(csaps(xo, n), xo);
    case 'medfilt'
        n       = medfilt2(n, [7 7]);
    otherwise
        error([smth, ' is not a valid smoothing option']);
end

%% Plot
hold all
switch ptype
    case 'contour'
        contour(xo{1}, xo{2}, n', 'LineWidth', 2);
    case 'contourf'
        contourf(xo{1}, xo{2}, n');
    case 'mesh'
        meshc(xo{1}, xo{2}, n');
    otherwise
        error([ptype, ' is not a valid plotting option']);
end
colormap(cmap);
set(gca, 'FontSize', fsize, 'FontName', fname);

if exist('x1lab', 'var')
    vs1 = x1lab;
end
if exist('x2lab', 'var')
    vs2 = x2lab;
end

if exist('units1', 'var')
    xlabel(['{\it', vs1, '} (', units1, ')']);
else
    xlabel(['{\it', vs1, '}']);
end
if exist('units2', 'var')
    ylabel(['{\it', vs2, '} (', units2, ')']);
else
    ylabel(['{\it', vs2, '}']);
end

% ylabel('{\itPDF}');
xlim(x1l);
ylim(x2l);
hold off
end