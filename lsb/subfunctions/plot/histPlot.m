function varargout = histPlot(z, varargin)
%HISTPLOT Plots a normalized histogram
%   HISTPLOT(z, 'PropertyName', PropertyValue, ...) plots a normalized 
%   histogram normalized by the number of data points.
%   Inputs:
%       z: vector of data
%       vs: variable name as a string (PropertyName: 'varname')
%       fsize: font size (PropertyName: 'fontSize')
%       ptype: plot type (PropertyName: 'plotType') (values: 'bar', 'line')
%       lmstyle: line type, plot symbol, and/or color designator
%       (PropertyName: 'lmStyle') (see "help plot" for designators)
%       lwidth: line width (PropertyName: 'lineWidth')
%       units: units of z as a string (PropertyName: 'units')
%   Outputs:
%       n: normalized frequency counts in each bin (normalized by the total
%           number of counts multiplied by the bin width)
%       xo: bin locations

%% Assign variables to varargin cell values
vs      = 'z';
fsize   = 16;
ptype   = 'bar';
lwidth  = 2;
lmstyle = 'b';
col     = 'b';
xl      = [max(nanmean(z))-4*max(nanstd(z)), max(nanmean(z))+4*max(nanstd(z))];         % Plot over +/- 4 standard deviations

for i = 1:2:length(varargin)
    switch lower(varargin{i})
        case 'varname'
            vs      = varargin{i+1};
        case 'fontsize'
            fsize   = varargin{i+1};
        case 'units'
            units   = varargin{i+1};
        case 'plottype'
            ptype   = varargin{i+1};
        case 'linewidth'
            lwidth  = varargin{i+1};
        case 'lmstyle'
            lmstyle = varargin{i+1};
            col     = varargin{i+1}(1);
        case 'color'
            col     = varargin{i+1};
        case 'xlabel'
            xlab    = varargin{i+1};
        case 'xlim'
            xl      = varargin{i+1};
        otherwise
            error('Invalid property name');
    end
end

%% Plot Parameters
fname   = 'Times New Roman';                    % font name
set(gcf, 'color', 'w');
set(gcf, 'Name', ['PDF of ', vs]);

%% Calculate plot parameters
[n, xo] = histn(z);                             % normalized histogram data
if iscell(n)
    XN  = [xo; n];
end

if nargout ~= 0
    varargout{1} = n;
    varargout{2} = xo;
end

%% Plot
hold all
switch ptype
    case 'area'
        if iscell(n)
            for i = 1:length(n)
                area(xo{i}, n{i});
            end
        else
            area(xo, n, lmstyle, 'color', col);
        end
    case 'bar'
        if iscell(n)
            for i = 1:length(n)
                bar(xo{i}, n{i});
            end
        else
            bar(xo, n, lmstyle, 'FaceColor', col);
        end
    case 'line'
        if iscell(n)
            plot(XN{:}, 'LineWidth', lwidth);
        else
            plot(xo, n, lmstyle, 'LineWidth', lwidth, 'color', col);
        end
    case 'stairs'
        if iscell(n)
            for i = 1:length(n)
                stairs(xo{i}, n{i}, 'LineWidth', lwidth);
            end
        else
            stairs(xo, n, lmstyle, 'LineWidth', lwidth, 'color', col);
        end
    case 'stem'
        if iscell(n)
            for i = 1:length(n)
                stem(xo{i}, n{i}, 'LineWidth', lwidth);
            end
        else
            stem(xo, n, lmstyle, 'LineWidth', lwidth, 'color', col);
        end
end

set(gca, 'FontSize', fsize, 'FontName', fname);

if exist('xlab', 'var')
    vs = xlab;
end

if exist('units', 'var')
    xlabel(['{\it', vs, '} (', units, ')']);
else
    xlabel(['{\it', vs, '}']);
end

ylabel('{\itPDF}');
xlim(xl);
hold off
end