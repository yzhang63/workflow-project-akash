STLD        = zeros(1, length(setTimes(m, :)));
uy          = STLD;
SL0s        = STLD;
SLmaxs      = STLD;
for i = 1:length(STLD)
    setName = [setNames{m}, int2str(setTimes(m, i))];
    inputParameters;
    InputEdgeParameters;
    [STLD(i), uy(i), SL0s(i), SLmaxs(i)] ...
            = loadSTLD(S, P, T, U, fuel, phi, angle, setName, method, frame, ...
            'vc7Folder', vc7Folder, 'delta_f', delta_f, 'filtSize', filtSize, 'numImages', N);
end
figure(hSL0);
plotSTLD(STLD, uy, SL0s/100, setSymbs{m}, lwidth, msize, fsize);
figure(hSLmax);
plotSTLD(STLD, uy, SLmaxs/100, setSymbs{m}, lwidth, msize, fsize, 'normType', 'SLmax');
if exist('legentry', 'var')
    legentry{end+1} = [num2str(U), ' m/s, ', fuel, ', {\phi} = ', num2str(phi)];
else
    legentry{1}     = [num2str(U), ' m/s, ', fuel, ', {\phi} = ', num2str(phi)];
end