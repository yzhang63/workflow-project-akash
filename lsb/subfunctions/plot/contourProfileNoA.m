function [] = contourProfileNoA(X, Y, u, U, d, dim, yl, clim, fsize)
%CONTOURPROFILENOA Generates a contour plot of profile data from PREPAREPROFILE
%   CONTOURPROFILENOA(X, Y, u, U, d, dim, yl, clim, fsize) takes the data 
%   structure generated from READIMX for .vc7 files and the outputs of 
%   PREPAREPROFILE and generates a contour plot.
%   Inputs:
%       X: 2-D array of x-positions (units specified in A.UnitX)
%       Y: 2-D array of y-positions (units specified in A.UnitY)
%       u: 2-D array of velocities (units specified in A.UnitI)
%       U: mean flow velocity (m/s)
%       d: burner diameter (mm)
%       dim: 0 = nondimensional (normalized by d, U), 1 = dimensional
%       yl: limits to plot (nondimensional: y/d)
%       clim: color axis limits (nondimensional: u/U)
%       fsize: font size
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

fname   = 'Times New Roman';            % font name

set(gcf, 'color', 'w');

if dim == 0
    X           = X/d;
    Y           = Y/d;
    u           = u/U;
else
    yl      = yl*d;
    clim    = clim*U;
end

[~, J]  = min(abs(Y(1, :) - yl(1)));

%% Create contour plot and adjust figure properties
contourf(X(:, 1:J), Y(:, 1:J), u(:, 1:J), 'linestyle', 'none');
colorbar;

set(gca, 'FontSize', fsize, 'FontName', fname);
axis image
if dim
    xlabel('{\itx} (mm)');
    ylabel('{\ity} (mm)');
    xlim([-d d]);
else
    xlabel(['{\itx} / {\itd}']);
    ylabel(['{\ity} / {\itd}']);
    xlim([-1 1]);
end
ylim([0 yl(2)]);
caxis(clim);
axis xy;
end