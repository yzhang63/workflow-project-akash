function [STLD, J] = calcSTLD(Y, u, Ufit, U, I, d, dim, lineStyle)
%CALCSTLD Calculates the local displacement turbulent flame speed
%   CALCSTLD(Y, u, Ufit, U, I, d, dim, lineStyle) calculates the local
%   displacement turbulent flame speed, ST,LD. ST,LD is defined as the
%   axial velocity at the location where the percent error between the
%   axial velocity and the fit of the axial velocity first exceeds 1%. The
%   initial region of the axial velocity (y/d < 0.3) is ignored.
%   Inputs:
%       Y: 2-D array of y-positions (units specified in A.UnitY)
%       u: 2-D array of velocities (units specified in A.UnitI)
%       Ufit: fit function outputted by FITPLOT (dimensional: m/s)
%       U: mean flow velocity (m/s)
%       I: index location of the axial centerline of the burner (u(I, :))
%       d: burner diameter (mm)
%       dim: 0 = nondimensional (normalized by d, U), 1 = dimensional
%       lineStyle: string of line/marker style for plotting
%   Outputs:
%       STLD: local displacement turbulent flame speed (m/s)
%       IJ: indices of the location of ST,LD in the array u ([I J])
%       
%       Details on the fields of the structure A can be found in the
%       DaVis Software Manual document titled "The ReadIMX Loader package
%       for Matlab(c)." This document can be found in the following
%       location on Deepthought: 
%       C:\Program Files\MATLAB\R2011a\toolbox\readimx4matlab_v1.5R1_2009_64\ReadIM7.pdf
%       If matlab has been updated to a newer version, make sure the path
%       is updated accordingly.

% plot(Y(I, :)/d, (u(I, :)-Ufit(Y(1, :)))/U, 'r-', 'LineWidth', 2);

lwidth  = 2;
msize   = 10;

[~, I1] = min(abs(Y(1, :)/d - 0.3));
I2      = find((u(I, 1:I1)-Ufit(Y(1, 1:I1)))/U > 0.01);
J       = I2(end);
J       = J + 1;
STLD    = u(I, J);
if dim == 0
    plot(Y(I, J)/d, u(I, J)/U, lineStyle, 'LineWidth', lwidth, 'MarkerSize', msize);
else
    plot(Y(I, J), u(I, J), lineStyle, 'LineWidth', lwidth, 'MarkerSize', msize);
end
end