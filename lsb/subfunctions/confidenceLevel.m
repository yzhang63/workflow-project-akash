function [cl, cls] = confidenceLevel(v, N)
%CONFIDENCELEVEL Returns 95% confidence levels for mean and rms quantities
%   CONFIDENCELEVEL(v, N) returns the 95% confidence levels for mean
%   and rms quantities for PIV vector arrays
%   obtained from PREPAREPROFILE
%   Inputs:
%       v: rms velocities
%       N: number of images
%   Outputs:
%       cl: mean 95% confidence level
%       cls: rms 95% confidence level

se      = v./sqrt(N);          % mean standard error
ses     = se./sqrt(2);         % rms standard error
cl      = 1.96*se;             % mean 95% confidence level
cls     = 1.96*ses;            % rms 95% confidence level
end