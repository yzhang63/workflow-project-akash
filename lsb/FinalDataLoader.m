setNames    = ...
    {'Cam_Date=121030_Time=112150';...  30 m/s, 50-50 H2-CO, phi = 0.55, angle = 0
    'Cam_Date=121030_Time=113727';...   30 m/s, 50-50 H2-CO, phi = 0.55, angle = 24
    'Cam_Date=121030_Time=170514';...   30 m/s, 70-30 H2-CO, phi = 0.51, angle = 0
    'Cam_Date=121030_Time=172305';...   30 m/s, 70-30 H2-CO, phi = 0.51, angle = 24
    'Cam_Date=121103_Time=112726';...   30 m/s, 100 H2, phi = 0.46, angle = 0
    'Cam_Date=121103_Time=114443';...   30 m/s, 100 H2, phi = 0.46, angle = 24
    'Cam_Date=121031_Time=195417';...   50 m/s, 50-50 H2-CO, phi = 0.55, angle = 0
    'Cam_Date=121031_Time=201041';...   50 m/s, 50-50 H2-CO, phi = 0.55, angle = 24
    'Cam_Date=121101_Time=190834';...   50 m/s, 70-30 H2-CO, phi = 0.51, angle = 0
    'Cam_Date=121101_Time=193502';...   50 m/s, 70-30 H2-CO, phi = 0.51, angle = 24
    'Cam_Date=121103_Time=121008';...   50 m/s, 100 H2, phi = 0.46, angle = 0
    'Cam_Date=121103_Time=125656'};   % 50 m/s, 100 H2, phi = 0.46, angle = 24

method      = 'graythresh';                             % binarizing method: 'graythresh' or 'kmeans'
for m = 1:length(setNames)
    setName = setNames{m};
    disp(setName);
    inputParameters;
    InputEdgeParameters;
    dataDir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', int2str(P), ' atm\', ...
        int2str(T), ' K\', int2str(U), ' mps\', fuel, '\phi = ', num2str(phi), '\', ...
        int2str(angle), ' deg\', expdate, '\masked images\Final Results\'];
    fnEnd       = [', shot ', int2str(frame), ', ', 'filt = ', int2str(filtSize(1)), ...
        ', ', method, ', images ', '1-', int2str(N)];
    datafn  = ['data, delta_f = ', num2str(delta_f, 2), fnEnd, '.mat'];
    load([dataDir, datafn], 'KAPPAFLPC', 'KAPPA_SFLP', 'KAPPAC', 'KAPPA_SC');
    disp(KAPPAFLPC);
    disp(KAPPA_SFLP);
    disp(KAPPAC(1));
    disp(KAPPA_SC(1));
    disp(' ');
    clearvars -except setNames method
end