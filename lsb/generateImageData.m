function [] = generateImagedata(N)
global ednum;
global S;
global P;
global T;
global U;
global fuel;      
global phi;       
global SL0;       
global expangle;  
global ednum;     
global exptime;   
global vc7Folder; 
global mvc7Folder;
global yl;        
global bs;        
global ol;        
%global N;         
global corrected; 
global filtSize;  
global xl;

global R;

global frame;
global rc;     
global expdate;
global d;      
global dim;    
global res;    
global psize;  
global fsize;  
global fname;  
global lwidth; 
global msize;  
global clim;   
global cmap;   

global delta_f0;
global SL0;
global SLmax;
global delta_SLmax;

global ftype;
global delta_f;
global c;

global method;

ParamStudy  = 0;                % if true, adds folder "Parameter Study" to the final directory
%inputParameters;                % script to define common input parameters
% N           = 10000;            % number of images
method      = 'graythresh';     % binarizing method: 'graythresh' or 'kmeans'
cbar        = 0.05;             % progress variable value
%disp('Total number of images for this experiment:');
%disp(N);
%B           = N/2;              % number of images to run in each for loop
%B           = N;
B = 1;
%disp('Number of images to run in each for loop:');
%disp(B);
%disp('phi =');
%disp(phi);

%% Edge Parameters
%InputEdgeParameters;        % script to define edge parameters

%% Figure adjustment parameters
xl      = [-1 1]*R*0.4;     % x limits (nondimensional: x/d)

%% Determine number of for loops and their indices
L       = ceil(N/B);        % determines number of for loops to run to keep memory from filling up
M       = 0:B:B*L;          % for loop indices
M(end)  = N;                % force final index to be N
numSets = length(M) - 1;    % number of image sets
%disp('Number of image sets: ');
%disp(numSets);

%% Load images and find flame edge
% Find progress variable field
disp('numSets, N');
disp(numSets);
disp(N);
for j = 1:numSets
    F   = M(j) + 1;                           % index of first image
    L   = M(j + 1);                           % index of last image
    Q   = L - F + 1;                          % number of images to be analyzed
    
    prevdir = cd;

    if rc == 1
        finalFolders = [expdate, '/masked images'];
    else
        finalFolders = expdate;
    end
  
    %data folder
    homedir = '/lustre/atlas/scratch/yzhang63/csc025/PIV';
    %homedir = '/lustre/widow/scratch/yzhang63/PIV';
    currdir = [homedir, '/LSB/reacting/S = ', num2str(S), '/', ...
        int2str(P), ' atm/', int2str(T), ' K/', int2str(U), ' mps/', ...
        fuel, '/phi = ', num2str(phi), '/', int2str(expangle), ' deg/', finalFolders];
    cd(currdir);
   % fnEnd       = [', shot ', int2str(frame), ', filt = ', int2str(filtSize(1)), ...
   %     ', ', method, ', images ', int2str(F), '-', int2str(L)];
    name                = ['B', sprintf('%0*d', 5, M(j)+1), '.im7'];

    %% To load images
    [X, Y, I, Im]    = loadImage(S, P, T, U, fuel, phi, expangle, expdate, name, ...
        c, xl, yl, frame, 'corrected', corrected, 'filtSize', filtSize);

    % To binarize images
    [BWs, I1]           = binarizeImages(X, Y, Im, [F, L], S, P, T, U, fuel, phi, expangle, ...
        expdate, c, xl, yl, frame, method, 'corrected', corrected, 'filtSize', filtSize);
    if j == 1
        BWmeanC         = zeros(size(BWs(:, :, 1)));
    end
    BWmean              = mean(BWs(:, :, ~I1), 3);
    BWmeanC             = BWmeanC + BWmean;
end
BWmeanC     = BWmeanC./numSets;
filedir = '/lustre/atlas/scratch/yzhang63/csc025/PIV/LSB';
bwmeancfile = [filedir, '/inter_data/BWmeanC_data'];
dlmwrite(bwmeancfile, BWmeanC);

%for j = 1:numSets
%    F   = M(j) + 1;                           % index of first image
%    L   = M(j + 1);                           % index of last image
%    Q   = L - F + 1;                          % number of images to be analyzed
%    
%    prevdir = cd;
%
%    if rc == 1
%        finalFolders = [expdate, '/masked images'];
%    else
%        finalFolders = expdate;
%    end
%  
%    %data folder
%    homedir = '/lustre/atlas/scratch/yzhang63/csc025/PIV';
%    %homedir = '/lustre/widow/scratch/yzhang63/PIV';
%    currdir = [homedir, '/LSB/reacting/S = ', num2str(S), '/', ...
%        int2str(P), ' atm/', int2str(T), ' K/', int2str(U), ' mps/', ...
%        fuel, '/phi = ', num2str(phi), '/', int2str(expangle), ' deg/', finalFolders];
%    cd(currdir);
%   % fnEnd       = [', shot ', int2str(frame), ', filt = ', int2str(filtSize(1)), ...
%   %     ', ', method, ', images ', int2str(F), '-', int2str(L)];
%    name                = ['B', sprintf('%0*d', 5, M(j)+1), '.im7'];
%
%    %% To load images
%    disp('Loading image: ');
%    disp(name);
%    [X, Y, I, Im] = loadImage(S, P, T, U, fuel, phi, expangle, expdate, name, ...
%        c, xl, yl, frame, 'corrected', corrected, 'filtSize', filtSize);
%
%%    disp('X info');
%%    disp(size(X));
%%    disp('Y info');
%%    disp(size(Y));
%%    disp('I info');
%%    disp(size(I));
%%    disp('Im info');
%%    disp(size(Im));
%%    disp('Im content');
%%    disp(Im);
%
%    % To binarize images
%%    disp('binarizing Images...');
%%    [BWs, I1]           = binarizeImages(X, Y, Im, 1, S, P, T, U, fuel, phi, expangle, ...
%%        expdate, c, xl, yl, frame, method, 'corrected', corrected, 'filtSize', filtSize);
%%    %[BWs, I1]           = binarizeImages(X, Y, Im, [F, L], S, P, T, U, fuel, phi, expangle, ...
%%    %    expdate, c, xl, yl, frame, method, 'corrected', corrected, 'filtSize', filtSize);
%%
%%    % To extract flame edge
%%    disp('Extracting flame Edge...');
%%    E                   = flameEdges(BWs, X, Y);
%%    cd(prevdir);
%%    
%%    %% Generate average progress variable, <c>, contour plot
%%    BWmean      = mean(BWs(:, :, ~I1), 3); % un-used code
%%    
%%    %% Curvature Calculations
%%    delta_p         = X(2) - X(1);                                  % pixel size (mm) no use
%%    %     delta_f         = 4*delta_p;
%%
%%    disp('Curvaturing...');
%%    tic; 
%%    tstart = tic;
%%    ftype = 'poly';
%%    [xDS, yDS, cDS, sDS, x, y, c_, xp, yp, xpp, ypp, s, kappa, R, nx, ny] = ...
%%        Curvature(X, Y, BWmeanC, E, 'delta_f', delta_f, 'fittype', ftype);
%%    telapsed = toc(tstart);
%%    disp('done');
%%    disp('Curvature calculation time:');
%%    fprintf(fid, '%1.4f\n', telapsed);
%%    disp(telapsed);
%end %for end

end %function end
