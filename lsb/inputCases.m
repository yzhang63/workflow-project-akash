function [] = inputCases(setName) % Define common input parameters for different cases
global ednum;
global S;
global P;
global T;
global U;
global fuel;      
global phi;       
global SL0;       
global expangle;  
global ednum;     
global exptime;   
global vc7Folder; 
global mvc7Folder;
global yl;        
global bs;        
global ol;        
%global N;         
global corrected; 
global filtSize;  

global R;

global frame;
global rc;     
global expdate;
global d;      
global dim;    
global res;    
global psize;  
global fsize;  
global fname;  
global lwidth; 
global msize;  
global clim;   
global cmap;   

global delta_f0;
global SL0;
global SLmax;
global delta_SLmax;

global ftype;
global delta_f;
global c;

switch setName
    case 'Cam_Date=121030_Time=112150'
        S           = 0.58;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 30;               % velocity (m/s)
        fuel        = '50-50 H2-CO';    % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.55;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 0;                % expangle (deg)
        ednum       = 20121030;         % experiment date as an integer value (yyyymmdd)
        exptime     = 112150;           % experiment time
        vc7Folder   = 'PIV_PreProc_MP(32x32_50%ov_ImgCorr)_PostProc';
        mvc7Folder  = 'PIV_PreProc_MP(12x12_50%ov_ImgCorr)_PostProc'; % masked images vc7 folder
        % Inputs below probably won't change as often as those above
        yl          = [0.4 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 1;            % number of images
        corrected   = 1;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [5 5];            % median filter size in pixels
    case 'Cam_Date=121030_Time=113727'
        S           = 0.58;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 30;               % velocity (m/s)
        fuel        = '50-50 H2-CO';    % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.55;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 24;               % expangle (deg)
        ednum       = 20121030;         % experiment date as an integer value (yyyymmdd)
        exptime     = 113727;           % experiment time
        vc7Folder   = 'PIV_PreProc_MP(32x32_50%ov_ImgCorr)_PostProc';
        mvc7Folder  = 'PIV_PreProc_MP(12x12_50%ov_ImgCorr)_PostProc'; % masked images vc7 folder
        % Inputs below probably won't change as often as those above
        yl          = [0.4 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 10000;            % number of images
        corrected   = 1;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [5 5];            % median filter size in pixels
    case 'Cam_Date=121030_Time=170514'
        S           = 0.58;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 30;               % velocity (m/s)
        fuel        = '70-30 H2-CO';    % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.51;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 0;                % expangle (deg)
        ednum       = 20121030;         % experiment date as an integer value (yyyymmdd)
        exptime     = 170514;           % experiment time
        vc7Folder   = 'PIV_PreProc_MP(32x32_50%ov_ImgCorr)_PostProc';
        mvc7Folder  = 'PIV_PreProc_MP(12x12_50%ov_ImgCorr)_PostProc'; % masked images vc7 folder
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 50;            % number of images 10000
        corrected   = 1;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [5 5];            % median filter size in pixels
    case 'Cam_Date=121030_Time=172305'
        S           = 0.58;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 30;               % velocity (m/s)
        fuel        = '70-30 H2-CO';    % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.51;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 24;               % expangle (deg)
        ednum       = 20121030;         % experiment date as an integer value (yyyymmdd)
        exptime     = 172305;           % experiment time
        vc7Folder   = 'PIV_PreProc_MP(32x32_50%ov_ImgCorr)_PostProc';
        mvc7Folder  = 'PIV_PreProc_MP(12x12_50%ov_ImgCorr)_PostProc'; % masked images vc7 folder
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 10000;            % number of images
        corrected   = 1;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [5 5];            % median filter size in pixels
    case 'Cam_Date=121031_Time=195417'
        S           = 0.58;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 50;               % velocity (m/s)
        fuel        = '50-50 H2-CO';    % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.55;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 0;                % expangle (deg)
        ednum       = 20121031;         % experiment date as an integer value (yyyymmdd)
        exptime     = 195417;           % experiment time
        vc7Folder   = 'PIV_PreProc_MP(32x32_50%ov_ImgCorr)_PostProc';
        mvc7Folder  = 'PIV_PreProc_MP(12x12_50%ov_ImgCorr)_PostProc'; % masked images vc7 folder
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 10000;            % number of images
        corrected   = 1;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [5 5];            % median filter size in pixels
    case 'Cam_Date=121031_Time=201041'
        S           = 0.58;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 50;               % velocity (m/s)
        fuel        = '50-50 H2-CO';    % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.55;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 24;               % expangle (deg)
        ednum       = 20121031;         % experiment date as an integer value (yyyymmdd)
        exptime     = 201041;           % experiment time
        vc7Folder   = 'PIV_PreProc_MP(32x32_50%ov_ImgCorr)_PostProc';
        mvc7Folder  = 'PIV_PreProc_MP(12x12_50%ov_ImgCorr)_PostProc'; % masked images vc7 folder
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 10000;            % number of images
        corrected   = 1;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [5 5];            % median filter size in pixels
    case 'Cam_Date=121101_Time=190834'
        S           = 0.58;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 50;               % velocity (m/s)
        fuel        = '70-30 H2-CO';    % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.51;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 0;                % expangle (deg)
        ednum       = 20121101;         % experiment date as an integer value (yyyymmdd)
        exptime     = 190834;           % experiment time
        vc7Folder   = 'PIV_PreProc_MP(32x32_50%ov_ImgCorr)_PostProc';
        mvc7Folder  = 'PIV_PreProc_MP(12x12_50%ov_ImgCorr)_PostProc'; % masked images vc7 folder
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 10000;            % number of images
        corrected   = 1;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [5 5];            % median filter size in pixels
    case 'Cam_Date=121101_Time=193502'
        S           = 0.58;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 50;               % velocity (m/s)
        fuel        = '70-30 H2-CO';    % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.51;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 24;               % expangle (deg)
        ednum       = 20121101;         % experiment date as an integer value (yyyymmdd)
        exptime     = 193502;           % experiment time
        vc7Folder   = 'PIV_PreProc_MP(32x32_50%ov_ImgCorr)_PostProc';
        mvc7Folder  = 'PIV_PreProc_MP(12x12_50%ov_ImgCorr)_PostProc'; % masked images vc7 folder
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 10000;            % number of images
        corrected   = 1;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [5 5];            % median filter size in pixels
    case 'Cam_Date=121103_Time=112726'
        S           = 0.58;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 30;               % velocity (m/s)
        fuel        = '100 H2';         % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.46;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 0;                % expangle (deg)
        ednum       = 20121103;         % experiment date as an integer value (yyyymmdd)
        exptime     = 112726;           % experiment time
        vc7Folder   = 'PIV_PreProc_MP(32x32_50%ov_ImgCorr)_PostProc';
        mvc7Folder  = 'PIV_PreProc_MP(12x12_50%ov_ImgCorr)_PostProc'; % masked images vc7 folder
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 10000;            % number of images
        corrected   = 1;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [5 5];            % median filter size in pixels
    case 'Cam_Date=121103_Time=114443'
        S           = 0.58;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 30;               % velocity (m/s)
        fuel        = '100 H2';         % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.46;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 24;               % expangle (deg)
        ednum       = 20121103;         % experiment date as an integer value (yyyymmdd)
        exptime     = 114443;           % experiment time
        vc7Folder   = 'PIV_PreProc_MP(32x32_50%ov_ImgCorr)_PostProc';
        mvc7Folder  = 'PIV_PreProc_MP(12x12_50%ov_ImgCorr)_PostProc'; % masked images vc7 folder
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 10000;            % number of images
        corrected   = 1;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [5 5];            % median filter size in pixels
    case 'Cam_Date=121103_Time=121008'
        S           = 0.58;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 50;               % velocity (m/s)
        fuel        = '100 H2';         % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.46;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 0;                % expangle (deg)
        ednum       = 20121103;         % experiment date as an integer value (yyyymmdd)
        exptime     = 121008;           % experiment time
        vc7Folder   = 'PIV_PreProc_MP(32x32_50%ov_ImgCorr)_PostProc';
        mvc7Folder  = 'PIV_PreProc_MP(12x12_50%ov_ImgCorr)_PostProc'; % masked images vc7 folder
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 10000;            % number of images
        corrected   = 1;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [5 5];            % median filter size in pixels
    case 'Cam_Date=121103_Time=125656'
        S           = 0.58;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 50;               % velocity (m/s)
        fuel        = '100 H2';         % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.46;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 24;               % expangle (deg)
        ednum       = 20121103;         % experiment date as an integer value (yyyymmdd)
        exptime     = 125656;           % experiment time
        vc7Folder   = 'PIV_PreProc_MP(32x32_50%ov_ImgCorr)_PostProc';
        mvc7Folder  = 'PIV_PreProc_MP(12x12_50%ov_ImgCorr)_PostProc'; % masked images vc7 folder
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 10000;            % number of images
        corrected   = 1;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [5 5];            % median filter size in pixels
    case 'Cam_Date=121103_Time=141350'
        S           = 0.58;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 30;               % velocity (m/s)
        fuel        = '100 CH4';        % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.9;              % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 0;                % expangle (deg)
        ednum       = 20121103;         % experiment date as an integer value (yyyymmdd)
        exptime     = 141350;           % experiment time
        vc7Folder   = 'PIV_PreProc_MP(32x32_50%ov_ImgCorr)_PostProc';
        mvc7Folder  = 'PIV_PreProc_MP(12x12_50%ov_ImgCorr)_PostProc'; % masked images vc7 folder
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 10000;            % number of images
        corrected   = 1;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [5 5];            % median filter size in pixels
    case 'Cam_Date=121103_Time=143224'
        S           = 0.58;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 30;               % velocity (m/s)
        fuel        = '100 CH4';        % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.9;              % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 24;               % expangle (deg)
        ednum       = 20121103;         % experiment date as an integer value (yyyymmdd)
        exptime     = 143224;           % experiment time
        vc7Folder   = 'PIV_PreProc_MP(32x32_50%ov_ImgCorr)_PostProc';
        mvc7Folder  = 'PIV_PreProc_MP(12x12_50%ov_ImgCorr)_PostProc'; % masked images vc7 folder
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 10000;            % number of images
        corrected   = 1;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [5 5];            % median filter size in pixels
    case '20120215-1-NR'
        S           = 0.57;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 20;               % velocity (m/s)
        fuel        = '100 CH4';        % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0;                % equivalence ratio (use 0 for nonreacting)
        expangle       = 0;                % expangle (deg)
        ednum       = 20120215;         % experiment date as an integer value (yyyymmdd)
        vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 1000;             % number of images
        corrected   = 0;                % 1 = world coordinates, 0 = raw coordinates
    case '20120215-1'
        S           = 0.57;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 20;               % velocity (m/s)
        fuel        = '100 CH4';        % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.9;              % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 0;                % expangle (deg)
        ednum       = 20120215;         % experiment date as an integer value (yyyymmdd)
        vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 1000;             % number of images
        corrected   = 0;                % 1 = world coordinates, 0 = raw coordinates
    case '20120222-1'
        S           = 0.57;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 20;               % velocity (m/s)
        fuel        = '100 CH4';        % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.9;              % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 0;                % expangle (deg)
        ednum       = 20120222;         % experiment date as an integer value (yyyymmdd)
        vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 1000;             % number of images
        corrected   = 0;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [9 9];            % median filter size in pixels
    case '20120222-2'
        S           = 0.57;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 30;               % velocity (m/s)
        fuel        = '100 CH4';        % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.9;              % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 0;                % expangle (deg)
        ednum       = 20120222;         % experiment date as an integer value (yyyymmdd)
        vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 1000;             % number of images
        corrected   = 0;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [9 9];            % median filter size in pixels
    case '20120222-3'
        S           = 0.57;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 30;               % velocity (m/s)
        fuel        = '100 CH4';        % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.9;              % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 24;               % expangle (deg)
        ednum       = 20120222;         % experiment date as an integer value (yyyymmdd)
        vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 1000;             % number of images
        corrected   = 0;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [9 9];            % median filter size in pixels
    case '20120222-4'
        S           = 0.57;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 40;               % velocity (m/s)
        fuel        = '100 CH4';        % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.9;              % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 0;                % expangle (deg)
        ednum       = 20120222;         % experiment date as an integer value (yyyymmdd)
        vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 1000;             % number of images
        corrected   = 0;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [9 9];            % median filter size in pixels
    case '20120222-5'
        S           = 0.57;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 40;               % velocity (m/s)
        fuel        = '100 CH4';        % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.9;              % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 24;               % expangle (deg)
        ednum       = 20120222;         % experiment date as an integer value (yyyymmdd)
        vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 1000;             % number of images
        corrected   = 0;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [9 9];            % median filter size in pixels
    case '20120301-1'
        S           = 0.57;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 20;               % velocity (m/s)
        fuel        = '50-50 H2-CH4';   % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.73;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 0;                % expangle (deg)
        ednum       = 20120301;         % experiment date as an integer value (yyyymmdd)
        vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';
        % Inputs below probably won't change as often as those above
        yl          = [0.4 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 1000;             % number of images
        corrected   = 0;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [9 9];            % median filter size in pixels
    case '20120301-2'
        S           = 0.57;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 20;               % velocity (m/s)
        fuel        = '50-50 H2-CH4';   % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.73;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 24;               % expangle (deg)
        ednum       = 20120301;         % experiment date as an integer value (yyyymmdd)
        vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 1000;             % number of images
        corrected   = 0;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [9 9];            % median filter size in pixels
    case '20120301-3'
        S           = 0.57;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 20;               % velocity (m/s)
        fuel        = '100 CH4';        % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.9;              % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 0;                % expangle (deg)
        ednum       = 20120301;         % experiment date as an integer value (yyyymmdd)
        vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 1000;             % number of images
        corrected   = 0;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [9 9];            % median filter size in pixels
    case '20120301-4'
        S           = 0.57;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 30;               % velocity (m/s)
        fuel        = '50-50 H2-CH4';   % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.73;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 0;                % expangle (deg)
        ednum       = 20120301;         % experiment date as an integer value (yyyymmdd)
        vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';
        % Inputs below probably won't change as often as those above
        yl          = [0.5 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 1000;             % number of images
        corrected   = 0;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [9 9];            % median filter size in pixels
    case '20120301-5'
        S           = 0.57;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 30;               % velocity (m/s)
        fuel        = '50-50 H2-CH4';   % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.73;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 24;               % expangle (deg)
        ednum       = 20120301;         % experiment date as an integer value (yyyymmdd)
        vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 1000;             % number of images
        corrected   = 0;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [9 9];            % median filter size in pixels
    case '20120301-6'
        S           = 0.58;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 40;               % velocity (m/s)
        fuel        = '50-50 H2-CH4';   % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.73;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 0;                % expangle (deg)
        ednum       = 20120301;         % experiment date as an integer value (yyyymmdd)
        vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';
        % Inputs below probably won't change as often as those above
        yl          = [0.4 1];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 1000;             % number of images
        corrected   = 0;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [9 9];            % median filter size in pixels
    case '20120301-7'
        S           = 0.58;             % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 40;               % velocity (m/s)
        fuel        = '50-50 H2-CH4';   % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.73;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 24;               % expangle (deg)
        ednum       = 20120301;         % experiment date as an integer value (yyyymmdd)
        vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.2];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 1000;             % number of images
        corrected   = 0;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [9 9];            % median filter size in pixels
    case '20120402-1'
        S           = 0.6;              % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 30;               % velocity (m/s)
        fuel        = '75-25 H2-CH4';   % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.63;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 0;                % expangle (deg)
        ednum       = 20120402;         % experiment date as an integer value (yyyymmdd)
        vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';
        % Inputs below probably won't change as often as those above
        yl          = [0.3 1.1];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 1000;             % number of images
        corrected   = 0;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [9 9];            % median filter size in pixels
    case '20120402-2'
        S           = 0.6;              % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 40;               % velocity (m/s)
        fuel        = '60-40 H2-CH4';   % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.69;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 0;                % expangle (deg)
        ednum       = 20120402;         % experiment date as an integer value (yyyymmdd)
        vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 1000;             % number of images
        corrected   = 0;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [9 9];            % median filter size in pixels
    case '20120402-3'
        S           = 0.6;              % swirl number
        P           = 1;                % pressure (atm)
        T           = 300;              % temperature (K)
        U           = 40;               % velocity (m/s)
        fuel        = '75-25 H2-CH4';   % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore if phi = 0)
        phi         = 0.63;             % equivalence ratio (use 0 for nonreacting)
        SL0         = 34;               % unstretched laminar flame speed (cm/s)
        expangle       = 0;                % expangle (deg)
        ednum       = 20120402;         % experiment date as an integer value (yyyymmdd)
        vc7Folder   = 'PIV_MP(32x32_50%ov)_PostProc';
        % Inputs below probably won't change as often as those above
        yl          = [0.2 1.5];        % y limits (nondimensional: y/d)
        bs          = 32;               % interrogation window binsize in pixels
        ol          = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
        N           = 1000;             % number of images
        corrected   = 0;                % 1 = world coordinates, 0 = raw coordinates
        filtSize    = [9 9];            % median filter size in pixels
    otherwise
        error('Invalid setName');
end % switch end

end %function end
