% Script to create image files from im7 images

%% Input parameters
indIni  = 1;                                                            % index of first im7 file
indFin  = 50;                                                           % index of last im7 file
shot    = 1;                                                            % image frame (1 or 2)

%% Load and save images
bits    = 8;    % number of bits to write for image file (16 is highest, but not all machines support it)

for i = indIni:indFin
    fn              = ['B', sprintf('%0*d', 5, i)];                     % im7 filename (e.g. 'B00001.im7')
    [A, X, Y, I]    = loadImageBasic([fn, '.im7'], shot);
    
    %% Scale data
    I   = (I - min(min(I)))./(max(max(I)) - min(min(I)));   % Scale intensity between [0, 1]
    I   = I.*(2^bits - 1);                                  % Scale to precision of bits
    switch bits
        case 8
            I   = uint8(I);
        case 16
            I   = uint16(I);
        otherwise
            error('unsupported precision value for variable bits');
    end
    
    %% Match histogram across images
    if i == indIni
        J   = I;
    else
        I   = imhistmatch(I, J);
    end
    
    %% Write image file
    imwrite(I, [fn, '_', int2str(shot), '.tif'], 'tiff');
end