% Script to plot ST,LD data

fsize   = 18;                           % font size
fname   = 'Times New Roman';            % font name
lwidth  = 2;                            % line width
msize   = 10;                           % marker size
method  = 'kmeans';                 % method: 'graythresh' or 'kmeans'
frame   = 1;                            % shot 1 or shot 2

figure;
hold on

S           = 0.57;
P           = 1;
T           = 300;
U           = 30;
fuel        = '100 CH4';
phi         = 0.9;
angle       = 24;
expdate     = '20120222';
SL          = 0.34;                         % unstretched laminar flame speed (m/s)
STLD        = zeros(1, length(angle));
uy          = STLD;
for i = 1:length(angle)
    [STLD(i), uy(i)]  = loadSTLD(S, P, T, U, fuel, phi, angle(i), expdate, method, frame);
end
plotSTLD(STLD, uy, SL, 'r+', lwidth, msize, fsize);
legentry{1} = [num2str(U), ' m/s, ', fuel, ', {\phi} = ', num2str(phi)];

S           = 0.57;
P           = 1;
T           = 300;
U           = 30;
fuel        = '50-50 H2-CH4';
phi         = 0.73;
angle       = [0 24];
expdate     = '20120301';
SL          = 0.34;                         % unstretched laminar flame speed (m/s)
STLD        = zeros(1, length(angle));
uy          = STLD;
for i = 1:length(angle)
    [STLD(i), uy(i)]  = loadSTLD(S, P, T, U, fuel, phi, angle(i), expdate, method, frame);
end
plotSTLD(STLD, uy, SL, 'r*', lwidth, msize, fsize);
legentry{2} = [num2str(U), ' m/s, ', fuel, ', {\phi} = ', num2str(phi)];

S           = 0.6;
P           = 1;
T           = 300;
U           = 30;
fuel        = '75-25 H2-CH4';
phi         = 0.63;
angle       = 0;
expdate     = '20120402';
SL          = 0.34;                         % unstretched laminar flame speed (m/s)
STLD        = zeros(1, length(angle));
uy          = STLD;
for i = 1:length(angle)
    [STLD(i), uy(i)]  = loadSTLD(S, P, T, U, fuel, phi, angle(i), expdate, method, frame);
end
plotSTLD(STLD, uy, SL, 'rd', lwidth, msize, fsize);
legentry{3} = [num2str(U), ' m/s, ', fuel, ', {\phi} = ', num2str(phi)];

S           = 0.57;
P           = 1;
T           = 300;
U           = 40;
fuel        = '100 CH4';
phi         = 0.9;
angle       = [0 24];
expdate     = '20120222';
SL          = 0.34;                         % unstretched laminar flame speed (m/s)
STLD        = zeros(1, length(angle));
uy          = STLD;
for i = 1:length(angle)
    [STLD(i), uy(i)]  = loadSTLD(S, P, T, U, fuel, phi, angle(i), expdate, method, frame);
end
plotSTLD(STLD, uy, SL, 'c+', lwidth, msize, fsize);
legentry{4} = [num2str(U), ' m/s, ', fuel, ', {\phi} = ', num2str(phi)];

S           = 0.58;
P           = 1;
T           = 300;
U           = 40;
fuel        = '50-50 H2-CH4';
phi         = 0.73;
angle       = [0 24];
expdate     = '20120301';
SL          = 0.34;                         % unstretched laminar flame speed (m/s)
STLD        = zeros(1, length(angle));
uy          = STLD;
for i = 1:length(angle)
    [STLD(i), uy(i)]  = loadSTLD(S, P, T, U, fuel, phi, angle(i), expdate, method, frame);
end
plotSTLD(STLD, uy, SL, 'c*', lwidth, msize, fsize);
legentry{5} = [num2str(U), ' m/s, ', fuel, ', {\phi} = ', num2str(phi)];

S           = 0.6;
P           = 1;
T           = 300;
U           = 40;
fuel        = '60-40 H2-CH4';
phi         = 0.69;
angle       = 0;
expdate     = '20120402';
SL          = 0.34;                         % unstretched laminar flame speed (m/s)
STLD        = zeros(1, length(angle));
uy          = STLD;
for i = 1:length(angle)
    [STLD(i), uy(i)]  = loadSTLD(S, P, T, U, fuel, phi, angle(i), expdate, method, frame);
end
plotSTLD(STLD, uy, SL, 'cs', lwidth, msize, fsize);
legentry{6} = [num2str(U), ' m/s, ', fuel, ', {\phi} = ', num2str(phi)];

S           = 0.6;
P           = 1;
T           = 300;
U           = 40;
fuel        = '75-25 H2-CH4';
phi         = 0.63;
angle       = 0;
expdate     = '20120402';
SL          = 0.34;                         % unstretched laminar flame speed (m/s)
STLD        = zeros(1, length(angle));
uy          = STLD;
for i = 1:length(angle)
    [STLD(i), uy(i)]  = loadSTLD(0.6, 1, 300, 40, '75-25 H2-CH4', 0.63, angle(i), '20120402', method, frame);
end
plotSTLD(STLD, uy, SL, 'cd', lwidth, msize, fsize);
legentry{7} = [num2str(U), ' m/s, ', fuel, ', {\phi} = ', num2str(phi)];
legend(legentry, 'fontsize', 10, 'location', 'best');

x1 = xlim;
xlim([0 x1(2)]);
y1 = ylim;
ylim([0 y1(2)]);
x2 = xlim;
STSL    = @(uySL) 1 + 1.73*uySL;
plot(x2, STSL(x2), 'k--', 'LineWidth', lwidth);