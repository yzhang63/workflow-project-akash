%% Input Parameters
if ~exist('setNames', 'var')                            % if not batch processing multiple sets
    setName = 'Cam_Date=121030_Time=112150';            % name of folder where dataset is located
end
inputParameters;                                        % script to define common input parameters
method      = 'graythresh';                             % binarizing method: 'graythresh' or 'kmeans'

%% Edge Parameters
InputEdgeParameters;                                    % script to define edge parameters

%% Load unconditioned data
prevdir     = cd;
currdir     = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', int2str(P), ' atm\', ...
    int2str(T), ' K\', int2str(U), ' mps\', fuel, '\phi = ', num2str(phi), '\', ...
    int2str(angle), ' deg\', expdate, '\Final Results'];
cd(currdir);
fnEnd       = [', shot ', int2str(frame), ', ', 'filt = ', int2str(filtSize(1)), ...
    ', ', method, ', images ', '1-', int2str(N)];
if exist('setNames', 'var')
    temp        = setNames;
end
load(['data, delta_f = ', num2str(delta_f, 2), fnEnd, '.mat']);
if exist('setNames', 'var')
    setNames    = temp;
end
cd(prevdir);

%% Additional Input Parameters
rc      = 1;                                            % reactant-conditioned

%% Tangential Strain Rate Calculations
kappa_s = tangentialStrainRates(S, P, T, U, fuel, phi, angle, expdate, c, N, EC, xC, yC, nxC, nyC, ...
    'rc', rc, 'vc7Folder', mvc7Folder);

%% Remove zeros from tangential strain rates - figure out later why there are zeros
%% Remove outliers from tangential strain rates using Thomson-Tau method
kappa_sF        = outliers(cell2mat(kappa_s));          % remove outliers
Inan            = isnan(kappa_sF) | (kappa_sF == 0);    % indices of NaNs or zeros
kappa_sF(Inan)  = [];                                   % remove NaNs and zeros
xF              = cell2mat(xC);
xF(Inan)        = [];                                   % x-coordinates for kappa_s that were NaNs
yF              = cell2mat(yC);
yF(Inan)        = [];                                   % y-coordinates for kappa_s that were NaNs
cF              = cell2mat(cC);
cF(Inan)        = [];                                   % c-coordinates for kappa_s that were NaNs

%% Calculate cumulative standard deviations of tangential strain rate
Skd             = cumStd(cell2mat(kappa_s));            % cum. std of curv., DS, no OR
SkdF            = cumStd(kappa_sF);                     % cum. std of curv., DS, OR

%% Generate plot of cumulative standard deviations of tangential strain rate
clear h
h(1)            = figure;
set(gcf, 'color', 'w');
set(gcf, 'Name', ['Cumulative Standard Deviations, ', ftype, ', delta_f = ', num2str(delta_f, 2)]);
plot(1:length(Skd), Skd, 'r', 1:length(SkdF), SkdF, 'g', 'LineWidth', lwidth);
set(gca, 'FontSize', fsize, 'FontName', fname);
xlabel('{\itN}');
ylabel('{\it\sigma} (1/s)');
legend([ftype, ', no OR'], [ftype, ', OR'], 'location', 'best');

%% PDFs of tangential strain rate
h(end+1)    = figure;
histPlot(kappa_sF, 'fontSize', fsize, 'varname', 'kappa_s', 'units', '1/s', 'xlabel', '{\kappa_s}', ...
    'plotType', 'stairs');
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);

h(end+1)    = figure;
histPlot(kappa_sF*(delta_f0/10)/SL0, 'fontSize', fsize, 'varname', 'kappa_sdeltaf0bySL0', ...
    'xlabel', '{\kappa_s\delta_f_,_0 /S_{L,0}}', 'plotType', 'stairs');
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);

%% Test area
% kappa_sF2       = kappa_sF;
% dSkdF   = 1;
% i       = 0;
% SkdF2   = 0;
% while dSkdF > 0.001
%     i               = i + 1;
%     kappa_sF2       = outliers(kappa_sF2);
%     Inan2           = isnan(kappa_sF2);                     % indices of NaNs
%     kappa_sF2(Inan2)= [];                                   % remove NaNs
%     SkdF1           = SkdF2;
%     SkdF2           = cumStd(kappa_sF2);                    % cum. std of curv., DS, OR
%     dSkdF           = abs(SkdF2(end) - SkdF1(end))/SkdF2(end); % relative change (%)
% end
% hold on
% plot(1:length(SkdF2), SkdF2, 'k', 'LineWidth', lwidth);

%% Calculate quantities at the instantaneous leading points of the flame
[xLPks, yLPks, sLP, kappa_sLP]  = LPcurvatures(xC, yC, sC, kappa_s);
[~, ~, ~, uLP, vLP]             = LPvelocities(S, P, T, U, fuel, phi, angle, expdate, ...
    c, N, EC, X, Y, BWmeanC, 'vc7Folder', mvc7Folder, 'rc', rc);
% Statistics
kappa_sFLP  = outliers(kappa_sLP);              % remove outliers
Inan        = isnan(kappa_sFLP);                % indices of NaNs
kappa_sFLP(Inan) = [];                          % remove NaNs
KAPPA_SFLP  = nanmean(kappa_sFLP);              % mean tangential strain rate at the leading points
ULP         = nanmean(uLP);                     % mean x component velocity of leading points
VLP         = nanmean(vLP);                     % mean y component velocity of leading points
upLP        = nanstd(uLP);                      % rms x component velocity of leading points
vpLP        = nanstd(vLP);                      % rms y component velocity of leading points

%% PDF of tangential strain rate conditioned on progress variable value
cbin        = 0.1;                              % Width of progress variable bins
cvals       = 0:cbin:1;                         % Progress variable bins
kappa_sc    = cell(length(cvals)-1, 1);
KAPPA_SC    = NaN(length(cvals)-1, 1);
CF          = KAPPA_SC;
legEntry    = kappa_sc;
n           = kappa_sc;
n2          = kappa_sc;
kout        = kappa_sc;
colOrd      = jet(length(cvals)-1);
h(end+1)    = figure;
hold on
for i = 1:length(cvals)-1
    c1          = cvals(i);
    c2          = cvals(i+1);
    Ic          = cF >= c1 & cF < c2;       % Indices of c1 <= cF < c2
    kappa_sc{i} = kappa_sF(Ic);             % tangential strain rates satisfying c1 <= cF < c2
    CF(i)       = nanmean(cF(Ic));          % mean progress variable satisfying c1 <= cF < c2
    KAPPA_SC(i) = nanmean(kappa_sc{i});     % mean tangential strain rates satisfying c1 <= cF < c2
    legEntry{i} = [num2str(cvals(i)), ' {\leq} {\it<c>} < ', num2str(cvals(i+1))];
    [n{i}, kout{i}] = histPlot(kappa_sc{i}, 'fontSize', fsize, 'varname', 'kappa_sC', ...
        'units', '1/s', 'xlabel', '{\kappa_s}', 'plotType', 'stairs', 'color', colOrd(i, :));
    w           = kout{i}(2) - kout{i}(1);  % bin width (1/mm)
    n2{i}       = n{i}.*abs(kout{i});
    n2{i}       = n2{i}/(w*sum(n2{i}));     % normalize height such that area under histogram is 1
end
legend(legEntry, 'location', 'best', 'fontSize', fsize-2);
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2), ...
    ', cbin = ', num2str(cbin)]);
hold off

h(end+1)    = figure;
set(gcf, 'color', 'w');
set(gcf, 'Name', 'AAW PDF of kappa_sC');
hold on
for i = 1:length(kappa_sc)
    stairs(kout{i}, n2{i}, 'LineWidth', lwidth, 'color', colOrd(i, :));
end
set(gca, 'FontSize', fsize, 'FontName', fname);
xlim([-1 1]*10^4);
xlabel('{\it\kappa_s} (1/s)');
ylabel('{\itPDF}');
legend(legEntry, 'location', 'best', 'fontSize', fsize-2);
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2), ...
    ', cbin = ', num2str(cbin)]);
hold off

h(end+1)    = figure;
set(gcf, 'color', 'w');
set(gcf, 'Name', 'kappa_sbar as fxn of cbar');
plot(CF, KAPPA_SC, 'bx', 'LineWidth', lwidth, 'MarkerSize', msize);
set(gca, 'FontSize', fsize, 'FontName', fname);
xlabel('{\it<c>}');
ylabel('$\bar{\kappa_s}$ (1/s)', 'interpreter', 'latex', 'FontSize', fsize+2);
xlim([0 1]);
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2), ...
    ', cbin = ', num2str(cbin)]);

%% Find velocities at the average position of the leading points
[uLPave, vLPave]    = velocitiesGivenLoc(S, P, T, U, fuel, phi, angle, expdate, c, N, XLPC, YLPC, ...
    'vc7Folder', mvc7Folder);
ULPave              = nanmean(uLPave);
% mean x component velocity at the average position of the leading points
VLPave              = nanmean(vLPave);
% mean y component velocity at the average position of the leading points

%% Find velocities at given progress variable values and upstream of flame
cbars   = 0.1:0.2:0.6;
uc      = zeros(N, length(cbars));
vc      = uc;

for i = 1:length(cbars)
    Cfuns   = @(y) Co(1)*(erf(Co(2)*(y - Co(3)))) + Co(4) - cbars(i);
    ys      = fzero(Cfuns, 0.6*d);
    [uc(:, i), vc(:, i)] = velocitiesGivenLoc(S, P, T, U, fuel, phi, angle, expdate, c, N, 0, ys, ...
        'vc7Folder', mvc7Folder);
end
clear Cfuns ys
ucp         = nanstd(uc);       % rms x component velocity at given progress variable values
vcp         = nanstd(vc);       % rms y component velocity at given progress variable values
% find velocity halfway between burner exit and <c> = 0.05 contour
[uUP, vUP]  = velocitiesGivenLoc(S, P, T, U, fuel, phi, angle, expdate, c, N, 0, y1C/2, ...
    'vc7Folder', mvc7Folder);

%% Generate PDFs of leading points properties
% Tangential strain rates
h(end+1)    = figure;
histPlot(kappa_sFLP, 'fontSize', fsize, 'varname', 'kappa_sLP', 'units', '1/s', ...
    'xlabel', '{\kappa_{s,LP}}', 'plotType', 'stairs');
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);

h(end+1)    = figure;
histPlot(kappa_sFLP*(delta_f0/10)/SL0, 'fontSize', fsize, 'varname', 'kappa_sLPdeltaf0bySL0', ...
    'xlabel', '{\kappa_{s,LP}\delta_f_,_0 /S_{L,0}}', 'plotType', 'stairs');
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);

% Instantaneous leading point velocity
h(end+1)    = figure;
f1_         = length(h);
histPlot(uLP-ULP, 'fontSize', fsize, 'varname', 'u''_L_P', 'units', 'm/s', ...
    'plotType', 'stairs', 'lmStyle', 'r');

h(end+1)    = figure; %#ok<*SAGROW>J
f2_         = length(h);
histPlot(vLP-VLP, 'fontSize', fsize, 'varname', 'v''_L_P', 'units', 'm/s', ...
    'plotType', 'stairs', 'lmStyle', 'r');

% Average position of leading points velocity
figure(h(f1_));
hold on
histPlot(uLPave-ULPave, 'fontSize', fsize, 'varname', 'u''', 'units', 'm/s', ...
    'plotType', 'stairs', 'lmstyle', 'b');
hold on
histPlot(uUP-nanmean(uUP), 'fontSize', fsize, 'varname', 'u''', 'units', 'm/s', ...
    'plotType', 'stairs', 'lmstyle', 'g');
legend('{\itu''_L_P}', ['{\itu''}({\it<c>} = ', num2str(CLPC, 2), ')'], '{\itu''_u_p}', ...
    'location', 'best');

figure(h(f2_));
hold on
histPlot(vLPave-VLPave, 'fontSize', fsize, 'varname', 'v''', 'units', 'm/s', ...
    'plotType', 'stairs', 'lmstyle', 'b');
hold on
histPlot(vUP-nanmean(vUP), 'fontSize', fsize, 'varname', 'v''', 'units', 'm/s', ...
    'plotType', 'stairs', 'lmstyle', 'g');
legend('{\itv''_L_P}', ['{\itv''}({\it<c>} = ', num2str(CLPC, 2), ')'], '{\itv''_u_p}', ...
    'location', 'best');

% velocity pdfs at specific progress variable values
legEntry    = cell(size(cbars));
h(end+1)    = figure;
histPlot(uc-ones(length(uc(:, 1)), 1)*nanmean(uc, 1), 'fontSize', fsize, ...
    'varname', 'u''_c', 'xlabel', 'u''', 'units', 'm/s', ...
    'plotType', 'stairs', 'lineWidth', lwidth);
for i = 1:length(legEntry)
    legEntry{i} = ['{\it<c>} = ', num2str(cbars(i))];
end
legend(legEntry, 'location', 'best');

h(end+1)    = figure;
histPlot(vc-ones(length(vc(:, 1)), 1)*nanmean(vc, 1), 'fontSize', fsize, ...
    'varname', 'v''_c', 'xlabel', 'v''', 'units', 'm/s', ...
    'plotType', 'stairs', 'lineWidth', lwidth);
legend(legEntry, 'location', 'best');

%% Plot turbulence intensity at different progress variable values
h(end+1)    = figure;
set(gcf, 'color', 'w');
set(gcf, 'Name', 'turbulence intensity at different progress variables');
plot(cbars, vcp/U, 'bo', cbars, ucp/U, 'go', 'LineWidth', lwidth);
set(gca, 'FontSize', fsize, 'FontName', fname);
xlabel('{\it<c>}');
ylabel('{\itu} / {\itU}_0');
legend('{\itu''_a_x}', '{\itu''_r_a_d}', 'location', 'best');

%% Save figures and data
currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
    int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
    fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', expdate, '\masked images'];
cd(currdir);

finDir  = 'Final Results';
if ~exist(finDir, 'dir');
    mkdir(finDir);
end
currdir = [currdir, '\', finDir];
cd(currdir);

for i = length(h):-1:1
    fn      = get(h(i), 'Name');
    I       = fn == '.';
    fn(I)   = '_';
    I       = fn == '<' | fn == '>';
    fn(I)   = [];
    fnFinal = [fn, fnEnd];
    saveas(h(i), fnFinal, 'fig');
    saveas(h(i), fnFinal, 'png');
    close(h(i));
end

save(['data, delta_f = ', num2str(delta_f, 2), fnEnd, '.mat'], '-v7.3');
cd(prevdir);