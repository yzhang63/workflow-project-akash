% Script that masks images based on binarized images

%% Input Parameters
% setName = 'Cam_Date=121030_Time=112150';     % name of folder where dataset is located
inputParameters;            % script to define common input parameters
% N       = 1000;             % number of images
method  = 'graythresh';     % binarizing method: 'graythresh' or 'kmeans'
B       = 750;              % number of images to run in each for loop to keep memory from filling up

%% Figure adjustment parameters
xl      = [-1 1]*R*5;       % x limits (nondimensional: x/d)
yl      = [-1 3];           % y limits (nondimensional: y/d)

%% Determine number of for loops and their indices
L       = ceil(N/B);        % determines number of for loops to run to keep memory from filling up
M       = 0:B:B*L;          % for loop indices
M(end)  = N;                % force final index to be N

%% Generate and apply masks to all images
tic
for j = 1:length(M)-1
    F   = M(j) + 1;                           % index of first image
    L   = M(j + 1);                           % index of last image
    Q   = L - F + 1;                          % number of images to be analyzed
    
    %% Load images and binarize
    name                = ['B', sprintf('%0*d', 5, F), '.im7'];     % image filename (e.g. 'B00001.im7')
    [~, X, Y, ~, Im]    = loadImage(S, P, T, U, fuel, phi, angle, expdate, name, ...
        c, xl, yl, 1, 'corrected', 0, 'filtSize', filtSize);
    [BW1s, J1]          = binarizeImages(X, Y, Im, [F, L], S, P, T, U, fuel, phi, angle, ...
        expdate, c, xl, yl, 1, method, 'corrected', 0, 'filtSize', filtSize);
    [~, X, Y, ~, Im]    = loadImage(S, P, T, U, fuel, phi, angle, expdate, name, ...
        c, xl, yl, 2, 'corrected', 0, 'filtSize', filtSize);
    [BW2s, J2]          = binarizeImages(X, Y, Im, [F, L], S, P, T, U, fuel, phi, angle, ...
        expdate, c, xl, yl, 2, method, 'corrected', 0, 'filtSize', filtSize);
    % When generating masks, always use raw images since processing in
    % DaVis uses raw images with corrections applied.
    clear Im;
    if strcmp(method, 'graythresh');
        BW1s            = imcomplement(BW1s);
        BW2s            = imcomplement(BW2s);
    end
    %% Load all images and apply image mask found from binarized images
    I1  = loadAllImages([F L], S, P, T, U, fuel, phi, angle, expdate, c, 1);
    IM1 = BW1s.*I1;
    clear I1;
    I2  = loadAllImages([F L], S, P, T, U, fuel, phi, angle, expdate, c, 2);
    IM2 = BW2s.*I2;
    clear I2;
    
    %% Save mask files
    prevdir = cd;
    currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', expdate];
    if ~isdir([currdir, '\masked images'])
        mkdir([currdir, '\masked images']);
    end
    cd([currdir, '\masked images']);
    parfor i = 1:Q
        name    = ['B', sprintf('%0*d', 5, i+F-1)];     % image filename (e.g. 'B00001.im7')
        outputImages2(IM1(:, :, i), IM2(:, :, i), name);
    end
    cd(prevdir);
    
    %% Clear large variables from memory
    clear BW1s BW2s IM1 IM2
end
toc