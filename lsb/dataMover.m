%% Common input parameters
proj        = 'Marshall_LSB_Oct_2012';          % name of Project in DaVis
dirProjs    = 'D:\My Projects';                 % Directory of projects
dirData     = '\\Vboxsvr\piv\LSB';                     % Directory of processed data

%% Sets to move data
setNames    = ...
    {'Cam_Date=121030_Time=112150';...  30 m/s, 50-50 H2-CO, phi = 0.55, angle = 0
    'Cam_Date=121030_Time=113727';...   30 m/s, 50-50 H2-CO, phi = 0.55, angle = 24
    'Cam_Date=121030_Time=170514';...   30 m/s, 70-30 H2-CO, phi = 0.51, angle = 0
    'Cam_Date=121030_Time=172305';...   30 m/s, 70-30 H2-CO, phi = 0.51, angle = 24
    'Cam_Date=121103_Time=112726';...   30 m/s, 100 H2, phi = 0.46, angle = 0
    'Cam_Date=121103_Time=114443';...   30 m/s, 100 H2, phi = 0.46, angle = 24
    'Cam_Date=121031_Time=195417';...   50 m/s, 50-50 H2-CO, phi = 0.55, angle = 0
    'Cam_Date=121031_Time=201041';...   50 m/s, 50-50 H2-CO, phi = 0.55, angle = 24
    'Cam_Date=121101_Time=190834';...   50 m/s, 70-30 H2-CO, phi = 0.51, angle = 0
    'Cam_Date=121101_Time=193502';...   50 m/s, 70-30 H2-CO, phi = 0.51, angle = 24
    'Cam_Date=121103_Time=121008';...   50 m/s, 100 H2, phi = 0.46, angle = 0
    'Cam_Date=121103_Time=125656'};   % 50 m/s, 100 H2, phi = 0.46, angle = 24

%% Move the data
for j = 1:length(setNames)
    setName     = setNames{j};                      % name of folder where dataset is located
    disp(' ');
    disp(['setName = ', setName]);
    inputCases;                                     % defines experimental parameters
    % unconditioned data
    rc          = 0;                                % 1 = reactant-conditioned, 0 = unconditioned
    dirNames    = {setName; 'Correction'; vc7Folder};
    for i = 1:length(dirNames)
        dirName     = dirNames{i};     %#ok<*NASGU> % name of folder containing data to move
        moveData;
    end
    % reactant-conditioned data
    rc          = 1;                                % 1 = reactant-conditioned, 0 = unconditioned
    dirNames    = {mvc7Folder; 'Exx'; 'Exy'; 'Eyx'; 'Eyy'; 'TimeMeanQF_Vector'};
    for i = 1:length(dirNames)
        dirName     = dirNames{i};     %#ok<*NASGU> % name of folder containing data to move
        moveData;
    end
end