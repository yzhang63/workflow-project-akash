function [] = inputParameters(setName) % define common, global input parameters
global ednum;
global S;
global P;
global T;
global U;
global fuel;      
global phi;       
global SL0;       
global expangle;  
global ednum;     
global exptime;   
global vc7Folder; 
global mvc7Folder;
global yl;        
global bs;        
global ol;        
global N;         
global corrected; 
global filtSize;  

global R;

global frame;
global rc;     
global expdate;
global d;      
global dim;    
global res;    
global psize;  
global fsize;  
global fname;  
global lwidth; 
global msize;  
global clim;   
global cmap;   

global delta_f0;
global SL0;
global SLmax;
global delta_SLmax;

global ftype;
global delta_f;
global c;

inputCases(setName);

frame   = 1;                    % laser+camera shot 1 or shot 2
rc      = 0;                    % for velocity vectors: 1 = reactant-conditioned, 0 = unconditioned
expdate = int2str(ednum);       % experiment date ('yyyymmdd')
d       = 36.3;                 % burner diameter (mm)
R       = 0.66;                 % ratio of the central channel to burner exit radii

%% Figure adjustment parameters - affect how figures are displayed, etc.
dim     = 0;                    % dimensions: 0 -> dimensionless, 1 -> dimensions
res     = 300;                  % resolution (dpi)
psize   = [5.833333, 4.313333]; % paper size (inches)
fsize   = 16;                   % font size
fname   = 'Times New Roman';    % font name
lwidth  = 2;                    % line width
msize   = 10;                   % marker size
clim    = [0.005 1];            % color axis limits (nondimensional: u/U)
cmap    = 'gray';               % colormap

% the following if statement is used to center the images on the geometric 
% center of the burner and to crop the burner out of the image. For later
% experiments, the images were centered better and the burner was masked
% out, so these steps became unnecessary. 
if ednum == 20120202
    c       = 1.384*d;          % center of the burner in the x-direction (mm) (20120202)
elseif ednum >= 20120210 && ednum < 20120402
    c       = -0.0479*d;        % center of the burner in the x-direction (mm) (20120210+)
    cb      = 670;              % pixel value at which to crop out burner (20120222)
elseif ednum >= 20120402 && ednum < 20121030
    c       = -0.3087;          % center of the burner in the x-direction (mm) (20120402+)
    cb      = 548;              % pixel value at which to crop out burner (20120402)
elseif ednum >= 20121030
    c       = 0;
    expdate = expdate(3:end);   % chop off first 2 digits from date
    expdate = ['Cam_Date=', expdate, '_Time=', int2str(exptime)];
end

%% Unstretched flame speed and flame thickness
% These numbers come from Chemkin calculations performed using the PREMIX
% module and the Davis mechanism for H2/CO blends and GRI-Mech 3.0 for CH4.

if phi ~= 0                     % if reacting
    switch fuel                 % the variable fuel2 is used to lookup the chemical properties
        case '100 CH4'
            fuel2 = 'CH4';
        case '50-50 H2-CO'
            fuel2 = '50H2';
        case '70-30 H2-CO'
            fuel2 = '70H2';
        case '100 H2'
            fuel2 = '100H2';
        case {'50-50 H2-CH4', '60-40 H2-CH4', '75-25 H2-CH4'}
            fuel2 = fuel;
        otherwise
            error('Invalid string for variable fuel');
    end
    prevdir = cd;               % store the current directory as a variable before changing directories

%    disp('Previous dir is:');
%    disp(prevdir);
    
    % define the directory where the Chemkin data is located. 
%     chemdir = ['E:\Chemkin\FinalResults\StretchedCalculations\', num2str(P), ...
%         'atm\', num2str(T), 'K\ConstantSL0\', num2str(SL0), 'cms\', fuel2, ...
%         '\ER0', num2str(100*phi), '\UnstretchedCalculations'];
%     
%     switch fuel2
%         case 'CH4'
%             [delta_f0, SL0] = FlameSpeedandThickness_GRI(chemdir);  % flame thickness (cm) and speed (cm/s)
%         case '50-50 H2-CH4'
%             delta_f0        = 0.00608*0.5 + 0.04671;                % flame thickness (cm)
%         case '60-40 H2-CH4'
%             delta_f0        = 0.00608*0.6 + 0.04671;                % flame thickness (cm)
%         case '75-25 H2-CH4'
%             delta_f0        = 0.00608*0.75 + 0.04671;               % flame thickness (cm)
%         otherwise
%             [delta_f0, SL0] = FlameSpeedandThickness_Davis(chemdir);% flame thickness (cm) and speed (cm/s)
%     end
%     
%     delta_f0        = delta_f0*10;                               % flame thickness (mm)
    delta_f0 = 0.605988764963387;
    SL0 = 34.568990000000000;
    cd(prevdir);                % return to the previous directory
    
    %% Maximum stretched flame speed (cm/s) and corresponding flame thickness (mm)
    % These values are also calculated in Chemkin using the OPPDIF module
    
    switch fuel
        case '100 CH4'
            SLmax       = 66.59507;
            delta_SLmax = 0.0336387858031121*10;
        case '50-50 H2-CO'
            SLmax       = 124.8257;
            delta_SLmax = 0.0214738897700675*10;
        case '70-30 H2-CO'
            SLmax       = 159.6531;
            delta_SLmax = 0.0183579587470294*10;
        case '100 H2'
            SLmax       = 210.2865;
            delta_SLmax = 0.0160045516863703*10;
        case '50-50 H2-CH4'
            SLmax       = 104.1975;
            delta_SLmax = 0.0226992964298288*10;
        case '60-40 H2-CH4'
            % Don't have these values yet, the below values are interpolated
            SLmax       = interp1([0 0.5 1], [66.59507 104.1975 210.2865], 0.6);
            delta_SLmax = ...
                interp1([0 0.5 1], 10*[0.0336387858031121 0.0226992964298288 0.0160045516863703], 0.6);
        case '75-25 H2-CH4'
            % Don't have these values yet, the below values are interpolated
            SLmax       = interp1([0 0.5 1], [66.59507 104.1975 210.2865], 0.75);
            delta_SLmax = ...
                interp1([0 0.5 1], 10*[0.0336387858031121 0.0226992964298288 0.0160045516863703], 0.75);
        otherwise
            error('Invalid string for variable fuel');
    end

    %disp('SLmax = ');
    %disp(SLmax);
    %disp('delta_SLmax = ');
    %disp(delta_SLmax);

end %if end

end %function end
