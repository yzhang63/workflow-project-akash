% Script to create video file from im7 images

%% Input parameters
indIni      = 1;                                     % index of first im7 file
indFin      = 10;                                     % index of last im7 file
shot        = 1;                                        % image frame (1 or 2)
% filtSize    = [5, 5];                                   % median filter size
% method      = 'graythresh';                             % binarization method

%% Figure and video properties
cmap    = 'gray';                                       % colormap
lwidth  = 2;                                            % line width
enlarge = 1.2;                                          % multiplying factor to determine figure size
bits    = 8;    % number of bits for image resolution (16 is highest, but not all machines support it)
d       = 36.3;                                         % burner diameter (mm)

%% Prepare the video file
vName   = ['B', sprintf('%0*d', 5, indIni), '-', ...
    'B', sprintf('%0*d', 5, indFin), '_', int2str(shot), ...
    '.avi'];                                            % filename (e.g. 'B00001-B00500_1.avi')
writObj = VideoWriter(vName);                           % initialize writer object
open(writObj);                                          % open writer object

%% Generate figure and set axes and figure properties
fn              = ['B', sprintf('%0*d', 5, 1)];     % im7 filename (e.g. 'B00001.im7')
[A, X, Y, I]    = loadImageBasic([fn, '.im7'], shot);
dispImage(A, X, Y, I, 16, 'gray');
posIni          = get(gcf, 'Position');
xFin            = posIni(1);
yFin            = posIni(2)+posIni(4)-round(enlarge*length(I(:, 1)));
wFin            = round(enlarge*length(I(1, :)));
gap             = wFin - length(I(1, :));
hFin            = length(I(:, 1)) + gap;
posFin          = [xFin, yFin, wFin, hFin];
set(gcf, 'Position', posFin);

for i = indIni:indFin
    fn              = ['B', sprintf('%0*d', 5, i)];     % im7 filename (e.g. 'B00001.im7')
    [A, X, Y, I]    = loadImageBasic([fn, '.im7'], shot);
%     Im              = medfilt2(I, filtSize);            % median filtered image
%     BW              = binarizeImage(Im, method);        % binarized image
%     E               = flameEdge(BW, X, Y);              % flame edge
    
    %% Scale data
    I   = (I - min(min(I)))./(max(max(I)) - min(min(I)));   % Scale intensity between [0, 1]
    I   = I.*(2^bits - 1);                                  % Scale to precision of bits
    switch bits
        case 8
            I   = uint8(I);
        case 16
            I   = uint16(I);
        otherwise
            error('unsupported precision value for variable bits');
    end
    
    %% Match histogram across images
    if i == indIni
        J   = I;
    else
        I   = imhistmatch(I, J);
    end
    
    %% Write video file
    dispImage(A, X, Y, I, 16, cmap);
%     hold on
%     plot(E(:, 1)/d, E(:, 2)/d, 'g', 'lineWidth', lwidth);
    frame   = getframe(gcf);
    writeVideo(writObj, frame);
end
close(writObj);