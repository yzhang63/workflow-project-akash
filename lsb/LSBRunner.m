% Script to analyze PIV data

%% Input Parameters
if ~exist('setNames', 'var')                            % if not batch processing multiple sets
    setName = '20120215-1-NR';            % name of folder where dataset is located
end
inputParameters;                                        % script to define common input parameters
method  = 'graythresh';                                 % binarizing method: 'graythresh' or 'kmeans'
rc      = 0;                % 1 = reactant-conditioned, 0 = unconditioned
yl      = [0.15 0.7];       % limits over which to perform linear fit (nondimensional: y/d)

if rc
    vc7Folder = mvc7Folder; %#ok<*UNRCH>
end

%% Edge Parameters
InputEdgeParameters;                                    % script to define edge parameters

%% Figure adjustment parameters
ymin    = 0.15;             % min downstream distance to plot (nondimensional: y/d)
ymax    = 1.5;              % max downstream distance to plot (nondimensional: y/d)
umax    = 1;                % max velocity to plot in 1-D plots (nondimensional: u/U)
a       = [-0.5 2];         % multiplicative factors to adjust umax for radial U plots
b       = 0.5;              % multiplicative factor to adjust umax for u' in the radial plots
e       = 0.5;              % multiplicative factor to adjust umax for u' in the axial plots

%% Extract mean and RMS quantities and plot on a quiver/contour plots
[meanV, rmsV]   = meanRMSextract(S, P, T, U, fuel, phi, angle, expdate, rc, 'vc7Folder', vc7Folder);
[X, Y, VX, VY]  = prepareIMX(meanV, c);
[~, ~, vx, vy]  = prepareIMX(rmsV, c);

clear h
h(1)    = figure;
u       = sqrt(2*vx.^2 + vy.^2);        % total turbulence intensity
contourProfile(rmsV, X, Y, u, U, d, dim, [ymin ymax], 0.6*clim, fsize);
hold on
quiverProfile(meanV, X, Y, VX, VY, U, d, dim, [ymin ymax], 1.4*clim, fsize);
if dim
    xlim([-1, 1]*d);
else
    xlim([-1, 1]);
end
hold off

h(end+1)= figure;
contourProfile(rmsV, X, Y, vx, U, d, dim, [ymin ymax], 0.3*clim, fsize);
if dim
    cb  = ylabel(colorbar, '{\itu''_r_a_d}');
else
    cb  = ylabel(colorbar, '{\itu''_r_a_d} / {\itU}_0');
end
if dim
    xlim([-1, 1]*d);
else
    xlim([-1, 1]);
end
set(gca, 'FontSize', fsize, 'FontName', fname);
set(cb, 'FontSize', fsize-2, 'FontName', fname);
set(gcf, 'Name', [rmsV.Source ' vx']);

h(end+1)= figure;
contourProfile(rmsV, X, Y, vy, U, d, dim, [ymin ymax], 0.5*clim, fsize);
if dim
    cb  = ylabel(colorbar, '{\itu''_a_x}');
else
    cb  = ylabel(colorbar, '{\itu''_a_x} / {\itU}_0');
end
set(gca, 'FontSize', fsize, 'FontName', fname);
set(cb, 'FontSize', fsize-2, 'FontName', fname);
set(gcf, 'Name', [rmsV.Source ' vy']);
if dim
    xlim([-1, 1]*d);
else
    xlim([-1, 1]);
end

h(end+1)= figure;
q       = 0.5*sqrt(vx.^2 + vy.^2);      % 2-D turbulent kinetic energy
contourProfile(rmsV, X, Y, q, U, d, dim, [ymin ymax], 0.3*clim, fsize);
if dim
    cb  = ylabel(colorbar, '{\itq''}');
else
    cb  = ylabel(colorbar, '{\itq''} / {\itU}_0');
end
set(gca, 'FontSize', fsize, 'FontName', fname);
set(cb, 'FontSize', fsize-2, 'FontName', fname);
set(gcf, 'Name', [rmsV.Source ' q']);
if dim
    xlim([-1, 1]*d);
else
    xlim([-1, 1]);
end

h(end+1)= figure;
contourProfile(rmsV, X, Y, u, U, d, dim, [ymin ymax], 0.6*clim, fsize);
if dim
    cb  = ylabel(colorbar, '{\itu''_r_m_s}');
else
    cb  = ylabel(colorbar, '{\itu''_r_m_s} / {\itU}_0');
end
set(gca, 'FontSize', fsize, 'FontName', fname);
set(cb, 'FontSize', fsize-2, 'FontName', fname);
set(gcf, 'Name', [rmsV.Source ' u']);
if dim
    xlim([-1, 1]*d);
else
    xlim([-1, 1]);
end

%% Extract strain rates and plot on contour plots
if rc
    [Xs, Ys, M_Exx, s_Exx, SE_Exx, A_Exx]   = avgStrainField(S, P, T, U, fuel, phi, angle, ...
        expdate, c, N, 'Exx', 'rc', rc, 'vc7Folder', vc7Folder);
    h(end+1)= figure;
    contourProfile(A_Exx, X, Y, M_Exx', 1000*U/d, d, dim, [ymin ymax], [-1 1], fsize);
    if dim
        cb  = ylabel(colorbar, '{\itE_{xx}}');
    else
        cb  = ylabel(colorbar, '{\itE_{xx}d} / {\itU}_0');
    end
    set(gca, 'FontSize', fsize, 'FontName', fname);
    set(cb, 'FontSize', fsize-2, 'FontName', fname);
    set(gcf, 'Name', [A_Exx.Source ' Exx']);
    
    [~, ~, M_Exy, s_Exy, SE_Exy, A_Exy]     = avgStrainField(S, P, T, U, fuel, phi, angle, ...
        expdate, c, N, 'Exy', 'rc', rc, 'vc7Folder', vc7Folder);
    h(end+1)= figure;
    contourProfile(A_Exy, X, Y, M_Exy', 1000*U/d, d, dim, [ymin ymax], [-1 1], fsize);
    if dim
        cb  = ylabel(colorbar, '{\itE_{xy}}');
    else
        cb  = ylabel(colorbar, '{\itE_{xy}d} / {\itU}_0');
    end
    set(gca, 'FontSize', fsize, 'FontName', fname);
    set(cb, 'FontSize', fsize-2, 'FontName', fname);
    set(gcf, 'Name', [A_Exy.Source ' Exy']);
    
    [~, ~, M_Eyx, s_Eyx, SE_Eyx, A_Eyx]     = avgStrainField(S, P, T, U, fuel, phi, angle, ...
        expdate, c, N, 'Eyx', 'rc', rc, 'vc7Folder', vc7Folder);
    h(end+1)= figure;
    contourProfile(A_Eyx, X, Y, M_Eyx', 1000*U/d, d, dim, [ymin ymax], [-3 3], fsize);
    if dim
        cb  = ylabel(colorbar, '{\itE_{yx}}');
    else
        cb  = ylabel(colorbar, '{\itE_{yx}d} / {\itU}_0');
    end
    set(gca, 'FontSize', fsize, 'FontName', fname);
    set(cb, 'FontSize', fsize-2, 'FontName', fname);
    set(gcf, 'Name', [A_Eyx.Source ' Eyx']);
    
    [~, ~, M_Eyy, s_Eyy, SE_Eyy, A_Eyy]     = avgStrainField(S, P, T, U, fuel, phi, angle, ...
        expdate, c, N, 'Eyy', 'rc', rc, 'vc7Folder', vc7Folder);
    h(end+1)= figure;
    contourProfile(A_Eyy, X, Y, M_Eyy', 1000*U/d, d, dim, [ymin ymax], [-1 1], fsize);
    if dim
        cb  = ylabel(colorbar, '{\itE_{yy}}');
    else
        cb  = ylabel(colorbar, '{\itE_{yy}d} / {\itU}_0');
    end
    set(gca, 'FontSize', fsize, 'FontName', fname);
    set(cb, 'FontSize', fsize-2, 'FontName', fname);
    set(gcf, 'Name', [A_Eyy.Source ' Eyy']);
end

%% Plot velocity as a function of axial distance at a given radial position
x       = 0;                % radial position (nondimensional: x/d)

[clx, clsx] = confidenceLevel(vx, N);
[cly, clsy] = confidenceLevel(vy, N);
clst        = sqrt(2*(vx.^2).*(clsx.^2) + (vy.^2).*(clsy.^2))./u;     % total rms standard error (m/s)

h(end+1)    = figure;
I           = axialPlot(meanV, X, Y, VY, cly, U, d, x, dim, [ymin ymax], umax, 'b-', fsize);
hold on

% Calculate flame parameters
if phi ~= 0
    if rc
        % load location of STLD from FlameEdgeRunnerCombineAllSets
        prevdir = cd;
        currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
            int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
            fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', ...
            expdate, '\Final Results'];
        cd(currdir);
        fnEnd   = [', shot ', int2str(frame), ', ', 'filt = ', int2str(filtSize(1)), ...
            ', ', method, ', images ', '1-', int2str(N)];
        load(['data, delta_f = ', num2str(delta_f, 2), fnEnd, '.mat'], 'y1C');
        cd(prevdir);
        [~, J]          = min(abs(Y(1, :) - y1C));
        STLD            = VY(I, J);             % local displacement flame speed (m/s)
        if dim == 0
            plot(Y(I, J)/d, VY(I, J)/U, 'rx', 'LineWidth', lwidth, 'MarkerSize', msize);
        else
            plot(Y(I, J), VY(I, J), 'rx', 'LineWidth', lwidth, 'MarkerSize', msize);
        end
    else
        [fo, gf]        = axialFit(X, Y, VY, x*d, yl*d);
        [Ufit, ay, y0]  = fitPlot(fo, Y, U, d, dim, 'b--');
        [STLD, J]       = calcSTLD(Y, VY, Ufit, U, I, d, dim, 'rx');
    end
    uy              = vy(I, J);     % axial turbulence intensity (m/s)
    ux              = vx(I, J);     % radial turbulence intensity (m/s)
    yf              = Y(I, J);      % flame axial location (mm)
end

axialPlot(meanV, X, Y, VX, clx, U, d, x, dim, [ymin ymax], umax, 'g-', fsize);
set(gcf, 'Name', [meanV.Source ' x = ' num2str(x) ', VY, VX']);
if phi == 0
    legend('{\itU_a_x}', '{\itU_r_a_d}', 'location', 'best');
else
    if rc
        legend('{\itU_a_x}', '{\itS_T_,_L_D}', '{\itU_r_a_d}', 'location', 'best');
    else
        legend('{\itU_a_x}', '{\itU_f_i_t}', '{\itS_T_,_L_D}', '{\itU_r_a_d}', 'location', 'best');
    end
end

h(end+1)= figure;
axialPlot(rmsV, X, Y, vy, clsy, U, d, x, dim, [ymin ymax], e*umax, 'b-', fsize);
hold on
axialPlot(rmsV, X, Y, vx, clsx, U, d, x, dim, [ymin ymax], e*umax, 'g-', fsize);
% axialPlot(rmsV, X, Y,  q, U, d, x, dim, [yl(1) ymax], e*umax, 'm-', fsize);
axialPlot(rmsV, X, Y,  u, clst, U, d, x, dim, [ymin ymax], e*umax, 'c-', fsize);

if phi ~= 0
    if dim == 0
        plot(Y(I, J)/d, vy(I, J)/U, 'rx', 'LineWidth', 2, 'MarkerSize', 10);
    else
        plot(Y(I, J), vy(I, J), 'rx', 'LineWidth', 2, 'MarkerSize', 10);
    end
end

set(gcf, 'Name', [rmsV.Source ' x = ' num2str(x) ', vy, vx, q']);
legend('{\itu''_a_x}', '{\itu''_r_a_d}', '{\itu''_r_m_s}', 'location', 'best');% removed q: '{\itq''}',

%% Plot strain as a function of axial distance at a given radial position
if rc
    h(end+1)= figure;
    axiPlot(X, Y, M_Exx', x, 'varname', 'Eij', 'lmstyle', 'r', 'dim', dim, ...
        'normParam', 1000*U/d, 'ylim', [ymin ymax], 'zlim', [-1, 1], 'yunit', 'mm', 'zunit', '1/s', ...
        'zlabel', '{\itE_{ij}d} / {\itU}_0', 'confidenceLevel', 1.96*SE_Exx');
    hold on
    axiPlot(X, Y, M_Exy', x, 'varname', 'Eij', 'lmstyle', 'g', 'dim', dim, ...
        'normParam', 1000*U/d, 'ylim', [ymin ymax], 'zlim', [-1, 1], 'yunit', 'mm', 'zunit', '1/s', ...
        'zlabel', '{\itE_{ij}d} / {\itU}_0', 'confidenceLevel', 1.96*SE_Exy');
    axiPlot(X, Y, M_Eyx', x, 'varname', 'Eij', 'lmstyle', 'b', 'dim', dim, ...
        'normParam', 1000*U/d, 'ylim', [ymin ymax], 'zlim', [-1, 1], 'yunit', 'mm', 'zunit', '1/s', ...
        'zlabel', '{\itE_{ij}d} / {\itU}_0', 'confidenceLevel', 1.96*SE_Eyx');
    axiPlot(X, Y, M_Eyy', x, 'varname', 'Eij', 'lmstyle', 'm', 'dim', dim, ...
        'normParam', 1000*U/d, 'ylim', [ymin ymax], 'zlim', [-1, 1], 'yunit', 'mm', 'zunit', '1/s', ...
        'zlabel', '{\itE_{ij}d} / {\itU}_0', 'confidenceLevel', 1.96*SE_Eyy');
    legend('{\itE_x_x}', '{\itE_x_y}', '{\itE_y_x}', '{\itE_y_y}', 'location', 'best');
end

%% Plot velocity as a function of radial distance at a given axial position
if phi == 0
    y       = 0.65184;          % axial position (nondimensional: y/d)
else
    y       = yf/d;             % axial position of leading edge of flame
end


h(end+1)= figure;
radialPlot(meanV, X, Y, VY, cly, U, d, y, dim, a*umax, 'b-', fsize);
hold on
radialPlot(meanV, X, Y, VX, clx, U, d, y, dim, a*umax, 'g-', fsize);
set(gcf, 'Name', [meanV.Source ' y = ' num2str(y) ', VY, VX']);
legend('{\itU_a_x}', '{\itU_r_a_d}', 'location', 'best');

h(end+1)= figure;
radialPlot(rmsV, X, Y, vy, clsy, U, d, y, dim, [0 b]*umax, 'b-', fsize);
hold on
radialPlot(rmsV, X, Y, vx, clsx, U, d, y, dim, [0 b]*umax, 'g-', fsize);
% radialPlot(rmsV, X, Y,  q, U, d, y, dim, [0 b]*umax, 'm-', fsize);
radialPlot(rmsV, X, Y,  u, clst, U, d, y, dim, [0 b]*umax, 'c-', fsize);
set(gcf, 'Name', [meanV.Source ' y = ' num2str(y) ', vy, vx, q']);
legend('{\itu''_a_x}', '{\itu''_r_a_d}', '{\itu''_r_m_s}', 'location', 'best');% removed q: '{\itq''}',

%% Plot strain as a function of radial distance at a given axial position
if rc
    h(end+1)= figure;
    radPlot(X, Y, M_Exx', y, 'varname', 'Eij', 'lmstyle', 'r', 'dim', dim, ...
        'normParam', 1000*U/d, 'xlim', [-0.5, 0.5], 'zlim', [-1, 1], 'xunit', 'mm', 'zunit', '1/s', ...
        'zlabel', '{\itE_{ij}d} / {\itU}_0', 'confidenceLevel', 1.96*SE_Exx');
    hold on
    radPlot(X, Y, M_Exy', y, 'varname', 'Eij', 'lmstyle', 'g', 'dim', dim, ...
        'normParam', 1000*U/d, 'xlim', [-0.5, 0.5], 'zlim', [-1, 1], 'xunit', 'mm', 'zunit', '1/s', ...
        'zlabel', '{\itE_{ij}d} / {\itU}_0', 'confidenceLevel', 1.96*SE_Exy');
    radPlot(X, Y, M_Eyx', y, 'varname', 'Eij', 'lmstyle', 'b', 'dim', dim, ...
        'normParam', 1000*U/d, 'xlim', [-0.5, 0.5], 'zlim', [-1, 1], 'xunit', 'mm', 'zunit', '1/s', ...
        'zlabel', '{\itE_{ij}d} / {\itU}_0', 'confidenceLevel', 1.96*SE_Eyx');
    radPlot(X, Y, M_Eyy', y, 'varname', 'Eij', 'lmstyle', 'm', 'dim', dim, ...
        'normParam', 1000*U/d, 'xlim', [-0.5, 0.5], 'zlim', [-1, 1], 'xunit', 'mm', 'zunit', '1/s', ...
        'zlabel', '{\itE_{ij}d} / {\itU}_0', 'confidenceLevel', 1.96*SE_Eyy');
    legend('{\itE_x_x}', '{\itE_x_y}', '{\itE_y_x}', '{\itE_y_y}', 'location', 'best');
end

%% Save figures and data
prevdir = cd;
if rc
    midFolders = [expdate, '\masked images'];
else
    midFolders = expdate;
end
if phi == 0
    currdir = ['\\Vboxsvr\piv\LSB\nonreacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        int2str(angle), ' deg\', midFolders, '\', vc7Folder, '\TimeMeanQF_Vector'];
else
    currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
        int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
        fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', ...
        midFolders, '\', vc7Folder, '\TimeMeanQF_Vector'];
end
cd(currdir);
for i = length(h):-1:1
    fn      = get(h(i), 'Name');
    I       = find(fn == '.');
    fn(I)   = '_';
    set(h(i), 'PaperUnits', 'inches', 'PaperSize', psize, 'PaperPosition', [0, 0, psize]);
    saveas(h(i), fn, 'fig');
    print(h(i), fullfile(currdir, [fn, '.png']), '-dpng', ['-r', num2str(res)]);
    close(h(i));
end
save('data.mat');
cd(prevdir);