% Script to plot ST,LD data for data taken in the Fall of 2012

%% Input parameters (universal)
clear;
setNames    = ...
    {'Cam_Date=121030_Time=';...     30 m/s, 50-50 H2-CO, phi = 0.55
    'Cam_Date=121030_Time=';...      30 m/s, 70-30 H2-CO, phi = 0.51
    'Cam_Date=121103_Time=';...      30 m/s, 100 H2, phi = 0.46
    'Cam_Date=121031_Time=';...      50 m/s, 50-50 H2-CO, phi = 0.55
    'Cam_Date=121101_Time=';...      50 m/s, 70-30 H2-CO, phi = 0.51
    'Cam_Date=121103_Time='};       %50 m/s, 100 H2, phi = 0.46
setTimes    = ...
    [112150, 113727;...              30 m/s, 50-50 H2-CO, phi = 0.55
    170514, 172305;...               30 m/s, 70-30 H2-CO, phi = 0.51
    112726, 114443;...               30 m/s, 100 H2, phi = 0.46
    195417, 201041;...               50 m/s, 50-50 H2-CO, phi = 0.55
    190834, 193502;...               50 m/s, 70-30 H2-CO, phi = 0.51
    121008, 125656];                %50 m/s, 100 H2, phi = 0.46
setSymbs    = ...
    {'r<';...                        30 m/s, 50-50 H2-CO, phi = 0.55
    'r>';...                         30 m/s, 70-30 H2-CO, phi = 0.51
    'rd';...                         30 m/s, 100 H2, phi = 0.46
    'b<';...                         50 m/s, 50-50 H2-CO, phi = 0.55
    'b>';...                         50 m/s, 70-30 H2-CO, phi = 0.51
    'bd'};                          %50 m/s, 100 H2, phi = 0.46
method      = 'graythresh';         % method: 'graythresh', 'kmeans', or 'linearfit'

%% Generate figure and plot flame speed data
hSL0        = figure;
hold on
hSLmax      = figure;
hold on

%% PlotSTLDRunnerSub
for m = 1:length(setNames)
    plotSTLDRunnerSub;
end

%% Generate legend and linear fit from Cheng
figure(hSL0);
legend(legentry, 'fontsize', 10, 'location', 'best');

x1 = xlim;
xlim([0 x1(2)]);
y1 = ylim;
ylim([0 y1(2)]);
x2 = xlim;
STSL    = @(uySL) 1 + 1.73*uySL;
plot(x2, STSL(x2), 'k--', 'LineWidth', lwidth);

figure(hSLmax);
legend(legentry, 'fontsize', 10, 'location', 'best');

x1 = xlim;
xlim([0 x1(2)]);
y1 = ylim;
ylim([0 y1(2)]);