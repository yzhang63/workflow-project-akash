%% Input Parameters
inputParameters;            % script to define common input parameters
yl      = [0.3 1.2];        % y limits (nondimensional: y/d)
bs      = 32;               % interrogation window binsize in pixels
ol      = 0.5;              % interrogation window overlap (0.5 = 50% overlap)
method  = 'graythresh';     % binarizing method: 'graythresh' or 'kmeans'
level   = 434;              % threshold level for binarizing images (currently does nothing)

%% Figure adjustment parameters
xl      = [-1 1]*R*0.5;     % x limits (nondimensional: x/d)
clear h

%% Load images and flame edge
prevdir = cd;
currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
    int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
    fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', expdate];
cd(currdir);
load(['data, shot ', int2str(frame), ', ', method, '.mat']);
cd(prevdir);

%% Find axial location of a given value of the average progress variable
cb      = 0.05;             % progress variable value
Cfun    = @(y) Co(1)*(erf(Co(2)*(y - Co(3)))) + Co(4) - cb;
y       = fzero(Cfun, 0.6*d);

%% Calculate velocity components at the leading points of the flame (reactant-conditioned)
[xLP, yLP, cLP, uLP, vLP] = LPvelocities(S, P, T, U, fuel, phi, angle, expdate, c, N, E, X, Y, BWmean);

%% Calculate statistics at the leading points of the flame (reactant-conditioned)
XLP     = mean(xLP);                        % mean x position of leading points
YLP     = mean(yLP);                        % mean y position of leading points
CLP     = mean(cLP);                        % mean <c> value of leading points
ULP     = mean(uLP);                        % mean x component velocity of leading points
VLP     = mean(vLP);                        % mean y component velocity of leading points
upLP    = std(uLP);                         % rms x component velocity of leading points
vpLP    = std(vLP);                         % rms y component velocity of leading points

%% Find velocities at the average position of the leading points
[uXY, vXY]  = velocitiesGivenLoc(S, P, T, U, fuel, phi, angle, expdate, c, N, XLP, YLP);

%% Generate histograms of leading points properties
h(end+1)    = figure;
histPlot(xLP, 'fontSize', fsize, 'varname', 'x_L_P', 'units', 'mm');

h(end+1)    = figure;
histPlot(yLP, 'fontSize', fsize, 'varname', 'y_L_P', 'units', 'mm');

h(end+1)    = figure;
histPlot(uLP, 'fontSize', fsize, 'varname', 'u_L_P', 'units', 'm/s');

h(end+1)    = figure;
histPlot(uXY, 'fontSize', fsize, 'varname', 'u_L_P_a_v_e', 'units', 'm/s');

h(end+1)    = figure;
histPlot(vLP, 'fontSize', fsize, 'varname', 'v_L_P', 'units', 'm/s');

h(end+1)    = figure;
histPlot(vXY, 'fontSize', fsize, 'varname', 'v_L_P_a_v_e', 'units', 'm/s');

h(end+1)    = figure;
histPlot(cLP, 'fontSize', fsize, 'varname', '<c>_L_P');

%% Display some binarized images to visually make sure nothing strange is happening
for i = 1:floor(length(BWs(1, 1, :))/50):length(BWs(1, 1, :))
    figure;
    dispImageNoA(X, Y, BWs(:,:,i), 16);
    hold on
    plot(E{i}(:, 1)/d, E{i}(:, 2)/d, 'g', 'LineWidth', 2);
    plot(xLP(i)/d, yLP(i)/d, 'gx', 'LineWidth', 2);
end

%% Find ST,LD, u'axial and other flame parameters

[meanV, rmsV]       = meanRMSextract(S, P, T, U, fuel, phi, angle, expdate, rc);
[Xv, Yv, VX, VY]    = prepareProfile(meanV, c);
[~, ~, vx, vy]      = prepareProfile(rmsV, c);
[clx, clsx]         = confidenceLevel(vx, N);
[cly, clsy]         = confidenceLevel(vy, N);

h(end+1)    = figure;
K           = axialPlot(meanV, Xv, Yv, VY, cly, U, d, x, 0, [0.2141 2], 1, 'b-', fsize);
hold on
[~, L]      = min(abs(Yv(1, :) - y));
plot(Yv(1, L)/d, VY(K, L)/U, 'rx', 'LineWidth', 2, 'MarkerSize', 10);
STLD        = VY(K, L);             % local displacement flame speed (m/s)
uy          = vy(K, L);             % axial turbulence intensity (m/s)
ux          = vx(K, L);             % radial turbulence intensity (m/s)
yf          = Yv(K, L);             % flame axial location (mm)
axialPlot(meanV, Xv, Yv, VX, clx, U, d, x, 0, [0.2141 2], 1, 'g-', fsize);
set(gcf, 'Name', [meanV.Source ' x = ' num2str(x) ', VY, VX']);
legend('{\itU_a_x}', '{\itS_T_,_L_D}', '{\itU_r_a_d}', 'location', 'best');

h(end+1)    = figure;
axialPlot(rmsV, Xv, Yv, vy, clsy, U, d, x, 0, [0.2141 2], 0.5, 'b-', fsize);
hold on
axialPlot(rmsV, Xv, Yv, vx, clsx, U, d, x, 0, [0.2141 2], 0.5, 'g-', fsize);
plot(Yv(1, L)/d, vy(K, L)/U, 'rx', 'LineWidth', 2, 'MarkerSize', 10);
set(gcf, 'Name', [rmsV.Source ' x = ' num2str(x) ', vy, vx']);
legend('{\itu''_a_x}', '{\itu''_r_a_d}', 'location', 'best');

%% Save figures and data
prevdir = cd;
if rc == 1
    expdate = [expdate, '\Reactant Conditioned'];
end
currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
    int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
    fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', expdate];
cd(currdir);

for i = length(h):-1:1
    fn      = get(h(i), 'Name');
    I       = fn == '.';
    fn(I)   = '_';
    I       = fn == '<' | fn == '>';
    fn(I)   = [];
    saveas(h(i), [fn, ', shot ', int2str(frame), ', ' method], 'fig');
    saveas(h(i), [fn, ', shot ', int2str(frame), ', ' method], 'png');
end

save(['data, shot ', int2str(frame), ', ' method, ', c = ', num2str(cb), '.mat']);
cd(prevdir);