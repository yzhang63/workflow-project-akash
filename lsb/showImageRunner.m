%% Input Parameters
S       = 0.6;             % swirl number
P       = 1;                % pressure (atm)
T       = 300;              % temperature (K)
U       = 30;               % velocity (m/s)
fuel    = '75-25 H2-CH4';        % fuel composition ('%F1-%F2 F1-F2') (Note: code will ignore this variable if phi = 0)
phi     = 0.63;              % equivalence ratio (use 0 for nonreacting)
angle   = 0;               % angle (deg)
expdate = '20120402';       % experiment date ('yyyymmdd')
name    = 'B00001.im7';     % filename
frame   = 1;                % shot number

figure;
A       = showImage(S, P, T, U, fuel, phi, angle, expdate, name, frame);