% joint PDF runner script

%% Input Parameters
if ~exist('setNames', 'var')                            % if not batch processing multiple sets
    setName = 'Cam_Date=121030_Time=112150';            % name of folder where dataset is located
end
inputParameters;                                        % script to define common input parameters
method      = 'graythresh';                             % binarizing method: 'graythresh' or 'kmeans'

%% Edge Parameters
InputEdgeParameters;                                    % script to define edge parameters

%% Load data
currdir     = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', int2str(P), ' atm\', ...
    int2str(T), ' K\', int2str(U), ' mps\', fuel, '\phi = ', num2str(phi), '\', ...
    int2str(angle), ' deg\', expdate, '\masked images\Final Results'];
fnEnd       = [', shot ', int2str(frame), ', filt = ', int2str(filtSize(1)), ...
    ', ', method, ', images ', '1-', int2str(N)];
if exist('setNames', 'var')
    temp        = setNames;
end
load([currdir, '\data, delta_f = ', num2str(delta_f, 2), fnEnd, '.mat'], ...
    'kappaC', 'kappa_s', 'kappaLPC', 'kappa_sLP', 'xC', 'yC', 'cC', 'delta_f', 'delta_f0', 'SL0');
if exist('setNames', 'var')
    setNames    = temp;
end
kappa_cLP = kappaLPC;
clear kappaLPC

%% Additional Input Parameters
rc              = 1;                                    % reactant-conditioned

%% Remove outliers from curvature and tangential strain rates using Thomson-Tau method
kappa_cF        = outliers(cell2mat(kappaC));           % remove outliers
kappa_sF        = outliers(cell2mat(kappa_s));
Inan            = isnan(kappa_cF) | isnan(kappa_sF);    % indices of NaNs
kappa_cF(Inan)  = [];
kappa_sF(Inan)  = [];                                   % remove NaNs
xF              = cell2mat(xC);
xF(Inan)        = [];                                   % x-coordinates for kappa_s that were NaNs
yF              = cell2mat(yC);
yF(Inan)        = [];                                   % y-coordinates for kappa_s that were NaNs
cF              = cell2mat(cC);
cF(Inan)        = [];                                   % c-coordinates for kappa_s that were NaNs
clear Inan kappaC kappa_s xC yC cC

%% joint PDFs of curvature and tangential strain rate
clear h
h(1)        = figure;
hist3Plot(kappa_cF, kappa_sF, 'fontSize', fsize, 'var1name', 'kappa_c', 'var2name', 'kappa_s', ...
    'units1', '1/mm', 'units2', '1/s', 'x1label', '{\kappa_c}', 'x2label', '{\kappa_s}', ...
    'plotType', 'contour', 'colormap', 'jet', 'smooth', 'medfilt');
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);
xlim(2.5*[-1, 1]);
ylim(4000*[-1, 1]);
hline(0, 'k--');
vline(0, 'k--');

h(end+1)    = figure;
hist3Plot(kappa_cF*delta_f0, kappa_sF*(delta_f0/10)/SL0, 'fontSize', fsize, ...
    'var1name', 'kappa_cdeltaf0', 'var2name', 'kappa_sdeltaf0bySL0', ...
    'x1label', '{\kappa_c\delta_f_,_0}', 'x2label', '{\kappa_s\delta_f_,_0 /S_{L,0}}', ...
    'plotType', 'contour', 'colormap', 'jet', 'smooth', 'medfilt');
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);
xlim(2.5*[-1, 1]*delta_f0);
ylim(4000*[-1, 1]*(delta_f0/10)/SL0);
hline(0, 'k--');
vline(0, 'k--');

%% joint PDFs of curvature and tangential strain rate conditioned on progress variable value
cbin        = 0.2;                              % Width of progress variable bins
cvals       = 0:cbin:1;                         % Progress variable bins
kappa_cc    = cell(length(cvals)-1, 1);
kappa_sc    = kappa_cc;

for i = 1:length(cvals)-1
    c1          = cvals(i);
    c2          = cvals(i+1);
    Ic          = cF >= c1 & cF < c2;       % Indices of c1 <= cF < c2
    kappa_cc{i} = kappa_cF(Ic);             % curvatures satisfying c1 <= cF < c2
    kappa_sc{i} = kappa_sF(Ic);             % tangential strain rates satisfying c1 <= cF < c2
    h(end+1)    = figure; %#ok<SAGROW>
    hist3Plot(kappa_cc{i}, kappa_sc{i}, 'fontSize', fsize, ...
        'var1name', 'kappa_c', 'var2name', 'kappa_s', 'units1', '1/mm', 'units2', '1/s', ...
        'x1label', '{\kappa_c}', 'x2label', '{\kappa_s}', ...
        'plotType', 'contour', 'colormap', 'jet', 'smooth', 'medfilt');
    set(gcf, 'Name', [get(gcf, 'Name'), ', ', ...
        num2str(cvals(i)), ' <= <c> < ', num2str(cvals(i+1)), ', ', ...
        ftype, ', delta_f = ', num2str(delta_f, 2)]);
    xlim(2.5*[-1, 1]);
    ylim(4000*[-1, 1]);
    %     if i == 1
    %         yl      = ylim;
    %     else
    %         ylim(yl);
    %     end
    hline(0, 'k--');
    vline(0, 'k--');
end
clear c1 c2 Ic xl yl

%% joint PDFs of curvature and tangential strain rate conditioned on instantaneous leading points
h(end+1)    = figure;
hist3Plot(kappa_cLP, kappa_sLP, 'fontSize', fsize, ...
    'var1name', 'kappa_cLP', 'var2name', 'kappa_sLP', ...
    'units1', '1/mm', 'units2', '1/s', 'x1label', '{\kappa_{c,LP}}', 'x2label', '{\kappa_{s,LP}}', ...
    'plotType', 'contour', 'colormap', 'jet', 'smooth', 'medfilt');
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);
xlim([0, 5]);
ylim(1000*[-1.5, 5]);
hline(0, 'k--');
vline(0, 'k--');

h(end+1)    = figure;
hist3Plot(kappa_cLP*delta_f0, kappa_sLP*(delta_f0/10)/SL0, 'fontSize', fsize, ...
    'var1name', 'kappa_cLPdeltaf0', 'var2name', 'kappa_sLPdeltaf0bySL0', ...
    'x1label', '{\kappa_{c,LP}\delta_f_,_0}', 'x2label', '{\kappa_{s,LP}\delta_f_,_0 /S_{L,0}}', ...
    'plotType', 'contour', 'colormap', 'jet', 'smooth', 'medfilt');
set(gcf, 'Name', [get(gcf, 'Name'), ', ', ftype, ', delta_f = ', num2str(delta_f, 2)]);
xlim([0, 5]*delta_f0);
ylim(1000*[-1.5, 5]*(delta_f0/10)/SL0);
hline(0, 'k--');
vline(0, 'k--');
clear xl

%% Save figures and data
prevdir = cd;
currdir = ['\\Vboxsvr\piv\LSB\reacting\S = ', num2str(S), '\', ...
    int2str(P), ' atm\', int2str(T), ' K\', int2str(U), ' mps\', ...
    fuel, '\phi = ', num2str(phi), '\', int2str(angle), ' deg\', expdate];
finDir  = 'Joint Statistics';

if ~exist([currdir, '\', finDir], 'dir');
    mkdir(currdir, finDir);
end

currdir = [currdir, '\', finDir];
cd(currdir);
for i = length(h):-1:1
    fn      = get(h(i), 'Name');
    I       = fn == '.';
    fn(I)   = '_';
    I       = fn == '<' | fn == '>';
    fn(I)   = [];
    fnFinal = [fn, fnEnd];
    saveas(h(i), fnFinal, 'fig');
    saveas(h(i), fnFinal, 'png');
    close(h(i));
end
clear fn I fnFinal h i
save(['data, delta_f = ', num2str(delta_f, 2), fnEnd, '.mat'], '-v7.3');
cd(prevdir);