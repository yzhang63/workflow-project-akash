% Small script to display the error in mean, std, and skewness

% o = input('order    = ');
% p = input('pRepeats = ');
% n = input('numPts   = ');
% 
% l = find(orders == o);
% m = find(pRepeats == p);
% n = find(ns == n);
% 
% emeans(l, m, n)
% estds(l, m, n)
% eskews(l, m, n)

%% Find error over specific parameter space
parm = [4 2 7;...
        4 3 7;...
        4 4 7;...
        5 2 7;...
        5 3 7;...
        6 2 13;...
        6 3 25;...
        7 2 13;...
        8 2 13;...
        8 2 25;...
        8 2 49;...
        8 3 49];

E = zeros(length(parm(:, 1)), 3);
EF = zeros(length(parm(:, 1)), 3);
for i = 1:length(parm(:, 1))
    l       = find(orders     == parm(i, 1));
    m       = find(pRepeats   == parm(i, 2));
    n       = find(ns         == parm(i, 3));
    E(i, :) = [emeans(l, m, n), estds(l, m, n), eskews(l, m, n)];
    EF(i,:) = [emeanF(l, m, n), estdF(l, m, n), eskewF(l, m, n)];
end

%% Find how much the error is reduced by filtering
dE = abs(E) - abs(EF);  % positive values represent reduction in error